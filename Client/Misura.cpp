//---------------------------------------------------------------------------

#pragma hdrstop

#include "Misura.h"
#include <IniFiles.hpp>
#include <stdio.h>
#include <System.StrUtils.hpp>
#include <System.SysUtils.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdmMisura *dmMisura;
//---------------------------------------------------------------------------
__fastcall TdmMisura::TdmMisura(TComponent* Owner)
	: TDataModule(Owner)
{
	TIniFile *Ini;

	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	SelProgramsPath = Ini->ReadString("MU" + IntToStr(Tag), "SelProgramsPath", "\\\\MEDICAO02\\MecaShare\\");
	CommandFile     = Ini->ReadString("MU" + IntToStr(Tag), "CommandFile"	 , "\\\\MEDICAO02\\MecaShare\\2do.txt");
	ProgramsPath    = Ini->ReadString("MU" + IntToStr(Tag), "ProgramsPath"	 , "c:\\mecano\\MecaShare\\");
	ResultsFile     = Ini->ReadString("MU" + IntToStr(Tag), "ResultsFile"	 , "ResPresetting.txt");
	delete Ini;
}
//---------------------------------------------------------------------------

int TdmMisura::SetProgram(AnsiString ProgramName) {
	FILE *f;
	int res = 0;
	
	try {
		f = fopen(CommandFile.c_str(), "w+");
		if (f) {
			fputs(ProgramName.c_str(),f);
			fclose(f);
		}
	} catch (...) {
		res = -1;
	}
	return res;
}
//---------------------------------------------------------------------------

AnsiString TdmMisura::Parser(AnsiString NomeCampo, AnsiString Telegramma, AnsiString Separatore)
{
	int fine, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	fine = tg.Pos(NomeCampo);
	if (fine > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, fine);
		fine = tg.Pos(Separatore);
		if (fine > cl) {
			res = tg.SubString(cl, fine - cl);
		}
	}
	return res;
}
//---------------------------------------------------------------------------

double TdmMisura::ConvertDecimalSeparator(AnsiString ValStr)
{
	double res = 0;
	AnsiString s;

	try {
		s = ReplaceStr(ValStr, ".", FormatSettings.DecimalSeparator);
		ValStr = ReplaceStr(s, ",", FormatSettings.DecimalSeparator);
		res = ValStr.ToDouble();
	} catch (...) {
	}
	return res;
}
//---------------------------------------------------------------------------

int TdmMisura::ReadResults(double &X, double &Y, double &Z, double &A, double &B, double &C) {
	FILE *f;
	char s[256], newval[256];
	int res = 0;

	try {
		f = fopen(ResultsFile.c_str(), "r");
		if (f) {
			if (fgets(s, 256, f)) {
				X = ConvertDecimalSeparator(Parser("RX=", s, "|"));
				Y = ConvertDecimalSeparator(Parser("RY=", s, "|"));
				Z = ConvertDecimalSeparator(Parser("RZ=", s, "|"));
				A = ConvertDecimalSeparator(Parser("RA=", s, "|"));
				B = ConvertDecimalSeparator(Parser("RB=", s, "|"));
				C = ConvertDecimalSeparator(Parser("RC=", s, "|"));
			}
			fclose(f);
			DeleteFile(ResultsFile);
		}
	} catch (...) {
		res = -1;
	}
	return res;
}
//---------------------------------------------------------------------------
