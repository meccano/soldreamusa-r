//---------------------------------------------------------------------------

#ifndef ScaffaleH
#define ScaffaleH
//---------------------------------------------------------------------------
#include "GLCrossPlatform.hpp"
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.AppEvnts.hpp>
#include "GLBaseClasses.hpp"
#include "GLCoordinates.hpp"
#include "GLGeomObjects.hpp"
#include "GLObjects.hpp"
#include "GLScene.hpp"
#include "GLVectorFileObjects.hpp"
#include "GLWin32Viewer.hpp"
#include "GLSpaceText.hpp"
#include <vector>
#include "DB.h"

//---------------------------------------------------------------------------
#define PALLET_RETTANGOLARE	0
#define PALLET_TONDO		1
//---------------------------------------------------------------------------
#define POS_LIBERA				0
#define POS_PRENOTATA_PRELIEVO	1
#define POS_PRENOTATA_DEPOSITO	2
#define POS_BLOCCATA			3
//---------------------------------------------------------------------------
typedef std::vector<std::vector<std::string> > IndexList;
//---------------------------------------------------------------------------
class TFrameScaffale : public TFrame
{
__published:	// IDE-managed Components
	TGLSceneViewer *GLSceneViewer1;
	TGLScene *GLScene1;
	TGLLightSource *Luce;
	TGLCamera *Camera;
	TGLDummyCube *Target;
	TGLDummyCube *Magazzino;
	TGLFreeForm *Scaffale;
	TPanel *pToolBar;
	TSpeedButton *SpeedButtonCurs;
	TSpeedButton *SpeedButtonNav;
	TSpeedButton *SpeedButtonPan;
	TSpeedButton *SpeedButtonSave;
	TSpeedButton *SpeedButtonLoad;
	TGLCube *Selezione;
	TGLCylinder *SelezioneC;
	void __fastcall SpeedButtonSaveClick(TObject *Sender);
	void __fastcall GLSceneViewer1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall GLSceneViewer1MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall SpeedButtonLoadClick(TObject *Sender);
	void __fastcall GLSceneViewer1BeforeRender(TObject *Sender);
	void __fastcall GLSceneViewer1DblClick(TObject *Sender);
protected:
private:	// User declarations
	int mx, my;
public:		// User declarations
	__fastcall TFrameScaffale(TComponent* Owner, int ID);
	void SelectPos(int n);
	void Init(int ID);
	void UpdatedIniPallets();
	void SaveConfigIni();
	void LoadConfigIni();
	void Zoom(int MouseDelta);
	void EvidenziaPallet(TGLCustomSceneObject *pick, TVector4f c);
	void EvidenziaPalletSelezionato(TGLCustomSceneObject *pick);
	void ModificaTestoPallet(TGLCustomSceneObject *pick, AnsiString s);
	void ModificaTesto2Pallet(TGLCustomSceneObject *pick, AnsiString s);
	void ColoraTestoPallet(TGLCustomSceneObject *pick, TVector4f c);
	void ColoraPallet(TGLCustomSceneObject *pick, TVector4f c, TVector4f c2);
	void AggiornaPresenza(int n, int idpallet, int tipopallet, int idpezzo, int stato, int stato_pezzo, int stato_pallet, int misurato, int macchina = 0, int lotto = 0, AnsiString Alias = "", AnsiString Hint = "");
	void SetLabel(int n, AnsiString s);
	void SetLabel2(int n, AnsiString s);
    void LoadDrawPalletParameter(int ID_DisegnoPallet);
	TGLCustomSceneObject *oldpick, *selected;
	int tipo_pallet;
	float origine;
	float X_base;
	int n_scaffali;
	int n_piani;
	float spazio_primo_scaffali;
	float spazio_scaffali;
	float spazio_ultimo_scaffali;
	float passo_pallet;
	float base_pallet, prof_pallet;
	float h_pallet;
	float h_pezzo;
	float turn_angle;
	float text_height;
	float angolo_primo_pallet;
	float angolo_tra_pallet;
	float raggio_mag;
	String Path;
	String IniFileName;
	String TexturePaths;
	UnicodeString ObjScaffale;
	int framewidth;
	int demomode;
	IndexList m_PalletList;
    IndexList m_HPianoList;
	TRecordList m_PostazioniTable;
	TRecordList m_DisegnoPalletTable;
	std::map<int, TGLSceneObject*> palletmap;
	std::map<int, TGLSceneObject*> pezzimap;
	std::map<int, TGLSpaceText*> txtmap;
	std::map<int, TGLSpaceText*> txt2map;
	std::map<int, int> tipopalletmap;
	//
	// The message-map table.
	//
//	BEGIN_MESSAGE_MAP
//	  MESSAGE_HANDLER(WM_ERASEBKGND, TWMEraseBkgnd, WmEraseBkgnd)
//	END_MESSAGE_MAP(TFrame)
};
//---------------------------------------------------------------------------
extern PACKAGE TFrameScaffale *FrameScaffale;
//---------------------------------------------------------------------------
#endif
