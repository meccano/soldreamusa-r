//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModificaTempi.h"
#include "StringTableID.h"
#include "InsAlpha.h"
#include "InsNum.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormModificaTempi *FormModificaTempi;
//---------------------------------------------------------------------------
__fastcall TFormModificaTempi::TFormModificaTempi(TComponent* Owner)
	: TForm(Owner)
{
	ID_Pallet = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaTempi::BitBtnOKClick(TObject *Sender)
{
	//Verifica Presenza Dati
	if ((leProgramma->Text != "") &&
		(leUtensile->Text != "") &&
		(leTempo->Text != ""))		 {
		//Inserisco su DB i nuovi valori
		DBDataModule->InserisciTempoUtensile(leProgramma->Text.ToInt(),
											 leUtensile->Text.ToInt(),
											 leTempo->Text.ToInt());
	}
}
//---------------------------------------------------------------------------
void __fastcall TFormModificaTempi::leProgrammaClick(TObject *Sender)
{
	TLabeledEdit *le;

	le = (TLabeledEdit*)Sender;
	InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
	InsNumForm->Valore->Text = le->Text;
	if (InsNumForm->ShowModal() == IDOK) {
		le->Text = InsNumForm->Valore->Text;
	}
}
//---------------------------------------------------------------------------

