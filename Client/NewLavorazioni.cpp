//---------------------------------------------------------------------------

#pragma hdrstop
#include <limits.h>

#include <fstream>
#include "NewLavorazioni.h"
#include "SelectPallet.h"
#include "Main.h"
#include "StringTableID.h"
#include <sys/stat.h>
#include <string>
#include "DB.h"
#include "util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MyDBGrid"
#pragma resource "*.dfm"

TFormNewLavorazioni *FormNewLavorazioni;
//---------------------------------------------------------------------------
__fastcall TFormNewLavorazioni::TFormNewLavorazioni(TComponent* Owner)
	: TForm(Owner)
{
	StringGrid1->Cells[0][0] = "ID Pallet";
	StringGrid1->Cells[1][0] = "Tipo Pallet";
	StringGrid1->Cells[2][0] = "Descr. Pallet";
	StringGrid1->Cells[3][0] = "Cod. Articolo";

    m_IDPezzo = 0;
	m_IDPallet = 0;
	PalletSelected = false;
	FaseSelected = false;
    CheckBox1->Checked = true;
}
//---------------------------------------------------------------------------

void TFormNewLavorazioni::CaricaComboBoxLavorazioni(int ID_Pallet)
{
	//Se non � stato selezionato un pallet non visualizzo alcuna lavorazione
	if(ID_Pallet == 0)
		return;

	UINT i, j;
	AnsiString tab;
	TPallet p;
	TPezzo pz;

	//Leggo la tabella pallet per ricavarmi il pezzo
	DBDataModule->LeggiPallet(ID_Pallet, p);

	//Leggo la tabella pezzi per ricavarmi l'articolo
	DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);

	if (pz.ID_Pezzo != 0) {
		tab.sprintf("Lavorazioni WHERE (ID_Articolo = %d) ", pz.ID_Articolo);

		DBDataModule->CaricaTabella(tab, ViewFasiLavorazioni);
		CmbLavorazioni->Clear();
		for (i = 0; i < ViewFasiLavorazioni.size(); i++) {
			CmbLavorazioni->Items->Add(ViewFasiLavorazioni[i]["Codice"] + " - " + ViewFasiLavorazioni[i]["Descrizione"]);
		}
	}
	else {
		CmbLavorazioni->Clear();
		tab.sprintf("select DISTINCT ID_Lavorazione from FasiLavorazione WHERE (ID_TipoPallet = %d)", p.ID_TipoPallet);
		DBDataModule->CaricaTabellaDaQuery(tab, ViewFasiLavorazioni);
		for (i = 0; i < ViewFasiLavorazioni.size(); i++) {
			tab.sprintf("Lavorazioni WHERE (ID_Lavorazione = %d) ", ViewFasiLavorazioni[i]["ID_Lavorazione"].ToIntDef(0));

			DBDataModule->CaricaTabella(tab, TabLavorazioni);
			for (j = 0; j < TabLavorazioni.size(); j++) {
				CmbLavorazioni->Items->Add(TabLavorazioni[j]["Codice"] + " - " + TabLavorazioni[j]["Descrizione"]);
			}
		}
	}
}
//---------------------------------------------------------------------------

void TFormNewLavorazioni::WriteStringGrid(int ID_Pallet)
{
	AnsiString sql;
	TPallet p;
	TPezzo pz;
	TArticolo a;
	int w0, w1;
 	TRecordList TabTipiPallet;

	//Leggo la tabella pallet per ricavarmi il pezzo
	DBDataModule->LeggiPallet(ID_Pallet, p);

	sql.sprintf("TipiPallet WHERE ID_TipoPallet = %d", p.ID_TipoPallet);
	DBDataModule->CaricaTabella(sql, TabTipiPallet);

	StringGrid1->Cells[0][1] = IntToStr(p.ID_Pallet);
	StringGrid1->Cells[1][1] = TabTipiPallet[0]["Codice_Tipopallet"].c_str();
	StringGrid1->Cells[2][1] = TabTipiPallet[0]["Descrizione_TipoPallet"].c_str();

    //Controllo che sul pallet sia presente un pezzo
    if (p.ID_Pezzo != 0) {
    	//Memorizzo ID Pezzo
        m_IDPezzo = p.ID_Pezzo;
        
        //Leggo la tabella pezzi per ricavarmi l'articolo
        DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);

        //Imposto il numero ordine nel campo edit
        edtNumOrdine->Text = pz.Descrizione;

        //Leggo la tabella pezzi per ricavarmi l'articolo
        DBDataModule->LeggiArticolo(pz.ID_Articolo, a);

        StringGrid1->Cells[3][1] = a.Codice_Articolo;
    }
    else {
        m_IDPezzo = 0;
        edtNumOrdine->Text = "";
        StringGrid1->Cells[3][1] = "";
    }

	StringGrid1->Canvas->Font->Name = "Swis721 Cn BT";
	StringGrid1->Canvas->Font->Size = 12;

	for (int ACol = 0; ACol < StringGrid1->ColCount; ACol++) {
		w0 = StringGrid1->Canvas->TextWidth(StringGrid1->Cells[ACol][0]);
		w1 = StringGrid1->Canvas->TextWidth(StringGrid1->Cells[ACol][1]);
		StringGrid1->ColWidths[ACol] = TMax(w0, w1) + 10;
	}
}
//---------------------------------------------------------------------------

void TFormNewLavorazioni::AggiornaGridFasi()
{
	DBGridFasi->RefreshDataSet();
//
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::OnActiveForm(TObject *Sender)
{
	CheckBox1->Checked = true;
	m_bPartProgramOk = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::OnChangeCmbLavor(TObject *Sender)
{
	AnsiString strsql;

	if (CmbLavorazioni->ItemIndex >= 0) {
		ADOQuery2->Close();
		strsql.printf("Select * from FasiLavorazione where ID_Lavorazione = %d ORDER BY Ordine_Seq", atoi(ViewFasiLavorazioni[CmbLavorazioni->ItemIndex]["ID_Lavorazione"].c_str()));
		ADOQuery2->SQL->Text = strsql;
		ADOQuery2->Open();
		FaseSelected = true;
		m_bPartProgramOk = true;
		DBGridFasi->AutoFitAll();
		DBGridFasi->Invalidate();
	}
}
//---------------------------------------------------------------------------

void TFormNewLavorazioni::SelectPallet(int ID_Pallet)
{
	TPallet p;
	TPezzo pz;    

	if (ID_Pallet) {
        m_IDPallet = ID_Pallet;
		WriteStringGrid(ID_Pallet);
		CaricaComboBoxLavorazioni(ID_Pallet);
		PalletSelected = true;
		ADOQuery2->Close();
		if (CmbLavorazioni->Items->Count > 0) {
			CmbLavorazioni->ItemIndex = 0;
			OnChangeCmbLavor(this);
		}
	}
	else {
		StringGrid1->Cells[0][1] = "";
		StringGrid1->Cells[1][1] = "";
		StringGrid1->Cells[2][1] = "";
		StringGrid1->Cells[3][1] = "";
        edtNumOrdine->Text = "";
		PalletSelected = false;
		CmbLavorazioni->Clear();
		m_IDPallet = 0;
        m_IDPezzo = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::OnFormClose(TObject *Sender, TCloseAction &Action)
{
	ADOQuery2->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::OnSelectedCell(TObject *Sender, int ACol, int ARow, bool &CanSelect)
{
	//Se clicco sulla prima cella che � il numero del pallet
	if (ACol == 0 && ARow == 1 && FormSelectPallet->ShowModal() == IDOK) {
		SelectPallet(FormSelectPallet->Selected);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect, TGridDrawState State)
{
	TStringGrid *sg;

	sg = (TStringGrid*)Sender;
	sg->Canvas->Brush->Color = (ARow == 0) ? clActiveCaption : clWhite;

	SetTextAlign(sg->Canvas->Handle, TA_CENTER | VTA_CENTER);
	sg->Canvas->FillRect(Rect);
	sg->Canvas->TextRect(Rect, (Rect.Left + Rect.Right) / 2, Rect.Top + 4, sg->Cells[ACol][ARow]);
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::OnClickEnableColumn(TColumn *Column)
{
	TPoint Pt;
	TGridCoord Coord;

	Pt = DBGridFasi->ScreenToClient(Mouse->CursorPos);
	Coord = DBGridFasi->MouseCoord(Pt.X, Pt.Y);

	if (Coord.X == 1) {
		ADOQuery2->Edit();
		qry2Abilitazione->Value = !qry2Abilitazione->Value;
		ADOQuery2->Post();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::BitBtnOKClick(TObject *Sender)
{
	TPezzo pz;
	TPallet p;
    TLavorazione l;

	//Se tutti i percorsi sono ok, se ho selezionato un pallet ed una fase
	if (m_bPartProgramOk && FaseSelected && PalletSelected) {
    	//Leggo il pallet selezionato
    	DBDataModule->LeggiPallet(m_IDPallet, p);
        //Controllo se il pallet ha associato un pezzo
        if (p.ID_Pezzo == 0) {
            //Ottengo l'articolo associato alla lavorazione
            DBDataModule->LeggiLavorazione(ViewFasiLavorazioni[CmbLavorazioni->ItemIndex]["ID_Lavorazione"].ToIntDef(0), l);

            //Creo il pezzo associato all'articolo presente nella lavorazione
            memset(&pz, 0, sizeof(pz));
            pz.ID_Articolo = l.ID_Articolo;

            //Aggiorno il pallet 
            p.ID_Pezzo = DBDataModule->CreaPezzo(pz);
            p.Stato_Pallet = 1;
            DBDataModule->AggiornaPallet(p);
        }
    	//Creo la lavorazione in corso
		DBDataModule->CreaLavoriInCorso(m_IDPallet,
										ViewFasiLavorazioni[CmbLavorazioni->ItemIndex]["ID_Lavorazione"].ToIntDef(0),
										1,
										LE_Pri->Text.ToIntDef(50));
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::DBGridFasiDrawColumnCell(TObject *Sender, const TRect &Rect,
          int DataCol, TColumn *Column, TGridDrawState State)
{
	if (qry2Prg_MU->AsString == "") {
		DBGridFasi->Canvas->Brush->Color = clBtnFace;
	}
	else if (!FileExists(qry2Prg_MU->AsString.c_str())) {
		DBGridFasi->Canvas->Brush->Color = clRed;
		m_bPartProgramOk = false;
	}
	else {
		DBGridFasi->Canvas->Brush->Color = clLime;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	if (ModalResult == mrOk && !m_bPartProgramOk) {
		Application->MessageBox(String::LoadStr(IDS_ERROR_PARTPROGRAM).w_str(), String::LoadStr(IDS_ALLARME).w_str(), MB_OK);
	}

	//Se l'operatore ha premuto il tasto OK controllo se i dati sono corretti
	CanClose = (ModalResult == mrOk) ? m_bPartProgramOk : true;
}
//---------------------------------------------------------------------------

void __fastcall TFormNewLavorazioni::TrackBar1Change(TObject *Sender)
{
	LE_Pri->Text = TrackBar1->Position;
}
//---------------------------------------------------------------------------

