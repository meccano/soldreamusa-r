//---------------------------------------------------------------------------
#pragma hdrstop

#include "Articoli.h"
#include "ModificaArticolo.h"
#include "StringTableID.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TArticoliForm *ArticoliForm;
//---------------------------------------------------------------------------
__fastcall TArticoliForm::TArticoliForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	PageControl1->ActivePage = Table;
}
//---------------------------------------------------------------------------

void TArticoliForm::Aggiorna()
{
	DBGrid1->RefreshDataSet();
	DBGrid1->AutoFitAll();
}
//---------------------------------------------------------------------------

void __fastcall TArticoliForm::FormActivate(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TArticoliForm::BitBtnAddClick(TObject *Sender)
{
	FormModificaArticolo->ImpostaDati(-1);
	FormModificaArticolo->ShowModal();
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TArticoliForm::BitBtnChangeClick(TObject *Sender)
{
	FormModificaArticolo->ImpostaDati(ADOQuery1ID_Articolo->Value);
	FormModificaArticolo->ShowModal();
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TArticoliForm::BitBtnDeleteClick(TObject *Sender)
{
	int i, res;
    WideString mess;

	// Sicuro di voler cancellare l'articolo?
	if ((Application->MessageBox(String::LoadStr(IDS_DELART).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	if (DBGrid1->SelectedRows->Count > 0) {
		for (i = 0; i < DBGrid1->SelectedRows->Count; i++) {
			DBGrid1->DataSource->DataSet->GotoBookmark(DBGrid1->SelectedRows->Items[i]);
			// Verifica che non sia in uso
			if (ADOQuery1ID_Articolo->AsInteger > 0) {
            	res = DBDataModule->ArticoloInUso(ADOQuery1ID_Articolo->AsInteger);
				if (res) {
                	//A seconda dell'errore compongo la stringa
                	mess = (res == 1) ? String::LoadStr(IDS_NODELART_PALLET).w_str() : String::LoadStr(IDS_NODELART_LAVOR).w_str();
                    //Visualizzo la stringa di errore
					Application->MessageBox(mess.c_bstr(), String::LoadStr(IDS_ALLARME).w_str() , MB_OK);
				} else {
					// OK cancella
					DBDataModule->CancellaArticolo(ADOQuery1ID_Articolo->Value);
                }
			}
		}
        DBGrid1->SelectedRows->Clear();
	} else {
		// Verifica che non sia in uso
		if (ADOQuery1ID_Articolo->AsInteger > 0) {
			res = DBDataModule->ArticoloInUso(ADOQuery1ID_Articolo->AsInteger);
			if (res) {
                //A seconda dell'errore compongo la stringa
                mess = (res == 1) ? String::LoadStr(IDS_NODELART_PALLET).w_str() : String::LoadStr(IDS_NODELART_LAVOR).w_str();
                //Visualizzo la stringa di errore
                Application->MessageBox(mess.c_bstr(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
			} else {
				// OK cancella
				DBDataModule->CancellaArticolo(ADOQuery1ID_Articolo->Value);
            }
		}
	}
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TArticoliForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (Shift.Contains(ssCtrl) && (Key == VK_DELETE))
		Key = 0; // ignore
}
//---------------------------------------------------------------------------

void __fastcall TArticoliForm::OnDoubleClickArticoli(TObject *Sender)
{
	FormModificaArticolo->ImpostaDati(ADOQuery1ID_Articolo->Value);
	if( FormModificaArticolo->ShowModal() == IDOK )
		Aggiorna();
}
//---------------------------------------------------------------------------

