//---------------------------------------------------------------------------

#pragma hdrstop

#include <IniFiles.hpp>
#include "NewJob.h"
#include "StringTableID.h"
#include "SelectPallet.h"
#include "Main.h"
#include "Robot.h"
#include "Define.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormNewJob *FormNewJob;
//---------------------------------------------------------------------------
__fastcall TFormNewJob::TFormNewJob(TComponent* Owner)
	: TForm(Owner)
{
	int i;
	TIniFile *pIni;

	pIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
    for (i = 0; i < N_MACCHINE_UT; i++) {
		PPFolderMU[i] = pIni->ReadString("MU" + IntToStr(i), "PPFolder", "C:\\meccano\\Programmi\\");
    }
	delete pIni;
	ID_Pallet = 0;
	PalletSelected = false;
	MachineSelected = false;
	ProgramSelected = false;
	ProgrammaRichiesto = false;
    m_PalletList.clear();
}
//---------------------------------------------------------------------------

void TFormNewJob::CaricaComboBoxMacchine(int tp)
{
	AnsiString tab;
	UINT i;

	if (tp > 0) {
		tab.sprintf("ElencoMacchine WHERE (ID_TipoPallet = %d) AND ((Tipo_Macchina <> 'BU') AND (Tipo_Macchina <> 'BI')) ORDER BY Tipo_Macchina, ID_Macchina", tp);
	} else {
		tab.sprintf("ElencoMacchine WHERE ((Tipo_Macchina <> 'BU') AND (Tipo_Macchina <> 'BI')) ORDER BY Tipo_Macchina, ID_Macchina");
	}
	DBDataModule->CaricaTabella(tab, RecList);
	ComboBox1->Clear();
	for (i = 0; i < RecList.size(); i++) {
		ComboBox1->Items->Add(RecList[i]["Descrizione_Macchina"]);
	}
	//Se ho un solo elemento seleziono sempre il primo
	if (RecList.size() == 1) {
		ComboBox1->ItemIndex = 0;
		MachineSelected = true;
	}
}
//---------------------------------------------------------------------------

void TFormNewJob::ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue)
{
	UINT i;

	for (i = 0; i < RecList.size(); i++) {
		if (rl[i][KeyField].ToIntDef(-1) == KeyValue ) {
			cb->ItemIndex = i;
			return;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::LE_ID_PalletClick(TObject *Sender)
{
	int idpallet;

	FormSelectPallet->ADOQuery1->SQL->Clear();
    FormSelectPallet->ADOQuery1->SQL->Add("SELECT * FROM PALLETLAVORABILI ORDER BY ID_Pallet");
	if (FormSelectPallet->ShowModal() == IDOK) {
		idpallet = FormSelectPallet->Selected;
		LE_ID_Pallet->Text = idpallet;
        m_PalletList.push_back(idpallet);
		DBDataModule->LeggiPallet(idpallet, p);
		CaricaComboBoxMacchine(p.ID_TipoPallet);
		PalletSelected = true;
		j.ID_Pallet = idpallet;
		LE_PRG->Text = p.Programma;
		Aggiorna();
	}
}
//---------------------------------------------------------------------------

void  TFormNewJob::SelectPallet(int idpallet, int TM, int MU)
{
	//Tipo missione
	ComboBox2->ItemIndex = TM;
	if (idpallet) {
		LE_ID_Pallet->Text = idpallet;
		DBDataModule->LeggiPallet(idpallet, p);
		CaricaComboBoxMacchine(p.ID_TipoPallet);
		if (MU != 0 ) {
			//Selezione della macchina...se passato
			ComboBox1->ItemIndex = MU - 1;   //L'indice parte da 0 :D
		}
		PalletSelected = true;
		j.ID_Pallet = idpallet;
		LE_PRG->Text = p.Programma;
//	   	ComboBox2->ItemIndex = 0;
	} else {
		LE_ID_Pallet->Text = "";
		LE_PRG->Text = "";
		ID_Pallet = 0;
		PalletSelected = false;
		MachineSelected = false;
		ProgramSelected = false;
	}
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::LE_PRGClick(TObject *Sender)
{
	if (j.Tipo_Macchina == "M") {
		OpenDialog1->InitialDir = PPFolderMU[j.ID_Macchina - 1];
		if (OpenDialog1->Execute() == IDOK) {
            if (ExtractFileName(OpenDialog1->FileName).Length() > 32) {
            	Application->MessageBox(String::LoadStr(IDS_ERR_FILENAMETOOLEN).w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);    
                LE_PRG->Text = "";
                ProgramSelected = false;
                Aggiorna();
            }
            else {
                LE_PRG->Text = OpenDialog1->FileName;
                j.Programma_Macchina = OpenDialog1->FileName;
                ProgramSelected = true;
                Aggiorna();
            }
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::ComboBox1Change(TObject *Sender)
{
	AnsiString Val, Clausula;

	j.Tipo_Macchina = RecList[ComboBox1->ItemIndex]["Tipo_Macchina"];
	j.ID_Macchina = RecList[ComboBox1->ItemIndex]["ID_Macchina"].ToIntDef(0);
	MachineSelected = true;
	Clausula.printf("WHERE ID_MACCHINA = %d And Tipo_Macchina = '%s'", RecList[ComboBox1->ItemIndex]["ID_Macchina"].ToIntDef(0), RecList[ComboBox1->ItemIndex]["Tipo_Macchina"]);
	DBDataModule->LeggiCampoTabella("Macchine", "Necessita_PRG", Clausula, Val);
	ProgrammaRichiesto = (Val.ToIntDef(1) == 1);
	LE_PRG->Text = "";
	j.Programma_Macchina = "";
	ProgramSelected = false;
	Aggiorna();
}
//---------------------------------------------------------------------------

void TFormNewJob::Aggiorna() {

	if (ComboBox2->ItemIndex == 1) {
		ProgramSelected = 1;
		LE_PRG->Enabled = true;
		LE_PRG->Text = "";
	}
	if ((ProgrammaRichiesto) && (ComboBox2->Text == "DROP")) {
		BitBtnOK->Enabled = (PalletSelected && MachineSelected && ProgramSelected);
	} else {
		BitBtnOK->Enabled = (PalletSelected && (ComboBox1->Text != ""));
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::BitBtnOKClick(TObject *Sender)
{
	TPallet p;
    TPezzo pz;
	UINT i;
	AnsiString logstr;
    
    for (i = 0; i < m_PalletList.size(); i++) {
		if (PalletSelected && m_PalletList[i] > 0) {
			j.ID_Pallet = m_PalletList[i];
			j.Tipo_Missione = ComboBox2->Text;
			j.Tipo_Macchina = RecList[ComboBox1->ItemIndex]["Tipo_Macchina"];
			j.ID_Macchina = RecList[ComboBox1->ItemIndex]["ID_Macchina"].ToIntDef(0);
			j.Programma_Macchina = LE_PRG->Text;
            j.Priorita = LE_Pri->Text.ToIntDef(50);
            DBDataModule->LeggiPallet(j.ID_Pallet, p);
			j.ID_Pezzo = p.ID_Pezzo;
            DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
            j.ID_Articolo = pz.ID_Articolo;
			j.ID_Job = DBDataModule->CreaJob(j);
			// LOG
			logstr.printf("Creato job Pallet %d, Tipo missione %s, Tipo macchina %s, macchina %d, Programma '%s'",
						  j.ID_Pallet, j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, j.Programma_Macchina);
			DBDataModule->Log("CLIENT", "JOBS", logstr);
            if (CheckBox1->Checked) {
                // LOG
                DBDataModule->Log("CLIENT", "JOBS", "Automatically Activate");
                DBDataModule->AttivaJob(j.ID_Job);
//				DBDataModule->LogAttivazioneProd(j.ID_Job);
            }
        }
    }
    //Una volta confermato, svuoto l'array
    m_PalletList.clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::FormActivate(TObject *Sender)
{
	CheckBox1->Checked = false;
    m_PalletList.clear();
	if (LE_ID_Pallet->Text != "") {
       m_PalletList.push_back(StrToInt(LE_ID_Pallet->Text)); 
    }	
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::FormClose(TObject *Sender, TCloseAction &Action)
{
	PalletSelected = false;
	MachineSelected = false;
	ProgramSelected = false;
	ComboBox1->ItemIndex = -1;
	ComboBox2->ItemIndex = 0;
	TrackBar1->Position = 50;
//	LE_Pri->Text = 50;
	LE_PRG->Text = "";
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::TrackBar1Change(TObject *Sender)
{
	LE_Pri->Text = TrackBar1->Position;
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::ComboBox2Change(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::ComboBox2Select(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormNewJob::BitBtnCANCELClick(TObject *Sender)
{
    m_PalletList.clear();
}
//---------------------------------------------------------------------------


