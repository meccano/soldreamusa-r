//---------------------------------------------------------------------------

#pragma hdrstop

#include "PLCTag.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

int PLCTagList[N_TAGS][3] = {{250,  0, 0}, //  0 RESET ALLARMI
						  {250,  0, 1}, //  1 RIPRISTINO
						  {250,  2, 0}, //  2 RESET BAIA1
						  {250,  2, 1}, //  3 RESET BAIA2
						  {145,  0, 0}, //  4 JOG+ TOTEM1
						  {145,  0, 2}, //  5 JOG+ TOTEM2
						  {145,  0, 1}, //  6 JOG- TOTEM1
						  {145,  0, 3}, //  7 JOG- TOTEM2
						  {145,  2, 0}, //  8 SETUP TOTEM1
						  {145,  2, 1}, //  9 SETUP TOTEM2
						  {145,  4, 0}, // 10 RESET TOTEM1
						  {145,  4, 1}, // 11 RESET TOTEM2
						  {148, 32, 0}, // 12 EXCL SCAN TOTEM1
						  {148, 32, 1}, // 13 EXCL SCAN TOTEM2
						  {250, 54, 0}, // 14 M6 PEL CHECK 320/400
						  {250, 54, 1}, // 15 M6 PEL CHECK S3
						  {250, 54, 2}, // 16 M6 PEL CHECK 148
						  {250, 54, 3}, // 17 M6 PEL CHECK 50
						  {250, 16, 0}, // 18 M1PELCHKS3
						  {250, 24, 0}, // 19 M2PELCHKPAL
						  {250, 24, 1}, // 20 M2PELCHKELE
						  {250, 32, 1}, // 21 M3PELCHKPAL
						  {250, 32, 2}, // 22 M3PELCHKELE
						  {148, 32, 2}, // 23 EXCL TAG TOTEM1
						  {148, 32, 3}, // 24 EXCL TAG TOTEM2
						  {250, 3, 0},  // 25 ABILITA-DISABILITA LA GESTIONE DEL PEZZO BAIA 1
						  {250, 3, 1}   // 26 ABILITA-DISABILITA LA GESTIONE DEL PEZZO BAIA 2
						 };

