//---------------------------------------------------------------------------

#pragma hdrstop

#include "ModbusTag.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

int ModbusTagList[N_TAGS][2] ={	{2006, 3}, // 0 RESET BAIA1
                                {2006, 4}, // 1 RESET BAIA2
                                {2006, 6}, // 2 FORZA pallet
                                {2006, 0}, // 3 Reset robot
                                {2001, 0}, // 4 Apri porta stazione 1
                                {2001, 1}, // 5 Chiudi porta stazione 1
                                {480, 0},  // 6 Esclusione presenza a bordo robot
                                {480, 5},  // 7 Esclusione controllo se deposito pieno
                                {480, 2},  // 8 Esclusione controllo PEL
                                {2111, 0}, // 9 scrittura postazione da pos asse attuale
                                {2111, 1}, // 10 scrittura postazione manuale
                                {2123, 0}, // 11 scrittura offset mag da pos asse attuale
                                {480, 3},  // 12 Esclusione presenza a bordo robot
                                {480, 4},  // 13 Esclusione Makino
                                {2006, 1}, // 14 Esclusione Makino
                                {2001, 8}, // 15 Apri porta stazione 2
                                {2001, 9}  // 16 Chiudi porta stazione 2
                                };

