//---------------------------------------------------------------------------

#ifndef MagazziniH
#define MagazziniH
//---------------------------------------------------------------------------
#include "CHILDWIN.h"
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.AppEvnts.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include "MyDBGrid.h"
#include "Scaffale.h"
#include "DB.h"
#include "GLCoordinates.hpp"
#include "GLCrossPlatform.hpp"
#include "GLObjects.hpp"
#include "GLSpaceText.hpp"
#include "GLWin32Viewer.hpp"
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Imaging.pngimage.hpp>
//---------------------------------------------------------------------------
class TMagazziniForm : public TMDIChild
{
__published:	// IDE-managed Components
	TApplicationEvents *ApplicationEvents1;
	TADOQuery *ADOQuery1;
	TDataSource *DataSource1;
	TIntegerField *ADOQuery1ID_Magazzino;
	TStringField *ADOQuery1Descrizione_Magazzino;
	TIntegerField *ADOQuery1ID_Postazione;
	TIntegerField *ADOQuery1Stato_Postazione;
	TIntegerField *ADOQuery1ID_Pallet;
	TIntegerField *ADOQuery1ID_TipoPallet;
	TStringField *ADOQuery1Codice_Tipopallet;
	TStringField *ADOQuery1Descrizione_TipoPallet;
	TIntegerField *ADOQuery1ID_Pezzo;
	TIntegerField *ADOQuery1ID_Articolo;
	TBCDField *ADOQuery1OffsetX;
	TBCDField *ADOQuery1OffsetY;
	TBCDField *ADOQuery1OffsetZ;
	TBCDField *ADOQuery1OffsetC;
	TStringField *ADOQuery1Descrizione_Pezzo;
	TTimer *TimerShow3d;
	TIntegerField *ADOQuery1Misura_Eseguita;
	TBCDField *ADOQuery1OffsetA;
	TBCDField *ADOQuery1OffsetB;
	TIntegerField *ADOQuery1Stato_Pezzo;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery1Descrizione_Articolo;
	TTimer *TimerUpd;
	TIntegerField *ADOQuery1Stato_Pallet;
	TIntegerField *ADOQuery1Extra_H_Pallet;
	TIntegerField *ADOQuery1ID_TipoPezzo;
	TTimer *TimerOVR;
	TTimer *TimerUpd2;
	TStringField *ADOQuery1Descrizione_Pallet;
	TIntegerField *ADOQuery1Extra_Altezza;
	TBCDField *ADOQuery1OffsetROT;
	TBCDField *ADOQuery1RawH;
	TBCDField *ADOQuery1Gap;
	TBCDField *ADOQuery1H;
	TBCDField *ADOQuery1DiffX;
	TBCDField *ADOQuery1DiffY;
	TBCDField *ADOQuery1DiffZ;
	TIntegerField *ADOQuery1FuoriTolleranza;
	TBCDField *ADOQuery1GapMin;
	TBCDField *ADOQuery1GapMax;
	TGroupBox *gbMissione;
	TBitBtn *bbMissione;
	TStaticText *stPrelievo;
	TStaticText *stDeposito;
	TPanel *pButton;
	TBitBtn *BitBtnBlocca;
	TBitBtn *BitBtnSvuota;
	TBitBtn *BitBtnSblocca;
	TBitBtn *BitBtnInsert;
	TBitBtn *BitBtnEdit;
	TBitBtn *BitBtnPick;
	TBitBtn *BitBtnJobCreate;
	TButton *bPrelievo;
	TButton *bDeposito;
	TStringField *ADOQuery1Alias;
	TStringField *ADOQuery1Descrizione;
	TStringField *ADOQuery1Label;
	TGridPanel *GridPanel1;
	TPageControl *PageControl1;
	TTabSheet *Storage1;
	TPageControl *PageControl2;
	TTabSheet *TabSheet1;
	TMyDBGrid *MyDBGrid1;
	TTabSheet *TabSheet2;
	TGroupBox *GroupBox1;
	TStaticText *StaticText3;
	TDBEdit *DBEdit4;
	TDBEdit *DBEdit5;
	TStaticText *StaticText4;
	TDBEdit *DBEdit6;
	TDBEdit *DBEdit7;
	TDBEdit *DBEdit12;
	TGroupBox *GroupBox2;
	TDBEdit *DBEdit2;
	TStaticText *StaticText1;
	TDBEdit *DBEdit1;
	TStaticText *StaticText2;
	TDBEdit *DBEdit3;
	TStaticText *StaticText12;
	TDBEdit *DBEdit16;
	TDBNavigator *DBNavigator1;
	TGroupBox *GroupBox3;
	TStaticText *StaticText5;
	TDBEdit *DBEdit8;
	TStaticText *StaticText6;
	TDBEdit *DBEdit9;
	TDBEdit *DBEdit10;
	TStaticText *StaticText8;
	TDBEdit *dbeX;
	TStaticText *StaticText9;
	TDBEdit *dbeY;
	TStaticText *StaticText10;
	TDBEdit *dbeZ;
	TStaticText *StaticText11;
	TDBEdit *dbeA;
	TStaticText *StaticText7;
	TDBEdit *dbeCodiceArticolo;
	TDBEdit *dbeDescrArticolo;
	TDBEdit *dbeB;
	TStaticText *StaticText13;
	TDBEdit *dbeC;
	TStaticText *StaticText14;
	TDBCheckBox *DBCheckBox4;
	TDBEdit *dbeROT;
	TStaticText *StaticText18;
	TStaticText *StaticText27;
	TDBEdit *dbeGap;
	TStaticText *StaticText28;
	TDBEdit *dbeH;
	TStaticText *StaticText29;
	TDBEdit *dbeGapMin;
	TStaticText *StaticText30;
	TDBEdit *dbeGapMax;
	TDBEdit *dbeDescrizionePezzo;
	TPanel *Panel9;
	TTabSheet *Legenda;
	TPanel *Panel8;
	TImage *Image1;
	TStringField *ADOQuery1Pallet;
	void __fastcall ApplicationEvents1Message(tagMSG &Msg, bool &Handled);
	void __fastcall PageControl1Change(TObject *Sender);
	void __fastcall FrameScaffale1GLSceneViewer1MouseEnter(TObject *Sender);
	//void __fastcall FrameScaffale5GLSceneViewer1MouseEnter(TObject *Sender);
	void __fastcall MyDBGrid1MouseEnter(TObject *Sender);
	void __fastcall ADOQuery1AfterScroll(TDataSet *DataSet);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall TimerShow3dTimer(TObject *Sender);
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall BitBtnPickClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BitBtnJobCreateClick(TObject *Sender);
	void __fastcall BitBtnBloccaClick(TObject *Sender);
	void __fastcall BitBtnSbloccaClick(TObject *Sender);
	void __fastcall BitBtnSvuotaClick(TObject *Sender);
	void __fastcall BitBtnEditClick(TObject *Sender);
	void __fastcall BitBtnInsertClick(TObject *Sender);
	void __fastcall MyDBGrid1DrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column, TGridDrawState State);
	void __fastcall GLSceneViewer1BeforeRender(TObject *Sender);
	void __fastcall TimerUpd2Timer(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ADOQuery1CalcFields(TDataSet *DataSet);
	void __fastcall bPrelievoClick(TObject *Sender);
	void __fastcall bDepositoClick(TObject *Sender);
	void __fastcall bbMissioneClick(TObject *Sender);
private:	// User declarations
	 TGLCustomSceneObject *oldpick, *selected;
public:		// User declarations
	__fastcall TMagazziniForm(TComponent* Owner);
	void SaveConfigIni(String FileName);
	void SelectPosDBGrid(int pos = 0);
	void Aggiorna();
	void Aggiorna3d();
	void AggiornaBottoni();
	void ColoraShape(int stato, TShape *t, TColor ColorON = clLime, TColor ColorOFF = clGray);
	void EvidenziaDBEdit(TDBEdit *e, bool val);
    void GeneraMissionePrelievoPallet(int ID_Pallet, int baia);
    void ImpostaAliases(int nmag);
	int n_utensili_x_piano;
	int n_piani;
	float raggio_totem;
	float base_utensile;
	float h_utensile;
	TFrameScaffale *FrameScaffale1;
	TRecordList RecList;
	TIndexList TabLotti, TabMiss;
	bool changing_override;
	int SelectedPage;
	double TolleranzaElettrodi, TolleranzaGAP, TolleranzaGAPMax;
	int RiutilizziElettrodi;
	int magprel, posprel, magdep, posdep, idpallet;
};
//---------------------------------------------------------------------------
extern PACKAGE TMagazziniForm *MagazziniForm;
//---------------------------------------------------------------------------
#endif
