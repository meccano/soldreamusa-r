//---------------------------------------------------------------------------

#ifndef JobsH
#define JobsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include "MyDBGrid.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
//---------------------------------------------------------------------------
class TJobsForm : public TMDIChild
{
__published:	// IDE-managed Components
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
	TDataSource *DataSource2;
	TADOQuery *ADOQuery2;
	TTimer *TimerUPD;
	TGridPanel *GridPanel1;
	TADOQuery *ADOQuery3;
	TDataSource *DataSource3;
	TAutoIncField *ADOQuery3ID_Missione;
	TIntegerField *ADOQuery3ID_Pallet;
	TStringField *ADOQuery3Descrizione_Macchina;
	TStringField *ADOQuery3Tipo_Missione;
	TStringField *ADOQuery3Programma_Macchina;
	TIntegerField *ADOQuery3Priorita;
	TDateTimeField *ADOQuery3Creazione;
	TIntegerField *ADOQuery1ID_Job;
	TIntegerField *ADOQuery1ID_Pallet;
	TStringField *ADOQuery1Tipo_Missione;
	TStringField *ADOQuery1Descrizione_Macchina;
	TStringField *ADOQuery1Programma_Macchina;
	TIntegerField *ADOQuery1Priorita;
	TDateTimeField *ADOQuery1Creazione;
	TIntegerField *ADOQuery2ID_Job;
	TIntegerField *ADOQuery2ID_Pallet;
	TStringField *ADOQuery2Tipo_Missione;
	TStringField *ADOQuery2Descrizione_Macchina;
	TStringField *ADOQuery2Programma_Macchina;
	TIntegerField *ADOQuery2Priorita;
	TDateTimeField *ADOQuery2Creazione;
	TPanel *pEsecuzione;
	TMyDBGrid *DBGrid3;
	TPanel *pSospesi;
	TMyDBGrid *DBGrid1;
	TPanel *pAttive;
	TMyDBGrid *DBGrid2;
	TGridPanel *GridPanel2;
	TPanel *Panel7;
	TPanel *Panel8;
	TPanel *Panel9;
	TBitBtn *BitBtnAdd;
	TBitBtn *BitBtnDelete;
	TBitBtn *BitBtnActivate;
	TBitBtn *BitBtnSuspend;
	TBitBtn *BitBtnAbort;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery2Codice_Articolo;
	TStringField *ADOQuery3Codice_Articolo;
	TStringField *ADOQuery3Tipo_Macchina;
	TIntegerField *ADOQuery3ID_Macchina;
	TStringField *ADOQuery2Commento;
	TStringField *ADOQuery3Commento;
	TBitBtn *BitBtnActAll;
	TBitBtn *BitBtnSuspAll;
	TBitBtn *BitBtnNewLavor;
	TStringField *ADOQuery1Descrizione;
	TIntegerField *ADOQuery2ID_Macchina;
	TStringField *ADOQuery2Tipo_Macchina;
	TIntegerField *ADOQuery2ID_Articolo;
	TDateTimeField *ADOQuery2Active;
	TStringField *ADOQuery2GruppoFase;
	TStringField *ADOQuery2GruppoPallet;
	TStringField *ADOQuery2Descrizione;
	TStringField *ADOQuery2Tipo_Missione_1;
	TStringField *ADOQuery3Descrizione;
	TStringField *ADOQuery1Commento;
	TStringField *ADOQuery3Descrizione_Op;
	TIntegerField *ADOQuery3ID_Pezzo;
	TIntegerField *ADOQuery3ID_Articolo;
	TStringField *ADOQuery1Descrizione_Op;
	TStringField *ADOQuery2Descrizione_Op;
	TBitBtn *bbDeleteAll;
	TIntegerField *ADOQuery1ID_Macchina;
	TStringField *ADOQuery1Tipo_Macchina;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall TimerUPDTimer(TObject *Sender);
	void __fastcall BitBtnAddClick(TObject *Sender);
	void __fastcall BitBtnActivateClick(TObject *Sender);
	void __fastcall BitBtnSuspendClick(TObject *Sender);
	void __fastcall BitBtnDeleteClick(TObject *Sender);
	void __fastcall BitBtnAbortClick(TObject *Sender);
	void __fastcall DBGrid3DrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column, TGridDrawState State);
	void __fastcall DBGrid1DblClick(TObject *Sender);
	void __fastcall DBGrid2DblClick(TObject *Sender);
	void __fastcall DBGrid2MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall BitBtnActAllClick(TObject *Sender);
	void __fastcall BitBtnSuspAllClick(TObject *Sender);
	void __fastcall OnClickNewLavor(TObject *Sender);
	void __fastcall bbDeleteAllClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TJobsForm(TComponent* Owner);
	void __fastcall MyShowHint(System::UnicodeString &HintStr, bool &CanShow, Vcl::Controls::THintInfo &HintInfo);
	bool VerificaSeMissioneInCorso(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int ID_Macchina);
	void AggiornaDB();
	void AggiornaBtn();
};
//---------------------------------------------------------------------------
extern PACKAGE TJobsForm *JobsForm;
//---------------------------------------------------------------------------
#endif
