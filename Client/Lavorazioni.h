//---------------------------------------------------------------------------

#ifndef LavorazioniH
#define LavorazioniH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include "MyDBGrid.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
//---------------------------------------------------------------------------
class TLavorazioniForm : public TMDIChild
{
__published:	// IDE-managed Components
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
    TIntegerField *qry1ID_Lavorazione;
    TStringField *qry1Descrizione;

	TDataSource *DataSource2;
	TADOQuery *ADOQuery2;
	TStringField *StringFieldADOQuery1Codice;
	TBitBtn *BitBtnMouveUp;
	TBitBtn *BitBtnMouveDown;
	TStringField *qry2Cod_Fase;
	TStringField *qry2Desc_Fase;
	TStringField *qry2Prg_MU;
	TStringField *qry2Desc_MU;
	TIntegerField *qry2Ordine_Seq;
	TStringField *qry2Messaggio_Op;
	TIntegerField *qry2Abilitazione;
	TIntegerField *qry2ID_Lavorazione;
	TIntegerField *qry2ID_Fase;
	TStringField *qry2Codice_Tipopallet;
	TStringField *qry2Descrizione_TipoPallet;
	TStringField *qry2Descrizione_Op;
	TGridPanel *GridPanel1;
	TPanel *pLavorazioni;
	TMyDBGrid *DBGridLavorazioni;
	TPanel *pFasi;
	TMyDBGrid *DBGridFasi;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery1Descrizione_Articolo;

	void __fastcall BitBtnAddLavorazioneClick(TObject *Sender);
	void __fastcall BitBtnChangeLavorazioneClick(TObject *Sender);
	void __fastcall BitBtnCopyLavorazioneClick(TObject *Sender);
	void __fastcall BitBtnDeleteLavorazioneClick(TObject *Sender);
	void __fastcall BitBtnAddFaseClick(TObject *Sender);
	void __fastcall BitBtnChangeFaseClick(TObject *Sender);
	void __fastcall BitBtnDeleteFaseClick(TObject *Sender);
	void __fastcall FormActive(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ADOQuery1AfterScroll(TDataSet *DataSet);
	void __fastcall OnDoubleClick(TObject *Sender);
	void __fastcall OnDoubleClickFasi(TObject *Sender);
	void __fastcall OnClickBtnMouveUp(TObject *Sender);
	void __fastcall OnClickBtnMouveDown(TObject *Sender);
	void __fastcall OnCellClickGridFasi(TColumn *Column);
	void __fastcall ADOQuery2AfterScroll(TDataSet *DataSet);

private:	// User declarations

public:		// User declarations
	__fastcall TLavorazioniForm(TComponent* Owner);
	void AggiornaGridLavorazioni();
	void AggiornaGridFasi();
	void Aggiorna();
	void AggiornaBtnUpDown();
	bool changing_override;
	int TrovaRecordPrec(int ID_Lavorazione, int OrdineSeq);
	int TrovaRecordNext(int ID_Lavorazione, int OrdineSeq);
	int SwapRowRecord(int ID_Lavorazione, int ID_Fase1, int ID_Fase2);
};
//---------------------------------------------------------------------------
extern PACKAGE TLavorazioniForm *LavorazioniForm;
//---------------------------------------------------------------------------
#endif
