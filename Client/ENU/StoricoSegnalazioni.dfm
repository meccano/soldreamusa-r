inherited StoricoSegnalazioniForm: TStoricoSegnalazioniForm
  Caption = 'Storico segnalazioni'
  ClientHeight = 710
  ClientWidth = 1079
  OnActivate = FormActivate
  OnCreate = FormCreate
  ExplicitWidth = 1095
  ExplicitHeight = 748
  PixelsPerInch = 96
  TextHeight = 19
  inherited PanelButtons: TPanel
    Left = 951
    Height = 661
    Font.Charset = ANSI_CHARSET
    Font.Name = 'Swis721 Cn BT'
    ParentFont = False
    ExplicitLeft = 951
    ExplicitHeight = 661
    inherited PanelClose: TPanel
      Top = 575
      ExplicitTop = 575
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 126
      Height = 248
      Align = alTop
      Caption = 'Filtri'
      TabOrder = 1
      object Label1: TLabel
        Left = 11
        Top = 25
        Width = 63
        Height = 19
        Caption = 'Data Inizio'
      end
      object Label2: TLabel
        Left = 11
        Top = 80
        Width = 57
        Height = 19
        Caption = 'Ora Inizio'
      end
      object Label3: TLabel
        Left = 11
        Top = 137
        Width = 56
        Height = 19
        Caption = 'Data Fine'
      end
      object Label4: TLabel
        Left = 11
        Top = 192
        Width = 50
        Height = 19
        Caption = 'Ora Fine'
      end
      object DateTimePicker1: TDateTimePicker
        Left = 11
        Top = 47
        Width = 106
        Height = 27
        Date = 41061
        Time = 41061
        TabOrder = 0
      end
      object DateTimePicker2: TDateTimePicker
        Left = 11
        Top = 102
        Width = 106
        Height = 27
        Date = 2
        Time = 2
        Kind = dtkTime
        TabOrder = 1
      end
      object DateTimePicker3: TDateTimePicker
        Left = 11
        Top = 159
        Width = 106
        Height = 27
        Date = 41061
        Time = 41061
        TabOrder = 2
      end
      object DateTimePicker4: TDateTimePicker
        Left = 11
        Top = 214
        Width = 106
        Height = 27
        Date = 2.999988425923220000
        Time = 2.999988425923220000
        Kind = dtkTime
        TabOrder = 3
      end
    end
    object StaticText1: TStaticText
      AlignWithMargins = True
      Left = 4
      Top = 342
      Width = 120
      Height = 23
      Margins.Top = 6
      Margins.Bottom = 6
      Align = alTop
      Alignment = taCenter
      Caption = 'StaticText1'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Swis721 Cn BT'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object rgFilter: TRadioGroup
      Left = 1
      Top = 249
      Width = 126
      Height = 87
      Align = alTop
      Caption = 'Filtra su'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Creazione'
        'Inizio lavoro'
        'Fine lavoro')
      ParentFont = False
      TabOrder = 3
      Visible = False
    end
    object bEsporta: TBitBtn
      AlignWithMargins = True
      Left = 4
      Top = 374
      Width = 120
      Height = 51
      Align = alTop
      Caption = 'Esporta'
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000871D0000871D00000000000000000000FFFFFFC8C8C8
        C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
        C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8FF
        FFFFC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
        C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
        C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFDDAE70D29544FCF7F1FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDAE70C87D19C87D19D29544FCF7F1FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C8C8C8C8C8C8C8C8C8C8
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDAE70C87D19C87D19C87D
        19C87D19D29544FCF7F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8C8C8C8
        C8C8C8C8C8C8C8C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDAE70C87D19
        C87D19C87D19C87D19C87D19C87D19D29544FCF7F1FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFC8C8C8C8C8C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDD
        AE70C87D19C87D19C87D19C87D19C87D19C87D19C87D19C87D19D29544FCF7F1
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFDDAE70C87D19C87D19C87D19C87D19C87D19C87D19C87D19C87D19C8
        7D19C87D19D29544FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF7F1C87D19C87D19C87D
        19C87D19F8EFE2FFFFFFFFFFFFF8F8F8B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E99393938C8C8CFFFFFF
        C87D19C87D19C87D19C87D19FFFFFF8C8C8C8C8C8C8C8C8C8C8C8CFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E99393938C
        8C8C909090FFFFFFC87D19C87D19C87D19C87D19FFFFFFC8C8C8C8C8C88C8C8C
        8C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9
        E99393938C8C8C909090BDBDBDFFFFFFC87D19C87D19C87D19C87D19FFFFFFC8
        C8C8C8C8C88C8C8C8C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFE9E9E99393938C8C8C909090BDBDBDC8C8C8FFFFFFC87D19C87D19C87D
        19C87D19FFFFFFC8C8C8C8C8C88C8C8C8C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF9393938C8C8C909090BDBDBDC8C8C8C8C8C8FFFFFF
        C87D19C87D19C87D19C87D19FFFFFFC8C8C8C8C8C88C8C8C8C8C8CFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8CBDBDBDC8C8C8C8
        C8C8C8C8C8FCFCFCC87D19C87D19C87D19C87D19F8F8F8C8C8C8C8C8C88C8C8C
        8C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C
        8CC8C8C8C8C8C8C8C8C8C8C8C8DDDDDDF8F8F8FFFFFFFFFFFFFCFCFCDDDDDDC8
        C8C8C8C8C88C8C8C8C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C8C8C8C8C8CC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
        C8C8C8C8C8C8C8C8C8C8C8C8C88C8C8C8C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8CC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
        C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8BDBDBD8C8C8C8C8C8CFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8CC8C8C8C8C8C8C8
        C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8BDBDBD9090908C8C8C
        939393FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C
        8CC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8BDBDBD90
        90908C8C8C939393E9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8C8C8C8C8C8CC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
        C8BDBDBD9090908C8C8C939393E9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8CC8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
        C8C8C8C8C8C8BDBDBD9090908C8C8C939393E9E9E9FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C8C8C8C8C8C8C8C8C
        8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C939393E9E9E9FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C8C8C8C8C
        8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C939393E9E9E9FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      TabOrder = 4
      OnClick = bEsportaClick
    end
    object BitBtn1: TBitBtn
      AlignWithMargins = True
      Left = 4
      Top = 431
      Width = 120
      Height = 51
      Align = alTop
      Caption = 'Acquisisci'#13#10'tutti'
      Glyph.Data = {
        0E060000424D0E06000000000000360000002800000016000000160000000100
        180000000000D8050000232E0000232E00000000000000000001FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFFFFFFFFFFFFFFEFF4EF005B008FB78FFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF9FC19F006D00006D00609960FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF508E50009A0000B900
        006E00609960EFF4EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFF10661000BB
        0000C80000C100007700307B30EFF4EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFAFCDAF00
        760000D10000D10000D10000D100008B00308030EFF5EFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
        70AA7000A10000DA0000DA0000DA0000DA0000DA00009A00107210CFE2CFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFF
        FFFFFFFF20802000CD0000E30000E30000E30000E30000E30000E30000B80010
        7710CFE4CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        0000FFFFFFDFEDDF007C0000EC0000EC0000EC0000D60000E50000EC0000EC00
        00EC0000C000007C009FCB9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF0000FFFFFF8FC58F00A90000F60000F60000C70010831000820000B8
        0000EE0000F60000F60000DF000083009FCD9FFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFF40A14003D70305FE0501CF01108910CFE7CFDF
        EFDF60B06000890000C00004F60405FE0505E60501890160B160FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000EFF7EF0088002BF72B18D318108F10CFE8CF
        FFFFFFFFFFFFFFFFFFDFEFDF60B460028F0211C41128F8282BF72B0B9E0B60B5
        60FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000AFDCAF17AA1736D536109510CFE9
        CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF0DF60B86005950529C72950
        F15014A31440AB40EFF8EFFFFFFFFFFFFFFFFFFF000060BD6017A917109C10CF
        EACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF1DF
        60BC60089C0840CB402EB62E30A930EFF8EFFFFFFFFFFFFF000010A11010A110
        CFEBCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFDFF2DF60C0600AA20A009B0020A820CFECCFFFFFFF000040B8
        41CFEDCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF3DF60C46000A20310A813CFEED0
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF4E060CB
        6C10B4270000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
      TabOrder = 5
      OnClick = BitBtn1Click
    end
  end
  inherited PanelTitle: TPanel
    Width = 1079
    ExplicitWidth = 1079
  end
  object Panel4: TPanel
    Left = 0
    Top = 49
    Width = 951
    Height = 661
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
  end
  object PageControl1: TPageControl
    AlignWithMargins = True
    Left = 3
    Top = 52
    Width = 945
    Height = 655
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Swis721 Cn BT'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnChange = PageControl1Change
    object tsAllarmi: TTabSheet
      Caption = 'Allarmi'
      object DBGrid1: TMyDBGrid
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 931
        Height = 615
        SortArrowColor = clBlue
        LineColor = clActiveCaption
        AutoWidthMin = 150
        Align = alClient
        DataSource = DataSource1
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Swis721 Cn BT'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'DataOra'
            Title.Caption = 'Date/Time'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Messaggio'
            Title.Caption = 'Messagge'
            Width = 500
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Acquisito'
            Title.Caption = 'Acknoledge'
            Width = 150
            Visible = True
          end>
      end
    end
    object tsLog: TTabSheet
      Caption = 'Log'
      ImageIndex = 1
      object dbgLog: TMyDBGrid
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 931
        Height = 615
        SortArrowColor = clBlue
        LineColor = clActiveCaption
        AutoWidthMin = 60
        Align = alClient
        DataSource = DataSource2
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Swis721 Cn BT'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataOra'
            Title.Caption = 'Data/Ora'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Sorgente'
            Title.Alignment = taCenter
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Tipo'
            Title.Alignment = taCenter
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Evento'
            Visible = True
          end>
      end
    end
    object tsProduzione: TTabSheet
      Caption = 'Produzione'
      ImageIndex = 2
      object gbgProd: TMyDBGrid
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 931
        Height = 615
        SortArrowColor = clBlue
        LineColor = clActiveCaption
        Align = alClient
        DataSource = DataSource3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Swis721 Cn BT'
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataOra'
            Title.Caption = 'Creazione'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Width = 250
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ID_Pallet'
            Title.Alignment = taCenter
            Title.Caption = 'Pallet'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Posizione'
            Title.Alignment = taCenter
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'Codice_Articolo'
            Title.Caption = 'Articolo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'NumOrdine'
            Title.Caption = 'Numero Ordine'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Visible = False
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Descrizione_Macchina'
            Title.Alignment = taCenter
            Title.Caption = 'Macchina'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Programma_Macchina'
            Title.Caption = 'Programma'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Attivazione'
            Title.Alignment = taCenter
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'Inizio_Lavoro'
            Title.Alignment = taCenter
            Title.Caption = 'Inizio lavoro'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fine_Lavoro'
            Title.Alignment = taCenter
            Title.Caption = 'Fine lavoro'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Width = 140
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'T_Lavoro'
            Title.Alignment = taCenter
            Title.Caption = 'T. lavoro'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Destinazione'
            Title.Alignment = taCenter
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -15
            Title.Font.Name = 'Swis721 Cn BT'
            Title.Font.Style = []
            Visible = False
          end>
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Eventi robot'
      ImageIndex = 3
      object dbgEventiRobot1: TMyDBGrid
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 931
        Height = 615
        SortArrowColor = clBlue
        LineColor = clActiveCaption
        Align = alClient
        DataSource = DataSource4
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -16
        TitleFont.Name = 'Swis721 Cn BT'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataOra'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Evento'
            Visible = True
          end>
      end
    end
  end
  object DataSource1: TDataSource
    AutoEdit = False
    DataSet = ADOQuery1
    Left = 208
    Top = 8
  end
  object ADOQuery1: TADOQuery
    Connection = DBDataModule.ADOConnection1
    CursorType = ctStatic
    ParamCheck = False
    Parameters = <>
    SQL.Strings = (
      'SELECT *, CASE WHEN Acquisito IS NULL THEN 0 ELSE 1 END AS Ack '
      'FROM Segnalazioni ORDER BY Ack, DataOra DESC')
    Left = 272
    Top = 8
    object ADOQuery1DataOra: TDateTimeField
      FieldName = 'DataOra'
    end
    object ADOQuery1Messaggio: TStringField
      FieldName = 'Messaggio'
      Size = 300
    end
    object ADOQuery1Allarme: TIntegerField
      FieldName = 'Allarme'
    end
    object ADOQuery1Acquisito: TDateTimeField
      FieldName = 'Acquisito'
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 152
    Top = 8
  end
  object DataSource2: TDataSource
    AutoEdit = False
    DataSet = ADOQuery2
    Left = 352
    Top = 8
  end
  object ADOQuery2: TADOQuery
    Connection = DBDataModule.ADOConnection1
    CursorType = ctStatic
    ParamCheck = False
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM Storico ORDER BY DataOra')
    Left = 416
    Top = 8
    object ADOQuery2DataOra: TDateTimeField
      FieldName = 'DataOra'
    end
    object ADOQuery2Sorgente: TStringField
      FieldName = 'Sorgente'
    end
    object ADOQuery2Tipo: TStringField
      FieldName = 'Tipo'
    end
    object ADOQuery2Evento: TStringField
      FieldName = 'Evento'
      Size = 200
    end
  end
  object DataSource3: TDataSource
    AutoEdit = False
    DataSet = ADOQuery3
    Left = 496
    Top = 8
  end
  object ADOQuery3: TADOQuery
    Connection = DBDataModule.ADOConnection1
    CursorType = ctStatic
    ParamCheck = False
    Parameters = <>
    SQL.Strings = (
      
        'SELECT  S.DataOra, S.Barcode, S.ID_Pallet, CAST(S.ID_Magazzino A' +
        'S VARCHAR) + '#39'/'#39' + CAST(S.ID_Postazione AS VARCHAR) AS Posizione' +
        ', S.Codice_Articolo, S.NumOrdine,'
      
        '        M.Descrizione_Macchina, S.Programma_Macchina, S.Attivazi' +
        'one, S.Inizio_Lavoro, S.Fine_Lavoro, Fine_Lavoro - Inizio_Lavoro' +
        ' AS T_Lavoro,'
      
        '        CAST(S.Magazzino_Destinazione AS VARCHAR) + '#39'/'#39' + CAST(S' +
        '.Postazione_Destinazione AS VARCHAR) AS Destinazione'
      'FROM    StoricoProduzione AS S LEFT OUTER JOIN'
      
        '        Macchine AS M ON S.ID_Macchina = M.ID_Macchina AND S.Tip' +
        'o_Macchina = M.Tipo_Macchina'
      'ORDER BY S.DataOra'
      '')
    Left = 560
    Top = 6
    object ADOQuery3DataOra: TDateTimeField
      FieldName = 'DataOra'
    end
    object ADOQuery3ID_Pallet: TIntegerField
      FieldName = 'ID_Pallet'
    end
    object ADOQuery3Posizione: TStringField
      FieldName = 'Posizione'
      ReadOnly = True
      Size = 61
    end
    object ADOQuery3Codice_Articolo: TStringField
      FieldName = 'Codice_Articolo'
      Size = 50
    end
    object ADOQuery3NumOrdine: TStringField
      FieldName = 'NumOrdine'
      Size = 255
    end
    object ADOQuery3Descrizione_Macchina: TStringField
      FieldName = 'Descrizione_Macchina'
      Size = 50
    end
    object ADOQuery3Attivazione: TDateTimeField
      Alignment = taCenter
      DisplayWidth = 15
      FieldName = 'Attivazione'
      DisplayFormat = 'dd-MM hh:mm:ss'
    end
    object ADOQuery3Programma_Macchina: TStringField
      FieldName = 'Programma_Macchina'
      Size = 200
    end
    object ADOQuery3Inizio_Lavoro: TDateTimeField
      Alignment = taCenter
      DisplayWidth = 15
      FieldName = 'Inizio_Lavoro'
      DisplayFormat = 'dd-MM hh:mm:ss'
    end
    object ADOQuery3Fine_Lavoro: TDateTimeField
      Alignment = taCenter
      DisplayWidth = 15
      FieldName = 'Fine_Lavoro'
      DisplayFormat = 'dd-MM hh:mm:ss'
    end
    object ADOQuery3T_Lavoro: TDateTimeField
      FieldName = 'T_Lavoro'
      ReadOnly = True
      DisplayFormat = 'hhh:mm:ss'
    end
    object ADOQuery3Destinazione: TStringField
      FieldName = 'Destinazione'
      ReadOnly = True
      Size = 61
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV file|*.csv'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 776
    Top = 8
  end
  object ADOQuery4: TADOQuery
    Connection = DBDataModule.ADOConnection1
    CursorType = ctStatic
    ParamCheck = False
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM EventiRobot ORDER BY DataOra DESC')
    Left = 637
    Top = 12
    object ADOQuery3ID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DataOra'
    end
    object ADOQuery3Sorgente: TWideStringField
      FieldName = 'Sorgente'
      Size = 255
    end
    object ADOQuery3Tipo: TWideStringField
      FieldName = 'Tipo'
      Size = 255
    end
    object ADOQuery3Evento: TStringField
      FieldName = 'Evento'
      Size = 255
    end
  end
  object DataSource4: TDataSource
    AutoEdit = False
    DataSet = ADOQuery4
    Left = 700
    Top = 13
  end
end
