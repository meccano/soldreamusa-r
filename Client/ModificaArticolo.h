//---------------------------------------------------------------------------

#ifndef ModificaArticoloH
#define ModificaArticoloH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "DB.h"
//---------------------------------------------------------------------------
class TFormModificaArticolo : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TBitBtn *BitBtnCANCEL;
	TGroupBox *GroupBox1;
	TLabeledEdit *LE_ID_Articolo;
	TStaticText *StaticText1;
	TComboBox *ComboBox1;
	TLabeledEdit *LE_Codice_Articolo;
	TLabeledEdit *LE_Descrizione;
	TBitBtn *BitBtnOK;
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall LE_Codice_ArticoloClick(TObject *Sender);
	void __fastcall LE_DescrizioneClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormModificaArticolo(TComponent* Owner);
	void ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue);
	void ImpostaDati(int ID_Pallet);
	TRecordList RecList;
	int ID_Articolo;
	TArticolo a;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormModificaArticolo *FormModificaArticolo;
//---------------------------------------------------------------------------
#endif
