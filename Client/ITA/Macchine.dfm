inherited MacchineForm: TMacchineForm
  Left = 274
  Top = 77
  Caption = 'Macchine'
  ClientHeight = 770
  ClientWidth = 1203
  Position = poDesigned
  OnKeyDown = FormKeyDown
  ExplicitWidth = 1219
  ExplicitHeight = 809
  PixelsPerInch = 96
  TextHeight = 19
  inherited PanelButtons: TPanel
    Left = 1075
    Height = 721
    ExplicitLeft = 1075
    ExplicitHeight = 721
    inherited PanelClose: TPanel
      Top = 635
      ExplicitTop = 635
    end
  end
  inherited PanelTitle: TPanel
    Width = 1203
    ExplicitWidth = 1203
  end
  object Panel4: TPanel
    Left = 0
    Top = 49
    Width = 1075
    Height = 721
    Align = alClient
    BorderWidth = 3
    TabOrder = 2
    object PageControl1: TPageControl
      AlignWithMargins = True
      Left = 7
      Top = 7
      Width = 1061
      Height = 707
      ActivePage = Macchine
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Swis721 Cn BT'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Macchine: TTabSheet
        Caption = 'Macchine utensili'
        ImageIndex = 2
        object GridPanel1: TGridPanel
          Left = 0
          Top = 0
          Width = 1053
          Height = 673
          Align = alClient
          Caption = 'GridPanel1'
          ColumnCollection = <
            item
              Value = 33.501554856890700000
            end
            item
              Value = 33.501554856890700000
            end
            item
              Value = 32.996890286218600000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = GroupBoxMU1
              Row = 0
            end
            item
              Column = 1
              Control = GroupBoxMU2
              Row = 0
            end
            item
              Column = 2
              Control = GroupBoxMU3
              Row = 0
            end>
          RowCollection = <
            item
              Value = 100
            end>
          ShowCaption = False
          TabOrder = 0
          object GroupBoxMU1: TGroupBox
            AlignWithMargins = True
            Left = 4
            Top = 4
            Width = 346
            Height = 665
            Align = alClient
            Caption = 'Fagima Jazz X 1'
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Swis721 Cn BT'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            DesignSize = (
              346
              665)
            object sPalletSbloccato1: TShape
              Left = 7
              Top = 30
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object lbl5: TLabel
              Left = 48
              Top = 28
              Width = 95
              Height = 19
              Caption = 'Pallet sbloccato'
            end
            object sPalletBloccato1: TShape
              Left = 7
              Top = 53
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object lbl6: TLabel
              Left = 48
              Top = 51
              Width = 88
              Height = 19
              Caption = 'Pallet bloccato'
            end
            object sAutomatico1: TShape
              Tag = 18
              Left = 7
              Top = 99
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object sAllarme1: TShape
              Left = 7
              Top = 145
              Width = 35
              Height = 17
              Brush.Color = clRed
            end
            object Label4: TLabel
              Left = 48
              Top = 97
              Width = 70
              Height = 19
              Caption = 'Automatico'
            end
            object Label5: TLabel
              Left = 48
              Top = 143
              Width = 46
              Height = 19
              Caption = 'Allarme'
            end
            object sPortaAperta1: TShape
              Left = 7
              Top = 76
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label14: TLabel
              Left = 48
              Top = 74
              Width = 74
              Height = 19
              Caption = 'Porta aperta'
            end
            object Label1: TLabel
              Left = 48
              Top = 120
              Width = 59
              Height = 19
              Caption = 'Fine Ciclo'
            end
            object sFineCiclo1: TShape
              Tag = 18
              Left = 7
              Top = 122
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object leAut1: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 343
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 108
              EditLabel.Height = 19
              EditLabel.Caption = 'Modalit'#224' macchina'
              EditLabel.Font.Charset = ANSI_CHARSET
              EditLabel.Font.Color = clWindowText
              EditLabel.Font.Height = -16
              EditLabel.Font.Name = 'Swis721 LtCn BT'
              EditLabel.Font.Style = []
              EditLabel.ParentFont = False
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 0
            end
            object leRun1: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 374
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 88
              EditLabel.Height = 19
              EditLabel.Caption = 'Stato macchina'
              EditLabel.Font.Charset = ANSI_CHARSET
              EditLabel.Font.Color = clWindowText
              EditLabel.Font.Height = -16
              EditLabel.Font.Name = 'Swis721 LtCn BT'
              EditLabel.Font.Style = []
              EditLabel.ParentFont = False
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 1
            end
            object leMotion1: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 405
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 55
              EditLabel.Height = 19
              EditLabel.Caption = 'Stato assi'
              EditLabel.Font.Charset = ANSI_CHARSET
              EditLabel.Font.Color = clWindowText
              EditLabel.Font.Height = -16
              EditLabel.Font.Name = 'Swis721 LtCn BT'
              EditLabel.Font.Style = []
              EditLabel.ParentFont = False
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 2
            end
            object leProg1: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 436
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 109
              EditLabel.Height = 19
              EditLabel.Caption = 'Prog. n esecuzione'
              EditLabel.Font.Charset = ANSI_CHARSET
              EditLabel.Font.Color = clWindowText
              EditLabel.Font.Height = -16
              EditLabel.Font.Name = 'Swis721 LtCn BT'
              EditLabel.Font.Style = []
              EditLabel.ParentFont = False
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 3
            end
            object leMainProg1: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 467
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 91
              EditLabel.Height = 19
              EditLabel.Caption = 'Prog. principale'
              EditLabel.Font.Charset = ANSI_CHARSET
              EditLabel.Font.Color = clWindowText
              EditLabel.Font.Height = -16
              EditLabel.Font.Name = 'Swis721 LtCn BT'
              EditLabel.Font.Style = []
              EditLabel.ParentFont = False
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 4
            end
            object grp7: TGroupBox
              AlignWithMargins = True
              Left = 5
              Top = 500
              Width = 336
              Height = 160
              Align = alBottom
              Caption = 'Pallet in macchina'
              TabOrder = 5
              DesignSize = (
                336
                160)
              object lePalletM1: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 28
                Width = 46
                Height = 25
                TabStop = False
                Color = clGradientInactiveCaption
                Ctl3D = False
                EditLabel.Width = 32
                EditLabel.Height = 19
                EditLabel.Caption = 'Pallet'
                LabelPosition = lpLeft
                LabelSpacing = 5
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 0
              end
              object leCodiceM1: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 59
                Width = 225
                Height = 25
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                Ctl3D = False
                EditLabel.Width = 47
                EditLabel.Height = 19
                EditLabel.Caption = 'Articolo'
                LabelPosition = lpLeft
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 1
              end
              object leDescM1: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 90
                Width = 225
                Height = 25
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                Ctl3D = False
                EditLabel.Width = 72
                EditLabel.Height = 19
                EditLabel.Caption = 'Programma'
                LabelPosition = lpLeft
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 2
              end
              object bbModificaM1: TBitBtn
                Tag = 1
                AlignWithMargins = True
                Left = 175
                Top = 121
                Width = 90
                Height = 29
                Caption = 'Modifica'
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
                  FFFFFFFFFFFFFCFDFE265F9C4F80BA6D96C6ACC3DEE8EEF6FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5383BCC0E1F6A6
                  D4F083B8DF3A79B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF36755E0E593D
                  0E593D0E593D0E593D23647BD5E6F5D7E9FACBE3F99FD9F4468BC3E8EFF60E59
                  3D0E593D36755EFFFFFF146043FFFFFFFFFFFFFDFEFEFBFEFDD4E1ED7FA7D2F9
                  FCFEBCE3F938BDE8519ACC498DC5E8EFF6E1F4EF146043FFFFFF1B684AFCFDFD
                  FCFEFEF9FDFCF6FBFAF1FAF76390C2C8E4F544D0F400C3F225B8E65198CB4D91
                  C6E8EFF61B684AFFFFFF247152FAFCFBF9FDFCF6FBFAF1FAF7EDF8F5E8EFF660
                  8FC4C2EAF828CBF300C3F225B8E65198CB5193C8E8EFF6FFFFFF2C7A5AF8FCFA
                  F6FBFAF1FAF7EDF8F5E8F6F2E3F4EFE8EFF66594C5C2EBF828CBF300C3F225B8
                  E65198CB5696CAFFFFFF368462F7FBFAF1FAF7EDF8F5E8F6F2E3F4EFDDF2ECD8
                  F0E9E8EFF66B97C7C2EBF828CBF300C3F227B8E65299CC6C9DCB3E8D6AF4FAF8
                  EDF8F5E8F6F2E3F4EFDDF2ECD8F0E9D2EEE6CDECE4E8EFF6709BCAC2EBF828CB
                  F300C3F23FBBE65693C7479672F1F9F7E9F6F2E3F4EFDDF2ECD8F0E9D2EEE6CD
                  ECE4C8EAE1C3E8DEE8EFF6759ECCC2EBF842D0F35E9FCEB9CCE34E9E79EEF9F5
                  E4F4EFDDF2ECD8F0E9D2EEE6CDECE4C8EAE1C3E8DEBFE6DBBBE5D9E8EFF679A1
                  CE7CA4CE59989BFFFFFF54A57FEEF8F5EBF7F4E8F6F2E4F5F0E1F4EFDEF2EDDB
                  F1EBD8F0E9D6EFE8D3EEE7D1EEE6C8E6E2C5E3E254A57FFFFFFF56A57F59AA83
                  59AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA
                  8359AA8354A47EFFFFFF54A27E99C9B29ACAB39ACAB39ACAB39ACAB39ACAB39A
                  CAB39ACAB39ACAB39ACAB39ACAB39ACAB39ACAB351A17CFFFFFF5BA98498C9B1
                  99C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9
                  B299C9B259A782FFFFFFA2CFBA5EAC875CAB855CAB855CAB855CAB855CAB855C
                  AB855CAB855CAB855CAB855CAB855CAB855CAB85A0CEB8FFFFFF}
                Spacing = 3
                TabOrder = 3
                OnClick = bbModificaMClick
              end
              object bbSvuotaM1: TBitBtn
                Tag = 1
                AlignWithMargins = True
                Left = 78
                Top = 121
                Width = 90
                Height = 29
                Caption = 'Svuota'
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
                  FFFFFFFFFFFFE3E3F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDEEFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7979C01114957979C0FFFFFFFF
                  FFFFFFFFFFFFFFFF7676BE1115967676BEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  7676BF121CA2112CBF121CA27676BFFFFFFFFFFFFF7676BF121DA2112DBF111D
                  A47374BEFFFFFFFFFFFFFFFFFF7879C2131EA61432C21432C31432C2121EA575
                  76C17576C1121EA51432C21432C31432C3121FA67576C1FFFFFFE0E0F12024A1
                  6176D71A39C71837C71837C71837C6131FA71420A91837C61837C71837C71B39
                  C86579D91F22A0E2E3F2FFFFFF787AC63C44B4657CDB1E40CB1C3ECB1C3ECB1C
                  3DCA1C3DCA1C3ECB1C3ECB1F40CC677CDB3A41B37577C5FFFFFFFFFFFFFFFFFF
                  7577C73D45B8667FDD2246CF2045CF2045CF2045CF2045CF2347D06981DD3942
                  B6787AC8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7578C9313EB9264DD3244DD424
                  4DD4244DD4244DD4254DD32E3BB7787ACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF7478CB182BB72953D82954D92954D92954D92954D92953D81829B67478
                  CBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7478CD1A2EBB2D5BDC2D5CDD2D5CDD2F
                  5EDD2F5DDD2D5CDD2D5CDD2D5BDC192DB97478CDFFFFFFFFFFFFFFFFFF7478CF
                  1A31BF3162E13163E23163E23365E27495EA7193EA3364E23163E23163E23162
                  E11A2FBE7478CFFFFFFFDCDEF3212CB96F94EC356AE6356AE6376CE67699ED3B
                  4AC54050C87397ED376BE6356AE6356AE67397ED1E29B7E2E3F5FFFFFF7479D2
                  4858CE7299F03B71EA799DF03C4BC8767BD37479D24353CB769AF03B71EA779C
                  F14353CB7479D2FFFFFFFFFFFFFFFFFF7076D34A5BD18DACF33C4CCA767BD5FF
                  FFFFFFFFFF7379D44354CD8DACF34354CD7076D3FFFFFFFFFFFFFFFFFFFFFFFF
                  FCFCFE767CD6212EC0767CD6FFFFFFFFFFFFFFFFFFFFFFFF767CD6212EC0767C
                  D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDEF5FFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFDCDEF5FFFFFFFFFFFFFFFFFFFFFFFF}
                Spacing = 3
                TabOrder = 4
                OnClick = bbSvuotaMClick
              end
            end
            object BitBtnSendProgramMU1: TBitBtn
              Tag = 1
              AlignWithMargins = True
              Left = 221
              Top = 13
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Invia'#13#10'programma'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                F6060000424DF606000000000000360000002800000018000000180000000100
                180000000000C006000000000000000000000000000000000000FFFFFF8B8685
                9995939894929894929894929894929894929894929894929894929894929894
                929894929894929894929894929894929894929894929894929995938B8685FF
                FFFF8F8B89E7E4E3E3E0DFE5E2E1E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2
                E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E5E2
                E1E3E0DFE7E4E38F8B89A19F9BE0DEDCBBB9B693908C96938F96938F96938F96
                938F96938F96938F96938F96938F96938F96938F96938F96938F96938F96938F
                96938F96938F93908CBBB9B6E0DEDCA19F9BAAA7A4EEEEEDDEDCDBD1CFCFD3D1
                D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3
                D1D0D3D1D0D3D1D0D3D1D1D6D2D3D4D1D1DFDCDCEEEEEDAAA7A4B4B0AEF6F5F4
                F1F0EFF2F1F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2
                F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F1FAF5F7D5DED2D5DCD0F2F0F1F8F6F6B4
                B0AEBDB9B6FBFCFCF6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6
                F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F9F7F9DFEADF08972913B0
                3E93BB8DFFFFFFBDB9B6C3BFBFFFFFFFFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFB
                FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFEFCFE
                E7F1E735C46421CD72A0CC9FFFFFFFC3BFBFCCC9C7FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFBEE8CAABDFB7F3F8F3FFFFFFCCC9C7B9B5B2E5E3E4
                E7E7E7E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6
                E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E8E6E7EDE8EBEEE8ECEAE7E9E5E3E5B9
                B5B2FDFDFDD9D7D6DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7
                DBD9D7DBD9D7DBDAD8DFE0E1E0E1E2E1E2E3E1E2E3E1E2E3E1E3E3E0E2E3DEDF
                E0DCDAD8D9D7D6FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB14A00C37123AB4504A94709A74406A74406
                A54304A33B00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB65814F5E4A0E29B16E3
                9E1BE29B19E39A16CA7515AA5014FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBB60
                16F3E0A9E3A225E3A32BE19D25E2A023CE7B1BB0581AFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFC16818F4E2B2E6AD37E6AD3AE5AA36E6AA33D28424B55E1DFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFC8721AF4E7B9E9B743EAB747E9B445E9B441
                D78E2DBD651FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCD791EF5E8C1EEC152ED
                C057ECBE52ECBE50DB9734C16C21FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDB9432DB9432D9902CD68B28D17F
                1EF7EACAEFC762F0C864EEC561EFC760DF9D3AC67120C57122C47123C36F24C3
                6F24FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF9932
                ECCC83F6E3ABEFC85FF5DA8CF3D070F2CF70F1CE6DF0CC6BEDC45FEBBA57ECBB
                52CF7E29C87824FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE19B2DEECD84FAEBC0F5D87DF4D77FF4D67CF3D479F3D276
                F3D176F3D473D99131D08229FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4A133EECF83FAEEC5F6DC83F6
                DB87F5DA84F5D982F6D980DD9A37D68D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8AF
                3FF5E2ACFBEEC4F7DE89F7E08DF8E495E7B248DE9832FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFEAB23EF5E3B0FAEAB5F8E396ECC157E3A337FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBB440F6E5B4F3D57CEAAE3C
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEB
                B746EBB947FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentFont = False
              Spacing = 10
              TabOrder = 6
              OnClick = BitBtnSendProgramClick
            end
            object BitBtnStartMU1: TBitBtn
              Tag = 1
              AlignWithMargins = True
              Left = 221
              Top = 73
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Start'#13#10'programma'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                F6060000424DF606000000000000360000002800000018000000180000000100
                180000000000C006000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFF1764DDB5C2F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF479EE925B2EAE9ECFBFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABB6EF33E7F61779E0FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF2587E521F7FB2465DCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFAEB9EE3DF0F908CCF27691E4FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D88E322EFFA08AEEEAEB7EC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8796E53B
                F2FA05D9F70B8CE7E7EBFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF249EE81CE2F907D7F91469DEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6482E13CEBFA04CCF708CAF73A70DCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F9FE2CB7ED13D9F8
                08C8F709B3F2758DE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF537EE03DF0FC05C4F609C5F80B99EDADB7E9FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFE1E5F82DAEEC11D1F808BFF608C4F90D6FE0E7EAF9FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFAFBDF04295E53E86E44A8BE51581E12EDFFA07BEF609BB
                F60AC0FB1453D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAD3F526C2EF12EAFA02D6F706D0F7
                06C4F608BFF609BAF60AB4F70BB2F8395FD1FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADCBF284
                EEF91CDEF80CCEF704C5F603BCF605B7F607B1F608AEF70897F28398E0FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF73B6EB93F4FC4BE3F954E1F94DDEF936D5F831CFF820C2F721
                C4F92194E9CFDBF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2EBFA81D3F272EAFB4ADEF94FDDF951DB
                F954DAF956DAFA55D9FC55DCFF5AAEEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6C6F098EEFA
                55E1FA4CDDF94FDAF950DBFA59A5E6A4B9E9AFC5EDB2C8EEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF72B3EA91F0FC46DCF94EDAF951D9F951D6FB7FB4EBFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFE2EBFA81D1F274E6FB4AD9F951D8F952D5FA53
                CDF8ADC3ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4C4EF9BECFB5CDD
                FA4FD8F952D4F952D4FA55B6EEEFF4FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF7EB9EB98EFFE4FD8F951D4F952D2F951D4FC5DA1E3FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFEFF3FC80CDF17CE6FD4CD5FA4FD3FA4ED0FA50D0FB
                91B5E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB6D0F27BC4EE7CBFEC7C
                BDEC7CBDEC7EBEED6AA3E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentFont = False
              Spacing = 10
              TabOrder = 7
              OnClick = BitBtnStartClick
            end
            object btnJobCreateMU1: TBitBtn
              Tag = 1
              AlignWithMargins = True
              Left = 221
              Top = 133
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Preleva'#13#10'pallet'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                C2040000424DC204000000000000420000002800000018000000180000000100
                10000300000080040000120B0000120B00000000000000000000007C0000E003
                00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FB556292529252925292529252925292529252925
                2925292529252925F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F734EE71C
                E71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CD65AFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7F734EE71CE71CE71CE71CE71CE71CE71CE71CE71C
                E71CE71CE71CE71CB556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A6B2D
                6B2D8C314A29E71CE71CE71CE71CE71C4A296B2D6B2D6B2D1863FF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FAD35E71CE71CE71CE71C6B2D
                DE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FB556E71CE71CE71CE71CE71C1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD772925E71CE71CE71CE71CEF3DFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                3146E71CE71CE71CE71C08217B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7F5A6BE71CE71CE71CE71CE71C734EFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8C31
                E71CE71CE71CE71C4A29DE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7F9452E71CE71CE71CE71CE71CD65AFF7FFF7FFF7F
                FF7FFF7FFF7FFF7F5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73EF3DD65A
                39679452E71C8C31FF7FFF7FFF7FFF7FFF7FFF7FFF7F1042E71CAD355A6BFF7F
                FF7FFF7FFF7FFF7FFF7FDE7BD65A3146EF3D396739679C73FF7FFF7FFF7FFF7F
                FF7FFF7F9452E71CAD35E71CE71C524ADE7BFF7FFF7FFF7FFF7F186308212925
                4A29E71CF75EFF7F7B6F7B6F7B6FF75E4A294A29E71C8C31DE7B7B6FAD35E71C
                4A29FF7FFF7FFF7FFF7F29256B2DD65AD65ACE3929251863E71CE71CE71CE71C
                E71CE71C08217B6FFF7FFF7FFF7FB5563146FF7FFF7FFF7FFF7FE71C734EAD35
                2925D65AE71C39670821E71CE71CE71CE71CE71C8C31FF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7F8C310821B556D65A6B2D4A2939676B2D4A294A292925
                E71CE71CE71C1863FF7FFF7FBD7710428C31FF7FFF7FFF7FFF7F7B6F4A29E71C
                E71C29251863FF7FFF7FFF7FFF7F7B6FCE39CE39082129257B6F18632925E71C
                EF3DFF7FFF7FFF7FFF7FFF7FBD77945294527B6FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7F1863E71C0821E71C4A29F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FB556E71C734EBD77FF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7F}
              ParentFont = False
              Spacing = 10
              TabOrder = 8
              OnClick = BitBtnJobCreateMClick
            end
          end
          object GroupBoxMU2: TGroupBox
            AlignWithMargins = True
            Left = 356
            Top = 4
            Width = 346
            Height = 665
            Align = alClient
            Caption = 'Fagima Jazz X 2'
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Swis721 LtCn BT'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            DesignSize = (
              346
              665)
            object sPalletSbloccato2: TShape
              Left = 6
              Top = 30
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label2: TLabel
              Left = 47
              Top = 28
              Width = 92
              Height = 19
              Caption = 'Pallet sbloccato'
            end
            object sPalletBloccato2: TShape
              Left = 6
              Top = 53
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label3: TLabel
              Left = 47
              Top = 52
              Width = 86
              Height = 19
              Caption = 'Pallet bloccato'
            end
            object sAutomatico2: TShape
              Tag = 18
              Left = 6
              Top = 99
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object sAllarme2: TShape
              Left = 6
              Top = 145
              Width = 35
              Height = 17
              Brush.Color = clRed
            end
            object Label7: TLabel
              Left = 47
              Top = 97
              Width = 65
              Height = 19
              Caption = 'Automatico'
            end
            object Label8: TLabel
              Left = 47
              Top = 143
              Width = 44
              Height = 19
              Caption = 'Allarme'
            end
            object sPortaAperta2: TShape
              Left = 6
              Top = 76
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label16: TLabel
              Left = 47
              Top = 74
              Width = 67
              Height = 19
              Caption = 'Porta aperta'
            end
            object sFineCiclo2: TShape
              Tag = 18
              Left = 6
              Top = 122
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label6: TLabel
              Left = 47
              Top = 120
              Width = 60
              Height = 19
              Caption = 'Fine Ciclo'
            end
            object leAut2: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 343
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 108
              EditLabel.Height = 19
              EditLabel.Caption = 'Modalit'#224' macchina'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 0
            end
            object leRun2: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 374
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 88
              EditLabel.Height = 19
              EditLabel.Caption = 'Stato macchina'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 1
            end
            object leMotion2: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 405
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 55
              EditLabel.Height = 19
              EditLabel.Caption = 'Stato assi'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 2
            end
            object leProg2: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 436
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 109
              EditLabel.Height = 19
              EditLabel.Caption = 'Prog. n esecuzione'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 3
            end
            object leMainProg2: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 467
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 91
              EditLabel.Height = 19
              EditLabel.Caption = 'Prog. principale'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 4
            end
            object GroupBox2: TGroupBox
              AlignWithMargins = True
              Left = 5
              Top = 500
              Width = 336
              Height = 160
              Align = alBottom
              Caption = 'Pallet in macchina'
              TabOrder = 5
              DesignSize = (
                336
                160)
              object lePalletM2: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 28
                Width = 46
                Height = 25
                TabStop = False
                Color = clGradientInactiveCaption
                Ctl3D = False
                EditLabel.Width = 33
                EditLabel.Height = 19
                EditLabel.Caption = 'Pallet'
                LabelPosition = lpLeft
                LabelSpacing = 5
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 0
              end
              object leCodiceM2: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 59
                Width = 231
                Height = 25
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                Ctl3D = False
                EditLabel.Width = 45
                EditLabel.Height = 19
                EditLabel.Caption = 'Articolo'
                LabelPosition = lpLeft
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 1
              end
              object leDescM2: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 90
                Width = 231
                Height = 25
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                Ctl3D = False
                EditLabel.Width = 64
                EditLabel.Height = 19
                EditLabel.Caption = 'Programma'
                LabelPosition = lpLeft
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 2
              end
              object bbModificaM2: TBitBtn
                Tag = 2
                AlignWithMargins = True
                Left = 174
                Top = 121
                Width = 90
                Height = 29
                Caption = 'Modifica'
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
                  FFFFFFFFFFFFFCFDFE265F9C4F80BA6D96C6ACC3DEE8EEF6FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5383BCC0E1F6A6
                  D4F083B8DF3A79B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF36755E0E593D
                  0E593D0E593D0E593D23647BD5E6F5D7E9FACBE3F99FD9F4468BC3E8EFF60E59
                  3D0E593D36755EFFFFFF146043FFFFFFFFFFFFFDFEFEFBFEFDD4E1ED7FA7D2F9
                  FCFEBCE3F938BDE8519ACC498DC5E8EFF6E1F4EF146043FFFFFF1B684AFCFDFD
                  FCFEFEF9FDFCF6FBFAF1FAF76390C2C8E4F544D0F400C3F225B8E65198CB4D91
                  C6E8EFF61B684AFFFFFF247152FAFCFBF9FDFCF6FBFAF1FAF7EDF8F5E8EFF660
                  8FC4C2EAF828CBF300C3F225B8E65198CB5193C8E8EFF6FFFFFF2C7A5AF8FCFA
                  F6FBFAF1FAF7EDF8F5E8F6F2E3F4EFE8EFF66594C5C2EBF828CBF300C3F225B8
                  E65198CB5696CAFFFFFF368462F7FBFAF1FAF7EDF8F5E8F6F2E3F4EFDDF2ECD8
                  F0E9E8EFF66B97C7C2EBF828CBF300C3F227B8E65299CC6C9DCB3E8D6AF4FAF8
                  EDF8F5E8F6F2E3F4EFDDF2ECD8F0E9D2EEE6CDECE4E8EFF6709BCAC2EBF828CB
                  F300C3F23FBBE65693C7479672F1F9F7E9F6F2E3F4EFDDF2ECD8F0E9D2EEE6CD
                  ECE4C8EAE1C3E8DEE8EFF6759ECCC2EBF842D0F35E9FCEB9CCE34E9E79EEF9F5
                  E4F4EFDDF2ECD8F0E9D2EEE6CDECE4C8EAE1C3E8DEBFE6DBBBE5D9E8EFF679A1
                  CE7CA4CE59989BFFFFFF54A57FEEF8F5EBF7F4E8F6F2E4F5F0E1F4EFDEF2EDDB
                  F1EBD8F0E9D6EFE8D3EEE7D1EEE6C8E6E2C5E3E254A57FFFFFFF56A57F59AA83
                  59AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA
                  8359AA8354A47EFFFFFF54A27E99C9B29ACAB39ACAB39ACAB39ACAB39ACAB39A
                  CAB39ACAB39ACAB39ACAB39ACAB39ACAB39ACAB351A17CFFFFFF5BA98498C9B1
                  99C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9
                  B299C9B259A782FFFFFFA2CFBA5EAC875CAB855CAB855CAB855CAB855CAB855C
                  AB855CAB855CAB855CAB855CAB855CAB855CAB85A0CEB8FFFFFF}
                Spacing = 3
                TabOrder = 3
                OnClick = bbModificaMClick
              end
              object bbSvuotaM2: TBitBtn
                Tag = 2
                AlignWithMargins = True
                Left = 78
                Top = 121
                Width = 90
                Height = 29
                Caption = 'Svuota'
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
                  FFFFFFFFFFFFE3E3F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDEEFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7979C01114957979C0FFFFFFFF
                  FFFFFFFFFFFFFFFF7676BE1115967676BEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  7676BF121CA2112CBF121CA27676BFFFFFFFFFFFFF7676BF121DA2112DBF111D
                  A47374BEFFFFFFFFFFFFFFFFFF7879C2131EA61432C21432C31432C2121EA575
                  76C17576C1121EA51432C21432C31432C3121FA67576C1FFFFFFE0E0F12024A1
                  6176D71A39C71837C71837C71837C6131FA71420A91837C61837C71837C71B39
                  C86579D91F22A0E2E3F2FFFFFF787AC63C44B4657CDB1E40CB1C3ECB1C3ECB1C
                  3DCA1C3DCA1C3ECB1C3ECB1F40CC677CDB3A41B37577C5FFFFFFFFFFFFFFFFFF
                  7577C73D45B8667FDD2246CF2045CF2045CF2045CF2045CF2347D06981DD3942
                  B6787AC8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7578C9313EB9264DD3244DD424
                  4DD4244DD4244DD4254DD32E3BB7787ACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF7478CB182BB72953D82954D92954D92954D92954D92953D81829B67478
                  CBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7478CD1A2EBB2D5BDC2D5CDD2D5CDD2F
                  5EDD2F5DDD2D5CDD2D5CDD2D5BDC192DB97478CDFFFFFFFFFFFFFFFFFF7478CF
                  1A31BF3162E13163E23163E23365E27495EA7193EA3364E23163E23163E23162
                  E11A2FBE7478CFFFFFFFDCDEF3212CB96F94EC356AE6356AE6376CE67699ED3B
                  4AC54050C87397ED376BE6356AE6356AE67397ED1E29B7E2E3F5FFFFFF7479D2
                  4858CE7299F03B71EA799DF03C4BC8767BD37479D24353CB769AF03B71EA779C
                  F14353CB7479D2FFFFFFFFFFFFFFFFFF7076D34A5BD18DACF33C4CCA767BD5FF
                  FFFFFFFFFF7379D44354CD8DACF34354CD7076D3FFFFFFFFFFFFFFFFFFFFFFFF
                  FCFCFE767CD6212EC0767CD6FFFFFFFFFFFFFFFFFFFFFFFF767CD6212EC0767C
                  D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDEF5FFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFDCDEF5FFFFFFFFFFFFFFFFFFFFFFFF}
                Spacing = 3
                TabOrder = 4
                OnClick = bbSvuotaMClick
              end
            end
            object BitBtnSendProgramMU2: TBitBtn
              Tag = 2
              AlignWithMargins = True
              Left = 221
              Top = 13
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Invia'#13#10'programma'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                F6060000424DF606000000000000360000002800000018000000180000000100
                180000000000C006000000000000000000000000000000000000FFFFFF8B8685
                9995939894929894929894929894929894929894929894929894929894929894
                929894929894929894929894929894929894929894929894929995938B8685FF
                FFFF8F8B89E7E4E3E3E0DFE5E2E1E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2
                E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E5E2
                E1E3E0DFE7E4E38F8B89A19F9BE0DEDCBBB9B693908C96938F96938F96938F96
                938F96938F96938F96938F96938F96938F96938F96938F96938F96938F96938F
                96938F96938F93908CBBB9B6E0DEDCA19F9BAAA7A4EEEEEDDEDCDBD1CFCFD3D1
                D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3
                D1D0D3D1D0D3D1D0D3D1D1D6D2D3D4D1D1DFDCDCEEEEEDAAA7A4B4B0AEF6F5F4
                F1F0EFF2F1F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2
                F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F1FAF5F7D5DED2D5DCD0F2F0F1F8F6F6B4
                B0AEBDB9B6FBFCFCF6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6
                F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F9F7F9DFEADF08972913B0
                3E93BB8DFFFFFFBDB9B6C3BFBFFFFFFFFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFB
                FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFEFCFE
                E7F1E735C46421CD72A0CC9FFFFFFFC3BFBFCCC9C7FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFBEE8CAABDFB7F3F8F3FFFFFFCCC9C7B9B5B2E5E3E4
                E7E7E7E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6
                E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E8E6E7EDE8EBEEE8ECEAE7E9E5E3E5B9
                B5B2FDFDFDD9D7D6DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7
                DBD9D7DBD9D7DBDAD8DFE0E1E0E1E2E1E2E3E1E2E3E1E2E3E1E3E3E0E2E3DEDF
                E0DCDAD8D9D7D6FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB14A00C37123AB4504A94709A74406A74406
                A54304A33B00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB65814F5E4A0E29B16E3
                9E1BE29B19E39A16CA7515AA5014FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBB60
                16F3E0A9E3A225E3A32BE19D25E2A023CE7B1BB0581AFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFC16818F4E2B2E6AD37E6AD3AE5AA36E6AA33D28424B55E1DFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFC8721AF4E7B9E9B743EAB747E9B445E9B441
                D78E2DBD651FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCD791EF5E8C1EEC152ED
                C057ECBE52ECBE50DB9734C16C21FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDB9432DB9432D9902CD68B28D17F
                1EF7EACAEFC762F0C864EEC561EFC760DF9D3AC67120C57122C47123C36F24C3
                6F24FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF9932
                ECCC83F6E3ABEFC85FF5DA8CF3D070F2CF70F1CE6DF0CC6BEDC45FEBBA57ECBB
                52CF7E29C87824FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE19B2DEECD84FAEBC0F5D87DF4D77FF4D67CF3D479F3D276
                F3D176F3D473D99131D08229FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4A133EECF83FAEEC5F6DC83F6
                DB87F5DA84F5D982F6D980DD9A37D68D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8AF
                3FF5E2ACFBEEC4F7DE89F7E08DF8E495E7B248DE9832FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFEAB23EF5E3B0FAEAB5F8E396ECC157E3A337FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBB440F6E5B4F3D57CEAAE3C
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEB
                B746EBB947FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentFont = False
              Spacing = 10
              TabOrder = 6
              OnClick = BitBtnSendProgramClick
            end
            object BitBtnStartMU2: TBitBtn
              Tag = 2
              AlignWithMargins = True
              Left = 221
              Top = 73
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Start'#13#10'programma'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                F6060000424DF606000000000000360000002800000018000000180000000100
                180000000000C006000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFF1764DDB5C2F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF479EE925B2EAE9ECFBFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABB6EF33E7F61779E0FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF2587E521F7FB2465DCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFAEB9EE3DF0F908CCF27691E4FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D88E322EFFA08AEEEAEB7EC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8796E53B
                F2FA05D9F70B8CE7E7EBFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF249EE81CE2F907D7F91469DEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6482E13CEBFA04CCF708CAF73A70DCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F9FE2CB7ED13D9F8
                08C8F709B3F2758DE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF537EE03DF0FC05C4F609C5F80B99EDADB7E9FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFE1E5F82DAEEC11D1F808BFF608C4F90D6FE0E7EAF9FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFAFBDF04295E53E86E44A8BE51581E12EDFFA07BEF609BB
                F60AC0FB1453D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAD3F526C2EF12EAFA02D6F706D0F7
                06C4F608BFF609BAF60AB4F70BB2F8395FD1FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADCBF284
                EEF91CDEF80CCEF704C5F603BCF605B7F607B1F608AEF70897F28398E0FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF73B6EB93F4FC4BE3F954E1F94DDEF936D5F831CFF820C2F721
                C4F92194E9CFDBF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2EBFA81D3F272EAFB4ADEF94FDDF951DB
                F954DAF956DAFA55D9FC55DCFF5AAEEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6C6F098EEFA
                55E1FA4CDDF94FDAF950DBFA59A5E6A4B9E9AFC5EDB2C8EEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF72B3EA91F0FC46DCF94EDAF951D9F951D6FB7FB4EBFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFE2EBFA81D1F274E6FB4AD9F951D8F952D5FA53
                CDF8ADC3ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4C4EF9BECFB5CDD
                FA4FD8F952D4F952D4FA55B6EEEFF4FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF7EB9EB98EFFE4FD8F951D4F952D2F951D4FC5DA1E3FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFEFF3FC80CDF17CE6FD4CD5FA4FD3FA4ED0FA50D0FB
                91B5E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB6D0F27BC4EE7CBFEC7C
                BDEC7CBDEC7EBEED6AA3E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentFont = False
              Spacing = 10
              TabOrder = 7
              OnClick = BitBtnStartClick
            end
            object btnJobCreateMU2: TBitBtn
              Tag = 2
              AlignWithMargins = True
              Left = 221
              Top = 133
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Preleva'#13#10'pallet'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                C2040000424DC204000000000000420000002800000018000000180000000100
                10000300000080040000120B0000120B00000000000000000000007C0000E003
                00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FB556292529252925292529252925292529252925
                2925292529252925F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F734EE71C
                E71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CD65AFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7F734EE71CE71CE71CE71CE71CE71CE71CE71CE71C
                E71CE71CE71CE71CB556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A6B2D
                6B2D8C314A29E71CE71CE71CE71CE71C4A296B2D6B2D6B2D1863FF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FAD35E71CE71CE71CE71C6B2D
                DE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FB556E71CE71CE71CE71CE71C1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD772925E71CE71CE71CE71CEF3DFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                3146E71CE71CE71CE71C08217B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7F5A6BE71CE71CE71CE71CE71C734EFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8C31
                E71CE71CE71CE71C4A29DE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7F9452E71CE71CE71CE71CE71CD65AFF7FFF7FFF7F
                FF7FFF7FFF7FFF7F5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73EF3DD65A
                39679452E71C8C31FF7FFF7FFF7FFF7FFF7FFF7FFF7F1042E71CAD355A6BFF7F
                FF7FFF7FFF7FFF7FFF7FDE7BD65A3146EF3D396739679C73FF7FFF7FFF7FFF7F
                FF7FFF7F9452E71CAD35E71CE71C524ADE7BFF7FFF7FFF7FFF7F186308212925
                4A29E71CF75EFF7F7B6F7B6F7B6FF75E4A294A29E71C8C31DE7B7B6FAD35E71C
                4A29FF7FFF7FFF7FFF7F29256B2DD65AD65ACE3929251863E71CE71CE71CE71C
                E71CE71C08217B6FFF7FFF7FFF7FB5563146FF7FFF7FFF7FFF7FE71C734EAD35
                2925D65AE71C39670821E71CE71CE71CE71CE71C8C31FF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7F8C310821B556D65A6B2D4A2939676B2D4A294A292925
                E71CE71CE71C1863FF7FFF7FBD7710428C31FF7FFF7FFF7FFF7F7B6F4A29E71C
                E71C29251863FF7FFF7FFF7FFF7F7B6FCE39CE39082129257B6F18632925E71C
                EF3DFF7FFF7FFF7FFF7FFF7FBD77945294527B6FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7F1863E71C0821E71C4A29F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FB556E71C734EBD77FF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7F}
              ParentFont = False
              Spacing = 10
              TabOrder = 8
              OnClick = BitBtnJobCreateMClick
            end
          end
          object GroupBoxMU3: TGroupBox
            AlignWithMargins = True
            Left = 708
            Top = 4
            Width = 341
            Height = 665
            Align = alClient
            Caption = 'Fagima Jazz X 3'
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Swis721 LtCn BT'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            DesignSize = (
              341
              665)
            object sPalletSbloccato3: TShape
              Left = 6
              Top = 30
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label11: TLabel
              Left = 47
              Top = 29
              Width = 92
              Height = 19
              Caption = 'Pallet sbloccato'
            end
            object sPalletBloccato3: TShape
              Left = 6
              Top = 53
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label12: TLabel
              Left = 47
              Top = 52
              Width = 86
              Height = 19
              Caption = 'Pallet bloccato'
            end
            object sAutomatico3: TShape
              Tag = 18
              Left = 6
              Top = 99
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object sAllarme3: TShape
              Left = 6
              Top = 145
              Width = 35
              Height = 17
              Brush.Color = clRed
            end
            object Label13: TLabel
              Left = 47
              Top = 97
              Width = 65
              Height = 19
              Caption = 'Automatico'
            end
            object Label20: TLabel
              Left = 47
              Top = 143
              Width = 44
              Height = 19
              Caption = 'Allarme'
            end
            object sPortaAperta3: TShape
              Left = 6
              Top = 76
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label22: TLabel
              Left = 47
              Top = 74
              Width = 67
              Height = 19
              Caption = 'Porta aperta'
            end
            object sFineCiclo3: TShape
              Tag = 18
              Left = 6
              Top = 122
              Width = 35
              Height = 17
              Brush.Color = clLime
            end
            object Label9: TLabel
              Left = 47
              Top = 120
              Width = 60
              Height = 19
              Caption = 'Fine Ciclo'
            end
            object leAut3: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 343
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 108
              EditLabel.Height = 19
              EditLabel.Caption = 'Modalit'#224' macchina'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 0
            end
            object leRun3: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 374
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 88
              EditLabel.Height = 19
              EditLabel.Caption = 'Stato macchina'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 1
            end
            object leMotion3: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 405
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 55
              EditLabel.Height = 19
              EditLabel.Caption = 'Stato assi'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 2
            end
            object leProg3: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 436
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 109
              EditLabel.Height = 19
              EditLabel.Caption = 'Prog. n esecuzione'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 3
            end
            object leMainProg3: TLabeledEdit
              Tag = 1
              Left = 127
              Top = 467
              Width = 190
              Height = 25
              TabStop = False
              Anchors = [akLeft, akBottom]
              Ctl3D = False
              EditLabel.Width = 91
              EditLabel.Height = 19
              EditLabel.Caption = 'Prog. principale'
              LabelPosition = lpLeft
              LabelSpacing = 5
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 4
            end
            object GroupBox4: TGroupBox
              AlignWithMargins = True
              Left = 5
              Top = 500
              Width = 331
              Height = 160
              Align = alBottom
              Caption = 'Pallet in macchina'
              TabOrder = 5
              DesignSize = (
                331
                160)
              object lePalletM3: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 28
                Width = 46
                Height = 25
                TabStop = False
                Color = clGradientInactiveCaption
                Ctl3D = False
                EditLabel.Width = 33
                EditLabel.Height = 19
                EditLabel.Caption = 'Pallet'
                LabelPosition = lpLeft
                LabelSpacing = 5
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 0
              end
              object leCodiceM3: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 59
                Width = 231
                Height = 25
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                Ctl3D = False
                EditLabel.Width = 45
                EditLabel.Height = 19
                EditLabel.Caption = 'Articolo'
                LabelPosition = lpLeft
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 1
              end
              object leDescM3: TLabeledEdit
                Tag = 1
                Left = 78
                Top = 90
                Width = 231
                Height = 25
                TabStop = False
                Anchors = [akLeft, akTop, akRight]
                Ctl3D = False
                EditLabel.Width = 64
                EditLabel.Height = 19
                EditLabel.Caption = 'Programma'
                LabelPosition = lpLeft
                ParentCtl3D = False
                ReadOnly = True
                TabOrder = 2
              end
              object bbModificaM3: TBitBtn
                Tag = 3
                AlignWithMargins = True
                Left = 174
                Top = 121
                Width = 90
                Height = 29
                Caption = 'Modifica'
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
                  FFFFFFFFFFFFFCFDFE265F9C4F80BA6D96C6ACC3DEE8EEF6FFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5383BCC0E1F6A6
                  D4F083B8DF3A79B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF36755E0E593D
                  0E593D0E593D0E593D23647BD5E6F5D7E9FACBE3F99FD9F4468BC3E8EFF60E59
                  3D0E593D36755EFFFFFF146043FFFFFFFFFFFFFDFEFEFBFEFDD4E1ED7FA7D2F9
                  FCFEBCE3F938BDE8519ACC498DC5E8EFF6E1F4EF146043FFFFFF1B684AFCFDFD
                  FCFEFEF9FDFCF6FBFAF1FAF76390C2C8E4F544D0F400C3F225B8E65198CB4D91
                  C6E8EFF61B684AFFFFFF247152FAFCFBF9FDFCF6FBFAF1FAF7EDF8F5E8EFF660
                  8FC4C2EAF828CBF300C3F225B8E65198CB5193C8E8EFF6FFFFFF2C7A5AF8FCFA
                  F6FBFAF1FAF7EDF8F5E8F6F2E3F4EFE8EFF66594C5C2EBF828CBF300C3F225B8
                  E65198CB5696CAFFFFFF368462F7FBFAF1FAF7EDF8F5E8F6F2E3F4EFDDF2ECD8
                  F0E9E8EFF66B97C7C2EBF828CBF300C3F227B8E65299CC6C9DCB3E8D6AF4FAF8
                  EDF8F5E8F6F2E3F4EFDDF2ECD8F0E9D2EEE6CDECE4E8EFF6709BCAC2EBF828CB
                  F300C3F23FBBE65693C7479672F1F9F7E9F6F2E3F4EFDDF2ECD8F0E9D2EEE6CD
                  ECE4C8EAE1C3E8DEE8EFF6759ECCC2EBF842D0F35E9FCEB9CCE34E9E79EEF9F5
                  E4F4EFDDF2ECD8F0E9D2EEE6CDECE4C8EAE1C3E8DEBFE6DBBBE5D9E8EFF679A1
                  CE7CA4CE59989BFFFFFF54A57FEEF8F5EBF7F4E8F6F2E4F5F0E1F4EFDEF2EDDB
                  F1EBD8F0E9D6EFE8D3EEE7D1EEE6C8E6E2C5E3E254A57FFFFFFF56A57F59AA83
                  59AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA8359AA
                  8359AA8354A47EFFFFFF54A27E99C9B29ACAB39ACAB39ACAB39ACAB39ACAB39A
                  CAB39ACAB39ACAB39ACAB39ACAB39ACAB39ACAB351A17CFFFFFF5BA98498C9B1
                  99C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9B299C9
                  B299C9B259A782FFFFFFA2CFBA5EAC875CAB855CAB855CAB855CAB855CAB855C
                  AB855CAB855CAB855CAB855CAB855CAB855CAB85A0CEB8FFFFFF}
                Spacing = 3
                TabOrder = 3
                OnClick = bbModificaMClick
              end
              object bbSvuotaM3: TBitBtn
                Tag = 3
                AlignWithMargins = True
                Left = 78
                Top = 121
                Width = 90
                Height = 29
                Caption = 'Svuota'
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
                  FFFFFFFFFFFFE3E3F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDEEFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7979C01114957979C0FFFFFFFF
                  FFFFFFFFFFFFFFFF7676BE1115967676BEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  7676BF121CA2112CBF121CA27676BFFFFFFFFFFFFF7676BF121DA2112DBF111D
                  A47374BEFFFFFFFFFFFFFFFFFF7879C2131EA61432C21432C31432C2121EA575
                  76C17576C1121EA51432C21432C31432C3121FA67576C1FFFFFFE0E0F12024A1
                  6176D71A39C71837C71837C71837C6131FA71420A91837C61837C71837C71B39
                  C86579D91F22A0E2E3F2FFFFFF787AC63C44B4657CDB1E40CB1C3ECB1C3ECB1C
                  3DCA1C3DCA1C3ECB1C3ECB1F40CC677CDB3A41B37577C5FFFFFFFFFFFFFFFFFF
                  7577C73D45B8667FDD2246CF2045CF2045CF2045CF2045CF2347D06981DD3942
                  B6787AC8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7578C9313EB9264DD3244DD424
                  4DD4244DD4244DD4254DD32E3BB7787ACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF7478CB182BB72953D82954D92954D92954D92954D92953D81829B67478
                  CBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7478CD1A2EBB2D5BDC2D5CDD2D5CDD2F
                  5EDD2F5DDD2D5CDD2D5CDD2D5BDC192DB97478CDFFFFFFFFFFFFFFFFFF7478CF
                  1A31BF3162E13163E23163E23365E27495EA7193EA3364E23163E23163E23162
                  E11A2FBE7478CFFFFFFFDCDEF3212CB96F94EC356AE6356AE6376CE67699ED3B
                  4AC54050C87397ED376BE6356AE6356AE67397ED1E29B7E2E3F5FFFFFF7479D2
                  4858CE7299F03B71EA799DF03C4BC8767BD37479D24353CB769AF03B71EA779C
                  F14353CB7479D2FFFFFFFFFFFFFFFFFF7076D34A5BD18DACF33C4CCA767BD5FF
                  FFFFFFFFFF7379D44354CD8DACF34354CD7076D3FFFFFFFFFFFFFFFFFFFFFFFF
                  FCFCFE767CD6212EC0767CD6FFFFFFFFFFFFFFFFFFFFFFFF767CD6212EC0767C
                  D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDEF5FFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFDCDEF5FFFFFFFFFFFFFFFFFFFFFFFF}
                Spacing = 3
                TabOrder = 4
                OnClick = bbSvuotaMClick
              end
            end
            object BitBtnStartMU3: TBitBtn
              Tag = 3
              AlignWithMargins = True
              Left = 216
              Top = 73
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Start'#13#10'programma'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                F6060000424DF606000000000000360000002800000018000000180000000100
                180000000000C006000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFF1764DDB5C2F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF479EE925B2EAE9ECFBFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABB6EF33E7F61779E0FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF2587E521F7FB2465DCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFAEB9EE3DF0F908CCF27691E4FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1D88E322EFFA08AEEEAEB7EC
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8796E53B
                F2FA05D9F70B8CE7E7EBFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF249EE81CE2F907D7F91469DEFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6482E13CEBFA04CCF708CAF73A70DCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F9FE2CB7ED13D9F8
                08C8F709B3F2758DE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF537EE03DF0FC05C4F609C5F80B99EDADB7E9FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFE1E5F82DAEEC11D1F808BFF608C4F90D6FE0E7EAF9FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFAFBDF04295E53E86E44A8BE51581E12EDFFA07BEF609BB
                F60AC0FB1453D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAD3F526C2EF12EAFA02D6F706D0F7
                06C4F608BFF609BAF60AB4F70BB2F8395FD1FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADCBF284
                EEF91CDEF80CCEF704C5F603BCF605B7F607B1F608AEF70897F28398E0FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF73B6EB93F4FC4BE3F954E1F94DDEF936D5F831CFF820C2F721
                C4F92194E9CFDBF4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2EBFA81D3F272EAFB4ADEF94FDDF951DB
                F954DAF956DAFA55D9FC55DCFF5AAEEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6C6F098EEFA
                55E1FA4CDDF94FDAF950DBFA59A5E6A4B9E9AFC5EDB2C8EEFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF72B3EA91F0FC46DCF94EDAF951D9F951D6FB7FB4EBFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFE2EBFA81D1F274E6FB4AD9F951D8F952D5FA53
                CDF8ADC3ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4C4EF9BECFB5CDD
                FA4FD8F952D4F952D4FA55B6EEEFF4FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF7EB9EB98EFFE4FD8F951D4F952D2F951D4FC5DA1E3FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFEFF3FC80CDF17CE6FD4CD5FA4FD3FA4ED0FA50D0FB
                91B5E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB6D0F27BC4EE7CBFEC7C
                BDEC7CBDEC7EBEED6AA3E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentFont = False
              Spacing = 10
              TabOrder = 6
              OnClick = BitBtnStartClick
            end
            object BitBtnSendProgramMU3: TBitBtn
              Tag = 3
              AlignWithMargins = True
              Left = 216
              Top = 13
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Invia'#13#10'programma'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                F6060000424DF606000000000000360000002800000018000000180000000100
                180000000000C006000000000000000000000000000000000000FFFFFF8B8685
                9995939894929894929894929894929894929894929894929894929894929894
                929894929894929894929894929894929894929894929894929995938B8685FF
                FFFF8F8B89E7E4E3E3E0DFE5E2E1E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2
                E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E6E3E2E5E2
                E1E3E0DFE7E4E38F8B89A19F9BE0DEDCBBB9B693908C96938F96938F96938F96
                938F96938F96938F96938F96938F96938F96938F96938F96938F96938F96938F
                96938F96938F93908CBBB9B6E0DEDCA19F9BAAA7A4EEEEEDDEDCDBD1CFCFD3D1
                D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3D1D0D3
                D1D0D3D1D0D3D1D0D3D1D1D6D2D3D4D1D1DFDCDCEEEEEDAAA7A4B4B0AEF6F5F4
                F1F0EFF2F1F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2
                F0F3F2F0F3F2F0F3F2F0F3F2F0F3F2F1FAF5F7D5DED2D5DCD0F2F0F1F8F6F6B4
                B0AEBDB9B6FBFCFCF6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6
                F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F6F9F7F9DFEADF08972913B0
                3E93BB8DFFFFFFBDB9B6C3BFBFFFFFFFFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFB
                FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFEFCFE
                E7F1E735C46421CD72A0CC9FFFFFFFC3BFBFCCC9C7FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFBEE8CAABDFB7F3F8F3FFFFFFCCC9C7B9B5B2E5E3E4
                E7E7E7E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6
                E6E7E6E6E7E6E6E7E6E6E7E6E6E7E6E6E8E6E7EDE8EBEEE8ECEAE7E9E5E3E5B9
                B5B2FDFDFDD9D7D6DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7DBD9D7
                DBD9D7DBD9D7DBDAD8DFE0E1E0E1E2E1E2E3E1E2E3E1E2E3E1E3E3E0E2E3DEDF
                E0DCDAD8D9D7D6FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB14A00C37123AB4504A94709A74406A74406
                A54304A33B00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB65814F5E4A0E29B16E3
                9E1BE29B19E39A16CA7515AA5014FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBB60
                16F3E0A9E3A225E3A32BE19D25E2A023CE7B1BB0581AFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFC16818F4E2B2E6AD37E6AD3AE5AA36E6AA33D28424B55E1DFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFC8721AF4E7B9E9B743EAB747E9B445E9B441
                D78E2DBD651FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCD791EF5E8C1EEC152ED
                C057ECBE52ECBE50DB9734C16C21FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDB9432DB9432D9902CD68B28D17F
                1EF7EACAEFC762F0C864EEC561EFC760DF9D3AC67120C57122C47123C36F24C3
                6F24FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF9932
                ECCC83F6E3ABEFC85FF5DA8CF3D070F2CF70F1CE6DF0CC6BEDC45FEBBA57ECBB
                52CF7E29C87824FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE19B2DEECD84FAEBC0F5D87DF4D77FF4D67CF3D479F3D276
                F3D176F3D473D99131D08229FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4A133EECF83FAEEC5F6DC83F6
                DB87F5DA84F5D982F6D980DD9A37D68D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8AF
                3FF5E2ACFBEEC4F7DE89F7E08DF8E495E7B248DE9832FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFEAB23EF5E3B0FAEAB5F8E396ECC157E3A337FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBB440F6E5B4F3D57CEAAE3C
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEB
                B746EBB947FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              ParentFont = False
              Spacing = 10
              TabOrder = 7
              OnClick = BitBtnSendProgramClick
            end
            object btnJobCreateMU3: TBitBtn
              Tag = 3
              AlignWithMargins = True
              Left = 216
              Top = 133
              Width = 120
              Height = 54
              Anchors = [akTop, akRight]
              Caption = 'Preleva'#13#10'pallet'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Swis721 Cn BT'
              Font.Style = []
              Glyph.Data = {
                C2040000424DC204000000000000420000002800000018000000180000000100
                10000300000080040000120B0000120B00000000000000000000007C0000E003
                00001F000000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FB556292529252925292529252925292529252925
                2925292529252925F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F734EE71C
                E71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CE71CD65AFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7F734EE71CE71CE71CE71CE71CE71CE71CE71CE71C
                E71CE71CE71CE71CB556FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD65A6B2D
                6B2D8C314A29E71CE71CE71CE71CE71C4A296B2D6B2D6B2D1863FF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FAD35E71CE71CE71CE71C6B2D
                DE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FB556E71CE71CE71CE71CE71C1863FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD772925E71CE71CE71CE71CEF3DFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                3146E71CE71CE71CE71C08217B6FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7F5A6BE71CE71CE71CE71CE71C734EFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F8C31
                E71CE71CE71CE71C4A29DE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7F9452E71CE71CE71CE71CE71CD65AFF7FFF7FFF7F
                FF7FFF7FFF7FFF7F5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F9C73EF3DD65A
                39679452E71C8C31FF7FFF7FFF7FFF7FFF7FFF7FFF7F1042E71CAD355A6BFF7F
                FF7FFF7FFF7FFF7FFF7FDE7BD65A3146EF3D396739679C73FF7FFF7FFF7FFF7F
                FF7FFF7F9452E71CAD35E71CE71C524ADE7BFF7FFF7FFF7FFF7F186308212925
                4A29E71CF75EFF7F7B6F7B6F7B6FF75E4A294A29E71C8C31DE7B7B6FAD35E71C
                4A29FF7FFF7FFF7FFF7F29256B2DD65AD65ACE3929251863E71CE71CE71CE71C
                E71CE71C08217B6FFF7FFF7FFF7FB5563146FF7FFF7FFF7FFF7FE71C734EAD35
                2925D65AE71C39670821E71CE71CE71CE71CE71C8C31FF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7F8C310821B556D65A6B2D4A2939676B2D4A294A292925
                E71CE71CE71C1863FF7FFF7FBD7710428C31FF7FFF7FFF7FFF7F7B6F4A29E71C
                E71C29251863FF7FFF7FFF7FFF7F7B6FCE39CE39082129257B6F18632925E71C
                EF3DFF7FFF7FFF7FFF7FFF7FBD77945294527B6FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7F1863E71C0821E71C4A29F75EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FB556E71C734EBD77FF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
                FF7FFF7FFF7F}
              ParentFont = False
              Spacing = 10
              TabOrder = 8
              OnClick = BitBtnJobCreateMClick
            end
          end
        end
      end
    end
  end
  object TimerUpd: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerUpdTimer
    Left = 144
    Top = 8
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 544
    Top = 8
  end
  object ADOQuery1: TADOQuery
    Connection = DBDataModule.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM PresetMacchine')
    Left = 480
    Top = 8
    object ADOQuery1ID_Macchina: TIntegerField
      FieldName = 'ID_Macchina'
    end
    object ADOQuery1Descrizione_Macchina: TStringField
      FieldName = 'Descrizione_Macchina'
      Size = 50
    end
    object ADOQuery1ID_TipoPallet: TIntegerField
      FieldName = 'ID_TipoPallet'
    end
    object ADOQuery1Codice_Tipopallet: TStringField
      FieldName = 'Codice_Tipopallet'
    end
    object ADOQuery1PresetX: TBCDField
      FieldName = 'PresetX'
      Precision = 18
    end
    object ADOQuery1PresetY: TBCDField
      FieldName = 'PresetY'
      Precision = 18
    end
    object ADOQuery1PresetZ: TBCDField
      FieldName = 'PresetZ'
      Precision = 18
    end
    object ADOQuery1PresetA: TBCDField
      FieldName = 'PresetA'
      Precision = 18
    end
    object ADOQuery1PresetB: TBCDField
      FieldName = 'PresetB'
      Precision = 18
    end
    object ADOQuery1PresetC: TBCDField
      FieldName = 'PresetC'
      Precision = 18
    end
    object ADOQuery1PresetROT: TBCDField
      FieldName = 'PresetROT'
      Precision = 18
    end
    object ADOQuery1ZeroX: TBCDField
      FieldName = 'ZeroX'
      Precision = 18
    end
    object ADOQuery1ZeroY: TBCDField
      FieldName = 'ZeroY'
      Precision = 18
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 
      'All known|*.DMS;*.PRG|DMS files|*.DMS|PRG files|*.PRG|All files|' +
      '*.*'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofNoValidate, ofEnableSizing]
    Left = 840
    Top = 8
  end
end
