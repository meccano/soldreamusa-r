object FrameScaffale: TFrameScaffale
  Left = 0
  Top = 0
  Width = 508
  Height = 466
  DoubleBuffered = True
  ParentDoubleBuffered = False
  TabOrder = 0
  object GLSceneViewer1: TGLSceneViewer
    Left = 0
    Top = 37
    Width = 508
    Height = 429
    Camera = Camera
    BeforeRender = GLSceneViewer1BeforeRender
    Buffer.AmbientColor.Color = {00000000000000000000000000000000}
    Buffer.ContextOptions = [roDoubleBuffer, roOpenGL_ES2_Context]
    Buffer.DepthPrecision = dp24bits
    Buffer.ShadeModel = smFlat
    FieldOfView = 71.129539489746100000
    Align = alClient
    OnDblClick = GLSceneViewer1DblClick
    OnMouseDown = GLSceneViewer1MouseDown
    OnMouseMove = GLSceneViewer1MouseMove
    TabOrder = 0
  end
  object pToolBar: TPanel
    Left = 0
    Top = 0
    Width = 508
    Height = 37
    Align = alTop
    BevelKind = bkSoft
    BevelOuter = bvNone
    BorderWidth = 2
    ParentBackground = False
    TabOrder = 1
    Visible = False
    object SpeedButtonCurs: TSpeedButton
      Left = 2
      Top = 2
      Width = 30
      Height = 29
      Align = alLeft
      GroupIndex = 1
      Down = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333FFF3333333333333707333333333333F777F3333333333370
        9033333333F33F7737F33333373337090733333337F3F7737733333330037090
        73333333377F7737733333333090090733333333373773773333333309999073
        333333337F333773333333330999903333333333733337F33333333099999903
        33333337F3333F7FF33333309999900733333337333FF7773333330999900333
        3333337F3FF7733333333309900333333333337FF77333333333309003333333
        333337F773333333333330033333333333333773333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ExplicitLeft = -10
      ExplicitTop = 0
      ExplicitHeight = 30
    end
    object SpeedButtonNav: TSpeedButton
      Left = 32
      Top = 2
      Width = 30
      Height = 29
      Align = alLeft
      GroupIndex = 1
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000130B0000130B00000000000000000001FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000DDFFFFFFFFFFFFBD6500BD6500FFFF
        FFBD6500BD6500FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        00DDFFFFFFFFFFFFFFFFFFBD6500BD6500BD6500FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000DDFFFFFFFFFFFFFFFFFFFFFFFFBD65
        00BD6500FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        00DDFFFFFFFFFFFFFFFFFFBD6500BD6500BD6500FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBD6500BD6500FFFF
        FFBD6500BD6500FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF473E38473E38473E3847
        3E38473E38B5B1AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        473E38473E38473E38FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF473E38473E
        38473E38FFFFFFFFFFFFFFFFFF473E38473E38FFFFFFFFFFFFFFFFFFFFFFFF00
        00DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF473E38473E38FFFFFF473E38473E38
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000DDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF473E38473E38473E38FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        00DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF473E38473E38473E38
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000DDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF473E38473E38FFFFFF473E38473E38FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF352F2B362F2B473E38473E38FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000DDFFFFFFFFFFFF39322E352F2B352F
        2B423934FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        00DDFFFFFF352F2B352F2B352F2B352F2B352F2BFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000DDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        00DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ExplicitLeft = 13
      ExplicitTop = 0
      ExplicitHeight = 30
    end
    object SpeedButtonPan: TSpeedButton
      Left = 62
      Top = 2
      Width = 30
      Height = 29
      Align = alLeft
      GroupIndex = 1
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000130B0000130B00000000000000000001FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF483F394B423C4B423C483F39483F39483F39FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF483F39958B85A9
        9E98A79B94A59890A0928B837770483F39FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF514842483F39A19B96BBB4AEB7AFAAB5ACA6B2A8A3B3A9A3D6D3D19C94
        90483F39FFFFFFFFFFFFFFFFFF483F3958504A989490BCBAB9C4C0BDC2BEBBC2
        BDB9C2BBB7BFB8B4E2E0DFEAEAEAEAEAEAAFAAA7483F39FFFFFFFFFFFF483F39
        7A7470EAEAEAEAEAEAA8A5A27A7470C5C4C2C4C2C1DCDADAEAEAEAEAEAEAEAEA
        EAEAEAEA5D544FFFFFFFFFFFFFFFFFFF534A455A524C564D484A413BBCBAB9CE
        CDCBDFDEDEEAEAEAEAEAEAEAEAEAEAEAEAE8E8E7544C46FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF483F39B2B0AED7D7D6D8D7D7E9E9E9EAEAEAEAEAEAEAEAEAEAEA
        EAC0BFBE534B45FFFFFFFFFFFFFFFFFF635C57483F39ACA9A6C5C4C2D1D0CFFF
        FFFFCECDCDE0E0E0EAEAEAEAEAEAE0E0E076706CFFFFFFFFFFFFFFFFFF5A524D
        898480BCBBB8CBCAC8E3E3E2FFFFFF92857ED1D0CFFFFFFFCAC9C7FFFFFFA29F
        9C5D5550FFFFFFFFFFFFFFFFFF59514BC6C5C3DDDDDCF2F2F1FFFFFFCECDCCE3
        E3E2FFFFFF92857EFFFFFFA6A19F625A55FFFFFFFFFFFFFFFFFFFFFFFF59514B
        FFFFFFE7E7E6D0CFCDDDDDDCF2F2F1FFFFFFE0DFDEFFFFFF79736E59514CFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF574F4977716C999692E4E2E1C3C1BF9C9894A0
        9C9979736E68605BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF615954615A55605953615A55FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ExplicitLeft = 45
      ExplicitTop = 0
      ExplicitHeight = 30
    end
    object SpeedButtonSave: TSpeedButton
      Left = 92
      Top = 2
      Width = 30
      Height = 29
      Align = alLeft
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF5F2E2E511E1E4C1E1E5429295227275024244E
        24244D21214C20204B1E1E4B1E1E3C1414300F0F442121FFFFFFFFFFFF6D3A39
        90665BA7847FD7C3C4C9B0B0C0A6A6B99C9CB29292AA8B8BA38181A48081643A
        352402002E0A0AFFFFFFFFFFFF7B4B49A47D6BC09F96E7CACBD7B6B6D0ACACC8
        A3A3C09A9AB89191B08787B28889794F45320B02350F0FFFFFFFFFFFFF855754
        B18D7BCAABA1EED8DAE0C4C4D8BBBBD1B1B1C9A7A7C29E9EB99595BB96978058
        4F390F06370F0FFFFFFFFFFFFF8D615EC3A18FD5BDB3F8E3E4E9CFCFE1C5C5DA
        BCBCD2B2B2CAA8A8C39E9EC4A0A18B625943160C390F0FFFFFFFFFFFFF956968
        CEAE9EE0CBC2F7E7E8F1DCDCECD3D3E4C9CADDC0C0D5B6B6CEABABCFAEAF966D
        644C1D133C1010FFFFFFFFFFFF9C7472D5B7A7E3D3C9FEFEFEFEFEFEFEFBFBFB
        EEEEF3E4E4EBDADAE3CECEE2CECE9E796F56271B3C1111FFFFFFFFFFFFA57E7B
        DEC4B7D6BBABD8BDAFD2B6A6CAAA9BBE9B8DB18D7CA47C6E99706292685B8255
        4565372B3F1313FFFFFFFFFFFFAD8887E7D0C4D7BBACD1B1A1CAA998C4A18EBE
        9985B78F7BAB836D9A6E5B885B49825443734439421515FFFFFFFFFFFFB59492
        EBD6CBEBDAD1F5E9E3F2E6E0F0E2DBECDED6E9DBD3E8D8D1E6D6CCDBC9C0AF8F
        81794B3E451717FFFFFFFFFFFFBD9D9CF1E0D5F8F0EBD0B5A7CCAE9CFBF3F0FE
        FEFEFEFEFEFEFEFEFEFEFEFEFEFED8C4B887594C471919FFFFFFFFFFFFC6A7A6
        F9EBE2FEF9F5A1755E854F34E1CEC5FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEE0CC
        C0A478684A1B1BFFFFFFFFFFFFCDB2B1FEF8F2FEFEFEC5A6969B6B54E2D1C8FE
        FEFEFEFEFEFEFEFEFEFEFEFEFEFEEDDED5A87D734B1F20FFFFFFFFFFFFCAB0AF
        CCB3B1C6A8A8C3A5A6BA9B9BAF8C8CA581819E7777966D6D8E6464875B5B7C4B
        4B5F2D2CC5BBBBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = SpeedButtonSaveClick
      ExplicitLeft = 3
      ExplicitTop = 3
      ExplicitHeight = 30
    end
    object SpeedButtonLoad: TSpeedButton
      Left = 122
      Top = 2
      Width = 30
      Height = 29
      Align = alLeft
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000232E0000232E00000000000000000001FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEFF2F82F68BB789ACFD6DEEEFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDAE1EE226CC922
        89F41E6CD1638CCACBD5EAFDFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFEBEFF76E94CD2172D01C7FEC238EFF218EFF268DFA377BD08DA8D2FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5EAF43D79C62189F43DA1FF5CAFFF57
        AAFF3BA2FF75BAF95886C7D2DCEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE
        5283C62690F952ACFF4E95E63C77C592C6F899CBF65B85C4E5EAF4FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFBFCDE61777DE52B3FF4891E187A3D1A7BADB5C
        97D85080C3E7EBF4FFFFFFF3F5F9A0B7DB9CB3D9B1C4E0FFFFFFFFFFFF628DC9
        339CFC65B5F8668FC8FFFFFFD9E2F0688BC6EBEFF7FFFFFFFFFFFFE9EDF51D6E
        C82082EF1C61C7FFFFFFFFFFFF266CC651B8FF599ADCC9D3E9FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFF7F8FB2E7ED12894FF1B66D1FFFFFFFFFFFF1364C7
        6BD0FF699DD5E4E9F4FFFFFFFFFFFFFFFFFFFDFDFEFBFBFDFFFFFFD7DFEF217C
        D8248CFF2C6DCCFFFFFFFFFFFF2F6DBE61A7DE4D85C7CDD8EBFFFFFFFFFFFFDB
        E3F0457ABEB6C7E3FFFFFF628FC82590F72E88FD608ACBFFFFFFFFFFFFD8E0EF
        C4D1E7C7D4E8F3F5F9FFFFFFDCE3F13781C631B1F07396C97197CC1D8AE9419D
        FF3C80E0B7C7E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBE2EF3E84C63A
        C4FA37BEFF1C89DA2499F447A8FF6AA6F75F88C6FCFCFDFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC9D5E93384C940D5FF3ACDFF3BBCFF55C5FF72C5FF77AEED5C86
        C6EAEEF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4C4E13782C63DB4E951
        D3FF71D9FF76B3E75F89C98CA8D3F6F7FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF2F4F99CB3D95089C776B9E44987CBDCE2EFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7
        F8FBABBFDE4979BDD3DCEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = SpeedButtonLoadClick
      ExplicitLeft = 170
      ExplicitTop = 0
    end
  end
  object GLScene1: TGLScene
    Left = 32
    Top = 56
    object Luce: TGLLightSource
      Ambient.Color = {0000803F0000803F0000803F0000803F}
      ConstAttenuation = 1
      Position.Coordinates = {00409CC500C0DA450000FAC50000803F}
      LightStyle = lsOmni
      SpotCutOff = 180
      SpotDirection.Coordinates = {00000000000000000000000000000000}
    end
    object Camera: TGLCamera
      DepthOfView = 300
      FocalLength = 300
      NearPlaneBias = 0.100000001490116000
      TargetObject = Target
      Position.Coordinates = {000000000000803F000020C10000803F}
      Direction.Coordinates = {00000000000000000000803F00000000}
      Up.Coordinates = {000000000000803F0000008000000000}
    end
    object Target: TGLDummyCube
      Position.Coordinates = {000000000000803F000000000000803F}
      Pickable = False
      CubeSize = 1
      EdgeColor.Color = {A9A5253FB1A8283EB1A8283E0000803F}
    end
    object Magazzino: TGLDummyCube
      CubeSize = 1
      EdgeColor.Color = {9A99193F9A99193F9A99193F0000803F}
      object Scaffale: TGLFreeForm
        Material.BlendingParams.UseBlendFunc = False
        Direction.Coordinates = {FDFFFFB240BF3C340000803F00000000}
        Up.Coordinates = {1BB66E280000803F40BF3CB400000000}
        Pickable = False
        UseMeshMaterials = False
      end
      object Selezione: TGLCube
        Material.FrontProperties.Diffuse.Color = {00000000000000000000803F0000803F}
        Material.PolygonMode = pmLines
        Visible = False
        Pickable = False
        CubeSize = {0000803F0000C03F0000803F}
      end
      object SelezioneC: TGLCylinder
        Material.FrontProperties.Diffuse.Color = {00000000000000000000803F0000803F}
        Material.PolygonMode = pmLines
        Visible = False
        Pickable = False
        BottomRadius = 0.500000000000000000
        Height = 1
        Loops = 5
        TopRadius = 0.500000000000000000
      end
    end
  end
end
