//---------------------------------------------------------------------------

#pragma hdrstop

#include "Lavorazioni.h"
#include "ModificaLavorazione.h"
#include "ModificaFase.h"
#include "StringTableID.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TLavorazioniForm *LavorazioniForm;
//---------------------------------------------------------------------------

__fastcall TLavorazioniForm::TLavorazioniForm(TComponent* Owner)
	: TMDIChild(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::BitBtnAddLavorazioneClick(TObject *Sender)
{
	FormModificaLavorazione->ImpostaDati(-1);
	if( FormModificaLavorazione->ShowModal() == IDOK )
        AggiornaGridLavorazioni();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::BitBtnChangeLavorazioneClick(TObject *Sender)
{
	FormModificaLavorazione->ImpostaDati(qry1ID_Lavorazione->Value);
	if( FormModificaLavorazione->ShowModal() == IDOK )
    	AggiornaGridLavorazioni();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::BitBtnCopyLavorazioneClick(TObject *Sender)
{
   	DBDataModule->ClonaLavorazione(qry1ID_Lavorazione->AsInteger);

    //Aggiorno le griglie
    Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::BitBtnDeleteLavorazioneClick(TObject *Sender)
{
 	int i;

	// Sicuro di voler cancellare la lavorazione?
	if ((Application->MessageBox(String::LoadStr(IDS_DELLAVOR).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;

	if (DBGridLavorazioni->SelectedRows->Count > 0) {
		for (i = 0; i < DBGridLavorazioni->SelectedRows->Count; i++) {
			DBGridLavorazioni->DataSource->DataSet->GotoBookmark(DBGridLavorazioni->SelectedRows->Items[i]);

			if (qry1ID_Lavorazione->AsInteger > 0) {
				//Verifica che non sia abbia un lotto associato
            	if (DBDataModule->LavorazioneAssociatoLotto(qry1ID_Lavorazione->Value)) {
                    Application->MessageBox(String::LoadStr(IDS_NO_DELLAVOR).w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
                }
                else {
					DBDataModule->CancellaLavorazione(qry1ID_Lavorazione->Value);
                }
            }
		}
        DBGridLavorazioni->SelectedRows->Clear();
	}
	else {
        if (qry1ID_Lavorazione->AsInteger > 0) {
            //Verifica che non sia abbia un lotto associato
            if (DBDataModule->LavorazioneAssociatoLotto(qry1ID_Lavorazione->Value)) {
				Application->MessageBox(String::LoadStr(IDS_NO_DELLAVOR).w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
            }
            else {
                DBDataModule->CancellaLavorazione(qry1ID_Lavorazione->Value);
            }
        }
	}
	AggiornaGridLavorazioni();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::BitBtnAddFaseClick(TObject *Sender)
{
	FormModificaFase->ImpostaDati(qry1ID_Lavorazione->Value, -1);
	if( FormModificaFase->ShowModal() == IDOK )
    	AggiornaGridFasi();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::BitBtnChangeFaseClick(TObject *Sender)
{
	FormModificaFase->ImpostaDati(qry1ID_Lavorazione->Value, qry2ID_Fase->Value);
	if( FormModificaFase->ShowModal() == IDOK )
	    AggiornaGridFasi();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::BitBtnDeleteFaseClick(TObject *Sender)
{
 	int i;

	// Sicuro di voler cancellare la lavorazione?
	if ((Application->MessageBox(String::LoadStr(IDS_DELLAVOR).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;

	if (DBGridFasi->SelectedRows->Count > 0) {
		for (i = 0; i < DBGridFasi->SelectedRows->Count; i++) {
			DBGridFasi->DataSource->DataSet->GotoBookmark(DBGridFasi->SelectedRows->Items[i]);

			// Verifica che non sia in uso
			if (qry2ID_Fase->AsInteger > 0)
  				DBDataModule->CancellaFase(qry1ID_Lavorazione->Value, qry2ID_Fase->Value);
  		}
		DBGridFasi->SelectedRows->Clear();
	}
	else {
		// Verifica che non sia in uso
		if (qry2ID_Fase->AsInteger > 0)
			DBDataModule->CancellaFase(qry1ID_Lavorazione->Value, qry2ID_Fase->Value);
	}
	AggiornaGridFasi();
}
//---------------------------------------------------------------------------

void TLavorazioniForm::AggiornaGridLavorazioni()
{
	DBGridLavorazioni->RefreshDataSet();
	DBGridLavorazioni->AutoFitAll();
}
//---------------------------------------------------------------------------

void TLavorazioniForm::AggiornaGridFasi()
{
   	DBGridFasi->RefreshDataSet();
	DBGridFasi->AutoFitAll();
	AggiornaBtnUpDown();
}
//---------------------------------------------------------------------------

void TLavorazioniForm::Aggiorna()
{
    AggiornaGridLavorazioni();
	AggiornaGridFasi();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::FormActive(TObject *Sender)
{
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (Shift.Contains(ssCtrl) && (Key == VK_DELETE))
		Key = 0; // ignore
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::ADOQuery1AfterScroll(TDataSet *DataSet)
{
	AnsiString title, strsql;

	if (!qry1ID_Lavorazione->IsNull) {
		ADOQuery2->Close();
        strsql.printf("Select * from FasiLavorazione where ID_Lavorazione = %d ORDER BY Ordine_Seq", qry1ID_Lavorazione->Value);
		ADOQuery2->SQL->Text = strsql;
		ADOQuery2->Open();

		title = "Fasi lavorazione selezionata: codice " + StringFieldADOQuery1Codice->AsString;
    }
    else {
        title = "Nessuna lavorazione selezionata";
    }

    //Aggiorno il titolo
	pFasi->Caption = title.c_str();

    //Refresh del dataset del dbgrid
    AggiornaGridFasi();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::OnDoubleClick(TObject *Sender)
{
	FormModificaLavorazione->ImpostaDati(qry1ID_Lavorazione->Value);
	if( FormModificaLavorazione->ShowModal() == IDOK )
    	AggiornaGridLavorazioni();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::OnDoubleClickFasi(TObject *Sender)
{
	FormModificaFase->ImpostaDati(qry1ID_Lavorazione->Value, qry2ID_Fase->Value);
	if( FormModificaFase->ShowModal() == IDOK )
	    AggiornaGridFasi();
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::OnClickBtnMouveUp(TObject *Sender)
{
	int ID_Fase, ID_FasePrec;

	try {
		// Get the value.
		ID_Fase = qry2ID_Fase->Value;
		ID_FasePrec = TrovaRecordPrec(qry1ID_Lavorazione->Value, qry2Ordine_Seq->Value);

		//Scambio i record
		SwapRowRecord(qry1ID_Lavorazione->Value, ID_Fase, ID_FasePrec);

		// Move to previous record.
		ADOQuery2->Prior();
	}
	catch (...) {
	}
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::OnClickBtnMouveDown(TObject *Sender)
{
	int ID_Fase, ID_FaseNext;

	try {
		ID_Fase = qry2ID_Fase->Value;
		ID_FaseNext = TrovaRecordNext(qry1ID_Lavorazione->Value, qry2Ordine_Seq->Value);

		//Scambio i record
		SwapRowRecord(qry1ID_Lavorazione->Value, ID_Fase, ID_FaseNext);

		// Move to next record.
		ADOQuery2->Next();
	}
	catch (...) {
	}
}
//---------------------------------------------------------------------------

int TLavorazioniForm::TrovaRecordPrec(int ID_Lavorazione, int OrdineSeq) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int ID_Fase = 0;

	strsql.printf("SELECT TOP (1) * from dbo.Fasi WHERE dbo.Fasi.ID_Lavorazione = %d AND Ordine_Seq < %d ORDER BY Ordine_Seq DESC", ID_Lavorazione, OrdineSeq);
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = DBDataModule->ADOConnection1;
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ID_Fase = ReadInt(ADOQuery, "ID_Fase");
		}
		ADOQuery->Close();
	} catch (Exception &E) {
	}
	delete ADOQuery;
	return ID_Fase;
}
//---------------------------------------------------------------------------

int TLavorazioniForm::TrovaRecordNext(int ID_Lavorazione, int OrdineSeq) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int ID_Fase = 0;

	strsql.printf("SELECT TOP (1) * from dbo.Fasi WHERE dbo.Fasi.ID_Lavorazione = %d AND Ordine_Seq > %d ORDER BY Ordine_Seq", ID_Lavorazione, OrdineSeq);
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = DBDataModule->ADOConnection1;
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ID_Fase = ReadInt(ADOQuery, "ID_Fase");
		}
		ADOQuery->Close();
	} catch (Exception &E) {
	}
	delete ADOQuery;
	return ID_Fase;
}
//---------------------------------------------------------------------------

int TLavorazioniForm::SwapRowRecord(int ID_Lavorazione, int ID_Fase1, int ID_Fase2) {
	TFase fase1, fase2, faseTmp;
	int res = 0;

	//Leggo le 2 fasi da invertire
	DBDataModule->LeggiFase(ID_Lavorazione, ID_Fase1, fase1);
	DBDataModule->LeggiFase(ID_Lavorazione, ID_Fase2, fase2);

	faseTmp = fase2;

	//Ordine fase
	fase2.OrdineSeq = fase1.OrdineSeq;
	fase1.OrdineSeq = faseTmp.OrdineSeq;

	DBDataModule->ModificaFase(fase1);
	DBDataModule->ModificaFase(fase2);

	AggiornaGridFasi();

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::OnCellClickGridFasi(TColumn *Column)
{
	AggiornaBtnUpDown();
}
//---------------------------------------------------------------------------

void TLavorazioniForm::AggiornaBtnUpDown()
{
	int row = DBGridFasi->Row;

	BitBtnMouveUp->Enabled = (row == 1) ? false : true;
	BitBtnMouveDown->Enabled = (row == ADOQuery2->RecordCount) ? false : true;
}
//---------------------------------------------------------------------------

void __fastcall TLavorazioniForm::ADOQuery2AfterScroll(TDataSet *DataSet)
{
	AggiornaBtnUpDown();
}
//---------------------------------------------------------------------------

