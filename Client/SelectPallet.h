//---------------------------------------------------------------------------

#ifndef SelectPalletH
#define SelectPalletH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MyDBGrid.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
//---------------------------------------------------------------------------
class TFormSelectPallet : public TForm
{
__published:	// IDE-managed Components
	TGridPanel *GridPanel1;
	TMyDBGrid *MyDBGrid1;
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TADOQuery *ADOQuery1;
	TDataSource *DataSource1;
	TIntegerField *ADOQuery1ID_Pallet;
	TIntegerField *ADOQuery1Stato_Pallet;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery1Descrizione_Articolo;
	TIntegerField *ADOQuery1Stato_Pezzo;
	TIntegerField *ADOQuery1Misura_Eseguita;
	TStringField *ADOQuery1Codice_Tipopallet;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall MyDBGrid1CellClick(TColumn *Column);
	void __fastcall BitBtnOKClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormSelectPallet(TComponent* Owner);
	int Selected;
	AnsiString MultiSelected;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSelectPallet *FormSelectPallet;
//---------------------------------------------------------------------------
#endif
