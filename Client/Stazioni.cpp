//---------------------------------------------------------------------------

#pragma hdrstop

#include "Stazioni.h"
#include "socket.h"
#include "StringTableID.h"
#include "Main.h"
#include "DB.h"
#include "InsertPallet.h"
#include "ModificaPallet.h"
#include "Clientdata.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma resource "*.dfm"
TStazioniForm *StazioniForm;
//---------------------------------------------------------------------------
__fastcall TStazioniForm::TStazioniForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	UINT i;

	DBDataModule->CaricaTabellaK("TipiPezzi", "ID_TipoPezzo", TipiPezzi);
	DBDataModule->CaricaTabella("TipiPallet", RecList);

	Aggiorna();
}
//---------------------------------------------------------------------------

void TStazioniForm::ImpostaComboBox(int tp)
{
	UINT i;

	for (i = 0; i < RecList.size(); i++) {
		if (RecList[i]["ID_TipoPallet"].ToIntDef(-1) == tp ) {
			return;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::BitBtnPickClick(TObject *Sender)
{
	TJob j;
    TPezzo pz;
	TPallet p;

	if ((Application->MessageBox(String::LoadStr(IDS_INSPALLET).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;

	// Genero Missione da stazione operatore
	memset(&j, 0, sizeof(j));
	DBDataModule->LeggiPalletInMacchina(TabControl1->TabIndex + 1, "S", p);
    DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
	j.ID_Pallet = p.ID_Pallet;
	j.Tipo_Missione = "PICK";
	j.Tipo_Macchina = "S";
	j.ID_Macchina = TabControl1->TabIndex + 1;
    j.ID_Pezzo = pz.ID_Pezzo;
    j.ID_Articolo = pz.ID_Articolo;
    j.Programma_Macchina = p.Programma;
    j.Descrizione_Op = "Prelievo Pallet";
    j.Priorita = 50;
    j.AzionePallet = 1;
    //Controllo che non sia gi� stata generato un job o una missione identica
    if (!DBDataModule->EsisteGiaJobPallet(j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, p.ID_Pallet) &&
    	!DBDataModule->EsisteGiaMissionePallet(j.Tipo_Missione, j.Tipo_Macchina, p.ID_Pallet)) {
        //Creo il JOB
        j.ID_Job = DBDataModule->CreaJob(j);
        //Attivo il JOB
        DBDataModule->AttivaJob(j.ID_Job);
        // LOG
        DBDataModule->Log("CLIENT", "BAIA", "PC %d: pallet = %d, articolo %s, prelievo richiesto dalla baia %d", MainForm->stationnumber, j.ID_Pallet, LE_Codice_Articolo->Text, j.ID_Macchina);
    }
}
//---------------------------------------------------------------------------

void TStazioniForm::ColoraShape(int stato, TShape *t, TColor ColorON, TColor ColorOFF)
{
	if (stato) {
		t->Brush->Color = ColorON;
	} else {
		t->Brush->Color = ColorOFF;
	}
}
//---------------------------------------------------------------------------

static AnsiString Descrizione_TP(int n) {
	AnsiString res;

	res = IntToStr(n);
	switch(n) {
	case TPALLET_500:
		res += " - 500x500";
		break;
	default:
		res = "";
	}
	return res;
}
//---------------------------------------------------------------------------

void TStazioniForm::Aggiorna() {
	TPallet p, ptag;
	TPezzo pz;
	TArticolo a;
	TMissioneDB m;
	AnsiString tipomacchina;
	int nmag, npos, tipomag, nmacc, nbaia, byte;
	bool OKInser = false;

//	if (MainForm->stationnumber > 0) {
//		LE_TAG->Text = ClientData.RFID[MainForm->stationnumber - 1].IDValue;
//		LE_TAGOLD->Text = ClientData.RFID[MainForm->stationnumber - 1].IDLast;
//	} else {
//		LE_TAG->Text = "---";
//		LE_TAGOLD->Text = "---";
//	}
	nbaia = TabControl1->TabIndex + 1;
    byte = (nbaia == 1) ? 4 : 10;
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte] & BIT0, sOKPrelievo);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte] & BIT1, sOKDeposito);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte] & BIT2, sAllarme, clRed);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte] & BIT3, sConfermaInserimento);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte + 1] & BIT0, sPresenzaPallet);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte] & BIT6, sPalletInFase);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte] & BIT7, sBaiaPrenotata);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte + 1] & BIT1, sTapparellaAperta);
	ColoraShape(ClientData.PLC.DB[DB_STAZIONI][byte + 1] & BIT2, sTapparellaChiusa);
    if (nbaia == 1) {
		ColoraShape(ClientData.PLC.DB[DB_ALLARMI][5] & BIT4, sEmergenzaBaia, clRed);
    }
    else {
		ColoraShape(ClientData.PLC.DB[DB_ALLARMI][5] & BIT5, sEmergenzaBaia, clRed);
    }
//	ColoraShape(!(ClientData.DatiRBT[0].R[102]), sBarrieraOK, clLime, clRed);
	ColoraShape(ClientData.DatiRBT[0].ErrAltezzaBaia[nbaia - 1], sControlloAltezza, clRed);

    //Controllo se la baia � settata in manuale
    m_iBaiaManuale = (nbaia == 1) ? (ClientData.PLC.DB[DB_STAZIONI][12] & BIT4) : (ClientData.PLC.DB[DB_STAZIONI][26] & BIT4);

    //Imposto lo stato del pulsante
    pnlButtonManualStation->BevelOuter = (m_iBaiaManuale) ? bvLowered : bvRaised;
    pnlButtonManualStation->Color = (m_iBaiaManuale) ? clLime : clBtnFace;

 	DBDataModule->LeggiPalletInMacchina(TabControl1->TabIndex + 1, "S", p);
	BitBtnEditPallet->Enabled = (p.ID_Pallet > 0);
	BitBtnCancella->Enabled = (p.ID_Pallet > 0);
	BitBtnSvuota->Enabled = (p.ID_Pezzo > 0) && (p.ID_Pezzo < 10000);
	LE_ID_Pallet->Text = p.ID_Pallet;
	LE_TipoPallet->Text = Descrizione_TP(p.ID_TipoPallet);
	if (p.ID_Pezzo) {
		DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
		LE_ID_Pezzo->Text = pz.ID_Pezzo;
		LE_ID_Articolo->Text = pz.ID_Articolo;
		DBDataModule->LeggiArticolo(pz.ID_Articolo, a);
		LE_Codice_Articolo->Text = a.Codice_Articolo;
		LE_Descrizione_Articolo->Text = a.Descrizione_Articolo;
		LE_Tipo_Pezzo->Text = a.ID_TipoPezzo;
		LE_Descrizione_Tipo_Pezzo->Text = TipiPezzi[LE_Tipo_Pezzo->Text]["Descrizione_Pezzo"];
	}
	else {
		LE_ID_Pezzo->Text= "";
		LE_ID_Articolo->Text = "";
		LE_Codice_Articolo->Text = "";
		LE_Descrizione_Articolo->Text = "";
		LE_Tipo_Pezzo->Text = "";
		LE_Descrizione_Tipo_Pezzo->Text = "";
	}
	if (MainForm->stationnumber > 0) {
//		DBDataModule->LeggiPallet(ClientData.RFID[MainForm->stationnumber - 1].IDLast, ptag);
		if (ptag.ID_Pallet > 0) {
			ImpostaComboBox(ptag.ID_TipoPallet);
			if ((DBDataModule->CercaPalletMagazzino(ptag.ID_Pallet, nmag, npos) == 0) ||
				(DBDataModule->CercaPalletMacchine(ptag.ID_Pallet, tipomacchina, nmacc) == 0)){
//				STMessage->Font->Color = clRed;
//				STMessage->Caption = String::LoadStr(IDS_PALLETSTORED).w_str();
				OKInser = false;
			} else {
//				STMessage->Font->Color = clGreen;
//				STMessage->Caption = String::LoadStr(IDS_PALLETOK).w_str();
				OKInser = true;
			}
		} else {
//			STMessage->Font->Color = clRed;
//			STMessage->Caption = String::LoadStr(IDS_NOPALLET).w_str();
			OKInser = false;
		}
	}
//	BitBtnInsert->Enabled = ((p.ID_Pallet == 0) && ((OKInser && (MainForm->stationnumber > 0)) || RB_IDOFF->Checked));
	if ((p.ID_Pallet != 0) && (ClientData.DatiRBT[0].R[107])) {
		if (DBDataModule->LeggiMissionePallet(p.ID_Pallet, "PICK", "S", m) == 0) {
			BitBtnPick->Enabled = false;
		} else {
			BitBtnPick->Enabled = true;
		}
	} else {
		BitBtnPick->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::TimerUPDTimer(TObject *Sender)
{
	TimerUPD->Enabled = false;
	Aggiorna();
	TimerUPD->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::BitBtnCreateClick(TObject *Sender)
{
	TPallet p;
	int idpallet = 0;
	int  res;

//	if (RB_IDOFF->Checked) {
		FormInsertPallet->UnFilterPalletType();
		res = FormInsertPallet->ShowModal();
		if (res == IDOK) {
			idpallet = FormInsertPallet->Selected;
		}
//	} else {
//		idpallet = ClientData.RFID[MainForm->stationnumber - 1].IDLast;
//	}
	DBDataModule->LeggiPallet(idpallet, p);
	if (p.ID_Pallet > 0) {
		p.Extra_Altezza = 0;
		DBDataModule->AggiornaPallet(p);
		DBDataModule->PalletInMacchina(TabControl1->TabIndex + 1, "S", p.ID_Pallet);
		// LOG
		logstr.sprintf("PC %d: inserito pallet %d alla baia %d", MainForm->stationnumber, p.ID_Pallet, TabControl1->TabIndex + 1);
		DBDataModule->Log("CLIENT", "STATION", logstr);
	}
//	RB_IDON->Checked = true;
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::BitBtnSvuotaClick(TObject *Sender)
{
	TPallet p;

	if ((Application->MessageBox(String::LoadStr(IDS_EMPTYPAL).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	// LOG
	logstr.sprintf("PC %d: svuotato pallet %s articolo %s alla baia %d", MainForm->stationnumber, LE_ID_Pallet->Text, LE_Codice_Articolo->Text, TabControl1->TabIndex + 1);
	DBDataModule->Log("CLIENT", "STATION", logstr);
	DBDataModule->LeggiPalletInMacchina(TabControl1->TabIndex + 1, "S", p);
	DBDataModule->SvuotaPallet(p);
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::BitBtnCancellaClick(TObject *Sender)
{
	if ((Application->MessageBox(String::LoadStr(IDS_EXTPALLET).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	// LOG
	logstr.sprintf("PC %d: estratto pallet %s articolo %s alla baia %d", MainForm->stationnumber, LE_ID_Pallet->Text, LE_Codice_Articolo->Text, TabControl1->TabIndex + 1);
	DBDataModule->Log("CLIENT", "STATION", logstr);
	DBDataModule->PalletInMacchina(TabControl1->TabIndex + 1, "S", 0);
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::BitBtnEditPalletClick(TObject *Sender)
{
	int  res;
	TPallet p;

	DBDataModule->LeggiPalletInMacchina(TabControl1->TabIndex + 1, "S", p);
	if (p.ID_Pallet > 0) {
		FormModificaPallet->m_PalletList.clear();
		FormModificaPallet->ImpostaDati(p.ID_Pallet);
		FormModificaPallet->m_PalletList.push_back(p.ID_Pallet);
		res = FormModificaPallet->ShowModal();
    }
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::bitBtnResetBaiaMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TPanel *p;

	p = (TPanel*)Sender;

	if (TabControl1->TabIndex == 0) {
		if (p) {
			SocketDataModule->ResetBitPLC(DB_STAZIONI, 4, 5);
			p->BevelOuter = bvLowered;
			p->Color = clBtnFace;
		}
	}
	else {
		if (p) {
			SocketDataModule->ResetBitPLC(DB_STAZIONI, 10, 5);
			p->BevelOuter = bvLowered;
			p->Color = clBtnFace;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::bitBtnResetBaiaMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	TPanel *p;

	p = (TPanel*)Sender;

	if (TabControl1->TabIndex == 0) {
		if (p) {
			SocketDataModule->SetBitPLC(DB_STAZIONI, 4, 5);
			p->BevelOuter = bvRaised;
			p->Color = clLime;
		}
	}
	else {
		if (p) {
			SocketDataModule->SetBitPLC(DB_STAZIONI, 10, 5);
			p->BevelOuter = bvRaised;
			p->Color = clLime;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TStazioniForm::pnlButtonManualStationClick(TObject *Sender)
{
	if ((TPanel*)Sender) {
    	if (m_iBaiaManuale) {
            SocketDataModule->ResetBitPLC(DB_STAZIONI, (TabControl1->TabIndex == 0) ? 4 : 10, 4);
        }
        else {
            SocketDataModule->SetBitPLC(DB_STAZIONI, (TabControl1->TabIndex == 0) ? 4 : 10, 4);
        }	
    }
}
//---------------------------------------------------------------------------


