//---------------------------------------------------------------------------

#include <vcl.h>
#include <map>
#pragma hdrstop

#include "Robot.h"
#include "ClientData.h"
#include "Socket.h"
#include "DB.h"
#include "Main.h"
#include "InsNum.h"
#include "InsertPallet.h"
#include "StringTableID.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma resource "*.dfm"
TRobotForm *RobotForm;
TPS PosMacchine;
//---------------------------------------------------------------------------
__fastcall TRobotForm::TRobotForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	changing_override = false;
	DBDataModule->CaricaDescrizionePosM(PosMacchine);
	TimerUpdTimer(NULL);
}
//---------------------------------------------------------------------------
void TRobotForm::ColoraShape(int stato, TShape *t, TColor ColorON, TColor ColorOFF)
{
	if (stato) {
		t->Brush->Color = ColorON;
	} else {
		t->Brush->Color = ColorOFF;
	}
}
//---------------------------------------------------------------------------

AnsiString DescrizioneAllarme(int n) {
	switch(n) {
	case 0 : return "OK";
	case 9 : return "Numero missione errato";
	case 10 : return "Posizione magazzino non correttta";
	case 11 : return "Numero magazzino non corretto";
	case 12 : return "Numero baia non corretto";
	case 13 : return "Numero pinza non corretto";
	case 14 : return "Strobe non corretto";
	case 15 : return "Macchina utensile non corretta";
	case 16 : return "Pallet a bordo!";
	case 17 : return "Posizione macchina utensile errato";
	case 18 : return "Pinza sbloccata";
	case 19 : return "Pezzo sbloccato";
	case 20 : return "Pallet mancante su baia";
	case 21 : return "Pallet presente su baia";
	case 24 : return "Pallet presente su AWEA";
	case 25 : return "Sensore BERO su pinza rotto";
	case 26 : return "Pinza non corretta";
	case 27 : return "Consenso ad entrare in MU perso";
	}
	return "?";
}
//---------------------------------------------------------------------------

static AnsiString Descrizione_TPMISS(int n) {
	AnsiString res;

	res = IntToStr(n);
	switch(n) {
	case TPMISS_MU:
		res += " - Macchina";
		break;
	case TPMISS_BAIA:
		res += " - Baia";
		break;
	case TPMISS_MAG:
		res += " - Magazzino";
		break;
		break;
	}
	return res;
}
//---------------------------------------------------------------------------

static AnsiString Descrizione_GRIP(int n) {
	AnsiString res;

	res = IntToStr(n);
	switch(n) {
	case GRIP_NONE:
		res += " - Nessuna pinza";
		break;
	case GRIP_PALLET:
		res += " - Pinza Pallet";
		break;
	case GRIP_TOOL:
		res += " - Pinza Attrezzaggio";
		break;
	default:
		res += " - ???";
	}
	return res;
}
//---------------------------------------------------------------------------

AnsiString Descrizione_Strobe(int n) {
	AnsiString res;

	res = IntToStr(n);
	switch(n) {
	case 0:
		res += " - Nessuna missione";
		break;
	case 1:
		res += " - Missione inviata";
		break;
	case 2:
		res += " - In esecuzione";
		break;
	case 3:
		res += " - Prelievo eseguito";
		break;
	case 4:
		res += " - Fine missione";
		break;
	case 20:
		res += " - Errore dati";
		break;
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::TimerUpdTimer(TObject *Sender)
{
	AnsiString tm = "";
	TPallet p;

	TimerUpd->Enabled = false;
	BitBtnAbort->Enabled = (MainForm->pwdlevel > 0);
	bbModificaR->Enabled = (MainForm->pwdlevel > 0);
	bbSvuotaR->Enabled = (MainForm->pwdlevel > 0);
	BitBtnCALLMASTER->Enabled = (MainForm->pwdlevel > 0);
	LabeledEdit1->Text = Descrizione_TPMISS(ClientData.Missione.Tipo_prelievo);
	LabeledEdit2->Text = ClientData.Missione.N_locazione_magazzino_pre;
	LabeledEdit3->Text = ClientData.Missione.N_magazzino_pre;
	LabeledEdit4->Text = ClientData.Missione.N_staz_operatore_pre;
	LabeledEdit5->Text = ClientData.Missione.N_MU_pre;
	LabeledEdit6->Text = Descrizione_TPMISS(ClientData.Missione.Tipo_deposito);
	LabeledEdit7->Text = ClientData.Missione.N_locazione_magazzino_dep;
	LabeledEdit8->Text = ClientData.Missione.N_magazzino_dep;
	LabeledEdit9->Text = ClientData.Missione.N_staz_operatore_dep;
	LabeledEdit10->Text = ClientData.Missione.N_MU_dep;
	LabeledEdit13->Text = Descrizione_Strobe(ClientData.Missione.Strobe);
	LabeledEdit14->Text = ClientData.Missione.N_pallet;
	LabeledEdit15->Text = ClientData.DatiRBT[0].R[9];
	if (!changing_override && !TimerOVR2->Enabled) {
		PanelOverride->Caption = IntToStr(ClientData.DatiRBT[0].Override) + " %";
		TrackBar1->Position = ClientData.DatiRBT[0].Override;
	}
	ColoraShape(ClientData.DatiRBT[0].Stato.Errore, Shape5, clRed);
	ColoraShape(ClientData.DatiRBT[0].Stato.Automatico, Shape6, clBlue);
	ColoraShape(ClientData.DatiRBT[0].Stato.Running, Shape7);
	ColoraShape(ClientData.DatiRBT[0].Stato.HOME, Shape12);
	ColoraShape(ClientData.DatiRBT[0].Stato.Presenza_pinza, Shape13);
	ColoraShape(ClientData.DatiRBT[0].Stato.Pinza_aperta, Shape14);
	ColoraShape(ClientData.DatiRBT[0].Stato.Pinza_chiusa, Shape15);
	ColoraShape(ClientData.DatiRBT[0].Stato.Presenza_pallet, Shape16);
	ColoraShape(ClientData.DatiRBT[0].ErrAltezzaBaia[0], Shape19, clRed, clLime);
	ColoraShape(ClientData.DatiRBT[0].ErrAltezzaBaia[1], Shape1, clRed, clLime);
	LabeledEdit11->Text = Descrizione_GRIP(ClientData.DatiRBT[0].Pinza);
	DBDataModule->LeggiPalletInMacchina(7, "R", p);
	LabeledEdit12->Text = p.ID_Pallet;
	TimerUpd->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::FormActivate(TObject *Sender)
{
	TimerUpd->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TRobotForm::TimerOVRTimer(TObject *Sender)
{
	AnsiString s;

	TimerOVR->Enabled = false;
	s.printf("MSGTYPE=OVERRIDE|VAL=%d|", TrackBar1->Position);
	SocketDataModule->Invia(s);
	TimerOVR2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::TrackBar1Change(TObject *Sender)
{
	changing_override = (TrackBar1->Position != ClientData.DatiRBT[0].Override);
	TimerOVR->Enabled = changing_override;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::TimerOVR2Timer(TObject *Sender)
{
	TimerOVR2->Enabled = false;
	changing_override = false;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnSTARTMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	AnsiString s;

	s.printf("MSGTYPE=START|VAL=%d|", 0xFFFF);
	SocketDataModule->Invia(s);
	TimerStart->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnHOLDMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	AnsiString s;

	s.printf("MSGTYPE=HOLD|VAL=%d|", 0xFFFF);
	SocketDataModule->Invia(s);
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnRESETMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	AnsiString s;

	s.printf("MSGTYPE=RESET|VAL=%d|", 0xFFFF);
	SocketDataModule->Invia(s);
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::TimerCallMasterTimer(TObject *Sender)
{
	AnsiString s;

	s.printf("MSGTYPE=CALLMASTER|VAL=%d|", 0);
	SocketDataModule->Invia(s);
	TimerCallMaster->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnCALLMASTERMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	AnsiString s;

    if (!ClientData.DatiRBT[0].Stato.Running) {
        s.printf("MSGTYPE=CALLMASTER|VAL=%d|", 0xFFFF);
        SocketDataModule->Invia(s);
    }
//	TimerCallMaster->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::TimerStartTimer(TObject *Sender)
{
	AnsiString s;

	s.printf("MSGTYPE=START|VAL=%d|", 0);
	SocketDataModule->Invia(s);
	TimerStart->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnAbortMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	DBDataModule->Log("CLIENT", "Robot", "MISSION ABORT");
	SocketDataModule->Invia("MSGTYPE=ABORTMISS|");
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnHOLDMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	AnsiString s;

	s.printf("MSGTYPE=HOLD|VAL=%d|", 0);
	SocketDataModule->Invia(s);
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::bbSvuotaRClick(TObject *Sender)
{
	if (MainForm->pwdlevel > 0) {
		DBDataModule->PalletInMacchina(7, "R", 0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::bbModificaRClick(TObject *Sender)
{
	int res, idpallet;
	TPallet p;

	if (MainForm->pwdlevel > 0) {
		FormInsertPallet->UnFilterPalletType();
		res = FormInsertPallet->ShowModal();
		if (res == IDOK) {
			idpallet = FormInsertPallet->Selected;
			DBDataModule->LeggiPallet(idpallet, p);
			if (p.ID_Pallet > 0) {
				DBDataModule->PalletInMacchina(7, "R", idpallet);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::sb5Click(TObject *Sender)
{
	TSpeedButton *sb;
	AnsiString s;

	sb = (TSpeedButton*)Sender;
	TrackBar1->Position = sb->Tag;
	changing_override = (TrackBar1->Position != ClientData.DatiRBT[0].Override);
	TimerOVR->Enabled = changing_override;
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnRESETMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	AnsiString s;

	s.printf("MSGTYPE=RESET|VAL=%d|", 0);
	SocketDataModule->Invia(s);
}
//---------------------------------------------------------------------------

void __fastcall TRobotForm::BitBtnCALLMASTERMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
	AnsiString s;

    if (!ClientData.DatiRBT[0].Stato.Running) {
        s.printf("MSGTYPE=CALLMASTER|VAL=%d|", 0);
        SocketDataModule->Invia(s);
    }
}
//---------------------------------------------------------------------------


