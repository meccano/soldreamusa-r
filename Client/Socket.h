//---------------------------------------------------------------------------

#ifndef SocketH
#define SocketH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ScktComp.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TSocketDataModule : public TDataModule
{
__published:	// IDE-managed Components
	TClientSocket *ClientSocket;
	TTimer *TimerConnected;
	TTimer *TimerTimeout;
	void __fastcall DataModuleDestroy(TObject *Sender);
	void __fastcall TimerConnectedTimer(TObject *Sender);
	void __fastcall ClientSocketError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall TimerTimeoutTimer(TObject *Sender);
	void __fastcall ClientSocketRead(TObject *Sender,
          TCustomWinSocket *Socket);
	void __fastcall ClientSocketDisconnect(TObject *Sender, TCustomWinSocket *Socket);
private:	// User declarations
public:		// User declarations
	__fastcall TSocketDataModule(TComponent* Owner);
	void Connetti();
	void Disconnetti();
	bool SeiConnesso();
	void Invia(AnsiString telegramma);
	void SetBitPLC(int db, int dw, int bit);
    void ResetBitPLC(int db, int dw, int bit);
    void WriteWordPlc(int db, int dw, int val);
    void WriteRealPlc(int db, int dw, float val);
    void WriteDWordPlc(int db, int dw, long val);
	bool connected;
	int readbytes;
};
//---------------------------------------------------------------------------
extern PACKAGE TSocketDataModule *SocketDataModule;
//---------------------------------------------------------------------------
#endif
