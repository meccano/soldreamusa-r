//---------------------------------------------------------------------------

#pragma hdrstop

#include "ModificaLavorazione.h"
#include "StringTableID.h"
#include "InsAlpha.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormModificaLavorazione *FormModificaLavorazione;
//---------------------------------------------------------------------------
__fastcall TFormModificaLavorazione::TFormModificaLavorazione(TComponent* Owner)
	: TForm(Owner)
{
	ID_Lavorazione = 0;
}
//---------------------------------------------------------------------------

void TFormModificaLavorazione::ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue)
{
	UINT i;

	for (i = 0; i < rl.size(); i++) {
		if (rl[i][KeyField].ToIntDef(-1) == KeyValue ) {
			cb->ItemIndex = i;
			return;
		}
	}
}
//---------------------------------------------------------------------------

void TFormModificaLavorazione::ImpostaDati(int ID)
{
	ID_Lavorazione = ID;
	LEID_Lavorazione->Enabled = false;
	CaricaComboBoxArticoli();

	if (ID >= 0) {
		DBDataModule->LeggiLavorazione(ID, strLavor);
		LEID_Lavorazione->Text = ID;
		LE_Descrizione->Text = strLavor.Descrizione;
		LE_CodiceLavorazione->Text = strLavor.Codice;
		ImpostaComboBox(cbbArticoli, TabArticoli, "ID_Articolo", strLavor.ID_Articolo);
	}
	else {
		cbbArticoli->ItemIndex = -1;
		LE_Descrizione->Text = "";
        LE_CodiceLavorazione->Text = "";
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLavorazione::BitBtnOKClick(TObject *Sender)
{
	//Non sono permesse lavorazioni con articolo non definito
    if( cbbArticoli->ItemIndex == -1 )
        return;

	strLavor.ID_Articolo = TabArticoli[cbbArticoli->ItemIndex]["ID_Articolo"].ToIntDef(0);
    strLavor.Codice = LE_CodiceLavorazione->Text;
    strLavor.Descrizione = LE_Descrizione->Text;

	if (ID_Lavorazione >= 0) {
		// Modifica Lavorazione
		strLavor.ID_Lavorazione = ID_Lavorazione;
		DBDataModule->ModificaLavorazione(strLavor);
	}
	else {
		// Crea nuova Lavorazione
	   strLavor.ID_Lavorazione = DBDataModule->CreaLavorazione(strLavor);
    }
}
//---------------------------------------------------------------------------

void TFormModificaLavorazione::CaricaComboBoxArticoli()
{
	AnsiString val;
	UINT i;

	DBDataModule->CaricaTabella("Articoli ORDER BY Codice_Articolo", TabArticoli);
	cbbArticoli->Clear();

	for (i = 0; i < TabArticoli.size(); i++) {
		val = TabArticoli[i]["Codice_Articolo"] + " - " + TabArticoli[i]["Descrizione_Articolo"];
		cbbArticoli->Items->Add(val);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLavorazione::cbbArticoliChange(TObject *Sender)
{
	strLavor.ID_Articolo = TabArticoli[cbbArticoli->ItemIndex]["ID_Articolo"].ToIntDef(0);
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLavorazione::LE_CodiceLavorazioneClick(TObject *Sender)        
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}	
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLavorazione::LE_DescrizioneClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}	
}
//---------------------------------------------------------------------------

