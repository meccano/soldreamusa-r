//---------------------------------------------------------------------------

#pragma hdrstop

#include <IniFiles.hpp>
#include "ModificaFase.h"
#include "StringTableID.h"
#include "InsAlpha.h"
#include "Main.h"
#include "NewJob.h"
#include "InsAlpha.h"
#include "Define.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormModificaFase *FormModificaFase;
//---------------------------------------------------------------------------
__fastcall TFormModificaFase::TFormModificaFase(TComponent* Owner)
	: TForm(Owner)
{
	int i;
	TIniFile *pIni;

	pIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
    for (i = 0; i < N_MACCHINE_UT; i++) {
		m_PPFolderMU[i] = pIni->ReadString("MU" + IntToStr(i + 1), "PPFolder", "C:\\meccano\\Programmi\\");
    }
	delete pIni;
	ID_Fase = 0;
	cbb1->Clear();
	ComboTipiPallet->Clear();
	ComboNewTipiPallet->Clear();
	ComboCodOperazione->Clear();
}
//---------------------------------------------------------------------------

void TFormModificaFase::ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue)
{
	UINT i;

	for (i = 0; i < rl.size(); i++) {
		if (rl[i][KeyField].ToIntDef(-1) == KeyValue ) {
			cb->ItemIndex = i;
			return;
		}
	}
}
//---------------------------------------------------------------------------

void TFormModificaFase::ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue, AnsiString KeyField2, AnsiString KeyValue2)
{
	UINT i;

	for (i = 0; i < rl.size(); i++) {
		if( rl[i][KeyField].ToIntDef(-1) == KeyValue && rl[i][KeyField2].c_str() == KeyValue2) {
			cb->ItemIndex = i;
			return;
		}
	}
}
//---------------------------------------------------------------------------

void TFormModificaFase::Aggiorna()
{
	AnsiString Tipo_Macchina, CodiceOp;
	int ID_TipoPallet, ID_Operazione;

	Tipo_Macchina = RecList[cbb1->ItemIndex]["Tipo_Macchina"];
	ID_TipoPallet = RecListTipiPallet[ComboTipiPallet->ItemIndex]["ID_TipoPallet"].ToIntDef(0);
	ID_Operazione = RecListOperazioni[ComboCodOperazione->ItemIndex]["ID_Operazione"].ToIntDef(0);
	CodiceOp = RecListOperazioni[ComboCodOperazione->ItemIndex]["Codice_Op"];

	//Controllo che siano compilati certi campi
	if ((Tipo_Macchina == "") || (ID_TipoPallet == 0) || (ID_Operazione == 0)) {
		//Se non � stata scelta la macchina o alcuna operazione non proseguo
		BitBtnOK->Enabled = false;
	}
	else if (LE_PRG->Text == "" && RecList[cbb1->ItemIndex]["Necessita_PRG"].ToIntDef(0)) {
		//Se la macchina utensile scelta ha bisogno del PartProgram ma non � selezionato allora non confermo
		BitBtnOK->Enabled = false;
	}
	else {
		BitBtnOK->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void TFormModificaFase::ImpostaDati(int IndexLavorazione, int IndexFase)
{
	ID_Fase = IndexFase;

	if (IndexFase >= 0)
		DBDataModule->LeggiFase(IndexLavorazione, IndexFase, strFase);

	//Gestione combo delle operazioni operatore
	ComboCodOperazione->Clear();
	CaricaComboBoxOperazioni();
	if (IndexFase >= 0)
		ImpostaComboBox(ComboCodOperazione, RecListOperazioni, "ID_Operazione", strFase.ID_Operazione);
	else
		ComboCodOperazione->ItemIndex = -1;

	//Gestione combo TipiPallet
	CaricaComboBoxTipiPallet();
	if (IndexFase >= 0)
		ImpostaComboBox(ComboTipiPallet, RecListTipiPallet, "ID_TipoPallet", strFase.ID_TipoPallet);
	else
		ComboTipiPallet->ItemIndex = -1;

	//Gestione combo per il cambio plattorello
	ComboNewTipiPallet->Clear();
	CaricaComboBoxNewTipiPallet();
	if (IndexFase >= 0)
		ImpostaComboBox(ComboNewTipiPallet, RecListTipiPallet, "ID_TipoPallet", strFase.ID_TipoPalletXCambio);
	else
		ComboNewTipiPallet->ItemIndex = -1;

	if (IndexFase >= 0) {
		//Gestione combo macchine
		CaricaComboBoxMacchine();
		if (strFase.Tipo_Macchina == "M") {
        	cbb1->ItemIndex = 2;
		}
        else {
			ImpostaComboBox(cbb1, RecList, "ID_Macchina", strFase.ID_Macchina, "Tipo_Macchina", strFase.Tipo_Macchina);
		}
		LE_CodiceFase->Text = strFase.Codice;
		LE_PRG->Text = strFase.Programma_MU;
		LE_Descrizione->Text = strFase.Descrizione;
		LabelMessaggioOp->Text = strFase.MessaggioOp;
		m_NumSequenza = strFase.OrdineSeq;
		ChkBoxAbilitaFase->Checked = strFase.Abilitazione;
	}
	else {
		cbb1->Clear();
		cbb1->ItemIndex = -1;
		strFase.ID_Lavorazione = IndexLavorazione;
		LE_Descrizione->Text = "";
		LE_PRG->Text = "";
		LE_CodiceFase->Text = "";
		LabelMessaggioOp->Text = "";
		m_NumSequenza = DBDataModule->CountFasiRecord(IndexLavorazione) + 1;
		ChkBoxAbilitaFase->Checked = false;
	}

	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::BitBtnOKClick(TObject *Sender)
{
	AnsiString CodiceOp;

	strFase.Codice = LE_CodiceFase->Text;
	strFase.Descrizione = LE_Descrizione->Text;
	strFase.ID_Macchina = RecList[cbb1->ItemIndex]["ID_Macchina"].ToIntDef(0);
	strFase.Tipo_Macchina = RecList[cbb1->ItemIndex]["Tipo_Macchina"].c_str();
	CodiceOp = RecListOperazioni[ComboCodOperazione->ItemIndex]["Codice_Op"];
	strFase.Programma_MU = LE_PRG->Text;
	strFase.ID_TipoPallet = RecListTipiPallet[ComboTipiPallet->ItemIndex]["ID_TipoPallet"].ToIntDef(0);
	strFase.ID_TipoPalletXCambio = RecListTipiPallet[ComboNewTipiPallet->ItemIndex]["ID_TipoPallet"].ToIntDef(0);
	strFase.MessaggioOp = LabelMessaggioOp->Text;
	strFase.OrdineSeq = m_NumSequenza;
	strFase.Abilitazione = ChkBoxAbilitaFase->Checked;
	strFase.ID_Operazione = RecListOperazioni[ComboCodOperazione->ItemIndex]["ID_Operazione"].ToIntDef(0);

	if (ID_Fase >= 0) {
		// Modifica Lavorazione
		strFase.ID_Fase = ID_Fase;
		DBDataModule->ModificaFase(strFase);
	}
    else {
		// Crea nuova Lavorazione
	   strFase.ID_Fase = DBDataModule->CreaFase(strFase);
	}
}
//---------------------------------------------------------------------------

void TFormModificaFase::CaricaComboBoxMacchine()
{
	AnsiString tab;
	AnsiString CodiceOp;
	UINT i;

	//Svuoto il combobox
	cbb1->Clear();
	//Se � gi� stata scelta una tipologia di operazione, filtro sul tipo scelto
	if (ComboCodOperazione->ItemIndex >= 0) {
		CodiceOp = RecListOperazioni[ComboCodOperazione->ItemIndex]["Codice_Op"];

		if (CodiceOp == "FRS") {
			tab.sprintf("Macchine WHERE Tipo_Macchina = 'M' ORDER BY Tipo_Macchina, ID_Macchina");
		}
		else {
			tab.sprintf("Macchine WHERE Tipo_Macchina = 'S' ORDER BY Tipo_Macchina, ID_Macchina");
		}
	}
	else {
		tab.sprintf("Macchine ORDER BY Tipo_Macchina, ID_Macchina");
	}

	DBDataModule->CaricaTabella(tab, RecList);
	for (i = 0; i < RecList.size(); i++) {
		cbb1->Items->Add(RecList[i]["Descrizione_Macchina"]);
    }

	if (CodiceOp == "TRN") {
		cbb1->Items->Add("Selezione Tornio AUTOMATICA");
	}

    //Se ho una sola macchina scelgo la prima
    if (RecList.size() == 1) {
        cbb1->ItemIndex = 0;

        strFase.Tipo_Macchina = RecList[0]["Tipo_Macchina"];
        strFase.ID_Macchina = RecList[0]["ID_Macchina"].ToIntDef(0);
    }
}
//---------------------------------------------------------------------------

void TFormModificaFase::CaricaComboBoxTipiPallet()
{
	UINT i;

    //Pulisco il combo box
	ComboTipiPallet->Clear();

	DBDataModule->CaricaTabella("TipiPallet", RecListTipiPallet);
	ComboTipiPallet->Clear();
	for (i = 0; i < RecListTipiPallet.size(); i++) {
		ComboTipiPallet->Items->Add(RecListTipiPallet[i]["Codice_Tipopallet"] + " - " +RecListTipiPallet[i]["Descrizione_TipoPallet"]);
    }

    //Se ho un solo tipo pallet scelgo il primo
    if (RecListTipiPallet.size() == 1) {
        ComboTipiPallet->ItemIndex = 0;
    }
}
//---------------------------------------------------------------------------

void TFormModificaFase::CaricaComboBoxNewTipiPallet()
{
	UINT i;

	DBDataModule->CaricaTabella("TipiPallet", RecListTipiPallet);
	ComboNewTipiPallet->Clear();
	for (i = 0; i < RecListTipiPallet.size(); i++) {
		ComboNewTipiPallet->Items->Add(RecListTipiPallet[i]["Codice_Tipopallet"] + " - " +RecListTipiPallet[i]["Descrizione_TipoPallet"]);
    }

    //Se ho un solo tipo pallet scelgo il primo
    if (RecListTipiPallet.size() == 1) {
        ComboNewTipiPallet->ItemIndex = 0;
    }
}
//---------------------------------------------------------------------------

void TFormModificaFase::CaricaComboBoxOperazioni()
{
	AnsiString tab;
	UINT i;

	tab.sprintf("OperazioniFasi WHERE Abilitazione_Op = 1 ORDER BY ID_Operazione");

	DBDataModule->CaricaTabella(tab, RecListOperazioni);
	ComboCodOperazione->Clear();
	for (i = 0; i < RecListOperazioni.size(); i++) {
		ComboCodOperazione->Items->Add(RecListOperazioni[i]["Descrizione_Op"]);
    }

    //Se ho una sola descrizione scelgo la prima
    if (RecListOperazioni.size() == 1) {
        ComboCodOperazione->ItemIndex = 0;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::OnChangeComboBoxTipiPallet(TObject *Sender)
{
	strFase.ID_TipoPallet = RecListTipiPallet[ComboTipiPallet->ItemIndex]["ID_TipoPallet"].ToIntDef(0);
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::OnChangeComboBoxMU(TObject *Sender)
{
	strFase.Tipo_Macchina = RecList[cbb1->ItemIndex]["Tipo_Macchina"];
	strFase.ID_Macchina = RecList[cbb1->ItemIndex]["ID_Macchina"].ToIntDef(0);
	LE_PRG->Text = "";
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::OnChangeComboBoxOperazioni(TObject *Sender)
{
	strFase.ID_Operazione = RecListOperazioni[ComboCodOperazione->ItemIndex]["ID_Operazione"].ToIntDef(0);

	//Aggiorno le stringhe nella combo box delle macchine
	CaricaComboBoxMacchine();

    //Aggiorno il combo dei tipi pallet
    CaricaComboBoxTipiPallet();

	//Aggiorno i dati della finestra
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::OnPartProgramClick(TObject *Sender)
{
	int ID_Macchina;

	if(strFase.Tipo_Macchina == "M") {
		ID_Macchina = strFase.ID_Macchina - 1;
		if (ID_Macchina > 999) {
        	ID_Macchina = 0;
		}
		dlgOpen1->InitialDir = m_PPFolderMU[ID_Macchina];
		if (dlgOpen1->Execute() == IDOK) {
            if (ExtractFileName(dlgOpen1->FileName).Length() > 32) {
            	Application->MessageBox(String::LoadStr(IDS_ERR_FILENAMETOOLEN).w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);   
                LE_PRG->Text = "";
                Aggiorna(); 
            }
            else {
                LE_PRG->Text = dlgOpen1->FileName;
                Aggiorna();
            }
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::OnChangeComboBoxNewTipiPallet(TObject *Sender)
{
	strFase.ID_TipoPalletXCambio = RecListTipiPallet[ComboNewTipiPallet->ItemIndex]["ID_TipoPallet"].ToIntDef(0);
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::LE_CodiceFaseClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}	
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::LE_DescrizioneClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}	
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaFase::LabelMessaggioOpClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}	
}
//---------------------------------------------------------------------------

