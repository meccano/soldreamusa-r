//---------------------------------------------------------------------------

#pragma hdrstop

#include "InsertPallet.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TFormInsertPallet *FormInsertPallet;
//---------------------------------------------------------------------------
__fastcall TFormInsertPallet::TFormInsertPallet(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormInsertPallet::FormClose(TObject *Sender, TCloseAction &Action)
{
	ADOQuery1->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormInsertPallet::FormShow(TObject *Sender)
{
	Selected = false;
	BitBtnOK->Enabled = false;
	ADOQuery1->Open();
	MyDBGrid1->AutoFitAll();
}
//---------------------------------------------------------------------------
void __fastcall TFormInsertPallet::MyDBGrid1CellClick(TColumn *Column)
{
	Selected = ADOQuery1ID_Pallet->Value;
	if (Selected)
		BitBtnOK->Enabled = true;
}
//---------------------------------------------------------------------------

void TFormInsertPallet::FilterPalletType(int tp)
{
	AnsiString strsql;

	strsql.printf("SELECT * FROM PALLETINSERIBILI WHERE ID_TipoPallet = %d", tp);
	ADOQuery1->SQL->Text = strsql;
	ADOQuery1->Filter = "";
	ADOQuery1->Filtered = false;
//	ADOQuery1->Filter = "ID_TipoPallet = " + IntToStr(tp);
//	ADOQuery1->Filtered = true;
}
//---------------------------------------------------------------------------

void TFormInsertPallet::UnFilterPalletType()
{
	AnsiString strsql;

	strsql.printf("SELECT * FROM PALLETINSERIBILI");
	ADOQuery1->SQL->Text = strsql;
	ADOQuery1->Filter = "";
	ADOQuery1->Filtered = false;
}
//---------------------------------------------------------------------------

void TFormInsertPallet::FilterTypePresent()
{
	ADOQuery1->Filter = "ID_TipoPallet <> 0 ";
	ADOQuery1->Filtered = true;
}
//---------------------------------------------------------------------------


