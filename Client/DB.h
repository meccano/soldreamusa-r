//---------------------------------------------------------------------------

#ifndef DBH
#define DBH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <map>
//---------------------------------------------------------------------------

typedef struct {
	int ID_Pallet;
	int ID_TipoPallet;
	AnsiString Alias;
	AnsiString Descrizione_Pallet;
	int ID_Pezzo;
	int Stato_Pallet;
	int Extra_Altezza;
	AnsiString Programma;
} TPallet;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Pezzo;
	int ID_Articolo;
	int Stato_Pezzo;
	int Misura_Eseguita;
	double OffsetX;
	double OffsetY;
	double OffsetZ;
	double OffsetA;
	double OffsetB;
	double OffsetC;
	double OffsetROT;
	double Gap;
	double GapMin;
	double GapMax;
	double H;
	AnsiString Descrizione;
} TPezzo;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Missione;
	int ID_Pallet;
	AnsiString Tipo_Missione;
	AnsiString Tipo_Macchina;
	int ID_Macchina;
	AnsiString Programma_Macchina;
	int ID_Articolo;
	int Priorita;
	int ID_Job;
	AnsiString Commento;
	AnsiString GruppoFase;
	AnsiString GruppoPallet;
	int ID_Pezzo;
	int AzionePallet;
	AnsiString GruppoLavorazione;
	AnsiString Descrizione_Op;
    int ID_Lotto;
    int ID_Lavoro;
    int IndiceLavoro;
} TMissioneDB;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Job;
	int ID_Pallet;
	AnsiString Tipo_Missione;
	AnsiString Tipo_Macchina;
	int ID_Macchina;
	AnsiString Programma_Macchina;
	int ID_Articolo;
	int Priorita;
	AnsiString Active;
	AnsiString Commento;
	AnsiString GruppoFase;
	AnsiString GruppoPallet;
	int ID_Pezzo;
	int AzionePallet;
	AnsiString GruppoLavorazione;
    AnsiString Descrizione_Op;
    int ID_Lotto;
    int ID_Lavoro;
    int IndiceLavoro;
} TJob;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Articolo;
	int ID_TipoPezzo;
	AnsiString Codice_Articolo;
	AnsiString Descrizione_Articolo;
	float DiamentroPezzo_Grezzo;
	float DiamentroPezzo_Semilavorato;
	float AltezzaPezzo;
	int ftc;
	int Faccia_Pinza;
	int TStazionamento;
} TArticolo;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lavorazione;
	AnsiString Codice;
	AnsiString Descrizione;
	int ID_Articolo;
} TLavorazione;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lavorazione;
	int ID_Fase;
	AnsiString Codice;
	AnsiString Descrizione;
	int ID_Operazione;
	int ID_Macchina;
	AnsiString Tipo_Macchina;
	AnsiString Programma_MU;
	int ID_TipoPallet;
	AnsiString MessaggioOp;
	int OrdineSeq;
	int Abilitazione;
	int ID_TipoPalletXCambio;
} TFase;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lavoro;
    int Indice;
	int ID_Lavorazione;
	int ID_Fase;
    int LavoroAttivo;
	int ID_Pallet;
    int FaseCompletata;
	int ID_Job;
	int ID_Missione;
	int Priorita;
} TLavoriInCorso;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lotto;
	AnsiString Codice;
	AnsiString Descrizione;
	int ID_Lavorazione;
    int ID_Pallet;
    int ID_Lavoro;
	int Qta;
	int QtaProdotta;
	int Sospeso;
	int Priority;
} TLotto;
//---------------------------------------------------------------------------

typedef std::map<AnsiString, std::map<int, std::map<int, std::map<int, AnsiString> > > > TPS;
//---------------------------------------------------------------------------

typedef std::map<int, std::map<AnsiString, AnsiString> > TRecordList;
//---------------------------------------------------------------------------

typedef std::map<AnsiString, std::map<AnsiString, AnsiString> > TIndexList;
//---------------------------------------------------------------------------

class TDBDataModule : public TDataModule
{
__published:	// IDE-managed Components
	TTimer *TimerDBCheck;
	TADOConnection *ADOConnection1;
	void __fastcall TimerDBCheckTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TDBDataModule(TComponent* Owner);
	void LeggiSegnalazioneAttiva(AnsiString &msg, int &all);
	void AcquisisciSegnalazioneAttiva();
	void AcquisisciSegnalazioni();
	void Segnalazione(AnsiString msg, int all);
	int GetSelfGenID(AnsiString TableName);
	void LeggiPallet(int ID_Pallet, TPallet &p);
	void CancellaPallet(int ID_Pallet);
	int NewPalletID();
	int CreaPallet(TPallet p);
	int CreaNuovoPallet(int TipoPallet, TPallet &newpallet);
	int PalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet);
	int LeggiPalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, TPallet &p);
	int AggiornaPallet(TPallet p);
	void CaricaDescrizionePosM(TPS &PosMacchine);
	int AggiornaCampoTabella(AnsiString Tabella, AnsiString CampoChiave, int ValoreChiave, AnsiString CampoModifica, AnsiString ValoreModifica, bool isstring = false);
	int AggiornaCampoTabella(AnsiString Tabella, AnsiString CampoChiave, AnsiString ValoreChiave, AnsiString CampoModifica, AnsiString ValoreModifica, bool isstring = false);
	bool CaricaTabella(AnsiString TableName, TRecordList &RecList);
	void CaricaTabellaDaQuery(AnsiString TableName, TRecordList &RecList);
	void CaricaTabellaK(AnsiString TableName, AnsiString KeyField, TIndexList &RecList);
	void CaricaTabellaKDaQuery(AnsiString TableName, AnsiString KeyField, TIndexList &RecList);
	int LeggiMissione(int ID_Missione, TMissioneDB &m);
	int CreaMissione(TMissioneDB m);
	int LeggiJob(int ID_Job, TJob &j);
    int EsisteGiaJob(AnsiString tpmiss, AnsiString tpmacc, int nmacc);
    int EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int ID_Pallet);
	int EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int nmacc, int ID_Pallet);
    int EsisteGiaMissionePallet(AnsiString tpmiss, AnsiString tpmacc, int pallet);
	int CreaJob(TJob j);
	int CreaLavoro(TLavoriInCorso lav);
    int CreaLavoriInCorso(int ID_Pallet, int ID_Lavorazione, int LavoroAttivo, int priorita = 50);
    int NewIDLavoro();
	void CancellaPezzo(int ID_Pezzo);
	int CreaPezzo(TPezzo pz);
	int AggiornaPezzo(TPezzo pz);
	void SvuotaPallet(TPallet &p);
	void LeggiPezzo(int ID_Pezzo, TPezzo &p);
	int CercaPalletMagazzino(int ID_Pallet, int &nmag, int &npos);
	int CercaTipoPalletMagazzino(int ID_TipoPallet, int &ID_Pallet);
	int CercaPalletMacchine(int ID_Pallet, AnsiString &tipomacchina, int &nmacchina);
	int LeggiArticolo(int ID_Articolo, TArticolo &a);
	void LeggiArticoloDaCodice(AnsiString Codice_Articolo, TArticolo &a);
	void CancellaMissione(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int ID_Macchina);
	void CancellaMissione(int ID_Missione);
	void CancellaMissioneFromPallet(int ID_Pallet);
	void CancellaJob(int ID_Job);
	void AttivaJob(int ID_Job);
	void AttivaJobFromPallet(int ID_Pallet);
	void DisattivaJob(int ID_Job);
	int CambiaStatoPostazione(int Magazzino, int Postazione, int stato);
	int PalletInMagazzino(int Magazzino, int Postazione, int ID_Pallet);
	void CancellaArticolo(int ID_Articolo);
	int ArticoloInUso(int ID_Articolo);
	int CreaArticolo(TArticolo a);
	int ModificaArticolo(TArticolo a);
	int LeggiMissionePallet(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, TMissioneDB &m);
	int SetStatoProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Programma_Attivo);
	int AggiornaLavaggioPezzo(int ID_Pezzo, int lavaggio);
	int AdattatoreInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet);
	int InserisciTempoUtensile(int P, int U, int t);
	void AggiornaPresetMacchina(int idmacchina, int tipopallet, AnsiString FieldName, double Value);
	int CambiaPrioritaJob(int id, int pri);
	void Log(AnsiString Sorgente, AnsiString Tipo, char *s, ...);
	void Log(AnsiString Sorgente, AnsiString Tipo, AnsiString Evento);
	int UtensileInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Id_Utensile);
	void LeggiPalletDaPezzo(int ID_Pezzo, TPallet &p);
	int LeggiIDPezzoInMacchina(int ID_Macchina, AnsiString Tipo_Macchina);
	int CleanupPezzi();
	void CancellaJobPallet(int ID_Pallet);
	void LogProd(TJob j);
	void LogAttivazioneProd(int ID_Job);
	void LogDisattivazioneProd(int ID_Job);
	void AttivaAllJob();
	void DisattivaAllJob();
	void CancellaTuttiJob();
	void SbloccaMacchina(int pos, AnsiString TypeM);
	void BloccaMacchina(int pos, AnsiString TypeM, int Valore);
	int ImpostaPezzoNonMisurato(int ID_Pezzo);
	int SetProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, AnsiString Programma_Macchina, int Programma_Attivo);
	int CancellaElettrodi(AnsiString NomePezzo);
	void CancellaMisurazioni(int ID_Pezzo);
	void CancellaMisurazioneSingola(int ID_Pezzo, AnsiString Name);
	int LeggiCampoTabella(AnsiString Tabella, AnsiString CampoDaLeggere, AnsiString ClauslaWhere, AnsiString &Valore);
	int LeggiAbilitazioneVerificaUtensili(int MU);
	int LeggiAbilitazioneControlloBalluff();
	int CreaLavorazione(TLavorazione strLavor);
	int ModificaLavorazione(TLavorazione strLavor);
	void ClonaLavorazione(int ID_Lavorazione);
	int LeggiLavorazione(int ID_Lavorazione, TLavorazione &strLavor);
    void CancellaLavorazione(int ID_Lavorazione);
	int LavorazioneInUso(int ID_Lavorazione);
	int CreaFase(TFase strFase);
	int ModificaFase(TFase strFase);
	void LeggiFase(int ID_Lavorazione, int ID_Fase, TFase &strFase);
	void CancellaFase(int ID_Lavorazione, int ID_Fase);
	int CreaLotto(TLotto &l);
	int ModificaLotto(TLotto &l);
	void LeggiLotto(int ID_Lotto, TLotto &l);
	void CancellaLotto(int ID_Lotto, int ID_Lavoro);
	int VerificaSeLottoAttivo(int ID_Lotto);
	int AttivaLotto(int ID_Lotto);
	int DisattivaLotto(int ID_Lotto);
	void CancellaJobsPallet(int ID_Pallet);
	int LavorazioneAssociatoLotto(int ID_Lavorazione);
	int PalletAssociatoLotto(int ID_Pallet);
	int CountFasiRecord(int ID_Lavorazione);
	void PermanenzaMag(int mag, int Valore);
	bool GetGruppoFaseID(int Lav, int Fase, AnsiString &ID);
	bool GetGruppoLavorazioneID(int Lav, AnsiString &ID);
	bool PezzoRibaltabile(int ID_Lav);
	bool GetGruppoPalletID(int Lav, int Fase, AnsiString &ID);
	void AggiornaExtraAltezzaPallet();
    void AttivaLavoroInCorso(int ID_Lavoro);
    void DisattivaLavoroInCorso(int ID_Lavoro);
	void LogMiss(AnsiString str, int ID_Pallet, AnsiString tipomiss, AnsiString tipomacc, int nmacc, int ID_Job, int ID_Missione);
	void LeggiProssimoLavoroInCorso(int ID_Lavoro, TLavoriInCorso &strLavoro);
	int RiattivaLavoriInCorso(int ID_Lavoro);
	int ResettaErrorePezzo(int ID_Pezzo);
	int AggiornaDescPallet(int id, AnsiString desc);
	void AggiornaPrioritaLavoroInCorso(int ID_Lavoro, int priorita);
	int PalletAssociatoLavorazione(int ID_Pallet);
	int AbortJobInLavoriInCorso(int ID_Job);
	int AbortMissioneInLavoriInCorso(int ID_Missione);
	void RiabilitaTutteFasiLavorazione(int ID_Lavorazione);
	void RiabilitaTutteFasiLotto(int ID_Lotto);
	int RipristinaLavoriInCorsoDelLotto(int ID_Lotto);
	int CambiaIndiceLavoro(TLavoriInCorso &lav);
	void DisattivaLavoriInCorsoNonCompletati(int ID_Lavoro);
	void CancellaLavorazioneAttiva(int ID_Lavoro);
	int PalletAssociatoLottoAttivo(int ID_Pallet);
	int LogAttivazioneLotto(int ID_Lotto);
	int LogDisattivazioneLotto(int ID_Lotto);
	int LeggiAbilitazioneStandby(int MU);
};
//---------------------------------------------------------------------------
extern PACKAGE TDBDataModule *DBDataModule;
extern AnsiString logstr;
//---------------------------------------------------------------------------
Variant ReadField(TADOQuery *q, AnsiString f);
int ReadInt(TADOQuery *q, AnsiString f);
AnsiString ReadString(TADOQuery *q, AnsiString f);

#endif
