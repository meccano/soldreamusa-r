//---------------------------------------------------------------------------
#pragma hdrstop

#include "Main.h"
#include "password.h"
#include "chiusura.h"
#include "keyboardlock.h"
#include "MSG.h"
#include "StoricoSegnalazioni.h"
#include "ClientData.h"
#include "Socket.h"
#include "Robot.h"
#include "Magazzini.h"
#include "Macchine.h"
#include "Articoli.h"
#include "Stazioni.h"
#include "Pallet.h"
#include "reinit.hpp"
#include "StringTableID.h"
#include "Jobs.h"
#include <inifiles.hpp>
#include <stdio.h>
#include "Lavorazioni.h"
#include "Lotti.h"
#include "Password.h"
//---------------------------------------------------------------------------
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent *Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
template <class T> void __fastcall TMainForm::CreateMDIChild(T *f, String Name, String Caption)
{
	bool exists = false;
	int i;

	SetVisible();
	SendMessage(ClientHandle, WM_SETREDRAW, FALSE, 0);
	for(i = MDIChildCount - 1; i >= 0; i--) {
		if (MDIChildren[i]->Name != Name) {
			((TMDIChild*)MDIChildren[i])->SetInvisible();
			MDIChildren[i]->Close();
		}
	}
	for(i = 0; i < MDIChildCount; i++) {
		if (MDIChildren[i]->Name == Name) {
			MDIChildren[i]->Caption = Caption;
			((T*)MDIChildren[i])->PanelTitle->Caption = Caption;
			ShowWindow(MDIChildren[i]->Handle, SW_MAXIMIZE);
			ShowWindow(MDIChildren[i]->Handle, SW_MAXIMIZE);
			((TMDIChild*)MDIChildren[i])->Mostra();
			exists = true;
			break;
		}
	}
	if (!exists) {
		Application->CreateForm(__classid(T), &f);
		f->Name = Name;
		f->Caption = Caption;
		f->PanelTitle->Caption = Caption;
		ShowWindow(f->Handle, SW_MAXIMIZE);
		f->Mostra();
	}
	SendMessage(ClientHandle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(ClientHandle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnEsciClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	int res;

	res = ChiusuraForm->ShowModal();
	CanClose = ((res == IDYES) || (res == IDOK));
	if (CanClose) {
		ShowCursor(true);
	}
	if (res == IDYES) {
		HANDLE hToken;
		TOKEN_PRIVILEGES tkp;

		// Get a token for this process.
		if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) {
			// Get the LUID for the shutdown privilege.
			LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);

			tkp.PrivilegeCount = 1;  // one privilege to set
			tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

			// Get the shutdown privilege for this process.
			AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);

			ExitWindowsEx(EWX_POWEROFF | EWX_FORCE, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	int lang;
	TIniFile *Ini;
	TCursor Save_Cursor = Screen->Cursor;

	Screen->Cursor = crHourGlass;    // Show hourglass cursor
	ShowWindow(Handle, SW_HIDE);
	if (DebugHook) {
		pwdlevel = 2;
	} else {
		pwdlevel = 0;
	}
	PWD = "";
	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	TouchPanel = Ini->ReadInteger("Impostazioni", "TouchPanel", 0);
	lang = Ini->ReadInteger("Language", "ID", 1033);
	stationnumber = Ini->ReadInteger("Impostazioni", "Station", 0);
	onlyelectrode = Ini->ReadInteger("Impostazioni", "onlyelectrode", 0);
	EnableManuals = Ini->ReadInteger("Elettrodi", "EnableManuals", 0);
	delete Ini;
	//if (DebugHook) {
		CambiaLingua(lang, false);
	//}
	LoadPassword();
	if (!TouchPanel) {
		BorderStyle = bsSizeable;
	} else {
		BorderStyle = bsNone;
		if (DebugHook) {
			LockKeyboard();
			ShowCursor(false);
		}
	}
	UpdControls();
	Visible = false;
	Screen->Cursor = Save_Cursor;
	TimerOraTimer(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AfterConstruction(void)
{
	FFormState = FFormState >> fsVisible;
	TForm::AfterConstruction();
}
//---------------------------------------------------------------------------

void  TMainForm::SetVisible()
{
	FFormState << fsVisible;
}
//---------------------------------------------------------------------------

void TMainForm::SavePassword() {
	FILE *f;
	char s[100] = "";
	int i;

	f = fopen("pwd.txt", "w");
	if (f) {
		for (i = 0; i < PWD.Length(); i++) {
			s[i] = PWD.c_str()[i] + 1 - (i % 3);
		}
		fprintf(f, "%s\n", s);
		fclose(f);
	}
}
//---------------------------------------------------------------------------

void TMainForm::LoadPassword() {
	FILE *f;
	char s[100] = "";
	UINT i;

	f = fopen("pwd.txt", "r");
	if (f) {
		fscanf(f, "%s", s);
		PWD = "";
		for (i = 0; i < strlen(s); i++) {
			PWD += (char)(s[i] - 1 + (i % 3));
		}
		fclose(f);
	}
}
//---------------------------------------------------------------------------

void TMainForm::UpdControls() {
	MainStatusBar->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TimerPwdTimer(TObject *Sender)
{
	TimerPwd->Enabled = false;
	pwdlevel = 0;
	UpdControls();
	if ((!FindWindowA("TAppBuilder", NULL)) && TouchPanel) {
		LockKeyboard();
		ShowCursor(false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TimerOraTimer(TObject *Sender)
{
	TimerOra->Enabled = false;
	timestr = FormatDateTime("dddd d mmmm yyyy h':'mm':'ss", Now());
    if (DBDataModule && DBDataModule->ADOConnection1->Connected) {
		DBDataModule->LeggiSegnalazioneAttiva(msg, all);
    }
	MainStatusBar->Refresh();
	TimerOra->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnPwdClick(TObject *Sender)
{
	int res;
	AnsiString NewPwd;

	PasswordDlg->BitBtn3->Visible = true;
	PasswordDlg->Label1->Caption = AnsiString::LoadStr(IDS_TYPEPWD);
	PasswordDlg->Password->Text = "";
	res = PasswordDlg->ShowModal();
	if (res == IDOK) {
		if (PasswordDlg->Password->Text == "meccanosrl") {
			pwdlevel = 2;
			ShowCursor(true);
		} else if (PasswordDlg->Password->Text == PWD)
			pwdlevel = 1;
		if (pwdlevel > 0) {
			UnlockKeyboard();
			if (pwdlevel == 2)
				TimerPwd->Enabled = false;
			else
				TimerPwd->Enabled = true;
		} else {
			ShowMSG(AnsiString::LoadStr(IDS_WRONGPWD));
		}
	} else if (res == IDYES) {
		if ((PasswordDlg->Password->Text == "meccanosrl") ||
			(PasswordDlg->Password->Text == PWD)) {
			PasswordDlg->BitBtn3->Visible = false;
			PasswordDlg->Label1->Caption = AnsiString::LoadStr(IDS_TYPENEW);
			PasswordDlg->Password->Text = "";
			res = PasswordDlg->ShowModal();
			if (res == IDOK) {
				NewPwd = PasswordDlg->Password->Text;
				PasswordDlg->Label1->Caption = AnsiString::LoadStr(IDS_CONFNEW);
				PasswordDlg->Password->Text = "";
				res = PasswordDlg->ShowModal();
				if (res == IDOK) {
					if (NewPwd == PasswordDlg->Password->Text) {
						PWD = NewPwd;
						SavePassword();
					} else
						ShowMSG(AnsiString::LoadStr(IDS_PWDERR));
				}
			}
		} else {
			ShowMSG(AnsiString::LoadStr(IDS_WRONGPWD));
		}
	} else {
		TimerPwdTimer(NULL);
	}
	UpdControls();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MainStatusBarDrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const System::Types::TRect &Rect)
{
	static int n = 0;
	static int n2 = 0;
	static int prev = 0;
	TCanvas *pCanvas = StatusBar->Canvas;
	int t, l;

	switch (Panel->Index) {
	case 0:
		StatusBar->Panels->Items[0]->Text = timestr;
		pCanvas->Brush->Color = clBtnFace;
		pCanvas->Font->Color = clBlack;
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 1:
		switch(pwdlevel) {
			case 1:
				n = 1;
				break;
			case 2:
				n = 2;
				break;
			default:
				n = 0;
		}
		l = (Rect.Width() - ImageList1->Width) / 2;
		t = (Rect.Height() - ImageList1->Height) / 2;
		ImageList1->Draw(StatusBar->Canvas, Rect.left + l, Rect.top + t, n);
		break;
	case 2:
		if (ClientData.watchdog != prev) {
			n2 = 1 - n2;
			prev = ClientData.watchdog;
		}
		l = (Rect.Width() - ImageList2->Width) / 2;
		t = (Rect.Height() - ImageList2->Height) / 2;
		ImageList2->Draw(StatusBar->Canvas, Rect.left + l, Rect.top + t, n2);
		break;
	case 3:
		if (SocketDataModule->SeiConnesso()) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		} else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 4:
		if (!SocketDataModule->SeiConnesso()) {
			pCanvas->Brush->Color = clGray;
			pCanvas->Font->Color = clWhite;
		}
		else if (ClientData.Robot_Connected) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		}
		else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 5:
		if (!SocketDataModule->SeiConnesso()) {
			pCanvas->Brush->Color = clGray;
			pCanvas->Font->Color = clWhite;
		}
		else if (ClientData.PLC_Connected) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		}
		else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 6:
		if (!SocketDataModule->SeiConnesso()) {
			pCanvas->Brush->Color = clGray;
			pCanvas->Font->Color = clWhite;
		}
		else if (ClientData.M_Connected[0]) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		}
		else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 7:
		if (!SocketDataModule->SeiConnesso()) {
			pCanvas->Brush->Color = clGray;
			pCanvas->Font->Color = clWhite;
		}
		else if (ClientData.M_Connected[1]) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		}
		else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 8:
		if (!SocketDataModule->SeiConnesso()) {
			pCanvas->Brush->Color = clGray;
			pCanvas->Font->Color = clWhite;
		}
		else if (ClientData.M_Connected[2]) {
			pCanvas->Brush->Color = clLime;
			pCanvas->Font->Color = clBlack;
		}
		else {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
		}
		pCanvas->FillRect(Rect);
		l = (Rect.Width() - pCanvas->TextWidth(Panel->Text)) / 2;
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + l, Rect.top + t, Panel->Text);
		break;
	case 9:
		if (msg == "") {
			pCanvas->Brush->Color = clBtnFace;
			pCanvas->Font->Color = clBlack;
			Panel->Text = AnsiString::LoadStr(IDS_NOALLARMI);
		} else if (all) {
			pCanvas->Brush->Color = clRed;
			pCanvas->Font->Color = clWhite;
			pCanvas->Font->Style = TFontStyles() << fsBold;
			Panel->Text = msg;
		} else {
			pCanvas->Brush->Color = clYellow;
			pCanvas->Font->Color = clBlack;
			Panel->Text = msg;
		}
		pCanvas->FillRect(Rect);
		t = (Rect.Height() - pCanvas->TextHeight(Panel->Text)) / 2;
		pCanvas->TextOut(Rect.left + 2, Rect.top + t, Panel->Text);
		break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MainStatusBarClick(TObject *Sender)
{
	DBDataModule->AcquisisciSegnalazioneAttiva();
	DBDataModule->LeggiSegnalazioneAttiva(msg, all);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Image4Click(TObject *Sender)
{
	if (BorderStyle == bsNone) {
		BorderStyle = bsSizeable;
	} else {
		BorderStyle = bsNone;
		ShowWindow(Handle, SW_MAXIMIZE);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnStoricoClick(TObject *Sender)
{
	CreateMDIChild(StoricoSegnalazioniForm, "StoricoSegnalazioniForm", AnsiString::LoadStr(IDS_ALLHIST));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnRobotClick(TObject *Sender)
{
	CreateMDIChild(RobotForm, "RobotForm", AnsiString::LoadStr(IDS_ROBOT));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::bbMagazziniClick(TObject *Sender)
{
	CreateMDIChild(MagazziniForm, "MagazziniForm", AnsiString::LoadStr(IDS_STORAGE));
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::BitBtnMacchineClick(TObject *Sender)
{
	CreateMDIChild(MacchineForm, "MacchineForm", AnsiString::LoadStr(IDS_MACCHINE));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnArticoliClick(TObject *Sender)
{
	CreateMDIChild(ArticoliForm, "ArticoliForm", AnsiString::LoadStr(IDS_ARTICOLI));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnStazioniClick(TObject *Sender)
{
	CreateMDIChild(StazioniForm, "StazioniForm", AnsiString::LoadStr(IDS_BAIE));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn3Click(TObject *Sender)
{
	TPoint p;

	p = BitBtn3->ClientToScreen(Point(0, BitBtn3->Height));
	PopupMenu1->Popup(p.X, p.Y);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn2Click(TObject *Sender)
{
	CreateMDIChild(PalletForm, "PalletForm", AnsiString::LoadStr(IDS_PALLET));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Inglese1Click(TObject *Sender)
{
	TIniFile *Ini;
	int lang, i;

	lang = ((TMenuItem*)Sender)->Tag;
	CambiaLingua(lang);
}
//---------------------------------------------------------------------------

void TMainForm::CambiaLingua(int lang, bool warning)
{
	TIniFile *Ini;
	int i;

	if (LoadNewResourceModule(lang)) {
		if (!warning)
			ReloadInheritedComponent(this, __classid(TForm));
//		ReinitializeForms();
//		MainForm->WindowState = wsMaximized;
//		MainForm->Left = 0;
//		MainForm->Top = 0;
//		MainForm->Width = Screen->Width;
//		MainForm->Height = Screen->Height;
//		if (!TouchPanel) {
//			BorderStyle = bsSizeable;
//		} else {
//			BorderStyle = bsNone;
//		}
//		ShowWindow(Handle, SW_MAXIMIZE);
		Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
		Ini->WriteInteger("Language", "ID", lang);
		delete Ini;
	}
	switch(lang) {
	case 1033: // USA
		BitBtn3->Glyph = NULL;
		ImageList3->GetBitmap(1, BitBtn3->Glyph);
		break;
	case 1040: // Italiano
		BitBtn3->Glyph = NULL;
		ImageList3->GetBitmap(0, BitBtn3->Glyph);
		break;
	}
	if (warning) {
		Application->MessageBox(String::LoadStr(IDS_RESTART).w_str(), String::LoadStr(IDS_AVVISO).w_str(), MB_OK);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnProduzioneClick(TObject *Sender)
{
	CreateMDIChild(JobsForm, "JobsForm", AnsiString::LoadStr(IDS_JOBS));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnLavorazioniClick(TObject *Sender)
{
	CreateMDIChild(LavorazioniForm, "LavorazioniForm", "Anagrafica Lavorazioni");
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtnLottiClick(TObject *Sender)
{
	CreateMDIChild(LottiForm, "LottiForm", "Elenco Lotti");
}
//---------------------------------------------------------------------------

