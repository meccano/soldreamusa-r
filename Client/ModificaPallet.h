//---------------------------------------------------------------------------

#ifndef ModificaPalletH
#define ModificaPalletH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "MyDBGrid.h"
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
#include <Data.Win.ADODB.hpp>
#include <Data.DB.hpp>
#include <vector>
#include "DB.h"
//---------------------------------------------------------------------------
class TFormModificaPallet : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GroupBox1;
	TLabeledEdit *LE_ID_Pallet;
	TGroupBox *GroupBox2;
	TPanel *Panel1;
	TComboBox *cbTipiPallet;
	TStaticText *StaticText1;
	TBitBtn *BitBtnSvuota;
	TComboBox *cbTipiPezzi;
	TComboBox *cbArticoli;
	TStaticText *StaticText2;
	TStaticText *StaticText3;
	TLabeledEdit *leDescrizionePallet;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TGridPanel *GridPanel1;
	TRadioGroup *rgStato;
	TRadioGroup *rgErrore;
	void __fastcall cbTipiPezziChange(TObject *Sender);
	void __fastcall BitBtnSvuotaClick(TObject *Sender);
	void __fastcall cbArticoliChange(TObject *Sender);
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall LE_XClick(TObject *Sender);
	void __fastcall OnChangeTipoPallet(TObject *Sender);
	void __fastcall leDescrizionePalletClick(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
          

private:	// User declarations
public:		// User declarations
	__fastcall TFormModificaPallet(TComponent* Owner);
	void ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue);
	void CaricaComboBoxArticoli(int tpz);
	void ImpostaDati(int ID_Pallet);
	void AggiornaComboTipiPezzi();
    bool CheckValues();
	TRecordList TabTipiPallet, TabTipiPezzi, TabArticoli;
	int ID_Pallet;
	TPallet p;
	TPezzo pz;
	TArticolo a;
    bool m_bCheckValues;
   	std::vector<int> m_PalletList;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormModificaPallet *FormModificaPallet;
//---------------------------------------------------------------------------
#endif
