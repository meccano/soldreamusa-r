//---------------------------------------------------------------------------

#ifndef InsAlphaH
#define InsAlphaH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Touch.Keyboard.hpp>
//---------------------------------------------------------------------------
class TInsAlphaForm : public TForm
{
__published:	// IDE-managed Components
	TTouchKeyboard *TouchKeyboard1;
	TLabeledEdit *Valore;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	void __fastcall FormActivate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TInsAlphaForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TInsAlphaForm *InsAlphaForm;
//---------------------------------------------------------------------------
#endif
