//---------------------------------------------------------------------------

#ifndef MacchineH
#define MacchineH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Dialogs.hpp>
#include "DB.h"
#include "MyDBGrid.h"
#include "Define.h"
//---------------------------------------------------------------------------
class TMacchineForm : public TMDIChild
{
__published:	// IDE-managed Components
	TPanel *Panel4;
	TTimer *TimerUpd;
	TPageControl *PageControl1;
	TTabSheet *Macchine;
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
	TStringField *ADOQuery1Descrizione_Macchina;
	TStringField *ADOQuery1Codice_Tipopallet;
	TBCDField *ADOQuery1PresetX;
	TBCDField *ADOQuery1PresetY;
	TBCDField *ADOQuery1PresetZ;
	TBCDField *ADOQuery1PresetA;
	TBCDField *ADOQuery1PresetB;
	TBCDField *ADOQuery1PresetC;
	TIntegerField *ADOQuery1ID_Macchina;
	TIntegerField *ADOQuery1ID_TipoPallet;
	TBCDField *ADOQuery1PresetROT;
	TOpenDialog *OpenDialog1;
	TBCDField *ADOQuery1ZeroX;
	TBCDField *ADOQuery1ZeroY;
	TGridPanel *GridPanel1;
	TGroupBox *GroupBoxMU1;
	TShape *sPalletSbloccato1;
	TLabel *lbl5;
	TShape *sPalletBloccato1;
	TLabel *lbl6;
	TShape *sAutomatico1;
	TShape *sAllarme1;
	TLabel *Label4;
	TLabel *Label5;
	TShape *sPortaAperta1;
	TLabel *Label14;
	TLabeledEdit *leAut1;
	TLabeledEdit *leRun1;
	TLabeledEdit *leMotion1;
	TLabeledEdit *leProg1;
	TLabeledEdit *leMainProg1;
	TGroupBox *grp7;
	TLabeledEdit *lePalletM1;
	TLabeledEdit *leCodiceM1;
	TLabeledEdit *leDescM1;
	TBitBtn *bbModificaM1;
	TBitBtn *bbSvuotaM1;
	TGroupBox *GroupBoxMU2;
	TShape *sPalletSbloccato2;
	TLabel *Label2;
	TShape *sPalletBloccato2;
	TLabel *Label3;
	TShape *sAutomatico2;
	TShape *sAllarme2;
	TLabel *Label7;
	TLabel *Label8;
	TShape *sPortaAperta2;
	TLabel *Label16;
	TLabeledEdit *leAut2;
	TLabeledEdit *leRun2;
	TLabeledEdit *leMotion2;
	TLabeledEdit *leProg2;
	TLabeledEdit *leMainProg2;
	TGroupBox *GroupBox2;
	TLabeledEdit *lePalletM2;
	TLabeledEdit *leCodiceM2;
	TLabeledEdit *leDescM2;
	TBitBtn *bbModificaM2;
	TBitBtn *bbSvuotaM2;
	TGroupBox *GroupBoxMU3;
	TShape *sPalletSbloccato3;
	TLabel *Label11;
	TShape *sPalletBloccato3;
	TLabel *Label12;
	TShape *sAutomatico3;
	TShape *sAllarme3;
	TLabel *Label13;
	TLabel *Label20;
	TShape *sPortaAperta3;
	TLabel *Label22;
	TLabeledEdit *leAut3;
	TLabeledEdit *leRun3;
	TLabeledEdit *leMotion3;
	TLabeledEdit *leProg3;
	TLabeledEdit *leMainProg3;
	TGroupBox *GroupBox4;
	TLabeledEdit *lePalletM3;
	TLabeledEdit *leCodiceM3;
	TLabeledEdit *leDescM3;
	TBitBtn *bbModificaM3;
	TBitBtn *bbSvuotaM3;
	TLabel *Label1;
	TShape *sFineCiclo1;
	TShape *sFineCiclo2;
	TLabel *Label6;
	TShape *sFineCiclo3;
	TLabel *Label9;
	TBitBtn *BitBtnSendProgramMU1;
	TBitBtn *BitBtnStartMU1;
	TBitBtn *btnJobCreateMU1;
	TBitBtn *BitBtnSendProgramMU2;
	TBitBtn *BitBtnStartMU2;
	TBitBtn *btnJobCreateMU2;
	TBitBtn *BitBtnStartMU3;
	TBitBtn *BitBtnSendProgramMU3;
	TBitBtn *btnJobCreateMU3;
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall bbSvuotaMClick(TObject *Sender);
	void __fastcall bbModificaMClick(TObject *Sender);
	void __fastcall BitBtnJobCreateMClick(TObject *Sender);
	void __fastcall BitBtnSendProgramClick(TObject *Sender);
	void __fastcall BitBtnStartClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TMacchineForm(TComponent* Owner);
	void ColoraShape(int stato, TShape *t, TColor ColorON = clLime, TColor ColorOFF = clGray);
	TIndexList ListaMacchine;
    AnsiString m_PPFolderMU[N_MACCHINE_UT];
};
//---------------------------------------------------------------------------
extern PACKAGE TMacchineForm *MacchineForm;
//---------------------------------------------------------------------------
#endif
