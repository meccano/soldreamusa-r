//---------------------------------------------------------------------------

#ifndef PalletH
#define PalletH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include "MyDBGrid.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TPalletForm : public TMDIChild
{
__published:	// IDE-managed Components
	TDataSource *DataSource1;
	TPanel *Panel1;
	TPageControl *PageControl1;
	TTabSheet *Table;
	TBitBtn *BitBtnCancellaPallet;
	TBitBtn *BitBtnModificaPallet;
	TBitBtn *BitBtnNuovoPallet;
	TGroupBox *GroupBox2;
	TLabeledEdit *LE_TAG;
	TLabeledEdit *LE_TAGOLD;
	TComboBox *ComboBox1;
	TBitBtn *BitBtnCreate;
	TStaticText *StaticText1;
	TBitBtn *BitBtnGen;
	TLabeledEdit *LE_NewID;
	TBitBtn *BitBtnWr;
	TBitBtn *BitBtnAb;
	TStaticText *STMessage;
	TRadioButton *RB_IDON;
	TRadioButton *RB_IDOFF;
	TBitBtn *BitBtnElettrodo;
	TTimer *TimerUPD;
	TDataSource *DataSource2;
	TADOQuery *ADOQuery2;
	TStringField *ADOQuery2Codice_Articolo;
	TStringField *ADOQuery2Descrizione_Articolo;
	TStringField *ADOQuery2Codice;
	TStringField *ADOQuery2Descrizione;
	TStringField *ADOQuery2CodFase;
	TIntegerField *ADOQuery2ID_Pallet;
	TStringField *ADOQuery2DescrFase;
	TIntegerField *ADOQuery2ID_Job;
	TIntegerField *ADOQuery2ID_Missione;
	TIntegerField *ADOQuery2LavoroAttivo;
	TIntegerField *ADOQuery2FaseCompletata;
	TIntegerField *ADOQuery2ID_Lavoro;
	TGridPanel *GridPanel1;
	TMyDBGrid *DBGrid1;
	TMyDBGrid *DBGridFasi;
	TBitBtn *BitBtnCreaLavoro;
	TBitBtn *bCancellaLavorazione;
	TADOQuery *ADOQuery1;
	TIntegerField *ADOQuery1ID_Pallet;
	TIntegerField *ADOQuery1ID_TipoPallet;
	TStringField *ADOQuery1Codice_Tipopallet;
	TStringField *ADOQuery1Descrizione_TipoPallet;
	TIntegerField *ADOQuery1Stato_Pallet;
	TIntegerField *ADOQuery1Extra_Altezza;
	TIntegerField *ADOQuery1ID_Pezzo;
	TIntegerField *ADOQuery1ID_TipoPezzo;
	TStringField *ADOQuery1Descrizione_Pezzo;
	TIntegerField *ADOQuery1ID_Articolo;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery1Descrizione_Articolo;
	TIntegerField *ADOQuery1Stato_Pezzo;
	TBCDField *ADOQuery1OffsetX;
	TBCDField *ADOQuery1OffsetY;
	TBCDField *ADOQuery1OffsetZ;
	TBCDField *ADOQuery1OffsetA;
	TBCDField *ADOQuery1OffsetB;
	TBCDField *ADOQuery1OffsetC;
	TIntegerField *ADOQuery1Misura_Eseguita;
	TStringField *ADOQuery1Descrizione_Pallet;
	TBCDField *ADOQuery1OffsetROT;
	TBCDField *ADOQuery1RawH;
	TBCDField *ADOQuery1Gap;
	TBCDField *ADOQuery1H;
	TBCDField *ADOQuery1GapMin;
	TBCDField *ADOQuery1GapMax;
	TStringField *ADOQuery1Descrizione;
	TStringField *ADOQuery1Posizione;
	TStringField *ADOQuery1Codice;
	TIntegerField *ADOQuery1ID_Lavoro;
	TIntegerField *ADOQuery1ID_Lavorazione;
	TAutoIncField *ADOQuery1ID_Lotto;
	TIntegerField *ADOQuery1Sospeso;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall TimerUPDTimer(TObject *Sender);
	void __fastcall BitBtnWrClick(TObject *Sender);
	void __fastcall BitBtnAbClick(TObject *Sender);
	void __fastcall BitBtnGenClick(TObject *Sender);
	void __fastcall BitBtnCreateClick(TObject *Sender);
	void __fastcall BitBtnCancellaPalletClick(TObject *Sender);
	void __fastcall BitBtnModificaPalletClick(TObject *Sender);
	void __fastcall LE_NewIDClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall BitBtnNuovoPalletClick(TObject *Sender);
	void __fastcall OnDoubleClickPalletGrid(TObject *Sender);
	void __fastcall DBGridFasiDrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol,
          TColumn *Column, TGridDrawState State);
	void __fastcall BitBtnAttivaClick(TObject *Sender);
	void __fastcall BitBtnDisattivaLottoClick(TObject *Sender);
	void __fastcall ADOQuery1AfterScroll(TDataSet *DataSet);
	void __fastcall bCancellaLavorazioneClick(TObject *Sender);
	void __fastcall DBGrid1DrawDataCell(TObject *Sender, const TRect &Rect, TField *Field,
          TGridDrawState State);
	void __fastcall BitBtnCreaLavoroClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TPalletForm(TComponent* Owner);
	void EvidenziaDBEdit(TDBEdit *e, bool val);
    void AggiornaComboTipiPallet();
	void AggiornaDB();
	void AggiornaRFID();
	void AggiornaQuery2();
	TRecordList RecList;
	bool m_bIsFormCreate;
};
//---------------------------------------------------------------------------
extern PACKAGE TPalletForm *PalletForm;
//---------------------------------------------------------------------------
#endif


