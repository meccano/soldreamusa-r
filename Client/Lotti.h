//---------------------------------------------------------------------------

#ifndef LottiH
#define LottiH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include "MyDBGrid.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
#include "DB.h"
//---------------------------------------------------------------------------
class TLottiForm : public TMDIChild
{
__published:	// IDE-managed Components
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
	TBitBtn *BitBtnAdd;
	TBitBtn *BitBtnDelete;
	TTimer *Timer1;
	TBitBtn *BitBtnChange;
	TBitBtn *BitBtnAttiva;
	TDataSource *DataSource2;
	TADOQuery *ADOQuery2;
	TBitBtn *BitBtnDisattivaLotto;
	TStringField *ADOQuery2Codice;
	TStringField *ADOQuery2Descrizione;
	TIntegerField *ADOQuery2ID_Pallet;
	TIntegerField *ADOQuery2Qta;
	TIntegerField *ADOQuery2Qta_Prodotta;
	TIntegerField *ADOQuery2Priorita;
	TIntegerField *ADOQuery2Sospeso;
	TStringField *ADOQuery2Codice_Lavorazione;
	TStringField *ADOQuery2Codice_Articolo;
	TStringField *ADOQuery2Descrizione_Articolo;
	TIntegerField *ADOQuery2ID_Lotto;
	TStringField *ADOQuery1Codice;
	TStringField *ADOQuery1Descrizione;
	TIntegerField *ADOQuery1ID_Pallet;
	TIntegerField *ADOQuery1Qta;
	TIntegerField *ADOQuery1Qta_Prodotta;
	TIntegerField *ADOQuery1Priorita;
	TIntegerField *ADOQuery1Sospeso;
	TStringField *ADOQuery1Codice_Lavorazione;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery1Descrizione_Articolo;
	TIntegerField *ADOQuery1ID_Lotto;
	TIntegerField *ADOQuery1ID_Lavoro;
	TDataSource *DataSource3;
	TADOQuery *ADOQuery3;
	TStringField *ADOQuery3Codice_Articolo;
	TStringField *ADOQuery3Descrizione_Articolo;
	TStringField *ADOQuery3Codice;
	TStringField *ADOQuery3Descrizione;
	TStringField *ADOQuery3CodFase;
	TIntegerField *ADOQuery3ID_Pallet;
	TStringField *ADOQuery3DescrFase;
	TIntegerField *ADOQuery3ID_Job;
	TIntegerField *ADOQuery3ID_Missione;
	TIntegerField *ADOQuery3LavoroAttivo;
	TIntegerField *ADOQuery3FaseCompletata;
	TIntegerField *ADOQuery3ID_Lavoro;
	TIntegerField *ADOQuery2ID_Lavoro;
	TBitBtn *bResetLotto;
	TGridPanel *GridPanel1;
	TPanel *pSospesi;
	TMyDBGrid *dbgLottiSospesi;
	TPanel *pAttivi;
	TMyDBGrid *dbgLottiAttivi;
	TPanel *pFasi;
	TMyDBGrid *DBGridFasi;
	TStringField *ADOQuery1Descrizione_Pallet;
	TStringField *ADOQuery2Descrizione_Pallet;
	TStringField *ADOQuery1Posizione;
	TStringField *ADOQuery2Posizione;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall BitBtnAddClick(TObject *Sender);
	void __fastcall BitBtnChangeClick(TObject *Sender);
	void __fastcall BitBtnDeleteClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);
	void __fastcall BitBtnAttivaClick(TObject *Sender);
	void __fastcall dbgLottiAttiviDrawColumnCell(TObject *Sender, const TRect &Rect,
          int DataCol, TColumn *Column, TGridDrawState State);
	void __fastcall dbgLottiSospesiCellClick(TColumn *Column);
	void __fastcall BitBtnDisattivaLottoClick(TObject *Sender);
	void __fastcall DBGridFasiDrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column, TGridDrawState State);
	void __fastcall ADOQuery2AfterScroll(TDataSet *DataSet);
	void __fastcall dbgLottiAttiviCellClick(TColumn *Column);
	void __fastcall bResetLottoClick(TObject *Sender);
	void __fastcall dbgLottiAttiviDblClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TLottiForm(TComponent* Owner);
	void Aggiorna();
	void AggiornaBtn();
	void AggiornaDBGridFasi();
	TIndexList RecList;
	bool SelezionatiNonAttivi;
};
//---------------------------------------------------------------------------
extern PACKAGE TLottiForm *LottiForm;
//---------------------------------------------------------------------------
#endif
