//---------------------------------------------------------------------------

#ifndef ArticoliH
#define ArticoliH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include "MyDBGrid.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
//---------------------------------------------------------------------------
class TArticoliForm : public TMDIChild
{
__published:	// IDE-managed Components
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
	TPageControl *PageControl1;
	TTabSheet *Table;
	TTabSheet *Details;
	TMyDBGrid *DBGrid1;
	TGroupBox *GroupBox1;
	TStaticText *StaticText1;
	TDBEdit *DBEdit1;
	TStaticText *StaticText2;
	TDBEdit *DBEdit3;
	TDBEdit *DBEdit2;
	TStaticText *StaticText3;
	TStaticText *StaticText4;
	TDBLookupComboBox *DBLookupComboBox1;
	TBitBtn *BitBtnAdd;
	TBitBtn *BitBtnDelete;
	TBitBtn *BitBtnChange;
	TAutoIncField *ADOQuery1ID_Articolo;
	TIntegerField *ADOQuery1ID_TipoPezzo;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery1Descrizione_Articolo;
	TBCDField *ADOQuery1Diametro_Pz_SemiL;
	TBCDField *ADOQuery1Diametro_Pezzo_Grezzo;
	TBCDField *ADOQuery1Altezza_Pz;
	TIntegerField *ADOQuery1ftc_Controllo;
	TStringField *ADOQuery1Descrizione_Pezzo;
	TIntegerField *ADOQuery1ID_TipoPallet;
	TIntegerField *ADOQuery1Faccia_pinza;
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall BitBtnAddClick(TObject *Sender);
	void __fastcall BitBtnChangeClick(TObject *Sender);
	void __fastcall BitBtnDeleteClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall OnDoubleClickArticoli(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TArticoliForm(TComponent* Owner);
	void Aggiorna();
};
//---------------------------------------------------------------------------
extern PACKAGE TArticoliForm *ArticoliForm;
//---------------------------------------------------------------------------
#endif
