//---------------------------------------------------------------------------

#ifndef NewLavorazioniH
#define NewLavorazioniH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include "MyDBGrid.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <vector>
//---------------------------------------------------------------------------
class TFormNewLavorazioni : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TGroupBox *GroupBox1;
	TStaticText *StaticText2;
	TCheckBox *CheckBox1;
	TComboBox *CmbLavorazioni;
	TDataSource *DataSource2;
	TStringGrid *StringGrid1;
	TADOQuery *ADOQuery2;
	TStringField *qry2Cod_Fase;
	TStringField *qry2Desc_Fase;
	TStringField *qry2Prg_MU;
	TStringField *qry2Desc_MU;
	TIntegerField *qry2Ordine_Seq;
	TStringField *qry2Messaggio_Op;
	TIntegerField *qry2Abilitazione;
	TIntegerField *qry2ID_Lavorazione;
	TIntegerField *qry2ID_Fase;
	TStringField *qry2Codice_Tipopallet;
	TStringField *qry2Descrizione_TipoPallet;
	TStringField *qry2Descrizione_Op;
	TMyDBGrid *DBGridFasi;
	TStringField *ADOQuery2Codice_Op;
	TIntegerField *ADOQuery2ID_TipoPalletxCambio;
	TStaticText *StaticText1;
    TStaticText *StaticText3;
    TEdit *edtNumOrdine;
	TLabeledEdit *LE_Pri;
	TTrackBar *TrackBar1;
	void __fastcall OnActiveForm(TObject *Sender);
	void __fastcall OnChangeCmbLavor(TObject *Sender);
	void __fastcall OnFormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall OnSelectedCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect, TGridDrawState State);
	void __fastcall OnClickEnableColumn(TColumn *Column);
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall DBGridFasiDrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol,
          TColumn *Column, TGridDrawState State);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall TrackBar1Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormNewLavorazioni(TComponent* Owner);

	void CaricaComboBoxLavorazioni(int ID_Pallet);
	void AggiornaGridFasi();
	void SelectPallet(int ID_Pallet);
	void WriteStringGrid(int ID_Pallet);
	int m_IDPallet;
    int m_IDPezzo;
	bool PalletSelected;
	bool FaseSelected;
	bool m_bPartProgramOk;
	TRecordList TabLavorazioni;
	TRecordList ViewFasiLavorazioni;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormNewLavorazioni *FormNewLavorazioni;
//---------------------------------------------------------------------------
#endif
