object FrameDBGrid: TFrameDBGrid
  Left = 0
  Top = 0
  Width = 550
  Height = 452
  TabOrder = 0
  object EnhDBGrid1: TEnhDBGrid
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 544
    Height = 446
    Cursor = crHandPoint
    TitleColorStart = clGreen
    TitleColorCenter = clLime
    TitleColorCenterPosition = 1
    TitleColorFinish = 12320699
    AltRowColor1Center = 8454143
    AltRowColor1Finish = clSilver
    AltRowColor2Start = clWhite
    AltRowColor2Center = clWhite
    AltRowColor2Finish = clSilver
    HotTrack = True
    AutoWidthMax = 50
    AutoWidthMin = 10
    ActiveCellFontColor = clWindowText
    SelectedCellFontColor = clWindowText
    Align = alClient
    Color = 33023
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLNCLI11.1;Integrated Security="";Persist Security Inf' +
      'o=False;User ID=sa;Initial Catalog=RP;Data Source=LORENZO-VAIO\S' +
      'qlExpress;Initial File Name="";Server SPN=""'
    LoginPrompt = False
    Provider = 'SQLNCLI11.1'
    Left = 48
    Top = 32
  end
  object ADOQuery1: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'Select * from Magazzino'
      '')
    Left = 48
    Top = 64
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 48
    Top = 120
  end
end
