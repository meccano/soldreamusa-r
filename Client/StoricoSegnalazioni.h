//---------------------------------------------------------------------------

#ifndef StoricoSegnalazioniH
#define StoricoSegnalazioniH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include "MyDBGrid.h"
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TStoricoSegnalazioniForm : public TMDIChild
{
__published:	// IDE-managed Components
	TPanel *Panel4;
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
	TDateTimeField *ADOQuery1DataOra;
	TStringField *ADOQuery1Messaggio;
	TIntegerField *ADOQuery1Allarme;
	TDateTimeField *ADOQuery1Acquisito;
	TGroupBox *GroupBox1;
	TLabel *Label1;
	TDateTimePicker *DateTimePicker1;
	TLabel *Label2;
	TDateTimePicker *DateTimePicker2;
	TLabel *Label3;
	TDateTimePicker *DateTimePicker3;
	TLabel *Label4;
	TDateTimePicker *DateTimePicker4;
	TTimer *Timer1;
	TPageControl *PageControl1;
	TTabSheet *tsAllarmi;
	TTabSheet *tsLog;
	TMyDBGrid *DBGrid1;
	TDataSource *DataSource2;
	TADOQuery *ADOQuery2;
	TMyDBGrid *dbgLog;
	TDateTimeField *ADOQuery2DataOra;
	TStringField *ADOQuery2Sorgente;
	TStringField *ADOQuery2Tipo;
	TStringField *ADOQuery2Evento;
	TStaticText *StaticText1;
	TTabSheet *tsProduzione;
	TMyDBGrid *gbgProd;
	TDataSource *DataSource3;
	TADOQuery *ADOQuery3;
	TDateTimeField *ADOQuery3DataOra;
	TIntegerField *ADOQuery3ID_Pallet;
	TStringField *ADOQuery3Codice_Articolo;
	TDateTimeField *ADOQuery3Attivazione;
	TStringField *ADOQuery3Programma_Macchina;
	TDateTimeField *ADOQuery3Inizio_Lavoro;
	TDateTimeField *ADOQuery3Fine_Lavoro;
	TStringField *ADOQuery3Posizione;
	TStringField *ADOQuery3Descrizione_Macchina;
	TStringField *ADOQuery3Destinazione;
	TRadioGroup *rgFilter;
	TDateTimeField *ADOQuery3T_Lavoro;
	TStringField *ADOQuery3NumOrdine;
	TBitBtn *bEsporta;
	TSaveDialog *SaveDialog1;
	TTabSheet *TabSheet1;
	TADOQuery *ADOQuery4;
	TAutoIncField *ADOQuery3ID;
	TDateTimeField *DateTimeField1;
	TWideStringField *ADOQuery3Sorgente;
	TWideStringField *ADOQuery3Tipo;
	TStringField *ADOQuery3Evento;
	TDataSource *DataSource4;
	TMyDBGrid *dbgEventiRobot1;
	TBitBtn *BitBtn1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall DBGrid1DrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column, TGridDrawState State);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall PageControl1Change(TObject *Sender);
	void __fastcall bEsportaClick(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TStoricoSegnalazioniForm(TComponent* Owner);
	void SalvataggioStorico(TMyDBGrid *grid, AnsiString FileName);
	void Aggiorna();
	void AggiornaLog();
	void AggiornaProduzione();
	void AggiornaEventiRobot1();
};
//---------------------------------------------------------------------------
extern PACKAGE TStoricoSegnalazioniForm *StoricoSegnalazioniForm;
//---------------------------------------------------------------------------
#endif
