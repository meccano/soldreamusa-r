//---------------------------------------------------------------------------

#pragma hdrstop

#include "Jobs.h"
#include "ClientData.h"
#include "Socket.h"
#include "Main.h"
#include "StringTableID.h"
#include "ModificaPallet.h"
#include "NewJob.h"
#include "Robot.h"
#include "InsNum.h"
#include "NewLavorazioni.h"
#include "Util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TJobsForm *JobsForm;
//---------------------------------------------------------------------------
__fastcall TJobsForm::TJobsForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	Application->OnShowHint = MyShowHint;
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::MyShowHint(System::UnicodeString &HintStr, bool &CanShow, Vcl::Controls::THintInfo &HintInfo) {
	int i;
	TCanvas * c;

	for (i = 0; i < Application->ComponentCount; i++) {
		if (dynamic_cast<THintWindow *>(Application->Components[i]) != NULL) {
			c = ((THintWindow*)(Application->Components[i]))->Canvas;
			c->Font->Size   = 14;
			c->Font->Style  << fsBold;
		}
	}
}
//---------------------------------------------------------------------------

void TJobsForm::AggiornaDB()
{
	DBGrid1->RefreshDataSet();
	DBGrid2->RefreshDataSet();
	DBGrid3->RefreshDataSet();
	DBGrid1->AutoFitAll();
	DBGrid2->AutoFitAll();
	DBGrid3->AutoFitAll();
}
//---------------------------------------------------------------------------

void TJobsForm::AggiornaBtn()
{
	bool bEnabled = (!DBDataModule->PalletAssociatoLottoAttivo(ADOQuery2ID_Pallet->AsInteger) && !DBDataModule->PalletAssociatoLavorazione(ADOQuery2ID_Pallet->AsInteger));

	BitBtnSuspend->Enabled = (MainForm->pwdlevel > 1) || bEnabled;
    BitBtnSuspAll->Enabled = (MainForm->pwdlevel > 1) || bEnabled;
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::FormActivate(TObject *Sender)
{
	AggiornaDB();
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::TimerUPDTimer(TObject *Sender)
{
	TimerUPD->Enabled = false;
	AggiornaDB();
    AggiornaBtn();
	TimerUPD->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::BitBtnAddClick(TObject *Sender)
{
    FormNewJob->m_PalletList.clear();
	FormNewJob->SelectPallet(0);
	FormNewJob->ShowModal();
	AggiornaDB();
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::BitBtnActivateClick(TObject *Sender)
{
	// LOG
	logstr.sprintf("PC %d: activate job %d pallet=%d article=%s type=%s pos=%s prog=%s",
		MainForm->stationnumber,
		ADOQuery1ID_Job->Value,
		ADOQuery1ID_Pallet->Value,
		ADOQuery1Codice_Articolo->AsAnsiString,
		ADOQuery1Tipo_Missione->AsAnsiString,
		ADOQuery1Descrizione_Macchina->AsAnsiString,
		ADOQuery1Programma_Macchina->AsAnsiString
		);
	DBDataModule->Log("CLIENT", "JOBS", logstr);
	DBDataModule->AttivaJob(ADOQuery1ID_Job->Value);
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::BitBtnSuspendClick(TObject *Sender)
{
	// LOG
	logstr.sprintf("PC %d: suspend job %d pallet=%d article=%s type=%s pos=%s prog=%s",
		MainForm->stationnumber,
		ADOQuery2ID_Job->Value,
		ADOQuery2ID_Pallet->Value,
		ADOQuery2Codice_Articolo->AsAnsiString,
		ADOQuery2Tipo_Missione->AsAnsiString,
		ADOQuery2Descrizione_Macchina->AsAnsiString,
		ADOQuery2Programma_Macchina->AsAnsiString
		);
	DBDataModule->Log("CLIENT", "JOBS", logstr);
	DBDataModule->DisattivaJob(ADOQuery2ID_Job->Value);
	DBDataModule->LogDisattivazioneProd(ADOQuery2ID_Pallet->Value);
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::BitBtnDeleteClick(TObject *Sender)
{
	// LOG
	DBDataModule->Log("CLIENT", "JOBS", "Delete Job");
	DBDataModule->CancellaJob(ADOQuery1ID_Job->Value);
}
//---------------------------------------------------------------------------

bool TJobsForm::VerificaSeMissioneInCorso(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int ID_Macchina) {
	// Ritorna TRUE se la missione � in esecuzione sul robot
	if (ID_Pallet != ClientData.Missione.N_pallet) {
		return false;
	}
	// Missione deposito macchina
	if ((Tipo_Missione == "DROP") &&
		(Tipo_Macchina == "M") &&
		(ClientData.Missione.Tipo_deposito == TPMISS_MU) &&
		(ID_Macchina == ClientData.Missione.N_MU_dep)) {
		return true;
	}
	// Missione prelievo macchina
	if ((Tipo_Missione == "PICK") &&
		(Tipo_Macchina == "M") &&
		(ClientData.Missione.Tipo_prelievo == TPMISS_MU) &&
		(ID_Macchina == ClientData.Missione.N_MU_pre)) {
		return true;
	}
	// Missione deposito baia
	if ((Tipo_Missione == "DROP") &&
		(Tipo_Macchina == "S") &&
		(ClientData.Missione.Tipo_deposito == TPMISS_BAIA) &&
		(ID_Macchina == ClientData.Missione.N_staz_operatore_dep)) {
		return true;
	}
	// Missione prelievo baia
	if ((Tipo_Missione == "PICK") &&
		(Tipo_Macchina == "S") &&
		(ClientData.Missione.Tipo_prelievo == TPMISS_BAIA) &&
		(ID_Macchina == ClientData.Missione.N_staz_operatore_pre)) {
		return true;
	}
	// Missione mag
	if ((Tipo_Missione == "DROP") &&
		(Tipo_Macchina != "S") && (Tipo_Macchina != "M") &&
		((ClientData.Missione.Tipo_prelievo == TPMISS_MAG) || (ClientData.Missione.Tipo_deposito == TPMISS_MAG))) {
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::BitBtnAbortClick(TObject *Sender)
{
	TJob j;

    BitBtnAbort->Enabled = false;
    //Controllo che ci siano record nella tabella missioni
	if (ADOQuery3->RecordCount > 0) {
    	//Controllo che ci sia una missione in corso
        if (!VerificaSeMissioneInCorso(ADOQuery3ID_Pallet->AsInteger, ADOQuery3Tipo_Missione->AsString, ADOQuery3Tipo_Macchina->AsString, ADOQuery3ID_Macchina->AsInteger)) {
            memset(&j, 0, sizeof(TJob));
            j.ID_Pallet = ADOQuery3ID_Pallet->Value;
            j.Tipo_Missione = (ADOQuery3Tipo_Missione->Value == "DROP") ? "DROP" : "PICK";
            j.Tipo_Macchina = ADOQuery3Tipo_Macchina->Value;
            j.ID_Macchina = ADOQuery3ID_Macchina->Value;
            j.Programma_Macchina = ADOQuery3Programma_Macchina->Value;
            j.Priorita = ADOQuery3Priorita->Value;
            j.Descrizione_Op = ADOQuery3Descrizione_Op->Value.c_str();
            j.ID_Articolo = ADOQuery3ID_Articolo->Value;
            j.ID_Pezzo = ADOQuery3ID_Pezzo->Value;
            // LOG
            DBDataModule->Log("CLIENT", "JOBS", "PC %d: abort job %d pallet=%d article=%s type=%s pos=%s prog=%s",
                MainForm->stationnumber,
                ADOQuery3ID_Missione->Value,
                ADOQuery3ID_Pallet->Value,
                ADOQuery3Codice_Articolo->AsAnsiString,
                ADOQuery3Tipo_Missione->AsAnsiString,
                ADOQuery3Descrizione_Macchina->AsAnsiString,
                ADOQuery3Programma_Macchina->AsAnsiString);
            
            DBDataModule->CreaJob(j);
			DBDataModule->CancellaMissione(ADOQuery3ID_Missione->Value);
			DBDataModule->AbortMissioneInLavoriInCorso(ADOQuery3ID_Missione->Value);
			AggiornaDB();
		}
	}
    BitBtnAbort->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::DBGrid3DrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column, TGridDrawState State)
{
	if (ADOQuery3->RecordCount == 0) {
		return;
	}

	if (VerificaSeMissioneInCorso(ADOQuery3ID_Pallet->AsInteger,
								  ADOQuery3Tipo_Missione->AsString,
								  ADOQuery3Tipo_Macchina->AsString,
								  ADOQuery3ID_Macchina->AsInteger)) {
		DBGrid3->Canvas->Brush->Color = clLime;
		DBGrid3->Canvas->Font->Color = clBlack;
	}
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::DBGrid1DblClick(TObject *Sender)
{
	InsNumForm->Caption = String::LoadStr(IDS_INSVALUE).w_str();
	InsNumForm->Valore->EditLabel->Caption = DBGrid1->Columns->Items[6]->Title->Caption;
	InsNumForm->Valore->Text = ADOQuery1Priorita->Value;
	if (InsNumForm->ShowModal() == IDOK) {
		DBDataModule->CambiaPrioritaJob(ADOQuery1ID_Job->Value, InsNumForm->Valore->Text.ToIntDef(0));
	}
	DBGrid1->RefreshDataSet();
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::DBGrid2DblClick(TObject *Sender)
{
	InsNumForm->Caption = String::LoadStr(IDS_INSVALUE).w_str();
	InsNumForm->Valore->EditLabel->Caption = DBGrid2->Columns->Items[6]->Title->Caption;
	InsNumForm->Valore->Text = ADOQuery2Priorita->Value;
	if (InsNumForm->ShowModal() == IDOK) {
		DBDataModule->CambiaPrioritaJob(ADOQuery2ID_Job->Value, InsNumForm->Valore->Text.ToIntDef(0));
	}
	DBGrid2->RefreshDataSet();
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::DBGrid2MouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
	TGridCoord Cell;
	int ActRec;
	TMyDBGrid * dbg;
	AnsiString commento;

	dbg = (TMyDBGrid*)Sender;
	Cell = dbg->MouseCoord(X, Y);
	if (dbg->Options.Contains(dgIndicator))
		Cell.X--;
	if (dbg->Options.Contains(dgTitles))
		Cell.Y--;
	if (dbg->DataLink->Active && (Cell.X >= 0) && (Cell.Y >= 0)) {
		ActRec = dbg->DataLink->ActiveRecord;
		try {
		  dbg->DataLink->ActiveRecord = Cell.Y;
		  commento = dbg->DataLink->DataSet->FieldByName("Commento")->AsString;
		  dbg->Hint = commento;
		  Application->ActivateHint(dbg->ClientToScreen(TPoint(X, Y)));
		} __finally {
		  dbg->DataLink->ActiveRecord = ActRec;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::BitBtnActAllClick(TObject *Sender)
{
	// LOG
	logstr.sprintf("PC %d: activate all jobs", MainForm->stationnumber);
	DBDataModule->Log("CLIENT", "JOBS", logstr);
	DBDataModule->AttivaAllJob();
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::BitBtnSuspAllClick(TObject *Sender)
{
	// LOG
	logstr.sprintf("PC %d: deactivate all jobs", MainForm->stationnumber);
	DBDataModule->Log("CLIENT", "JOBS", logstr);
	DBDataModule->DisattivaAllJob();
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::OnClickNewLavor(TObject *Sender)
{
	FormNewLavorazioni->SelectPallet(0);
	FormNewLavorazioni->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TJobsForm::bbDeleteAllClick(TObject *Sender)
{
	if ((Application->MessageBox(String::LoadStr(IDS_CANCALLLAV).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	// LOG
	DBDataModule->Log("CLIENT", "JOBS", "PC %d: delete all jobs", MainForm->stationnumber);
	DBDataModule->CancellaTuttiJob();
}
//---------------------------------------------------------------------------

