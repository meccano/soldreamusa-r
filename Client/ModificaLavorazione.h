//---------------------------------------------------------------------------

#ifndef ModificaLavorazioneH
#define ModificaLavorazioneH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "DB.h"
//---------------------------------------------------------------------------
class TFormModificaLavorazione : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TGroupBox *GroupBox1;
	TLabeledEdit *LE_Descrizione;
    TLabeledEdit *LEID_Lavorazione;
	TComboBox *cbbArticoli;
	TStaticText *txt1;
	TLabeledEdit *LE_CodiceLavorazione;
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall cbbArticoliChange(TObject *Sender);
	void __fastcall LE_CodiceLavorazioneClick(TObject *Sender);
	void __fastcall LE_DescrizioneClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormModificaLavorazione(TComponent* Owner);
	void ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue);
	void ImpostaDati(int ID);
	void CaricaComboBoxArticoli();
	TRecordList TabArticoli;
	int ID_Lavorazione;
	TLavorazione strLavor;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormModificaLavorazione *FormModificaLavorazione;
//---------------------------------------------------------------------------
#endif
