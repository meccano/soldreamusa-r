//---------------------------------------------------------------------------

#ifndef RobotH
#define RobotH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.ComCtrls.hpp>
//---------------------------------------------------------------------------
class TRobotForm : public TMDIChild
{
__published:	// IDE-managed Components
	TBitBtn *BitBtnSTART;
	TBitBtn *BitBtnCALLMASTER;
	TBitBtn *BitBtnHOLD;
	TBitBtn *BitBtnRESET;
	TImage *Image1;
	TTimer *TimerUpd;
	TPanel *Panel1;
	TPanel *Panel4;
	TTrackBar *TrackBar1;
	TGroupBox *GroupBox2;
	TShape *Shape5;
	TShape *Shape6;
	TLabel *Label5;
	TLabel *Label6;
	TShape *Shape7;
	TLabel *Label7;
	TShape *Shape12;
	TLabel *Label12;
	TTimer *TimerOVR;
	TTimer *TimerCallMaster;
	TTimer *TimerStart;
	TGroupBox *GroupBox4;
	TBitBtn *BitBtnAbort;
	TLabeledEdit *LabeledEdit12;
	TTimer *TimerOVR2;
	TBitBtn *bbSvuotaR;
	TBitBtn *bbModificaR;
	TGridPanel *GridPanel1;
	TPanel *Panel13;
	TPanel *PanelOverride;
	TPanel *Panel5;
	TSpeedButton *sb5;
	TSpeedButton *sb20;
	TSpeedButton *sb50;
	TSpeedButton *sb80;
	TGroupBox *GroupBox1;
	TLabeledEdit *LabeledEdit1;
	TLabeledEdit *LabeledEdit2;
	TLabeledEdit *LabeledEdit3;
	TLabeledEdit *LabeledEdit4;
	TLabeledEdit *LabeledEdit5;
	TLabeledEdit *LabeledEdit6;
	TLabeledEdit *LabeledEdit8;
	TLabeledEdit *LabeledEdit9;
	TLabeledEdit *LabeledEdit10;
	TLabeledEdit *LabeledEdit7;
	TLabeledEdit *LabeledEdit13;
	TLabeledEdit *LabeledEdit14;
	TGroupBox *GroupBox3;
	TShape *Shape13;
	TLabel *Label13;
	TShape *Shape14;
	TLabel *Label14;
	TShape *Shape15;
	TLabel *Label15;
	TLabel *Label17;
	TShape *sAttrezzaggioPresente;
	TLabeledEdit *LabeledEdit11;
	TLabel *Label16;
	TShape *Shape16;
	TShape *Shape1;
	TLabel *Label1;
	TShape *Shape19;
	TLabel *Label19;
	TLabeledEdit *LabeledEdit15;
	void __fastcall TimerUpdTimer(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall TimerStartTimer(TObject *Sender);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall TimerOVRTimer(TObject *Sender);
	void __fastcall TimerOVR2Timer(TObject *Sender);
	void __fastcall TimerCallMasterTimer(TObject *Sender);
	void __fastcall BitBtnSTARTMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall BitBtnHOLDMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall BitBtnRESETMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall BitBtnCALLMASTERMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall BitBtnAbortMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall BitBtnHOLDMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall bbSvuotaRClick(TObject *Sender);
	void __fastcall bbModificaRClick(TObject *Sender);
	void __fastcall sb5Click(TObject *Sender);
	void __fastcall BitBtnRESETMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall BitBtnCALLMASTERMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
private:	// User declarations
public:		// User declarations
	__fastcall TRobotForm(TComponent* Owner);
	void ColoraShape(int stato, TShape *t, TColor ColorON = clLime, TColor ColorOFF = clGray);
	bool changing_override;
};
//---------------------------------------------------------------------------
extern PACKAGE TRobotForm *RobotForm;
//---------------------------------------------------------------------------
#endif
