//---------------------------------------------------------------------------

#pragma hdrstop
#include <IniFiles.hpp>
#include "Lotti.h"
#include "ModificaLotto.h"
#include "Main.h"
#include "StringTableID.h"
#include "ClientData.h"
#include "InsNum.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TLottiForm *LottiForm;
//---------------------------------------------------------------------------
__fastcall TLottiForm::TLottiForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	SelezionatiNonAttivi = false;
    Aggiorna();
}
//---------------------------------------------------------------------------

void TLottiForm::Aggiorna()
{
	AnsiString title;

	//aggiornamento a video delle tabelle
	dbgLottiAttivi->RefreshDataSet();
	dbgLottiSospesi->RefreshDataSet();
	dbgLottiAttivi->AutoFitAll();
	dbgLottiSospesi->AutoFitAll();

    if (SelezionatiNonAttivi) {
    	title.sprintf("Fasi lotto sospeso selezionato: pallet = %d", ADOQuery1ID_Pallet->AsInteger);
    }
    else {
    	title.sprintf("Fasi lotto attivo selezionato: pallet = %d", ADOQuery2ID_Pallet->AsInteger);
    }
	pFasi->Caption = title.c_str();
}
//---------------------------------------------------------------------------

void TLottiForm::AggiornaBtn()
{
	BitBtnAdd->Enabled 		        = (MainForm->pwdlevel > 0);
	BitBtnChange->Enabled 	        = (MainForm->pwdlevel > 0) && SelezionatiNonAttivi;
	BitBtnDelete->Enabled 	        = (MainForm->pwdlevel > 0) && SelezionatiNonAttivi;
	bResetLotto->Enabled 	        = (MainForm->pwdlevel > 0) && SelezionatiNonAttivi;
	BitBtnAttiva->Enabled	        = SelezionatiNonAttivi;
	BitBtnDisattivaLotto->Enabled	= !SelezionatiNonAttivi;
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::FormActivate(TObject *Sender)
{
	dbgLottiSospesiCellClick(NULL);
    AggiornaBtn();
	Timer1Timer(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::BitBtnAddClick(TObject *Sender)
{
	FormModificaLotto->ImpostaDati(-1);
	FormModificaLotto->ShowModal();
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::BitBtnChangeClick(TObject *Sender)
{
    //Log
    DBDataModule->Log("CLIENT", "LOTTI", "Modificato lotto sul pallet %d", ADOQuery1ID_Pallet->AsInteger);
	FormModificaLotto->ImpostaDati(ADOQuery1ID_Lotto->Value);
	FormModificaLotto->ShowModal();
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::BitBtnDeleteClick(TObject *Sender)
{
	int i;

	// Sicuro di voler cancellare il lotto?
	if ((Application->MessageBox(String::LoadStr(IDS_DELLOTTO).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	if (dbgLottiSospesi->SelectedRows->Count > 0) {
		for (i = 0; i < dbgLottiSospesi->SelectedRows->Count; i++) {
			dbgLottiSospesi->DataSource->DataSet->GotoBookmark(dbgLottiSospesi->SelectedRows->Items[i]);
			if (ADOQuery1ID_Lotto->AsString != "") {
            	//Log
            	DBDataModule->Log("CLIENT", "LOTTI", "Cancellato lotto sul pallet %d", ADOQuery1ID_Pallet->AsInteger);
				DBDataModule->CancellaLotto(ADOQuery1ID_Lotto->AsInteger, ADOQuery1ID_Lavoro->AsInteger);
			}
		}
		dbgLottiSospesi->SelectedRows->Clear();
	} else {
		// Verifica che non sia in uso
		if (ADOQuery1ID_Lotto->AsString != "") {
            //Log
            DBDataModule->Log("CLIENT", "LOTTI", "Cancellato lotto sul pallet %d", ADOQuery1ID_Pallet->AsInteger);
			DBDataModule->CancellaLotto(ADOQuery1ID_Lotto->AsInteger, ADOQuery1ID_Lavoro->AsInteger);
		}
	}
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (Shift.Contains(ssCtrl) && (Key == VK_DELETE))
		Key = 0; // ignore
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::Timer1Timer(TObject *Sender)
{
	Timer1->Enabled = false;
	AggiornaBtn();
	Timer1->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::FormDeactivate(TObject *Sender)
{
	Timer1->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::BitBtnAttivaClick(TObject *Sender)
{
	int i;
	TLavoriInCorso l;
	TPallet p;
	TPezzo pz;
	String sMsg;

	// Sicuro di voler attivare il lotto?
	if ((Application->MessageBox(String::LoadStr(IDS_ACTIVELOTTO).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	if (dbgLottiSospesi->SelectedRows->Count > 0) {
		for (i = 0; i < dbgLottiSospesi->SelectedRows->Count; i++) {
			dbgLottiSospesi->DataSource->DataSet->GotoBookmark(dbgLottiSospesi->SelectedRows->Items[i]);
			// Verifica che non sia in uso
			if (DBDataModule->PalletAssociatoLavorazione(ADOQuery1ID_Pallet->AsInteger)) {
				sMsg.printf(String::LoadStr(IDS_LAVGIAATTIVA).w_str() , ADOQuery1ID_Pallet->AsInteger);
				Application->MessageBox(sMsg.w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
			} else if (ADOQuery1ID_Lotto->Value > 0) {
				DBDataModule->LeggiProssimoLavoroInCorso(ADOQuery1ID_Lavoro ->Value, l);
				DBDataModule->LeggiPallet(ADOQuery1ID_Pallet->Value, p);
            	if (p.ID_Pallet > 0) {
                    DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
                    if ((l.Indice == pz.Stato_Pezzo) || ((l.Indice == STATO_PEZZO_GREZZO) && (pz.Stato_Pezzo == STATO_PEZZO_LAVORATO))) {
                        //Log
                        DBDataModule->Log("CLIENT", "LOTTI", "Attivato lotto sul pallet %d", ADOQuery1ID_Pallet->AsInteger);
                        DBDataModule->RiabilitaTutteFasiLotto(ADOQuery1ID_Lotto->Value);
                        //DBDataModule->PreimpostazioneLottoCustomEurofluid(ADOQuery1ID_Lotto->Value, "PRODUZIONE", 1003);
                        DBDataModule->RipristinaLavoriInCorsoDelLotto(ADOQuery1ID_Lotto->Value);
                        DBDataModule->AttivaLotto(ADOQuery1ID_Lotto->AsInteger);
                        DBDataModule->AttivaLavoroInCorso(ADOQuery1ID_Lavoro->AsInteger);

                    } else {
                        sMsg.printf(String::LoadStr(IDS_PALLNONOK).w_str(), ADOQuery1ID_Pallet->AsInteger);
                        Application->MessageBox(sMsg.w_str(), String::LoadStr(IDS_ALLARME).w_str(), MB_OK);
                    }
                }
			}
		}
		dbgLottiSospesi->SelectedRows->Clear();
	}
    else {
		// Verifica che non sia in uso
		if (DBDataModule->PalletAssociatoLavorazione(ADOQuery1ID_Pallet->AsInteger)) {
			sMsg.printf(L"Lavorazione gi� attiva per il pallet %d!", ADOQuery1ID_Pallet->AsInteger);
			Application->MessageBox(sMsg.w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
		} else if (ADOQuery1ID_Lotto->Value > 0) {
			DBDataModule->LeggiProssimoLavoroInCorso(ADOQuery1ID_Lavoro->Value, l);
			DBDataModule->LeggiPallet(ADOQuery1ID_Pallet->Value, p);
            if (p.ID_Pallet > 0) {
                DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
                if ((l.Indice == pz.Stato_Pezzo) || ((l.Indice == STATO_PEZZO_GREZZO) && (pz.Stato_Pezzo == STATO_PEZZO_LAVORATO))) {
                    //Log
                    DBDataModule->Log("CLIENT", "LOTTI", "Attivato lotto sul pallet %d", p.ID_Pallet);
                    DBDataModule->RiabilitaTutteFasiLotto(ADOQuery1ID_Lotto->Value);
                    DBDataModule->RipristinaLavoriInCorsoDelLotto(ADOQuery1ID_Lotto->Value);
                    DBDataModule->AttivaLotto(ADOQuery1ID_Lotto->AsInteger);
                    DBDataModule->AttivaLavoroInCorso(ADOQuery1ID_Lavoro->AsInteger);

                } else {
                    sMsg.printf(String::LoadStr(IDS_PALLNONOK).w_str(), p.ID_Pallet);
                    Application->MessageBox(sMsg.w_str(), String::LoadStr(IDS_ALLARME).w_str(), MB_OK);
                }
            }
		}
	}
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::dbgLottiAttiviDrawColumnCell(TObject *Sender, const TRect &Rect,
          int DataCol, TColumn *Column, TGridDrawState State)
{
	if (ADOQuery2->RecordCount == 0) {
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::dbgLottiSospesiCellClick(TColumn *Column)
{
	SelezionatiNonAttivi = true;
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::BitBtnDisattivaLottoClick(TObject *Sender)
{
	int i;

	// Sicuro di voler disattivare il lotto?
	if ((Application->MessageBox(String::LoadStr(IDS_DEACTIVELOTTO).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	if (dbgLottiAttivi->SelectedRows->Count > 0) {
		for (i = 0; i < dbgLottiAttivi->SelectedRows->Count; i++) {
			dbgLottiAttivi->DataSource->DataSet->GotoBookmark(dbgLottiAttivi->SelectedRows->Items[i]);
            //Controllo se non esista una missione in corso per quel pallet con lotto attivo
            if (ADOQuery2ID_Pallet->AsInteger == ClientData.Missione.N_pallet) {
				Application->MessageBox(String::LoadStr(IDS_DEACTLOTTO).w_str(), String::LoadStr(IDS_ALLARME).w_str(), MB_OK);
                continue;
            }
			if (ADOQuery2ID_Lotto->AsString != "") {
                //Log
                DBDataModule->Log("CLIENT", "LOTTI", "Disattivato lotto sul pallet %d", ADOQuery2ID_Pallet->AsInteger);
                DBDataModule->DisattivaLavoriInCorsoNonCompletati(ADOQuery2ID_Lavoro->AsInteger);
                DBDataModule->DisattivaLotto(ADOQuery2ID_Lotto->AsInteger);
                DBDataModule->CancellaJobPallet(ADOQuery2ID_Pallet->AsInteger);
			}
		}
		dbgLottiAttivi->SelectedRows->Clear();
	} else {
        //Controllo se non esista una missione in corso per quel pallet con lotto attivo
        if (ADOQuery2ID_Pallet->AsInteger == ClientData.Missione.N_pallet) {
			Application->MessageBox(String::LoadStr(IDS_DEACTLOTTO).w_str(), String::LoadStr(IDS_ALLARME).w_str(), MB_OK);
            return;
        }
		// Verifica che non sia in uso
		if (ADOQuery1ID_Lotto->AsString != "") {
            //Log
			DBDataModule->Log("CLIENT", "LOTTI", "Disattivato lotto sul pallet %d", ADOQuery2ID_Pallet->AsInteger);
            DBDataModule->DisattivaLavoriInCorsoNonCompletati(ADOQuery2ID_Lavoro->AsInteger);
            DBDataModule->DisattivaLotto(ADOQuery2ID_Lotto->AsInteger);
            DBDataModule->CancellaJobPallet(ADOQuery2ID_Pallet->AsInteger);
		}
	}
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::DBGridFasiDrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column, TGridDrawState State)
{
	if (ADOQuery3FaseCompletata->Value == 1) {
		DBGridFasi->Canvas->Brush->Color = clGreen;
	}
	else if (ADOQuery3ID_Job->Value != 0 && ADOQuery3ID_Missione->Value == 0) {
		DBGridFasi->Canvas->Brush->Color = clYellow;
	}
	else if (ADOQuery3ID_Missione->Value != 0) {
		DBGridFasi->Canvas->Brush->Color = clLime;
	}
	else {
		DBGridFasi->Canvas->Brush->Color = clBtnFace;
	}
}
//---------------------------------------------------------------------------

void TLottiForm::AggiornaDBGridFasi()
{
	AnsiString strsql;
	int idpallet, idlavoro;

	if (SelezionatiNonAttivi) {
		idpallet = ADOQuery1ID_Pallet->Value;
		idlavoro = ADOQuery1ID_Lavoro ->Value;
	} else {
		idpallet = ADOQuery2ID_Pallet->Value;
		idlavoro = ADOQuery2ID_Lavoro->Value;
	}
	ADOQuery3->Close();
	strsql.printf("Select * from FasiInLavorazione where ID_Pallet = %d AND ID_Lavoro = %d ORDER BY Indice", idpallet, idlavoro);
	ADOQuery3->SQL->Text = strsql;
	ADOQuery3->Open();
	DBGridFasi->AutoFitAll();
	DBGridFasi->Invalidate();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::ADOQuery2AfterScroll(TDataSet *DataSet)
{
	AggiornaDBGridFasi();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::dbgLottiAttiviCellClick(TColumn *Column)
{
	SelezionatiNonAttivi = false;
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::bResetLottoClick(TObject *Sender)
{
    TPallet p;
    TPezzo pz;

	if ((Application->MessageBox(String::LoadStr(IDS_RESETLOTTO).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;

	//Log
    DBDataModule->Log("CLIENT", "LOTTI", "Resettato lotto sul pallet %d", ADOQuery1ID_Pallet->AsInteger);
    DBDataModule->RiattivaLavoriInCorso(ADOQuery1ID_Lavoro->AsInteger);
    DBDataModule->LeggiPallet(ADOQuery1ID_Pallet->AsInteger, p);
    DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
    pz.Stato_Pezzo = 0;
    DBDataModule->AggiornaPezzo(pz);
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TLottiForm::dbgLottiAttiviDblClick(TObject *Sender)
{
	TLotto l;

	InsNumForm->Caption = String::LoadStr(IDS_INSVALUE).w_str();
	InsNumForm->Valore->EditLabel->Caption = dbgLottiAttivi->Columns->Items[8]->Title->Caption;
	InsNumForm->Valore->Text = ADOQuery2Priorita->Value;
	if (InsNumForm->ShowModal() == IDOK) {
		DBDataModule->LeggiLotto(ADOQuery2ID_Lotto->Value, l);
		l.Priority = InsNumForm->Valore->Text.ToIntDef(50);
		DBDataModule->AggiornaPrioritaLavoroInCorso(ADOQuery2ID_Lavoro->Value, l.Priority);
		DBDataModule->ModificaLotto(l);
	}
	dbgLottiAttivi->RefreshDataSet();
}
//---------------------------------------------------------------------------

