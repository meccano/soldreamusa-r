//---------------------------------------------------------------------------
#pragma hdrstop

#include <Vcl.Dialogs.hpp>
#include <IniFiles.hpp>
#include "Macchine.h"
#include "ClientData.h"
#include "Socket.h"
#include "PLCFunct.h"
#include "Robot.h"
#include "Main.h"
#include "InsNum.h"
#include "StringTableID.h"
#include "InsNum.h"
#include "SelectPallet.h"
#include "InsertPallet.h"
#include "NewJob.h"
#include "StringTableID.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TMacchineForm *MacchineForm;
//---------------------------------------------------------------------------
__fastcall TMacchineForm::TMacchineForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	int i;
	TIniFile *pIni;

	pIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
    for (i = 1; i <= N_MACCHINE_UT; i++) {
		m_PPFolderMU[i - 1] = pIni->ReadString("MU" + IntToStr(i), "PPFolder", "C:\\meccano\\Programmi\\");
    }
	delete pIni;

	PageControl1->TabIndex = 0;
	TimerUpdTimer(NULL);
}
//---------------------------------------------------------------------------

void TMacchineForm::ColoraShape(int stato, TShape *t, TColor ColorON, TColor ColorOFF)
{
	if (stato) {
		t->Brush->Color = ColorON;
	} else {
		t->Brush->Color = ColorOFF;
	}
}
//---------------------------------------------------------------------------

AnsiString AutDesc(int n) {
	switch(n) {
	case 0 : return "MDI";
	case 1 : return "MEMORY";
	case 2 : return "****";
	case 3 : return "EDIT";
	case 4 : return "HAANDLE";
	case 5 : return "JOG";
	case 6 : return "Teach in JOG";
	case 7 : return "Teach in HANDLE";
	case 8 : return "INC feed";
	case 9 : return "REFERENCE";
	case 10 : return "REMOTE";
	}
	return "?";
}
//---------------------------------------------------------------------------

AnsiString RunDesc(int n) {
	switch(n) {
	case 0 : return "****(reset)";
	case 1 : return "STOP";
	case 2 : return "HOLD";
	case 3 : return "START";
	case 4 : return "MSTR";
	}
	return "?";
}
//---------------------------------------------------------------------------

AnsiString MotionDesc(int n) {
	switch(n) {
	case 0 : return "***";
	case 1 : return "MOTION";
	case 2 : return "DWELL";
	}
	return "?";
}
//---------------------------------------------------------------------------

void __fastcall TMacchineForm::TimerUpdTimer(TObject *Sender)
{
	short i, n;
	AnsiString s, LetturaCampo;

	TimerUpd->Enabled = false;
    DBDataModule->CaricaTabellaK("ListaPalletForcaEsterna", "ID_Macchina", ListaMacchine);

	// MU1
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][0] & BIT1, sPalletSbloccato1);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][0] & BIT2, sPalletBloccato1);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][0] & BIT3, sFineCiclo1);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][0] & BIT4, sPortaAperta1);
	ColoraShape(ClientData.PLC.DB[DB_ALLARMI][1] & BIT3, sAllarme1, clRed);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][0] & BIT0, sAutomatico1);

	lePalletM1->Text = ListaMacchine[1]["ID_Pallet"];
	leCodiceM1->Text = ListaMacchine[1]["Codice_Articolo"];
	leDescM1->Text = ListaMacchine[1]["Programma"];
	leAut1->Text = AutDesc(ClientData.DatiMU[0].aut);
	leRun1->Text = RunDesc(ClientData.DatiMU[0].run);
	leMotion1->Text = MotionDesc(ClientData.DatiMU[0].motion);
	s.sprintf("O%04d",ClientData.DatiMU[0].running_prg);
	leProg1->Text = s;
	s.sprintf("O%04d",ClientData.DatiMU[0].main_prg);
	leMainProg1->Text = s;

	// MU2
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][1] & BIT2, sPalletSbloccato2);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][1] & BIT1, sPalletBloccato2);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][1] & BIT3, sFineCiclo2);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][1] & BIT4, sPortaAperta2);
	ColoraShape(ClientData.PLC.DB[DB_ALLARMI][1] & BIT4, sAllarme2, clRed);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][1] & BIT0, sAutomatico2);

	lePalletM2->Text = ListaMacchine[2]["ID_Pallet"];
	leCodiceM2->Text = ListaMacchine[2]["Codice_Articolo"];
	leDescM2->Text = ListaMacchine[2]["Programma"];
	leAut2->Text = AutDesc(ClientData.DatiMU[1].aut);
	leRun2->Text = RunDesc(ClientData.DatiMU[1].run);
	leMotion2->Text = MotionDesc(ClientData.DatiMU[1].motion);
	s.sprintf("O%04d",ClientData.DatiMU[1].running_prg);
	leProg2->Text = s;
	s.sprintf("O%04d",ClientData.DatiMU[1].main_prg);
	leMainProg2->Text = s;

    //MU3
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][2] & BIT2, sPalletSbloccato3);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][2] & BIT1, sPalletBloccato3);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][2] & BIT3, sFineCiclo3);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][2] & BIT4, sPortaAperta3);
	ColoraShape(ClientData.PLC.DB[DB_ALLARMI][1] & BIT5, sAllarme3, clRed);
	ColoraShape(ClientData.PLC.DB[DB_PLC_MU_IN][2] & BIT0, sAutomatico3);

	lePalletM3->Text = ListaMacchine[3]["ID_Pallet"];
	leCodiceM3->Text = ListaMacchine[3]["Codice_Articolo"];
	leDescM3->Text = ListaMacchine[3]["Programma"];
	leAut3->Text = AutDesc(ClientData.DatiMU[2].aut);
	leRun3->Text = RunDesc(ClientData.DatiMU[2].run);
	leMotion3->Text = MotionDesc(ClientData.DatiMU[2].motion);
	s.sprintf("O%04d",ClientData.DatiMU[2].running_prg);
	leProg3->Text = s;
	s.sprintf("O%04d",ClientData.DatiMU[2].main_prg);
	leMainProg3->Text = s;

	// Part programs
	TimerUpd->Enabled = true;

	// Abilitazione pulsanti
	bbSvuotaM1->Enabled = (MainForm->pwdlevel > 0);
	bbSvuotaM2->Enabled = (MainForm->pwdlevel > 0);
	bbSvuotaM3->Enabled = (MainForm->pwdlevel > 0);
	bbModificaM1->Enabled = (MainForm->pwdlevel > 0);
	bbModificaM2->Enabled = (MainForm->pwdlevel > 0);
	bbModificaM3->Enabled = (MainForm->pwdlevel > 0);
    btnJobCreateMU1->Enabled = ((lePalletM1->Text.ToIntDef(-1) > 0) && (MainForm->pwdlevel > 0));
	btnJobCreateMU2->Enabled = ((lePalletM2->Text.ToIntDef(-1) > 0) && (MainForm->pwdlevel > 0));
	btnJobCreateMU3->Enabled = ((lePalletM3->Text.ToIntDef(-1) > 0) && (MainForm->pwdlevel > 0));
	BitBtnStartMU1->Enabled = (MainForm->pwdlevel > 0);
	BitBtnStartMU2->Enabled = (MainForm->pwdlevel > 0);
	BitBtnStartMU3->Enabled = (MainForm->pwdlevel > 0);
}
//---------------------------------------------------------------------------

void __fastcall TMacchineForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (Shift.Contains(ssCtrl) && (Key == VK_DELETE))
		Key = 0; // ignore
}
//---------------------------------------------------------------------------

void __fastcall TMacchineForm::bbSvuotaMClick(TObject *Sender)
{
	TBitBtn *b;

	if (MainForm->pwdlevel > 0) {
		b = (TBitBtn*)Sender;
		DBDataModule->PalletInMacchina(b->Tag, "M", 0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMacchineForm::bbModificaMClick(TObject *Sender)
{
	TBitBtn *b;
	int res, idpallet;
	TPallet p;

	if (MainForm->pwdlevel > 0) {
		b = (TBitBtn*)Sender;
//		FormInsertPallet->FilterPalletType(1);
		res = FormInsertPallet->ShowModal();
		if (res == IDOK) {
			idpallet = FormInsertPallet->Selected;
			DBDataModule->LeggiPallet(idpallet, p);
			if (p.ID_Pallet > 0) {
				DBDataModule->PalletInMacchina(b->Tag, "M", idpallet);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMacchineForm::BitBtnJobCreateMClick(TObject *Sender)
{
   	TBitBtn *bb;

	bb = (TBitBtn*)Sender;
	if (bb->Tag == 1) {
		FormNewJob->SelectPallet(lePalletM1->Text.ToIntDef(0), 1, bb->Tag);
	} else if (bb->Tag == 2) {
		FormNewJob->SelectPallet(lePalletM2->Text.ToIntDef(0), 1, bb->Tag);
	} else if (bb->Tag == 3) {
		FormNewJob->SelectPallet(lePalletM3->Text.ToIntDef(0), 1, bb->Tag);
	}
	FormNewJob->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TMacchineForm::BitBtnSendProgramClick(TObject *Sender)
{
	AnsiString prg, s;
	TBitBtn *b;

	b = (TBitBtn*)Sender;
	OpenDialog1->InitialDir = m_PPFolderMU[b->Tag - 1];
	if (OpenDialog1->Execute() == IDOK) {
		prg = OpenDialog1->FileName;
        try {
            s.printf("MSGTYPE=SENDPROGRAM|MU=%d|PRG=%s|", b->Tag, prg.c_str());
            SocketDataModule->Invia(s);
        }
        catch(...) {}
        // LOG
        DBDataModule->Log("CLIENT", "MACCHINE", "MU%d: Invio Programma %s", b->Tag, prg.c_str());
	}
}
//---------------------------------------------------------------------------

void __fastcall TMacchineForm::BitBtnStartClick(TObject *Sender)
{
	AnsiString s;
	TBitBtn *b;

	b = (TBitBtn*)Sender;

	try {
        //Formatto la stringa da inviare al socket
        s.printf("MSGTYPE=STARTPROGRAM|MU=%d|", b->Tag);
        SocketDataModule->Invia(s);
    }
    catch(...) {}
    // LOG
    DBDataModule->Log("CLIENT", "MACCHINE", "MU%d: Start Programma", b->Tag);
}
//---------------------------------------------------------------------------

