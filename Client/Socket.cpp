//---------------------------------------------------------------------------
#pragma hdrstop

#include <inifiles.hpp>
#include "Socket.h"
#include "Main.h"
#include "ClientData.h"
#include "LogTxt.h"
#include "util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSocketDataModule *SocketDataModule;
TClientData ClientData;

//---------------------------------------------------------------------------
__fastcall TSocketDataModule::TSocketDataModule(TComponent* Owner)
	: TDataModule(Owner)
{
	int Port;
	AnsiString ServerAddress;
	TIniFile *Ini;

	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	Port = Ini->ReadInteger("Socket", "PortNumber", 11000);
	if (Port != 0)
		ClientSocket->Port = Port;
	else
		Ini->WriteInteger("Socket", "PortNumber", ClientSocket->Port);
	ServerAddress = Ini->ReadString("Socket", "ServerAddress", "");
	if (ServerAddress != "")
		ClientSocket->Address = ServerAddress;
	else
		Ini->WriteString("Socket", "ServerAddress", ClientSocket->Address);
	connected = false;
	delete Ini;
}
//---------------------------------------------------------------------------
void TSocketDataModule::Connetti() {
	try {
		ClientSocket->Open();
		connected = true;
		readbytes = 0;
		TimerTimeout->Enabled = true;
	} catch(...) {
		Disconnetti();
		connected = false;
	}
}
//---------------------------------------------------------------------------

void TSocketDataModule::Disconnetti() {
	ClientSocket->Close();
	connected = false;
	TimerTimeout->Enabled = false;
}
//---------------------------------------------------------------------------

bool TSocketDataModule::SeiConnesso() {
	return ClientSocket->Active;
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::DataModuleDestroy(TObject *Sender)
{
	Disconnetti();
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::TimerConnectedTimer(TObject *Sender)
{
	TimerConnected->Enabled = false;
//	connected = SeiConnesso();
	if (!connected) {
		Connetti();
	}
	TimerTimeout->Enabled = true;
	TimerConnected->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::ClientSocketError(TObject *Sender,
	  TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
	Disconnetti();
	ErrorCode = 0;
    LogTxt("ClientSocketError");
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::DataModuleCreate(TObject *Sender)
{
	Connetti();
	TimerConnected->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::TimerTimeoutTimer(TObject *Sender)
{
	Disconnetti();
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::ClientSocketRead(TObject *Sender,
	  TCustomWinSocket *Socket)
{
	int nletti, ncopiati, nresto, startpos, scartati;
	TClientData buf;
	char *p1, *p2, *start, *t;
	bool extradata;

	connected = true;
	TimerTimeout->Enabled = false;
	try {
		nletti = Socket->ReceiveBuf(&buf, sizeof(TClientData));
		start = (char *)(&buf);
		// nletto sono i caratteri ricevuti nell'ultima lettura (questa che sto elaborando)
		// readbytes sono i caratteri letti fino a questo momento (anche in pi� letture precedenti)
		if (readbytes == 0) {
			// Se non ho ancora letto nulla in precedenza devo iniziare con un inizio pacchetto, identificabile da "SYNC"
			// Aspetto un inizio pacchetto, cerco l'ultimo in coda (se ne ho diversi all'interno dello stesso telegramma
			// tanto vale prendere l'ultimo che � il pi� aggiornato
			t = strstr((char *)(&buf), "SYNC");
			if (t != NULL) {
				// Se trovo un inizio pacchetto lo memorizzo in start
				start = t;
			}
			// Controllo se ci sono altri sync per prendere l'ultimo
			while (t != NULL) {
				t++; // inizio la ricerca dal carattere successivo
				t = strstr(t, "SYNC");
				if (t != NULL) {
					start = t;
				}
			}
			// A questo punto in start mi trovo il puntatore all'inizio dell'ultimo pacchetto
			scartati = start - (char *)(&buf);
			nletti -= scartati;
		}
		if (((readbytes > 0) && (nletti > 4)) || ((nletti >= 4) && (start[0] == 'S') && (start[1] == 'Y') && (start[2] == 'N') && (start[3] == 'C'))) {
			p1 = start;
			p2 = (char*)&ClientData;
			p2 = p2 + readbytes;
			extradata = ((UINT)(readbytes + nletti) > sizeof(ClientData));
			// se ho ricevuto pi� caratteri di quelli che mancano alla fine di un pacchetto
			// leggo solo quelli che servono
			if (extradata) {
				ncopiati = sizeof(ClientData) - readbytes;
				nresto = nletti - ncopiati;
			} else {
				ncopiati = nletti;
				nresto = 0;
			}
			memcpy(p2, p1, ncopiati);
			// Se ho completato un pacchetto faccio il refresh dati
			if ((UINT)(readbytes + ncopiati) == sizeof(ClientData)) {
				readbytes = 0;
				MainForm->MainStatusBar->Refresh();
			} else {
				readbytes = readbytes + nletti;
			}
			// Se restano dei caratteri me ne sbatto
		}
	} catch(...) {};
}
//---------------------------------------------------------------------------

void TSocketDataModule::Invia(AnsiString telegramma)
{
	if (SeiConnesso()) {
		ClientSocket->Socket->SendText(telegramma);
		Sleep(250);
    }
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::ClientSocketDisconnect(TObject *Sender, TCustomWinSocket *Socket)
{
	Disconnetti();
}
//---------------------------------------------------------------------------


void TSocketDataModule::SetBitPLC(int db, int dw, int bit)
{
	AnsiString s;

    s.printf("MSGTYPE=SETBITPLC|DB=%d|DW=%d|BIT=%d|", db, dw, bit);
    Invia(s);
}
//---------------------------------------------------------------------------

void TSocketDataModule::ResetBitPLC(int db, int dw, int bit)
{
	AnsiString s;

    s.printf("MSGTYPE=RESETBITPLC|DB=%d|DW=%d|BIT=%d|", db, dw, bit);
    Invia(s);
}
//---------------------------------------------------------------------------

void TSocketDataModule::WriteWordPlc(int db, int dw, int val)
{
	AnsiString s;

    s.printf("MSGTYPE=WRITEWORDPLC|DB=%d|DW=%d|VAL=%ld|", db, dw, val);
    Invia(s);
}
//---------------------------------------------------------------------------

void TSocketDataModule::WriteRealPlc(int db, int dw, float val)
{
	AnsiString s;

    s.printf("MSGTYPE=WRITEDWORDPLC|DB=%d|DW=%d|VAL=%ld|", db, dw, dtol(val));
    Invia(s);
}
//---------------------------------------------------------------------------

void TSocketDataModule::WriteDWordPlc(int db, int dw, long val)
{
	AnsiString s;

    s.printf("MSGTYPE=WRITEDWORDPLC|DB=%d|DW=%d|VAL=%ld|", db, dw, val);
    Invia(s);
}
//---------------------------------------------------------------------------


