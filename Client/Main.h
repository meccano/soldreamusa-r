//----------------------------------------------------------------------------
#ifndef MainH
#define MainH
//----------------------------------------------------------------------------
#include "ChildWin.h"
#include <System.Classes.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.GIFImg.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.Imaging.jpeg.hpp>
//#include <System.ImageList.hpp>
//----------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:
	TToolBar *ToolBar2;
	TPanel *Panel1;
	TImage *Image3;
	TImage *Image4;
	TBitBtn *BitBtnEsci;
	TBitBtn *BitBtnPwd;
	TBitBtn *BitBtnProduzione;
	TStatusBar *MainStatusBar;
	TBitBtn *BitBtnRobot;
	TBitBtn *bbMagazzini;
	TTimer *TimerPwd;
	TTimer *TimerOra;
	TImageList *ImageList1;
	TBitBtn *BitBtnStorico;
	TImageList *ImageList2;
	TBitBtn *BitBtnStazioni;
	TBitBtn *BitBtnMacchine;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtnArticoli;
	TBitBtn *BitBtn2;
	TBitBtn *BitBtn3;
	TPopupMenu *PopupMenu1;
	TImageList *ImageList3;
	TMenuItem *Italiano1;
	TMenuItem *Inglese1;
	TMenuItem *Portoghese1;
	TBitBtn *BitBtnLotti;
	TBitBtn *BitBtn4;
	TImage *Image1;
	TImage *Image2;
	TImage *Image5;
	TImage *Image6;
	void __fastcall BitBtnEsciClick(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall TimerOraTimer(TObject *Sender);
	void __fastcall TimerPwdTimer(TObject *Sender);
	void __fastcall BitBtnPwdClick(TObject *Sender);
	void __fastcall MainStatusBarDrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const TRect &Rect);
	void __fastcall BitBtnStoricoClick(TObject *Sender);
	void __fastcall MainStatusBarClick(TObject *Sender);
	void __fastcall BitBtnRobotClick(TObject *Sender);
	void __fastcall bbMagazziniClick(TObject *Sender);
	void __fastcall Image4Click(TObject *Sender);
	void __fastcall BitBtnMacchineClick(TObject *Sender);
	void __fastcall BitBtnArticoliClick(TObject *Sender);
	void __fastcall BitBtnStazioniClick(TObject *Sender);
	void __fastcall BitBtn3Click(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall Inglese1Click(TObject *Sender);
	void __fastcall BitBtnProduzioneClick(TObject *Sender);
	void __fastcall BitBtnLavorazioniClick(TObject *Sender);
	void __fastcall BitBtnLottiClick(TObject *Sender);
private:
	template <class T> void __fastcall CreateMDIChild(T *form, String Name, String Caption);
public:
	virtual __fastcall TMainForm(TComponent *Owner);
	void __fastcall AfterConstruction(void);
	void SetVisible();
	void CambiaLingua(int lang, bool warning = true);
	void SavePassword();
	void LoadPassword();
	void UpdControls();
	int pwdlevel, all;
	AnsiString PWD;
	int TouchPanel, stationnumber, onlyelectrode, EnableManuals;
	AnsiString msg, timestr;
};
//----------------------------------------------------------------------------
extern TMainForm *MainForm;
extern TMDIChild *__fastcall MDIChildCreate(void);
//----------------------------------------------------------------------------
#endif    
