//---------------------------------------------------------------------------

#ifndef InsertPalletH
#define InsertPalletH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MyDBGrid.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
//---------------------------------------------------------------------------
class TFormInsertPallet : public TForm
{
__published:	// IDE-managed Components
	TGridPanel *GridPanel1;
	TMyDBGrid *MyDBGrid1;
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TADOQuery *ADOQuery1;
	TDataSource *DataSource1;
	TIntegerField *ADOQuery1ID_Pallet;
	TStringField *ADOQuery1Codice_Tipopallet;
	TIntegerField *ADOQuery1Stato_Pallet;
	TStringField *ADOQuery1Codice_Articolo;
	TStringField *ADOQuery1Descrizione_Articolo;
	TIntegerField *ADOQuery1ID_TipoPallet;
	TStringField *ADOQuery1Descrizione_Pallet;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall MyDBGrid1CellClick(TColumn *Column);
private:	// User declarations
public:		// User declarations
	__fastcall TFormInsertPallet(TComponent* Owner);
	void FilterPalletType(int tp);
	void FilterTypePresent();
	void UnFilterPalletType();
	int Selected;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormInsertPallet *FormInsertPallet;
//---------------------------------------------------------------------------
#endif
