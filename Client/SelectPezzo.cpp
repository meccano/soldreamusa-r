//---------------------------------------------------------------------------

#pragma hdrstop

#include "SelectPezzo.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TFormSelectPezzo *FormSelectPezzo;
//---------------------------------------------------------------------------
__fastcall TFormSelectPezzo::TFormSelectPezzo(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormSelectPezzo::FormClose(TObject *Sender, TCloseAction &Action)
{
	ADOQuery1->Close();
}
//---------------------------------------------------------------------------
void __fastcall TFormSelectPezzo::MyDBGrid1CellClick(TColumn *Column)
{
	Selected = ADOQuery1ID_Pezzo->Value;
	if (Selected)
		BitBtnOK->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TFormSelectPezzo::FormShow(TObject *Sender)
{
	Selected = false;
	BitBtnOK->Enabled = false;
	ADOQuery1->Filtered = false;
	ADOQuery1->Open();
	MyDBGrid1->AutoFitAll();
}
//---------------------------------------------------------------------------
