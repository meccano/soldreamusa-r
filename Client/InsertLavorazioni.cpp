//---------------------------------------------------------------------------

#pragma hdrstop

#include "InsertLavorazioni.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TFormInsertLavorazioni *FormInsertLavorazioni;
//---------------------------------------------------------------------------
__fastcall TFormInsertLavorazioni::TFormInsertLavorazioni(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormInsertLavorazioni::FormClose(TObject *Sender, TCloseAction &Action)
{
	ADOQuery1->Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormInsertLavorazioni::FormShow(TObject *Sender)
{
	Selected = false;
	BitBtnOK->Enabled = false;
	ADOQuery1->Open();
	MyDBGrid1->AutoFitAll();
}
//---------------------------------------------------------------------------
void __fastcall TFormInsertLavorazioni::MyDBGrid1CellClick(TColumn *Column)
{
	Selected = ADOQuery1ID_Lavorazione->Value;
	if (Selected)
		BitBtnOK->Enabled = true;
}
//---------------------------------------------------------------------------

void TFormInsertLavorazioni::FilterPalletType(int tp)
{
	ADOQuery1->Filter = "ID_Lavorazione = " + IntToStr(tp);
	ADOQuery1->Filtered = true;
}
//---------------------------------------------------------------------------

void TFormInsertLavorazioni::UnFilterPalletType()
{
	ADOQuery1->Filter = "";
	ADOQuery1->Filtered = false;
}
//---------------------------------------------------------------------------


