//---------------------------------------------------------------------------
#pragma hdrstop

#include <IniFiles.hpp>
#include "Main.h"
#include "DB.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDBDataModule *DBDataModule;
AnsiString logstr;
//---------------------------------------------------------------------------

Variant ReadField(TADOQuery *q, AnsiString f)
{
	Variant res = "";

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->Value;
	return res;
}
//---------------------------------------------------------------------------

int ReadInt(TADOQuery *q, AnsiString f)
{
	int res = 0;

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->Value;
	return res;
}
//---------------------------------------------------------------------------

AnsiString ReadString(TADOQuery *q, AnsiString f)
{
	AnsiString res = "";

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->AsString;
	return res;
}
//---------------------------------------------------------------------------

float ReadFloat(TADOQuery *q, AnsiString f)
{
	float res = 0.0;

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->AsFloat;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaCampoTabella(AnsiString Tabella, AnsiString CampoChiave, int ValoreChiave, AnsiString CampoModifica, AnsiString ValoreModifica, bool isstring) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;
	AnsiString Val;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		if (isstring) {
			Val = "'" + ValoreModifica + "'";
		} else {
			Val = ValoreModifica;
		}
		strsql.printf("UPDATE %s SET %s = %s WHERE %s = %d",
			Tabella.c_str(),
			CampoModifica.c_str(),
			Val.c_str(),
			CampoChiave.c_str(),
			ValoreChiave
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaCampoTabella(AnsiString Tabella, AnsiString CampoChiave, AnsiString ValoreChiave, AnsiString CampoModifica, AnsiString ValoreModifica, bool isstring) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;
	AnsiString Val;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		if (isstring) {
			Val = "'" + ValoreModifica + "'";
		} else {
			Val = ValoreModifica;
		}
		strsql.printf("UPDATE %s SET %s = %s WHERE %s = '%s'",
			Tabella,
			CampoModifica,
			Val,
			CampoChiave,
			ValoreChiave
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

__fastcall TDBDataModule::TDBDataModule(TComponent* Owner)
	: TDataModule(Owner)
{
	TIniFile *Ini;
	AnsiString ConnectionString;
	TCursor Save_Cursor = Screen->Cursor;

	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	Screen->Cursor = crHourGlass;    // Show hourglass cursor
	ADOConnection1->Close();
	ConnectionString = Ini->ReadString("Database", "ConnectionString", "");
	delete Ini;
	if (ConnectionString != "")
		ADOConnection1->ConnectionString = ConnectionString;
	try {
		ADOConnection1->Open();
//		AcquisisciSegnalazioni();
	} catch (...) {}
	Screen->Cursor = Save_Cursor;
	TimerDBCheck->Enabled = true;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiSegnalazioneAttiva(AnsiString &msg, int &all) {

	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		msg = "";
		all = 0;
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP (1) * FROM Segnalazioni WHERE Acquisito IS NULL ORDER BY Allarme DESC, DataOra");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			msg = ReadString(ADOQuery, "Messaggio");
			all = ReadInt(ADOQuery, "Allarme");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AcquisisciSegnalazioneAttiva() {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP 1 * FROM Segnalazioni WHERE Acquisito IS NULL ORDER BY Allarme DESC, DataOra");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ADOQuery->Edit();
			ADOQuery->FieldByName("Acquisito")->AsDateTime = Now();
			ADOQuery->Post();
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AcquisisciSegnalazioni() {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Segnalazioni SET Acquisito = GetDate() WHERE Acquisito IS NULL");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::Segnalazione(AnsiString msg, int all) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP 1 * FROM Segnalazioni WHERE (Messaggio = '%s') AND (Acquisito IS NULL)", msg);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			// Messaggio gi� inviato
			ADOQuery->Close();
			delete ADOQuery;
			return;
		}
		ADOQuery->Close();
		strsql.printf("INSERT INTO Segnalazioni (Messaggio, Allarme) VALUES ('%s', %d)", msg, all);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void __fastcall TDBDataModule::TimerDBCheckTimer(TObject *Sender)
{
	TimerDBCheck->Enabled = false;
	try {
		if (!ADOConnection1->Connected) {
			ADOConnection1->Open();
		}
	} catch(...) {};
	TimerDBCheck->Enabled = true;
}
//---------------------------------------------------------------------------

int TDBDataModule::GetSelfGenID(AnsiString TableName) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT IDENT_CURRENT( '%s' ) AS ID", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiPallet(int ID_Pallet, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	memset(&p, 0, sizeof(TPallet));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pallet	= ReadInt(ADOQuery, "ID_Pallet");
			p.ID_TipoPallet	= ReadInt(ADOQuery, "ID_TipoPallet");
			p.Alias	= ReadString(ADOQuery, "Alias");
			p.Descrizione_Pallet = ReadString(ADOQuery, "Descrizione_Pallet");
			p.ID_Pezzo = ReadInt(ADOQuery, "ID_Pezzo");
			p.Stato_Pallet = ReadInt(ADOQuery, "Stato_Pallet");
			p.Programma	= ReadString(ADOQuery, "Programma");
			p.Extra_Altezza	= ReadInt(ADOQuery, "Extra_Altezza");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiArticolo(int ID_Articolo, TArticolo &a) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	memset(&a, 0, sizeof(TArticolo));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Articoli WHERE ID_Articolo = %d", ID_Articolo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			a.ID_Articolo                  = ReadInt(ADOQuery, "ID_Articolo");
			a.ID_TipoPezzo                 = ReadInt(ADOQuery, "ID_TipoPezzo");
			a.Codice_Articolo              = ReadString(ADOQuery, "Codice_Articolo");
			a.Descrizione_Articolo         = ReadString(ADOQuery, "Descrizione_Articolo");
			a.DiamentroPezzo_Semilavorato  = ReadFloat(ADOQuery, "Diametro_Pz_SemiL");
			a.DiamentroPezzo_Grezzo	   	   = ReadFloat(ADOQuery, "Diametro_Pezzo_Grezzo");
			a.AltezzaPezzo	   	           = ReadFloat(ADOQuery, "Altezza_Pz");
			a.ftc                          = ReadInt(ADOQuery, "ftc_Controllo");
			a.Faccia_Pinza                 = ReadInt(ADOQuery, "Faccia_Pinza");
			a.TStazionamento			   = ReadInt(ADOQuery, "TStazionamento");
			res = 1;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiArticoloDaCodice(AnsiString Codice_Articolo, TArticolo &a) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	memset(&a, 0, sizeof(TArticolo));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Articoli WHERE Codice_Articolo = '%s'", Codice_Articolo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			a.ID_Articolo                  = ReadInt(ADOQuery, "ID_Articolo");
			a.ID_TipoPezzo                 = ReadInt(ADOQuery, "ID_TipoPezzo");
			a.Codice_Articolo              = ReadString(ADOQuery, "Codice_Articolo");
			a.Descrizione_Articolo         = ReadString(ADOQuery, "Descrizione_Articolo");
			a.DiamentroPezzo_Semilavorato  = ReadFloat(ADOQuery, "Diametro_Pz_SemiL");
			a.DiamentroPezzo_Grezzo	   	   = ReadFloat(ADOQuery, "Diametro_Pezzo_Grezzo");
			a.AltezzaPezzo	   	           = ReadFloat(ADOQuery, "Altezza_Pz");
			a.ftc                          = ReadInt(ADOQuery, "ftc_Controllo");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaArticolo(int ID_Articolo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Articoli WHERE ID_Articolo = %d AND (NOT EXISTS (SELECT * FROM Pezzi WHERE (Articoli.ID_Articolo = ID_Articolo)))", ID_Articolo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaPallet(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Pallet WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		strsql.printf("DELETE FROM Missioni WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		strsql.printf("DELETE FROM Jobs WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaLavorazione(TLavorazione strLavor)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Lavorazioni "
					  "(ID_Articolo, Codice, Descrizione) "
					  "VALUES (%d, '%s', '%s') ",
			strLavor.ID_Articolo,
			strLavor.Codice.c_str(),
			strLavor.Descrizione.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	}
    catch(Exception &E) {};
	delete ADOQuery;
	return GetSelfGenID("Lavorazioni");
}
//---------------------------------------------------------------------------

int TDBDataModule::ModificaLavorazione(TLavorazione strLavor)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Lavorazioni "
					  "SET ID_Articolo = %d, Codice = '%s', Descrizione = '%s' "
					  "WHERE (ID_Lavorazione = %d) ",
			strLavor.ID_Articolo,
			strLavor.Codice.c_str(),
			strLavor.Descrizione.c_str(),
			strLavor.ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	}
	catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::ClonaLavorazione(int ID_Lavorazione)
{
	AnsiString strsql;
    TFase strFase;
    TLavorazione strLavor;
	TADOQuery *ADOQuery;
	int NewIdLavor = -1;

	try {
        //Leggo la lavorazione in corso
        LeggiLavorazione(ID_Lavorazione, strLavor);

        //Creo una nuova lavorazione
        NewIdLavor = CreaLavorazione(strLavor);

        //Controllo che la creazione sia andata a buon fine
        if (NewIdLavor > 0) {
            ADOQuery = new TADOQuery(NULL);
            ADOQuery->Connection = ADOConnection1;

            strsql.printf("SELECT * FROM Fasi WHERE ID_Lavorazione = %d", ID_Lavorazione);
            ADOQuery->SQL->Text = strsql;
            ADOQuery->Open();

            while (!ADOQuery->Eof) {
                strFase.ID_Lavorazione  	= NewIdLavor;
                strFase.Codice 				= ReadString(ADOQuery, "Codice");
                strFase.Descrizione 		= ReadString(ADOQuery, "Descrizione");
                strFase.ID_Operazione		= ReadInt(ADOQuery, "ID_Operazione");
                strFase.ID_Macchina 		= ReadInt(ADOQuery, "ID_Macchina");
                strFase.Tipo_Macchina 		= ReadString(ADOQuery, "Tipo_Macchina");
                strFase.Programma_MU 		= ReadString(ADOQuery, "Programma_MU");
                strFase.ID_TipoPallet   	= ReadInt(ADOQuery, "ID_TipoPallet");
                strFase.MessaggioOp 		= ReadString(ADOQuery, "Messaggio_Op");
                strFase.OrdineSeq       	= ReadInt(ADOQuery, "Ordine_Seq");
                strFase.Abilitazione		= ReadInt(ADOQuery, "Abilitazione");

                //Creo la nuova fase
                CreaFase(strFase);

                //Passo al record successivo
                ADOQuery->Next();
            }
            ADOQuery->Close();
        }
	}
    catch(Exception &E) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiLavorazione(int ID_Lavorazione, TLavorazione &strLavor)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = false;

	memset(&strLavor, 0, sizeof(TLavorazione));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lavorazioni WHERE ID_Lavorazione = %d", ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			strLavor.ID_Lavorazione = ReadInt(ADOQuery, "ID_Lavorazione");
			strLavor.ID_Articolo    = ReadInt(ADOQuery, "ID_Articolo");
			strLavor.Codice			= ReadString(ADOQuery, "Codice");
			strLavor.Descrizione	= ReadString(ADOQuery, "Descrizione");
			res = true;
		}
		ADOQuery->Close();
	}
	catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaLavorazione(int ID_Lavorazione)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;

        //Cancello tutte le fasi associate ad una lavorazione
		strsql.printf("DELETE FROM Fasi WHERE ID_Lavorazione = %d", ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();

        //Cancello la lavorazione
		strsql.printf("DELETE FROM Lavorazioni WHERE ID_Lavorazione = %d", ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	}
    catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LavorazioneInUso(int ID_Lavorazione)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Fasi WHERE ID_Lavorazione = %d", ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof)
			res = 1;

		ADOQuery->Close();
	}
    catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaLotto(TLotto &l)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
    	//Creo la lavorazione in corso da associare al lotto
		l.ID_Lavoro = DBDataModule->CreaLavoriInCorso(l.ID_Pallet, l.ID_Lavorazione, !l.Sospeso, l.Priority);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Lotti (Codice, Descrizione, ID_Lavorazione, ID_Lavoro, Qta, Sospeso, Priorita, Qta_Prodotta, ID_Pallet) "
					  "VALUES ('%s', '%s', %d, %d, %d, %d, %d, %d, %d) ",
			l.Codice.c_str(),
			l.Descrizione.c_str(),
			l.ID_Lavorazione,
            l.ID_Lavoro,
			l.Qta,
			l.Sospeso,
			l.Priority,
			l.QtaProdotta,
            l.ID_Pallet
            );
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	}
    catch(Exception &E) {};
	delete ADOQuery;
	return GetSelfGenID("Lotti");
}
//---------------------------------------------------------------------------

int TDBDataModule::ModificaLotto(TLotto &l)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Lotti "
					  "SET Codice = '%s', Descrizione = '%s', ID_Lavorazione = %d, Qta = %d, Priorita = %d, Sospeso = %d, ID_Pallet = %d, Qta_Prodotta = %d "
					  "WHERE (ID_Lotto = %d) ",
			l.Codice.c_str(),
			l.Descrizione.c_str(),
			l.ID_Lavorazione,
			l.Qta,
            l.Priority,
            l.Sospeso,
			l.ID_Pallet,
			l.QtaProdotta,
			l.ID_Lotto
            );
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	}
	catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiLotto(int ID_Lotto, TLotto &l)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	memset(&l, 0, sizeof(TLotto));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lotti WHERE ID_Lotto = %d", ID_Lotto);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			l.ID_Lotto 		 = ReadInt(ADOQuery, "ID_Lotto");
			l.Codice		 = ReadString(ADOQuery, "Codice");
			l.Descrizione	 = ReadString(ADOQuery, "Descrizione");
			l.ID_Lavorazione = ReadInt(ADOQuery, "ID_Lavorazione");
			l.Qta			 = ReadInt(ADOQuery, "Qta");
			l.QtaProdotta	 = ReadInt(ADOQuery, "Qta_Prodotta");
			l.Sospeso	 	 = ReadInt(ADOQuery, "Sospeso");
            l.ID_Pallet		 = ReadInt(ADOQuery, "ID_Pallet");
            l.ID_Lavoro		 = ReadInt(ADOQuery, "ID_Lavoro");
			l.Priority	 	 = ReadInt(ADOQuery, "Priorita");
		}
		ADOQuery->Close();
	}
    catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaLotto(int ID_Lotto, int ID_Lavoro) {
	TLotto l;
	TPallet p;
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;

        //Leggo il lotto
        LeggiLotto(ID_Lotto, l);

        //Leggo il pallet associato al lotto
        LeggiPallet(l.ID_Pallet, p);

        //Elimino il pezzo dal pallet
        if (p.ID_Pallet != 0) {
            SvuotaPallet(p);
        }

		//Cancello le lavorazioni associate
		strsql.printf("DELETE FROM LavoriInCorso WHERE ID_Lavoro = %d", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();

		//Cancello la lavorazione
		strsql.printf("DELETE FROM Lotti WHERE ID_Lotto = %d", ID_Lotto);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	}
    catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::VerificaSeLottoAttivo(int ID_Lotto) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT ID_Lotto FROM Lotti WHERE ID_Lotto = %d AND Sospeso = 0", ID_Lotto);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		res = ADOQuery->RecordCount;
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AttivaLotto(int ID_Lotto) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Lotti SET Sospeso = 0 WHERE (ID_Lotto = %d) ", ID_Lotto);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::DisattivaLavoriInCorsoNonCompletati(int ID_Lavoro) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		strSQLUpdate.printf("UPDATE LavoriInCorso SET LavoroAttivo = 0, ID_Job = 0, ID_Missione = 0 WHERE ID_Lavoro = %d AND FaseCompletata = 0", ID_Lavoro);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::DisattivaLotto(int ID_Lotto) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Lotti SET Sospeso = 1 WHERE (ID_Lotto = %d) ", ID_Lotto);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
        if (res > 0) {
            strsql.printf("DELETE Jobs WHERE ID_Lotto = %d", ID_Lotto);
            ADOQuery->SQL->Text = strsql;
            ADOQuery->ExecSQL();
            strsql.printf("DELETE Missioni WHERE ID_Lotto = %d", ID_Lotto);
            ADOQuery->SQL->Text = strsql;
            ADOQuery->ExecSQL();
        	//Log per disattivazione di un lotto
			LogDisattivazioneLotto(ID_Lotto);
        }
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------


int TDBDataModule::PalletAssociatoLottoAttivo(int ID_Pallet)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lotti WHERE (ID_Pallet = %d) AND (Sospeso = 0)", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if(!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Lotto");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LogAttivazioneLotto(int ID_Lotto) {
	TADOQuery *ADOQuery;
	AnsiString strsql;
	TLotto l;
    TLavorazione lav;
	TArticolo a;
	int res = 0;

	try {
		LeggiLotto(ID_Lotto, l);
		LeggiLavorazione(l.ID_Lavorazione, lav);
		LeggiArticolo(lav.ID_Articolo, a);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO StoricoLotti (ID_Lotto, Attivazione, QtaAttivazione, CodiceLotto, DescrizioneLotto, ID_Pallet, CodiceArticolo, DescrizioneArticolo) "
					  "VALUES (%d, GetDate(), %d, '%s', '%s', %d, '%s', '%s')",
					  ID_Lotto,
					  l.QtaProdotta,
					  l.Codice.c_str(),
					  l.Descrizione.c_str(),
                      l.ID_Pallet,
					  a.Codice_Articolo.c_str(),
					  a.Descrizione_Articolo.c_str()
					  );
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LogDisattivazioneLotto(int ID_Lotto) {
	TADOQuery *ADOQuery;
	AnsiString strsql;
	TLotto l;
	int res = 0;

	try {
		LeggiLotto(ID_Lotto, l);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE StoricoLotti SET Disattivazione = GetDate(), QtaDisattivazione = %d WHERE (ID_Lotto = %d) AND (Disattivazione IS NULL)", l.QtaProdotta, ID_Lotto);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
    return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::RiabilitaTutteFasiLavorazione(int ID_Lavorazione) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Fasi SET Abilitazione = 1 WHERE (ID_Lavorazione = %d)", ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::RiabilitaTutteFasiLotto(int ID_Lotto) {
	TLotto l;

	LeggiLotto(ID_Lotto, l);
	RiabilitaTutteFasiLavorazione(l.ID_Lavorazione);
}
//---------------------------------------------------------------------------

int TDBDataModule::RipristinaLavoriInCorsoDelLotto(int ID_Lotto)
{
    TLavoriInCorso lav;
	TRecordList TabFasi;
	AnsiString strsql;
	TLotto l;
	int lavoroattivo = 0;
	int i;

	LeggiLotto(ID_Lotto, l);
	strsql.printf("FasiLavorazione where (ID_Lavorazione = %d AND Abilitazione = 1) ORDER BY Ordine_Seq", l.ID_Lavorazione);
	DBDataModule->CaricaTabella(strsql, TabFasi);
	for (i = TabFasi.size() - 1; i >= 0 ; i--) {
        memset(&lav, 0, sizeof(lav));
		lav.ID_Lavoro = l.ID_Lavoro;
		lav.Indice = i;
		lav.ID_Lavorazione = TabFasi[i]["ID_Lavorazione"].ToIntDef(0);
		lav.ID_Fase = TabFasi[i]["ID_Fase"].ToIntDef(0);
        lav.ID_Pallet = l.ID_Pallet;
		lav.LavoroAttivo = lavoroattivo;
		lav.Priorita = l.Priority;
		if (!CambiaIndiceLavoro(lav)) {
			lav.FaseCompletata = lav.LavoroAttivo;
			CreaLavoro(lav);
		}
		lavoroattivo = lav.LavoroAttivo;
	}
	return l.ID_Lavoro;
}
//---------------------------------------------------------------------------

int TDBDataModule::CambiaIndiceLavoro(TLavoriInCorso &lav)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int attivo, res = 0;

	try {
		attivo = lav.LavoroAttivo;
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM LavoriInCorso "
					  "WHERE (ID_Lavoro = %d) AND (ID_Fase = %d)",
			lav.ID_Lavoro,
			lav.ID_Fase
			);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			attivo = ReadInt(ADOQuery, "LavoroAttivo");
		}
		ADOQuery->Close();
		lav.LavoroAttivo = attivo;
		strsql.printf("UPDATE LavoriInCorso SET Indice = %d "
					  "WHERE (ID_Lavoro = %d) AND (ID_Fase = %d)",
			lav.Indice,
			lav.ID_Lavoro,
			lav.ID_Fase
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::PalletAssociatoLotto(int ID_Pallet)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lotti WHERE (ID_Pallet = %d)", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if(!ADOQuery->Eof) {
        	res = ReadInt(ADOQuery, "ID_Lotto");
        }
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LavorazioneAssociatoLotto(int ID_Lavorazione)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lotti WHERE (ID_Lavorazione = %d)", ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if(!ADOQuery->Eof) {
        	res = ReadInt(ADOQuery, "ID_Lotto");
        }
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaFase(TFase strFase)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Fasi "
					  "(ID_Lavorazione, Codice, Descrizione, ID_Operazione, ID_Macchina, "
					  "Tipo_Macchina, Programma_MU, ID_TipoPallet, Messaggio_Op, Ordine_Seq, Abilitazione, ID_TipoPalletxCambio) "
					  "VALUES (%d, '%s', '%s', %d, %d, '%s', '%s', %d, '%s', %d, %d, %d) ",
					  strFase.ID_Lavorazione,
					  strFase.Codice.c_str(),
					  strFase.Descrizione.c_str(),
					  strFase.ID_Operazione,
					  strFase.ID_Macchina,
					  strFase.Tipo_Macchina.c_str(),
					  strFase.Programma_MU.c_str(),
					  strFase.ID_TipoPallet,
					  strFase.MessaggioOp.c_str(),
					  strFase.OrdineSeq,
					  strFase.Abilitazione,
					  strFase.ID_TipoPalletXCambio);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	}
    catch(Exception &E) {};
	delete ADOQuery;
	return GetSelfGenID("Fasi");
}
//---------------------------------------------------------------------------

int TDBDataModule::ModificaFase(TFase strFase)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Fasi "
					  "SET Codice = '%s',Descrizione = '%s', ID_Macchina = %d, Tipo_Macchina = '%s',"
					  " ID_Operazione = %d, Programma_MU = '%s', ID_TipoPallet = %d, Messaggio_Op = '%s',"
					  " Ordine_Seq = %d, Abilitazione = %d, ID_TipoPalletxCambio = %d "
					  "WHERE (ID_Lavorazione = %d AND ID_Fase = %d) ",
					  strFase.Codice.c_str(),
					  strFase.Descrizione.c_str(),
					  strFase.ID_Macchina,
					  strFase.Tipo_Macchina.c_str(),
					  strFase.ID_Operazione,
					  strFase.Programma_MU.c_str(),
					  strFase.ID_TipoPallet,
					  strFase.MessaggioOp.c_str(),
					  strFase.OrdineSeq,
					  strFase.Abilitazione,
					  strFase.ID_TipoPalletXCambio,
					  strFase.ID_Lavorazione,
					  strFase.ID_Fase);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	}
    catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiFase(int ID_Lavorazione, int ID_Fase, TFase &strFase)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	memset(&strFase, 0, sizeof(TFase));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Fasi WHERE ID_Lavorazione = %d AND ID_Fase = %d", ID_Lavorazione, ID_Fase);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
            strFase.ID_Lavorazione 	= ReadInt(ADOQuery, "ID_Lavorazione");
            strFase.ID_Fase			= ReadInt(ADOQuery, "ID_Fase");
			strFase.Codice 			= ReadString(ADOQuery, "Codice");
			strFase.Descrizione 	= ReadString(ADOQuery, "Descrizione");
			strFase.ID_Operazione	= ReadInt(ADOQuery, "ID_Operazione");
			strFase.ID_Macchina 	= ReadInt(ADOQuery, "ID_Macchina");
			strFase.Tipo_Macchina 	= ReadString(ADOQuery, "Tipo_Macchina");
			strFase.Programma_MU 	= ReadString(ADOQuery, "Programma_MU");
			strFase.ID_TipoPallet   = ReadInt(ADOQuery, "ID_TipoPallet");
			strFase.MessaggioOp 	= ReadString(ADOQuery, "Messaggio_Op");
			strFase.OrdineSeq       = ReadInt(ADOQuery, "Ordine_Seq");
			strFase.Abilitazione	= ReadInt(ADOQuery, "Abilitazione");
			strFase.ID_TipoPalletXCambio = ReadInt(ADOQuery, "ID_TipoPalletxCambio");
		}
		ADOQuery->Close();
	}
    catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaFase(int ID_Lavorazione, int ID_Fase)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Fasi WHERE ID_Lavorazione = %d AND ID_Fase = %d", ID_Lavorazione, ID_Fase);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	}
    catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

AnsiString FiltraApici(AnsiString s) {
	int i;
	AnsiString res = "";

	for (i = 1; i <= s.Length(); i++) {
		if (s[i] == '\'') {
			res += "\\\'";
		} else {
			res += s[i];
        }
	}
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::NewPalletID() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int id, newid = -1;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "SELECT * FROM Pallet ORDER BY ID_Pallet";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		newid = 1;
		while (!ADOQuery->Eof) {
			id = ReadInt(ADOQuery, "ID_Pallet");
			if (newid == id) {
				newid++;
				ADOQuery->Next();
			} else {
				break;
			}
		}
		res = newid;
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaPallet(TPallet p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Pallet (ID_Pallet, Alias, ID_TipoPallet, ID_Pezzo, Stato_Pallet, Extra_Altezza) VALUES (%d, '%s', %d, %d, %d, %d)",
			p.ID_Pallet,
            p.Alias.c_str(),
			p.ID_TipoPallet,
			p.ID_Pezzo,
			p.Stato_Pallet,
			p.Extra_Altezza
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaNuovoPallet(int TipoPallet, TPallet &newpallet) {
	TPallet p;

	p.ID_Pallet = NewPalletID();
	p.ID_TipoPallet = TipoPallet;
	p.Alias = IntToStr(p.ID_Pallet);
	p.ID_Pezzo = 0;
	p.Stato_Pallet = 0;
	if (p.ID_Pallet > 0) {
		CreaPallet(p);
		newpallet = p;
	}
	return p.ID_Pallet;
}
//---------------------------------------------------------------------------

int TDBDataModule::PalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Id_Pallet = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'", ID_Pallet, ID_Macchina, Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiPalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&p, 0, sizeof(TPallet));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT M.ID_Macchina, M.Tipo_Macchina, M.ID_Pallet, P.* "
					  "FROM Macchine AS M           "
					  "LEFT OUTER JOIN Pallet AS P  "
					  "ON M.ID_Pallet = P.ID_Pallet "
					  "WHERE (M.ID_Macchina = %d) 	"
					  "AND (M.Tipo_Macchina = '%s') ", ID_Macchina, Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pallet	= ReadInt(ADOQuery, "ID_Pallet");
			p.ID_TipoPallet	= ReadInt(ADOQuery, "ID_TipoPallet");
			p.ID_Pezzo	= ReadInt(ADOQuery, "ID_Pezzo");
			p.Stato_Pallet = ReadInt(ADOQuery, "Stato_Pallet");
			p.Extra_Altezza = ReadInt(ADOQuery, "Extra_Altezza");
			p.Programma = ReadString(ADOQuery, "Programma");
			p.Alias = ReadString(ADOQuery, "Alias");
			p.Descrizione_Pallet = ReadString(ADOQuery, "Descrizione_Pallet");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaPallet(TPallet p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET ID_TipoPallet = %d, Descrizione_Pallet = '%s', ID_Pezzo = %d, Stato_Pallet = %d, Extra_Altezza = %d, Programma = '%s' WHERE ID_Pallet = %d",
			p.ID_TipoPallet,
			p.Descrizione_Pallet.c_str(),
			p.ID_Pezzo,
			p.Stato_Pallet,
			p.Extra_Altezza,
			p.Programma.c_str(),
			p.ID_Pallet
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaDescrizionePosM(TPS &PosMacchine) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	AnsiString s, tm;
	int m, p, tp;

	PosMacchine.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "SELECT * FROM PosMacchine";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			tm	= ReadString(ADOQuery, "Tipo_Macchina");
			m	= ReadInt(ADOQuery, "ID_Macchina");
			p	= ReadInt(ADOQuery, "Posizione");
			s	= ReadString(ADOQuery, "Codice_Tipopallet");
			tp	= ReadInt(ADOQuery, "ID_TipoPallet");
			PosMacchine[tm][m][p][tp] = IntToStr(p) + " - " + s;
			ADOQuery->Next();
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaTabellaDaQuery(AnsiString TableName, TRecordList &RecList) {
	AnsiString campo;
	TADOQuery *ADOQuery;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = TableName.c_str();
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[i][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

bool TDBDataModule::CaricaTabella(AnsiString TableName, TRecordList &RecList) {
	AnsiString strsql, campo;
	TADOQuery *ADOQuery;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM %s", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[i][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
    return (i > 0);
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaTabellaK(AnsiString TableName, AnsiString KeyField, TIndexList &RecList) {
	AnsiString strsql, campo, chiave;
	TADOQuery *ADOQuery;
	Variant k;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM %s", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				chiave = ADOQuery->FieldByName(KeyField)->AsString;
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[chiave][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaTabellaKDaQuery(AnsiString TableName, AnsiString KeyField, TIndexList &RecList) {
	AnsiString strsql, campo, chiave;
	TADOQuery *ADOQuery;
	Variant k;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = TableName.c_str();
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				chiave = ADOQuery->FieldByName(KeyField)->AsString;
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[chiave][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiMissione(int ID_Missione, TMissioneDB &m)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&m, 0, sizeof(TMissioneDB));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni WHERE ID_Missione = %d", ID_Missione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			m.ID_Missione		   = ReadInt(ADOQuery, 		"ID_Missione");
			m.ID_Pallet            = ReadInt(ADOQuery, 		"ID_Pallet");
			m.ID_Macchina          = ReadInt(ADOQuery, 		"ID_Macchina");
			m.Tipo_Macchina        = ReadString(ADOQuery, 	"Tipo_Macchina");
			m.Tipo_Missione        = ReadString(ADOQuery, 	"Tipo_Missione");
			m.Programma_Macchina   = ReadString(ADOQuery, 	"Programma_Macchina");
			m.ID_Articolo          = ReadInt(ADOQuery, 		"ID_Articolo");
			m.Priorita             = ReadInt(ADOQuery, 		"Priorita");
			m.ID_Job               = ReadInt(ADOQuery, 		"ID_Job");
			m.GruppoFase		   = ReadString(ADOQuery, 	"GruppoFase");
			m.GruppoPallet		   = ReadString(ADOQuery, 	"GruppoPallet");
			m.ID_Pezzo             = ReadInt(ADOQuery, 		"ID_Pezzo");
			m.AzionePallet         = ReadInt(ADOQuery, 		"AzionePallet");
			m.GruppoLavorazione	   = ReadString(ADOQuery, 	"GruppoLavorazione");
 			m.Commento			   = ReadString(ADOQuery, 	"Commento");
			m.Descrizione_Op	   = ReadString(ADOQuery, 	"Descrizione_Op");
            m.ID_Lotto   		   = ReadInt(ADOQuery, 		"ID_Lotto");
            m.ID_Lavoro   		   = ReadInt(ADOQuery, 		"ID_Lavoro");
            m.IndiceLavoro 		   = ReadInt(ADOQuery, 		"IndiceLavoro");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaMissionePallet(AnsiString tpmiss, AnsiString tpmacc, int pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND ID_Pallet = %d", tpmacc.c_str(), tpmiss.c_str(), pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaMissione(TMissioneDB m) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Missioni "
					  "(ID_Pallet, ID_Macchina, Tipo_Macchina, Tipo_Missione, Programma_Macchina, ID_Articolo, Priorita, ID_Job, Commento, Descrizione_Op) "
					  "VALUES (%d, %d, '%s', '%s', '%s', %d, %d, %d, '%s', '%s') ",
			m.ID_Pallet,
			m.ID_Macchina,
			m.Tipo_Macchina,
			m.Tipo_Missione,
			m.Programma_Macchina.c_str(),
			m.ID_Articolo,
			m.Priorita,
			m.ID_Job,
			m.Commento.c_str(),
			m.Descrizione_Op.c_str()
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
       	LogMiss("Create Mission", m.ID_Pallet, m.Tipo_Missione, m.Tipo_Macchina, m.ID_Macchina, 0, res);
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiJob(int ID_Job, TJob &j)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&j, 0, sizeof(TJob));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs WHERE ID_Job = %d", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			j.ID_Job             = ReadInt(ADOQuery, "ID_Job");
			j.ID_Pallet          = ReadInt(ADOQuery, "ID_Pallet");
			j.ID_Macchina        = ReadInt(ADOQuery, "ID_Macchina");
			j.Tipo_Macchina      = ReadString(ADOQuery, "Tipo_Macchina");
			j.Tipo_Missione      = ReadString(ADOQuery, "Tipo_Missione");
			j.Programma_Macchina = ReadString(ADOQuery, "Programma_Macchina");
			j.ID_Articolo        = ReadInt(ADOQuery, "ID_Articolo");
			j.Priorita           = ReadInt(ADOQuery, "Priorita");
			j.GruppoFase		 = ReadString(ADOQuery, "GruppoFase");
			j.GruppoPallet		 = ReadString(ADOQuery, "GruppoPallet");
			j.ID_Pezzo			 = ReadInt(ADOQuery, "ID_Pezzo");
			j.AzionePallet		 = ReadInt(ADOQuery, "AzionePallet");
			j.GruppoLavorazione  = ReadString(ADOQuery, "GruppoLavorazione");
			j.Descrizione_Op	 = ReadString(ADOQuery, "Descrizione_Op");
			j.Commento			 = ReadString(ADOQuery, "Commento");
            j.ID_Lotto			 = ReadInt(ADOQuery, "ID_Lotto");
            j.ID_Lavoro			 = ReadInt(ADOQuery, "ID_Lavoro");
            j.IndiceLavoro		 = ReadInt(ADOQuery, "IndiceLavoro");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJob(AnsiString tpmiss, AnsiString tpmacc, int nmacc) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s'",
			nmacc, tpmacc.c_str(), tpmiss.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND ID_Pallet = %d",
			tpmacc.c_str(), tpmiss.c_str(), ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int nmacc, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND ID_Pallet = %d",
			nmacc, tpmacc.c_str(), tpmiss.c_str(), ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaJob(TJob j) {
	int ID_Job;
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Jobs "
					  "(ID_Pallet, ID_Macchina, Tipo_Macchina, Tipo_Missione, Programma_Macchina, ID_Articolo, Priorita, GruppoFase, GruppoPallet, ID_Pezzo, AzionePallet, GruppoLavorazione, Commento, Descrizione_Op) "
					  "VALUES (%d, %d, '%s', '%s', '%s', %d, %d, '%s', '%s', %d, %d, '%s', '%s', '%s') ",
			j.ID_Pallet,
			j.ID_Macchina,
			j.Tipo_Macchina.c_str(),
			j.Tipo_Missione.c_str(),
			j.Programma_Macchina.c_str(),
			j.ID_Articolo,
			j.Priorita,
			j.GruppoFase.c_str(),
			j.GruppoPallet.c_str(),
			j.ID_Pezzo,
			j.AzionePallet,
			j.GruppoLavorazione.c_str(),
			j.Commento.c_str(),
            j.Descrizione_Op.c_str()
			);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
        ID_Job = GetSelfGenID("Jobs");
//		LogProd(j);
       	LogMiss("Create Job", j.ID_Pallet, j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, ID_Job, 0);
	} catch(Exception &E) {};
	delete ADOQuery;
	return ID_Job;
}
//---------------------------------------------------------------------------

int TDBDataModule::CambiaPrioritaJob(int id, int pri) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Jobs SET Priorita = %d WHERE ID_Job = %d", pri, id);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaLavoriInCorso(int ID_Pallet, int ID_Lavorazione, int LavoroAttivo, int priorita)
{
    TLavoriInCorso lav;
	TRecordList TabFasi;
	AnsiString strsql;
	int IdLavoro, p = priorita;
	UINT i;

    strsql.printf("FasiLavorazione where (ID_Lavorazione = %d AND Abilitazione = 1) ORDER BY Ordine_Seq", ID_Lavorazione);
    DBDataModule->CaricaTabella(strsql, TabFasi);

    //Trovo il nuovo ID univoco da descrivere le fasi delle lavorazioni
    IdLavoro = DBDataModule->NewIDLavoro();

	for (i = 0; i < TabFasi.size(); i++) {
        memset(&lav, 0, sizeof(lav));
        lav.ID_Lavoro = IdLavoro;
        lav.Indice = i;
        lav.ID_Lavorazione = TabFasi[i]["ID_Lavorazione"].ToIntDef(0);
        lav.ID_Fase = TabFasi[i]["ID_Fase"].ToIntDef(0);
        lav.ID_Pallet = ID_Pallet;
        lav.LavoroAttivo = LavoroAttivo;
        lav.Priorita = p;
        CreaLavoro(lav);
    }
	return IdLavoro;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaLavoro(TLavoriInCorso lav)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO LavoriInCorso "
					  "(ID_Lavoro, Indice, ID_Lavorazione, ID_Fase, ID_Pallet, LavoroAttivo, FaseCompletata, Priorita) "
					  "VALUES (%d, %d, %d, %d, %d, %d, %d, %d)",
			lav.ID_Lavoro,
			lav.Indice,
			lav.ID_Lavorazione,
			lav.ID_Fase,
			lav.ID_Pallet,
            lav.LavoroAttivo,
			lav.FaseCompletata,
			lav.Priorita
			);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		lav.ID_Lavoro = GetSelfGenID("LavoriInCorso");
	} catch(Exception &E) {};
	delete ADOQuery;
	return lav.ID_Lavoro;
}
//---------------------------------------------------------------------------

int TDBDataModule::NewIDLavoro()
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int id, newid = -1;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "SELECT DISTINCT ID_Lavoro FROM LavoriInCorso ORDER BY ID_Lavoro";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		newid = 1;
		while (!ADOQuery->Eof) {
			id = ReadInt(ADOQuery, "ID_Lavoro");
			if (newid == id) {
				newid++;
				ADOQuery->Next();
			} else {
				break;
			}
		}
		res = newid;
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogMiss(AnsiString str, int ID_Pallet, AnsiString tipomiss, AnsiString tipomacc, int nmacc, int ID_Job, int ID_Missione)
{
	AnsiString res;

    res = str;
	logstr.sprintf("Mission: %s, pallet=%d, tipomiss=%s, tipomacc=%s, nmacc=%d, ID_Job=%d, ID_Missione=%d", res.c_str(), ID_Pallet, tipomiss.c_str(), tipomacc.c_str(), nmacc, ID_Job, ID_Missione);
	DBDataModule->Log("CLIENT", "DB", logstr);
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaPezzo(TPezzo pz) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Pezzi "
					  "(ID_Articolo, Stato_Pezzo, Misura_Eseguita, "
					  "OffsetX, OffsetY, OffsetZ, OffsetA, OffsetB, OffsetC, OffsetROT, Gap, GapMin, GapMax, H, Descrizione) "
					  "VALUES (%d, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, '%s') ",
			pz.ID_Articolo,
			pz.Stato_Pezzo,
			pz.Misura_Eseguita,
			pz.OffsetX,
			pz.OffsetY,
			pz.OffsetZ,
			pz.OffsetA,
			pz.OffsetB,
			pz.OffsetC,
			pz.OffsetROT,
			pz.Gap,
			pz.GapMin,
			pz.GapMax,
			pz.H,
			pz.Descrizione.c_str()
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return GetSelfGenID("Pezzi");
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaPezzo(TPezzo pz) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pezzi SET "
					  "ID_Articolo = %d, "
					  "Stato_Pezzo = %d, "
					  "Misura_Eseguita = %d, "
					  "OffsetX = %f, "
					  "OffsetY = %f, "
					  "OffsetZ = %f, "
					  "OffsetA = %f, "
					  "OffsetB = %f, "
					  "OffsetC = %f, "
					  "OffsetROT = %f, "
					  "Gap = %f, "
					  "GapMin = %f, "
					  "GapMax = %f, "
					  "H = %f, "
					  "Descrizione = '%s' "
					  "WHERE ID_Pezzo = %d",
			pz.ID_Articolo,
			pz.Stato_Pezzo,
			pz.Misura_Eseguita,
			pz.OffsetX,
			pz.OffsetY,
			pz.OffsetZ,
			pz.OffsetA,
			pz.OffsetB,
			pz.OffsetC,
			pz.OffsetROT,
			pz.Gap,
			pz.GapMin,
			pz.GapMax,
			pz.H,
			pz.Descrizione.c_str(),
			pz.ID_Pezzo
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaLavaggioPezzo(int ID_Pezzo, int lavaggio) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pezzi SET Programma_Lavaggio =  %d WHERE ID_Pezzo = %d", lavaggio, ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaArticolo(TArticolo a) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Articoli "
					  "(ID_TipoPezzo, Codice_Articolo, Descrizione_Articolo, Diametro_Pezzo_Grezzo, Diametro_Pz_SemiL, Altezza_Pz, ftc_Controllo,Faccia_Pinza, TStazionamento) "
					  "VALUES (%d, '%s', '%s', %g, %g, %g, %d, %d, %d) ",
			a.ID_TipoPezzo,
			a.Codice_Articolo.c_str(),
			a.Descrizione_Articolo.c_str(),
			a.DiamentroPezzo_Grezzo,
			a.DiamentroPezzo_Semilavorato,
			a.AltezzaPezzo,
			a.ftc,
			a.Faccia_Pinza,
			a.TStazionamento);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return GetSelfGenID("Articoli");
}
//---------------------------------------------------------------------------

int TDBDataModule::ModificaArticolo(TArticolo a) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Articoli "
					  "SET ID_TipoPezzo = %d, Codice_Articolo = '%s', Descrizione_Articolo = '%s', "
					  "Diametro_Pezzo_Grezzo = %g, Diametro_Pz_SemiL = %g, Altezza_Pz = %g, "
					  "ftc_Controllo = %d , Faccia_Pinza = %d, TStazionamento = %d"
					  "WHERE (ID_Articolo = %d) ",
			a.ID_TipoPezzo,
			a.Codice_Articolo.c_str(),
			a.Descrizione_Articolo.c_str(),
			a.DiamentroPezzo_Grezzo,
			a.DiamentroPezzo_Semilavorato,
			a.AltezzaPezzo,
			a.ftc,
			a.Faccia_Pinza,
			a.TStazionamento,
			a.ID_Articolo);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaPezzo(int ID_Pezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Pezzi WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		strsql.printf("DELETE FROM Misurazioni WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaMisurazioni(int ID_Pezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Misurazioni WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaMisurazioneSingola(int ID_Pezzo, AnsiString Name) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Misurazioni WHERE ID_Pezzo = %d AND NomePezzo = '%s'", ID_Pezzo, Name.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::SvuotaPallet(TPallet &p) {
	if (p.ID_Pezzo) {
		CancellaPezzo(p.ID_Pezzo);
		p.ID_Pezzo = 0;
		p.Stato_Pallet = 0;
		p.Programma = "";
		AggiornaPallet(p);
	}
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiPezzo(int ID_Pezzo, TPezzo &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	memset(&p, 0, sizeof(TPezzo));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pezzi WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pezzo           = ReadInt(ADOQuery, "ID_Pezzo");
			p.ID_Articolo        = ReadInt(ADOQuery, "ID_Articolo");
			p.Stato_Pezzo        = ReadInt(ADOQuery, "Stato_Pezzo");
			p.Misura_Eseguita    = ReadInt(ADOQuery, "Misura_Eseguita");
			p.OffsetX            = ReadFloat(ADOQuery, "OffsetX");
			p.OffsetY            = ReadFloat(ADOQuery, "OffsetY");
			p.OffsetZ            = ReadFloat(ADOQuery, "OffsetZ");
			p.OffsetA            = ReadFloat(ADOQuery, "OffsetA");
			p.OffsetB            = ReadFloat(ADOQuery, "OffsetB");
			p.OffsetC            = ReadFloat(ADOQuery, "OffsetC");
			p.OffsetROT          = ReadFloat(ADOQuery, "OffsetROT");
			p.Gap          		 = ReadFloat(ADOQuery, "Gap");
			p.GapMin      		 = ReadFloat(ADOQuery, "GapMin");
			p.GapMax       		 = ReadFloat(ADOQuery, "GapMax");
			p.H          		 = ReadFloat(ADOQuery, "H");
			p.Descrizione		 = ReadString(ADOQuery, "Descrizione");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPalletMagazzino(int ID_Pallet, int &nmag, int &npos) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Postazioni "
					  "WHERE ID_Pallet = %d 	"
					  , ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			nmag = ReadInt(ADOQuery, "ID_Magazzino");
			npos = ReadInt(ADOQuery, "ID_Postazione");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaTipoPalletMagazzino(int ID_TipoPallet, int &ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	/*
		SELECT * FROM Magazzino
		WHERE  Magazzino.ID_Pallet NOT IN (SELECT Jobs.ID_Pallet FROM Jobs INNER JOIN
											   Magazzino ON  Jobs.ID_Pallet = Magazzino.ID_Pallet
											   WHERE Jobs.ID_Pallet = Magazzino.ID_Pallet)
		AND Magazzino.ID_Pallet NOT IN (SELECT Missioni.ID_Pallet FROM Missioni INNER JOIN
											   Magazzino ON  Missioni.ID_Pallet = Magazzino.ID_Pallet
											   WHERE Missioni.ID_Pallet = Magazzino.ID_Pallet)
		AND Magazzino.ID_TipoPallet = %d AND Magazzino.ID_Pezzo = 0 AND Magazzino.ID_Pezzo IS NOT NULL
	*/

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Magazzino                                                                         "
					  "	WHERE  Magazzino.ID_Pallet NOT IN (SELECT Jobs.ID_Pallet FROM Jobs INNER JOIN                  "
					  "										   Magazzino ON  Jobs.ID_Pallet = Magazzino.ID_Pallet      "
					  "										   WHERE Jobs.ID_Pallet = Magazzino.ID_Pallet)             "
					  "	AND Magazzino.ID_Pallet NOT IN (SELECT Missioni.ID_Pallet FROM Missioni INNER JOIN             "
					  "										   Magazzino ON  Missioni.ID_Pallet = Magazzino.ID_Pallet  "
					  "										   WHERE Missioni.ID_Pallet = Magazzino.ID_Pallet)         "
					  "	AND Magazzino.ID_TipoPallet = %d AND Magazzino.ID_Pezzo = 0 AND Magazzino.ID_Pezzo IS NOT NULL "
					  , ID_TipoPallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ID_Pallet = ReadInt(ADOQuery, "ID_Pallet");
			res = 0;
		} else
			ID_Pallet = res;
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ArticoloInUso(int ID_Articolo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pezzi WHERE ID_Articolo = %d", ID_Articolo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
        else {
			ADOQuery->Close();
            strsql.printf("SELECT * FROM Lavorazioni WHERE ID_Articolo = %d", ID_Articolo);
            ADOQuery->SQL->Text = strsql;
            ADOQuery->Open();
            if (!ADOQuery->Eof) {
                res = 2;
            }
        }
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPalletMacchine(int ID_Pallet, AnsiString &tipomacchina, int &nmacchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine "
					  "WHERE ID_Pallet = %d   "
					  "OR ID_Utensile = %d 	  "
					  "OR Adattatore = %d 	  "
					  , ID_Pallet, ID_Pallet, ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			tipomacchina = ReadString(ADOQuery, "Tipo_Macchina");
			nmacchina 	 = ReadInt(ADOQuery, "ID_Macchina");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaMissione(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int ID_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Missioni "
					  "WHERE ID_Pallet = %d "
					  "AND Tipo_Missione = '%s' "
					  "AND Tipo_Macchina = '%s' "
					  "AND ID_Macchina = %d ",
					  ID_Pallet, Tipo_Missione, Tipo_Macchina, ID_Macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaMissione(int ID_Missione) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
    TMissioneDB m;
	int res = -1;

	try {
    	//Leggo la missione da cancellare
		if (LeggiMissione(ID_Missione, m) == 0) {
        	//Log
            LogMiss("Delete Miss", m.ID_Pallet, m.Tipo_Missione, m.Tipo_Macchina, m.ID_Macchina, m.ID_Job, m.ID_Missione);
        }
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Missioni WHERE ID_Missione = %d ", ID_Missione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaMissioneFromPallet(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Missioni WHERE ID_Pallet = %d ", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaJob(int ID_Job) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
    TJob j;
	int res = -1;

	try {
    	//Leggo la missione da cancellare
		if (LeggiJob(ID_Job, j) == 0) {
        	//Log
            LogMiss("Delete Job", j.ID_Pallet, j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, j.ID_Job, 0);
        }
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Jobs WHERE ID_Job = %d", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaTuttiJob() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Jobs WHERE Active IS NULL");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaJobPallet(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Jobs WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		strsql.printf("DELETE Missioni WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AttivaJob(int ID_Job) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Jobs SET Active = GetDate() WHERE ID_Job = %d", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AttivaJobFromPallet(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Jobs SET Active = GetDate() WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AttivaAllJob() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "UPDATE Jobs SET Active = GetDate() WHERE Active IS NULL";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::DisattivaJob(int ID_Job) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Jobs SET Active = NULL WHERE ID_Job = %d", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::DisattivaAllJob() {

	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "UPDATE Jobs SET Active = NULL WHERE NOT(Active IS NULL)";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::CambiaStatoPostazione(int Magazzino, int Postazione, int stato) {

	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Postazioni SET Stato_Postazione = %d WHERE ID_Magazzino = %d AND ID_Postazione = %d", stato, Magazzino, Postazione);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::PalletInMagazzino(int Magazzino, int Postazione, int ID_Pallet) {

	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Postazioni SET ID_Pallet = %d WHERE ID_Magazzino = %d AND ID_Postazione = %d", ID_Pallet, Magazzino, Postazione);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------


int TDBDataModule::LeggiMissionePallet(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, TMissioneDB &m) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&m, 0, sizeof(TMissioneDB));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE ID_Pallet = %d "
					  "AND Tipo_Missione = '%s' "
					  "AND Tipo_Macchina = '%s' "
					  "ORDER BY Priorita DESC, ID_Missione", ID_Pallet, Tipo_Missione, Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			m.ID_Missione		   = ReadInt(ADOQuery, 		"ID_Missione");
			m.ID_Pallet            = ReadInt(ADOQuery, 		"ID_Pallet");
			m.ID_Macchina          = ReadInt(ADOQuery, 		"ID_Macchina");
			m.Tipo_Macchina        = ReadString(ADOQuery, 	"Tipo_Macchina");
			m.Tipo_Missione        = ReadString(ADOQuery, 	"Tipo_Missione");
			m.Programma_Macchina   = ReadString(ADOQuery, 	"Programma_Macchina");
			m.ID_Articolo          = ReadInt(ADOQuery, 		"ID_Articolo");
			m.Priorita             = ReadInt(ADOQuery, 		"Priorita");
			m.ID_Job               = ReadInt(ADOQuery, 		"ID_Job");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::SetStatoProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Programma_Attivo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Programma_Attivo = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'",
			Programma_Attivo,
			ID_Macchina,
			Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AdattatoreInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Adattatore = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'", ID_Pallet, ID_Macchina, Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::InserisciTempoUtensile(int P, int U, int t) {
	AnsiString strsql, testo;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO TempiLavoroUtensile (Programma, Utensile, Min_Lavoro) VALUES (%d, %d, %d)",
					  P, U, t);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::AggiornaPresetMacchina(int idmacchina, int tipopallet, AnsiString FieldName, double Value) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE PosizioniMacchine SET %s = %f WHERE ID_Macchina = %d AND ID_TipoPallet = %d AND Tipo_Macchina = 'M'", FieldName, Value, idmacchina, tipopallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::Log(AnsiString Sorgente, AnsiString Tipo, char *s, ...)
{
	va_list argptr;
	AnsiString logstr;

    //Costruisco la stringa
    va_start(argptr, s);
    logstr.vprintf(s, argptr);
    va_end(argptr);

    //Log
    Log(Sorgente, Tipo, logstr);
}
//---------------------------------------------------------------------------

void TDBDataModule::Log(AnsiString Sorgente, AnsiString Tipo, AnsiString Evento)
{
	TADOQuery *ADOQuery;
	AnsiString strsql;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Storico (Sorgente, Tipo, Evento) VALUES ('%s', '%s', '%s')", Sorgente, Tipo, Evento);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::UtensileInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Id_Utensile) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Id_Utensile = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'", Id_Utensile, ID_Macchina, Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CleanupPezzi() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "DELETE FROM Pezzi WHERE (NOT EXISTS (SELECT * FROM Pallet WHERE (Pezzi.ID_Pezzo = ID_Pezzo)))";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogProd(TJob j)
{
	TADOQuery *ADOQuery;
	AnsiString strsql;
	TPezzo pz;
	TArticolo a;
	int nmag, npos;

	try {
		LeggiPezzo(j.ID_Pezzo, pz);
		LeggiArticolo(j.ID_Articolo, a);
        //Se non trovo il pallet a magazzino resetto i campi
		if (CercaPalletMagazzino(j.ID_Pallet, nmag, npos) != 0) {
        	nmag = npos = 0;
        }
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO StoricoProduzione (ID_Pallet, ID_Magazzino, ID_Postazione, Codice_Articolo, Tipo_Macchina, ID_Macchina, Programma_Macchina, NumOrdine, ID_Job) "
					  "VALUES (%d, %d, %d, '%s', '%s', %d, '%s', '%s', %d)",
                      j.ID_Pallet,
                      nmag,
                      npos,
                      a.Codice_Articolo.c_str(),
                      j.Tipo_Macchina.c_str(),
                      j.ID_Macchina,
                      j.Programma_Macchina.c_str(),
                      pz.Descrizione.c_str(),
                      j.ID_Job
                      );
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogAttivazioneProd(int ID_Job)
{
	TADOQuery *ADOQuery;
	AnsiString strsql;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE StoricoProduzione SET Attivazione = GetDate() WHERE (ID_Job = %d) AND (Attivazione IS NULL)", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogDisattivazioneProd(int ID_Job)
{
	TADOQuery *ADOQuery;
	AnsiString strsql;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP 1 * FROM StoricoProduzione WHERE (ID_Job = %d) AND NOT(Attivazione IS NULL) ORDER BY DataOra DESC", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ADOQuery->Edit();
			ADOQuery->FieldByName("Attivazione")->Clear();
			ADOQuery->Post();
		}
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::ImpostaPezzoNonMisurato(int ID_Pezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pezzi SET Misura_Eseguita = 0 WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::SetProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, AnsiString Programma_Macchina, int Programma_Attivo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Programma_Macchina = '%s', Programma_Attivo = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'",
			Programma_Macchina.c_str(),
			Programma_Attivo,
			ID_Macchina,
			Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CancellaElettrodi(AnsiString NomePezzo) {
	AnsiString strsql;

	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Elettrodi WHERE NomePezzo = '%s'", NomePezzo.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {}
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiCampoTabella(AnsiString Tabella, AnsiString CampoDaLeggere, AnsiString ClauslaWhere, AnsiString &Valore) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	Valore = "";
	strsql.printf("SELECT %s FROM %s %s", CampoDaLeggere.c_str(), Tabella.c_str(), ClauslaWhere.c_str());
	try {

		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			Valore = ADOQuery->FieldByName(CampoDaLeggere)->AsString;
		}
	} catch (Exception &E) {
		res = -1;
	}
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiAbilitazioneVerificaUtensili(int MU)
{
	AnsiString Stato, sClausula;

	sClausula.printf("WHERE DescrizioneCampo = 'VerificaUtensiliMU%d'", MU);
	LeggiCampoTabella("Parametri", "ValoreCampo", sClausula.c_str(), Stato);
	return Stato.ToIntDef(0);
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiAbilitazioneStandby(int MU)
{
	AnsiString Stato, sClausula;

	sClausula.printf("WHERE DescrizioneCampo = 'AbilitaStandbyMU%d'", MU);
	LeggiCampoTabella("Parametri", "ValoreCampo", sClausula.c_str(), Stato);
	return Stato.ToIntDef(0);
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiAbilitazioneControlloBalluff()
{
	AnsiString Stato, sClausula;

	sClausula.printf("WHERE DescrizioneCampo = 'ControlloBalluffON'");
	LeggiCampoTabella("Parametri", "ValoreCampo", sClausula.c_str(), Stato);
	return Stato.ToIntDef(0);
}
//---------------------------------------------------------------------------

int TDBDataModule::CountFasiRecord(int ID_Lavorazione) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int RecordCount = 0;

	strsql.printf("SELECT MAX (Ordine_Seq) AS Number from dbo.Fasi WHERE dbo.Fasi.ID_Lavorazione = %d", ID_Lavorazione);
	try {

		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			RecordCount = ReadInt(ADOQuery, "Number");
		}
		ADOQuery->Close();
	} catch (Exception &E) {
	}
	delete ADOQuery;
	return RecordCount;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiPalletDaPezzo(int ID_Pezzo, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	memset(&p, 0, sizeof(TPallet));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT P.*, A.Codice_Articolo "
					  "FROM Articoli AS A INNER JOIN "
					  "Pezzi AS Pz ON A.ID_Articolo = Pz.ID_Articolo RIGHT OUTER JOIN "
					  "Pallet AS P ON Pz.ID_Pezzo = P.ID_Pezzo "
					  "WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pallet		= ReadInt(ADOQuery, "ID_Pallet");
			p.ID_TipoPallet	= ReadInt(ADOQuery, "ID_TipoPallet");
			p.ID_Pezzo		= ReadInt(ADOQuery, "ID_Pezzo");
			p.Stato_Pallet	= ReadInt(ADOQuery, "Stato_Pallet");
			p.Extra_Altezza	= ReadInt(ADOQuery, "Extra_Altezza");
			p.Descrizione_Pallet = ReadString(ADOQuery, "Descrizione_Pallet");
			p.Programma = ReadString(ADOQuery, "Programma");
			p.Alias = ReadString(ADOQuery, "Alias");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiIDPezzoInMacchina(int ID_Macchina, AnsiString Tipo_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine "
					  "WHERE (ID_Macchina = %d) "
					  "AND (Tipo_Macchina = '%s') "
					  , ID_Macchina, Tipo_Macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Pallet");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::SbloccaMacchina(int pos, AnsiString TypeM) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		//Sblocco la macchina selezionata
		strSQLUpdate.printf("UPDATE Macchine SET Prenotata = 0 WHERE Tipo_Macchina = '%s' "
							"AND ID_Macchina = %d", TypeM, pos);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::BloccaMacchina(int pos, AnsiString TypeM, int Valore) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		//Sblocco la macchina selezionata
		strSQLUpdate.printf("UPDATE Macchine SET Prenotata = 999 WHERE Tipo_Macchina = '%s' "
							"AND ID_Macchina = %d", TypeM, pos);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::PermanenzaMag(int mag, int Valore) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		//Sblocco la macchina selezionata
		strSQLUpdate.printf("UPDATE Magazzini SET Permanenza = %d WHERE ID_Magazzino = %d"
							, Valore, mag);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

bool TDBDataModule::GetGruppoPalletID(int Lav, int Fase, AnsiString &ID)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	long i;
	bool bFound = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		for (i = 0; i < LONG_MAX && !bFound; i++) {
			ID.sprintf("1-%d-%d-%ld", Lav, Fase, i);
			strsql.printf("SELECT TOP (1) * FROM Jobs WHERE GruppoPallet = '%s'", ID.c_str());
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			bFound = ADOQuery->Eof;
			ADOQuery->Close();
		}
	} catch(...) {};
	delete ADOQuery;

	return bFound;
}
//---------------------------------------------------------------------------

bool TDBDataModule::GetGruppoFaseID(int Lav, int Fase, AnsiString &ID)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	long i;
	bool bFound = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		for (i = 0; i < LONG_MAX && !bFound; i++) {
			ID.sprintf("1-%d-%d-%ld", Lav, Fase, i);
			strsql.printf("SELECT TOP (1) * FROM Jobs WHERE GruppoFase = '%s'", ID.c_str());
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			bFound = ADOQuery->Eof;
			ADOQuery->Close();
		}
	} catch(...) {};
	delete ADOQuery;

	return bFound;
}
//---------------------------------------------------------------------------

bool TDBDataModule::GetGruppoLavorazioneID(int Lav, AnsiString &ID)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	long i;
	bool bFound = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		for (i = 0; i < LONG_MAX && !bFound; i++) {
			ID.sprintf("1-%d-%ld", Lav, i);
			strsql.printf("SELECT TOP (1) * FROM Jobs WHERE GruppoLavorazione = '%s'", ID.c_str());
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			bFound = ADOQuery->Eof;
			ADOQuery->Close();
		}
	} catch(...) {};
	delete ADOQuery;

	return bFound;
}
//---------------------------------------------------------------------------

bool TDBDataModule::PezzoRibaltabile(int ID_Lav)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	double H;
	bool res = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT Articoli.Altezza_Pz FROM Articoli                                  "
					  "	INNER JOIN Lavorazioni ON Articoli.ID_Articolo = Lavorazioni.ID_Articolo "
					  "	WHERE ID_Lavorazione = %d", ID_Lav);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			H = ReadFloat(ADOQuery, "Altezza_Pz");
			//Ora � hardcoded...e credo ci rimarr�... 185 mm
			if (H < 185) {
				res = true;
			}
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::AggiornaExtraAltezzaPallet() {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	//Questa query la utilizzo nella gestione dell'aggiornamento automantico del flag extra altezza
	//dei pallet. Nel caso in cui l'operatore cambi il diamentro massimo tale per cui il pallet deve
	//essere posizionato nei posti in alto dei magazzini, aggiorno tutta la tabella
	try {
		strsql = (" UPDATE	p                                                                 	  "
				  "	SET     Extra_Altezza = 1                                                     "
				  "	FROM    Pallet AS p INNER JOIN                                        		  "
				  "			Pezzi AS pz ON p.ID_Pezzo = pz.ID_Pezzo INNER JOIN                    "
				  "			Articoli AS a ON pz.ID_Articolo = a.ID_Articolo CROSS JOIN            "
				  "			Parametri AS par                                                      "
				  "			WHERE   (par.Indice = 1 AND a.Diametro_Pezzo_Grezzo > par.ValoreCampo)");
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AttivaLavoroInCorso(int ID_Lavoro) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		strSQLUpdate.printf("UPDATE LavoriInCorso SET LavoroAttivo = 1 WHERE ID_Lavoro = %d", ID_Lavoro);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::DisattivaLavoroInCorso(int ID_Lavoro) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		strSQLUpdate.printf("UPDATE LavoriInCorso SET LavoroAttivo = 0, ID_Job = 0, ID_Missione = 0 WHERE ID_Lavoro = %d", ID_Lavoro);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AggiornaPrioritaLavoroInCorso(int ID_Lavoro, int priorita) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		//Sblocco la macchina selezionata
		strSQLUpdate.printf("UPDATE LavoriInCorso SET priorita = %d WHERE ID_Lavoro = %d", priorita, ID_Lavoro);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaJobsPallet(int ID_Pallet) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		strSQLUpdate.printf("DELETE Jobs WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiProssimoLavoroInCorso(int ID_Lavoro, TLavoriInCorso &strLavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.sprintf("SELECT TOP (1) * FROM LavoriInCorso WHERE ID_Lavoro = %d AND FaseCompletata = 0 ORDER BY ID_Lavoro, Indice", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			strLavoro.ID_Lavoro			= ReadFloat(ADOQuery, "ID_Lavoro");
			strLavoro.Indice			= ReadFloat(ADOQuery, "Indice");
			strLavoro.ID_Lavorazione   	= ReadFloat(ADOQuery, "ID_Lavorazione");
			strLavoro.ID_Fase   		= ReadFloat(ADOQuery, "ID_Fase");
			strLavoro.ID_Pallet   		= ReadFloat(ADOQuery, "ID_Pallet");
			strLavoro.ID_Job   			= ReadFloat(ADOQuery, "ID_Job");
			strLavoro.ID_Missione   	= ReadFloat(ADOQuery, "ID_Missione");
			strLavoro.LavoroAttivo   	= ReadFloat(ADOQuery, "LavoroAttivo");
			strLavoro.FaseCompletata   	= ReadFloat(ADOQuery, "FaseCompletata");
			strLavoro.Priorita   		= ReadFloat(ADOQuery, "Priorita");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::PalletAssociatoLavorazione(int ID_Pallet)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM LavoriInCorso AS L "
					  "WHERE (ID_Pallet = %d) AND (LavoroAttivo <> 0) "
					  "AND (NOT EXISTS (SELECT * FROM Lotti WHERE (ID_Lavoro = L.ID_Lavoro)))"
			, ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if(!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Lavoro");
        }
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AbortJobInLavoriInCorso(int ID_Job)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET FaseCompletata = 0, ID_Job = 0, ID_Missione = 0 WHERE (ID_Job = %d)", ID_Job);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AbortMissioneInLavoriInCorso(int ID_Missione)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET FaseCompletata = 0, ID_Job = 0, ID_Missione = 0 WHERE (ID_Missione = %d)", ID_Missione);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::RiattivaLavoriInCorso(int ID_Lavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET FaseCompletata = 0, ID_Job = 0, ID_Missione = 0 WHERE (ID_Lavoro = %d)", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ResettaErrorePezzo(int ID_Pezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pezzi SET Stato_Pezzo = Stato_Pezzo %% 100 WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaDescPallet(int id, AnsiString desc) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET Descrizione_Pallet = '%s' WHERE ID_Pallet = %d", desc.c_str(), id);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaLavorazioneAttiva(int ID_Lavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;

		//Cancello le lavorazioni associate
		strsql.printf("DELETE FROM LavoriInCorso WHERE ID_Lavoro = %d", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	}
    catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

