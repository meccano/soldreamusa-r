//---------------------------------------------------------------------------

#pragma hdrstop

#include "Pallet.h"
#include "ClientData.h"
#include "Socket.h"
#include "Main.h"
#include "StringTableID.h"
#include "ModificaPallet.h"
#include "Robot.h"
#include "InsNum.h"
#include "NewLavorazioni.h"
#define Abs(n) 				(((n) < 0) ? -(n) : (n))
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TPalletForm *PalletForm;
//---------------------------------------------------------------------------
__fastcall TPalletForm::TPalletForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	m_bIsFormCreate = false;

	RB_IDOFF->Checked = true;
	PageControl1->ActivePage = Table;
	DBDataModule->CaricaTabella("TipiPallet", RecList);
}
//---------------------------------------------------------------------------

void TPalletForm::AggiornaComboTipiPallet()
{
	UINT i;

	ComboBox1->Clear();
	for (i = 0; i < RecList.size(); i++) {
		ComboBox1->Items->Add(RecList[i]["ID_TipoPallet"] + " - " + RecList[i]["Codice_Tipopallet"]);
	}

	//Se ho un solo elemento seleziono sempre il primo
	if (RecList.size() == 1) {
		ComboBox1->ItemIndex = 0;
	}
}
//---------------------------------------------------------------------------

void TPalletForm::AggiornaDB()
{
	LockWindowUpdate(DBGrid1->Handle);

	DBGrid1->RefreshDataSet();
	DBGrid1->AutoFitAll();

	LockWindowUpdate(0);
}
//---------------------------------------------------------------------------

void TPalletForm::AggiornaRFID()
{
/*	TPallet ptag;
	int nmag, npos, nmacc, UseTAG = 0;
	AnsiString tipomacchina;
	bool OKInser = false, OKElettrodo = false;
	static int vecchioTAG = 0;
	TLocateOptions Opts;

	memset(&ptag, 0, sizeof(TPallet));
	if (MainForm->stationnumber > 0) {
		LE_TAG->Text = ClientData.RFID[MainForm->stationnumber - 1].IDValue;
		LE_TAGOLD->Text = ClientData.RFID[MainForm->stationnumber - 1].IDLast;
		if ((LE_TAGOLD->Text == LE_NewID->Text) || (LE_NewID->Text == "")) {
			BitBtnWr->Enabled = false;
			BitBtnAb->Enabled = false;
		} else {
			BitBtnWr->Enabled = true;
		}
		if (RB_IDON->Checked) {
			// Uso lettore TAG
			UseTAG = ClientData.RFID[MainForm->stationnumber - 1].IDLast;
		} else {
			// se lettore escluso uso id generato
			UseTAG = LE_NewID->Text.ToIntDef(0);
		}
	} else {
		LE_TAG->Text = "---";
		LE_TAGOLD->Text = "---";
		BitBtnWr->Enabled = false;
		BitBtnAb->Enabled = false;
		if (RB_IDOFF) {
			// se lettore escluso uso id generato
			UseTAG = LE_NewID->Text.ToIntDef(0);
		}
	}
	DBDataModule->LeggiPallet(UseTAG, ptag);
	if ((UseTAG == 0) && RB_IDON->Checked) {
		STMessage->Font->Color = clBlue;
		STMessage->Caption = String::LoadStr(IDS_WRITEID).w_str();
		OKInser = false;
		OKElettrodo = false;
	} else if (UseTAG == 0) {
		STMessage->Font->Color = clBlue;
		STMessage->Caption = String::LoadStr(IDS_GENNEWID).w_str();
		OKInser = false;
		OKElettrodo = false;
	} else if (ptag.ID_Pallet > 0) {
		if ((DBDataModule->CercaPalletMagazzino(ptag.ID_Pallet, nmag, npos) == 0) ||
			(DBDataModule->CercaPalletMacchine(ptag.ID_Pallet, tipomacchina, nmacc) == 0)){
			STMessage->Font->Color = clRed;
			STMessage->Caption = String::LoadStr(IDS_PALLETSTORED).w_str();
			OKInser = false;
			OKElettrodo = false;
		} else {
			STMessage->Font->Color = clRed;
			STMessage->Caption = String::LoadStr(IDS_PALLETEXISTS).w_str();
			OKInser = false;
			OKElettrodo = true;
		}
	} else {
		STMessage->Font->Color = clGreen;
		STMessage->Caption = String::LoadStr(IDS_PALLETOKNEW).w_str();
		OKInser = true;
		OKElettrodo = true;
	}
	if (vecchioTAG != UseTAG) {
		// Letto nuovo TAG
		Opts.Clear();
		Opts << loPartialKey;
		ADOQuery1->Locate("ID_Pallet", IntToStr(UseTAG), Opts);
		vecchioTAG = UseTAG;
	}
	ComboBox1->Enabled = OKInser;
	BitBtnCreate->Enabled = (OKInser && (ComboBox1->ItemIndex >= 0));    */
//	BitBtnElettrodo->Enabled = (OKElettrodo && (RecList[ComboBox1->ItemIndex]["ID_TipoPallet"].ToIntDef(0) == TPALLET_50));
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::FormActivate(TObject *Sender)
{
	AggiornaDB();
    AggiornaComboTipiPallet();
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::TimerUPDTimer(TObject *Sender)
{
	TimerUPD->Enabled = false;
	AggiornaRFID();
    if (PageControl1->ActivePageIndex == 1) {
		LockWindowUpdate(DBGridFasi->Handle);

    	DBGridFasi->RefreshDataSet();
        DBGridFasi->Repaint();

		LockWindowUpdate(0);
    }
    BitBtnNuovoPallet->Enabled = (MainForm->pwdlevel > 0);
	BitBtnCancellaPallet->Enabled = (MainForm->pwdlevel > 0);
	TimerUPD->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnWrClick(TObject *Sender)
{
	AnsiString s;

	if (MainForm->stationnumber > 0) {
		s.printf("MSGTYPE=WRITETAG|POS=%d|VAL=%d|", MainForm->stationnumber, LE_NewID->Text.ToIntDef(-1));
		SocketDataModule->Invia(s);
	}
	BitBtnAb->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnAbClick(TObject *Sender)
{
	AnsiString s;

	if (MainForm->stationnumber > 0) {
		s.printf("MSGTYPE=WRITETAG|POS=%d|VAL=%d|", MainForm->stationnumber, -1);
		SocketDataModule->Invia(s);
	}
	BitBtnAb->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnGenClick(TObject *Sender)
{
	int newid;

	newid = DBDataModule->NewPalletID();
	LE_NewID->Text = IntToStr(newid);
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnCreateClick(TObject *Sender)
{
/*	TPallet p;
	int UseTAG;
	TLocateOptions Opts;

	if (RB_IDON->Checked) {
		// Uso lettore TAG
		UseTAG = ClientData.RFID[MainForm->stationnumber - 1].IDLast;
	} else {
		// se lettore escluso uso id generato
		UseTAG = LE_NewID->Text.ToIntDef(0);
	}
	if (UseTAG > 0) {
    	memset(&p, 0, sizeof(TPallet));
		p.ID_Pallet = UseTAG;
		p.ID_TipoPallet = RecList[ComboBox1->ItemIndex]["ID_TipoPallet"].ToIntDef(0);
		p.ID_Pezzo = 0;
		p.Stato_Pallet = 0;
		p.Extra_Altezza = 0;
        p.Alias = IntToStr(p.ID_Pallet);
		DBDataModule->CreaPallet(p);
		BitBtnCreate->Enabled = false;
		ComboBox1->Enabled = false;
		ComboBox1->ItemIndex = -1;
//		RB_IDON->Checked = true;
		AggiornaDB();
		Opts.Clear();
		Opts << loPartialKey;
		ADOQuery1->Locate("ID_Pallet", IntToStr(UseTAG), Opts);
        LE_NewID->Text = 0;
	}    */
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnCancellaPalletClick(TObject *Sender)
{
	int i;

	if ((Application->MessageBox(String::LoadStr(IDS_DELETEPALLET).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	if (DBGrid1->SelectedRows->Count > 0) {
		for (i = 0; i < DBGrid1->SelectedRows->Count; i++) {
			DBGrid1->DataSource->DataSet->GotoBookmark(DBGrid1->SelectedRows->Items[i]);
			if (ADOQuery1ID_Pezzo->AsInteger > 0) {
				DBDataModule->CancellaPezzo(ADOQuery1ID_Pezzo->AsInteger);
			}
			DBDataModule->CancellaPallet(ADOQuery1ID_Pallet->AsInteger);
            //Log
			DBDataModule->Log("CLIENT", "PALLET", "Cancellato Pallet=%d", ADOQuery1ID_Pallet->AsInteger);
		}
	} else {
		if (ADOQuery1ID_Pezzo->AsInteger > 0) {
			DBDataModule->CancellaPezzo(ADOQuery1ID_Pezzo->AsInteger);
		}
		DBDataModule->CancellaPallet(ADOQuery1ID_Pallet->AsInteger);
        //Log
        DBDataModule->Log("CLIENT", "PALLET", "Cancellato Pallet=%d", ADOQuery1ID_Pallet->AsInteger);
	}
	DBGrid1->SelectedRows->Clear();
	AggiornaDB();
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnModificaPalletClick(TObject *Sender)
{
	FormModificaPallet->m_PalletList.clear();
	FormModificaPallet->ImpostaDati(ADOQuery1ID_Pallet->Value);
	FormModificaPallet->m_PalletList.push_back(ADOQuery1ID_Pallet->Value);
	FormModificaPallet->ShowModal();
	AggiornaDB();
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (Shift.Contains(ssCtrl) && (Key == VK_DELETE))
		Key = 0; // ignore
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnNuovoPalletClick(TObject *Sender)
{
	TPallet p;
	TLocateOptions Opts;
	int newid;

	newid = DBDataModule->NewPalletID();
	if (newid > 0) {
		p.ID_Pallet = newid;
		p.ID_TipoPallet = 0;
		p.Stato_Pallet = 0;
		p.Extra_Altezza = 0;
		p.Alias = IntToStr(newid);
        p.ID_Pezzo = 0;
		DBDataModule->CreaPallet(p);
		//AggiornaDB();
		Opts.Clear();
		Opts << loPartialKey;
		ADOQuery1->Locate("ID_Pallet", IntToStr(newid), Opts);
		FormModificaPallet->m_PalletList.clear();
		FormModificaPallet->ImpostaDati(newid);
		FormModificaPallet->m_PalletList.push_back(newid);
		if (FormModificaPallet->ShowModal() == mrCancel) {
        	//Se non confermo le modifiche allora cancello il pallet creato
			DBDataModule->CancellaPallet(newid);
		}
        else {
            //Log
            DBDataModule->Log("CLIENT", "PALLET", "Creato Pallet=%d", newid);
        }
		AggiornaDB();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::LE_NewIDClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsNumForm->Valore->Text = le->Text;
		if (InsNumForm->ShowModal() == IDOK) {
			le->Text = InsNumForm->Valore->Text;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::OnDoubleClickPalletGrid(TObject *Sender)
{
	BitBtnModificaPalletClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::DBGridFasiDrawColumnCell(TObject *Sender, const TRect &Rect,
          int DataCol, TColumn *Column, TGridDrawState State)
{
	if ((ADOQuery2LavoroAttivo->Value == 0) && (ADOQuery2FaseCompletata->Value == 1)) {
		DBGridFasi->Canvas->Brush->Color = clAqua;
	}
	else if (ADOQuery2LavoroAttivo->Value  == 0) {
		DBGridFasi->Canvas->Brush->Color = clSkyBlue;
	}
	else if (ADOQuery2FaseCompletata->Value == 1) {
		DBGridFasi->Canvas->Brush->Color = clGreen;
	}
	else if (ADOQuery2ID_Job->Value != 0 && ADOQuery2ID_Missione->Value == 0) {
		DBGridFasi->Canvas->Brush->Color = clYellow;
	}
	else if (ADOQuery2ID_Missione->Value != 0) {
		DBGridFasi->Canvas->Brush->Color = clLime;
	}
	else {
		DBGridFasi->Canvas->Brush->Color = clBtnFace;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnAttivaClick(TObject *Sender)
{
	DBDataModule->AttivaLavoroInCorso(ADOQuery2ID_Lavoro->Value);
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnDisattivaLottoClick(TObject *Sender)
{
	DBDataModule->DisattivaLavoroInCorso(ADOQuery2ID_Lavoro->Value);
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::ADOQuery1AfterScroll(TDataSet *DataSet)
{
	if (m_bIsFormCreate) {
        AggiornaQuery2();
    }
}
//---------------------------------------------------------------------------

void TPalletForm::AggiornaQuery2()
{
	AnsiString strsql;

	ADOQuery2->Close();
	strsql.printf("Select * from FasiInLavorazione where ID_Pallet = %d AND ID_Lavoro = %d", ADOQuery1ID_Pallet->Value, ADOQuery1ID_Lavoro->Value);
	ADOQuery2->SQL->Text = strsql;
	ADOQuery2->Open();
	DBGridFasi->AutoFitAll();
	DBGridFasi->Invalidate();
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::bCancellaLavorazioneClick(TObject *Sender)
{
	TPallet p;
	int ID_Lavoro;

	ID_Lavoro = DBDataModule->PalletAssociatoLavorazione(ADOQuery1ID_Pallet->Value);
	if (ID_Lavoro == ADOQuery2ID_Lavoro->Value) {
    	DBDataModule->LeggiPallet(ADOQuery1ID_Pallet->Value, p);
		DBDataModule->CancellaLavorazioneAttiva(ID_Lavoro);
        if (p.ID_Pallet > 0) {
        	DBDataModule->SvuotaPallet(p);
        }
		AggiornaDB();
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::DBGrid1DrawDataCell(TObject *Sender, const TRect &Rect,
          TField *Field, TGridDrawState State)
{
	if ((ADOQuery1ID_Lotto->Value != 0) && (ADOQuery1Sospeso->Value == 0)) {
		DBGrid1->Canvas->Brush->Color = clAqua;
	}
	else if ((ADOQuery1ID_Lotto->Value == 0) && (ADOQuery1ID_Lavoro->Value != 0)) {
		DBGrid1->Canvas->Brush->Color = clLime;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::BitBtnCreaLavoroClick(TObject *Sender)
{
	if (DBDataModule->PalletAssociatoLottoAttivo(ADOQuery1ID_Pallet->Value)) {
		Application->MessageBox(String::LoadStr(IDS_LOTTOGIAATTIVO).w_str() , String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
		return;
	}
	if (DBDataModule->PalletAssociatoLavorazione(ADOQuery1ID_Pallet->Value)) {
		Application->MessageBox(String::LoadStr(IDS_LAVGIAATTIVAPS).w_str() , String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
		return;
	}
	FormNewLavorazioni->SelectPallet(ADOQuery1ID_Pallet->Value);
	if (FormNewLavorazioni->ShowModal() == IDOK) {
    	AggiornaDB();
    }
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::FormResize(TObject *Sender)
{
	//Controllo se � la prima volta che passo da qui
	if (!m_bIsFormCreate) {
    	//Dichiaro la form creata e visualizzata
        m_bIsFormCreate = true;
		//Aggiorno la query2 delle lavorazioni
        AggiornaQuery2();
    }
}
//---------------------------------------------------------------------------

void __fastcall TPalletForm::FormDeactivate(TObject *Sender)
{
    //Dichiaro la form non creata e visualizzata
    m_bIsFormCreate = false;
}
//---------------------------------------------------------------------------

