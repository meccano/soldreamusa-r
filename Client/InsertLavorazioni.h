//---------------------------------------------------------------------------

#ifndef InsertLavorazioniH
#define InsertLavorazioniH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MyDBGrid.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
//---------------------------------------------------------------------------
class TFormInsertLavorazioni : public TForm
{
__published:	// IDE-managed Components
	TGridPanel *GridPanel1;
	TMyDBGrid *MyDBGrid1;
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TADOQuery *ADOQuery1;
	TDataSource *DataSource1;
	TAutoIncField *ADOQuery1ID_Lavorazione;
	TStringField *ADOQuery1Codice;
	TStringField *ADOQuery1Descrizione;
	TStringField *ADOQuery1Articolo;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall MyDBGrid1CellClick(TColumn *Column);
private:	// User declarations
public:		// User declarations
	__fastcall TFormInsertLavorazioni(TComponent* Owner);
	void FilterPalletType(int tp);
	void UnFilterPalletType();
	int Selected;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormInsertLavorazioni *FormInsertLavorazioni;
//---------------------------------------------------------------------------
#endif
