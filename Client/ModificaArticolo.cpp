//---------------------------------------------------------------------------

#pragma hdrstop
#include "ModificaArticolo.h"
#include "StringTableID.h"
#include "InsAlpha.h"
#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormModificaArticolo *FormModificaArticolo;
//---------------------------------------------------------------------------
__fastcall TFormModificaArticolo::TFormModificaArticolo(TComponent* Owner)
	: TForm(Owner)
{
	ID_Articolo = 0;
}
//---------------------------------------------------------------------------

void TFormModificaArticolo::ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue)
{
	UINT i;

	for (i = 0; i < rl.size(); i++) {
		if (rl[i][KeyField].ToIntDef(-1) == KeyValue ) {
			cb->ItemIndex = i;
			return;
		}
	}
}
//---------------------------------------------------------------------------

void TFormModificaArticolo::ImpostaDati(int ID) {
	UINT i;

	ID_Articolo = ID;
	LE_ID_Articolo->Enabled = false;
	memset(&a, 0, sizeof(TArticolo));
	DBDataModule->CaricaTabella("TipiPezzi", RecList);

	ComboBox1->Clear();
	for (i = 0; i < RecList.size(); i++) {
		ComboBox1->Items->Add(RecList[i]["ID_TipoPezzo"] + " - " + RecList[i]["Descrizione_Pezzo"]);
	}
	if (ID >= 0) {
		LE_ID_Articolo->Text = ID;
		DBDataModule->LeggiArticolo(ID, a);
		ImpostaComboBox(ComboBox1, RecList, "ID_TipoPezzo", a.ID_TipoPezzo);
		LE_Codice_Articolo->Text = a.Codice_Articolo;
		LE_Descrizione->Text = a.Descrizione_Articolo;
	}
	else {
		//Se ho un solo elemento seleziono sempre il primo
		ComboBox1->ItemIndex = (RecList.size() == 1) ? 0 : -1;
		LE_Codice_Articolo->Text = "";
		LE_Descrizione->Text = "";
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaArticolo::BitBtnOKClick(TObject *Sender)
{
	TArticolo aaa;

	a.ID_TipoPezzo 			= RecList[ComboBox1->ItemIndex]["ID_TipoPezzo"].ToIntDef(0);
	a.Codice_Articolo   	= LE_Codice_Articolo->Text;
	a.Descrizione_Articolo 	= LE_Descrizione->Text;
	if (ID_Articolo >= 0) {
		// Modifica articolo
		a.ID_Articolo = ID_Articolo;
		DBDataModule->ModificaArticolo(a);
	} else {
		// Controllo non esista gi�
		DBDataModule->LeggiArticoloDaCodice(a.Codice_Articolo, aaa);
		if (aaa.ID_Articolo ) {
			Application->MessageBox(String::LoadStr(IDS_ESISTEARTICOLO).w_str(), String::LoadStr(IDS_ALLARME).w_str(), MB_OK);
		} else {
			// Crea nuovo articolo
			DBDataModule->CreaArticolo(a);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaArticolo::LE_Codice_ArticoloClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaArticolo::LE_DescrizioneClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}
}
//---------------------------------------------------------------------------

