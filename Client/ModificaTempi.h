//---------------------------------------------------------------------------

#ifndef ModificaTempiH
#define ModificaTempiH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "DB.h"
//---------------------------------------------------------------------------
class TFormModificaTempi : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GroupBox1;
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TLabeledEdit *leProgramma;
	TLabeledEdit *leUtensile;
	TLabeledEdit *leTempo;
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall leProgrammaClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormModificaTempi(TComponent* Owner);
	TRecordList TabArticoli;
	int ID_Pallet;
	TPallet p;
	TArticolo Articolo;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormModificaTempi *FormModificaTempi;
//---------------------------------------------------------------------------
#endif
