//---------------------------------------------------------------------------

#ifndef ModificaLottoH
#define ModificaLottoH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.ComCtrls.hpp>
#include "DB.h"
//---------------------------------------------------------------------------
class TFormModificaLotto : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TGroupBox *GroupBox1;
	TLabeledEdit *LE_Descrizione;
	TLabeledEdit *LE_CodiceLotto;
	TLabeledEdit *leQtaDaProdurre;
	TComboBox *cbArticoli;
	TStaticText *StaticText3;
	TLabeledEdit *leQtaProdotta;
	TStaticText *StaticText2;
	TComboBox *CmbLavorazioni;
	TTrackBar *TrackBar1;
	TLabeledEdit *LE_Pri;
	TCheckBox *CheckBox1;
	TLabeledEdit *leIdPallet;
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall cbArticoliChange(TObject *Sender);
	void __fastcall CmbLavorazioniChange(TObject *Sender);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall leIdPalletClick(TObject *Sender);
	void __fastcall leQtaDaProdurreChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormModificaLotto(TComponent* Owner);
	void ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue);
	void ImpostaDati(int ID);
	void PreparaDati(int ID);
	void CaricaComboBoxArticoli();
    void CaricaComboBoxLavorazioni(int ID_Articolo);
	TRecordList RecList, m_TabLavorazioni, m_TabArticoli;
	int ID_Lotto;
	int ID_Lavorazione;
    bool m_PalletSelected;
    int m_IdArticolo;
	TLotto strLotto;
	TLavorazione strLavorazione;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormModificaLotto *FormModificaLotto;
//---------------------------------------------------------------------------
#endif
