//---------------------------------------------------------------------------

#pragma hdrstop

#include "SelectPallet.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TFormSelectPallet *FormSelectPallet;
//---------------------------------------------------------------------------
__fastcall TFormSelectPallet::TFormSelectPallet(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormSelectPallet::FormClose(TObject *Sender, TCloseAction &Action)
{
	if (!MyDBGrid1->Options.Contains(dgMultiSelect)) {
		ADOQuery1->Close();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormSelectPallet::FormShow(TObject *Sender)
{
//	Selected = false;
	BitBtnOK->Enabled = false;
	ADOQuery1->Filtered = false;
	ADOQuery1->Open();
	MyDBGrid1->AutoFitAll();
}
//---------------------------------------------------------------------------

void __fastcall TFormSelectPallet::MyDBGrid1CellClick(TColumn *Column)
{
	Selected = ADOQuery1ID_Pallet->Value;
	if (Selected || (MyDBGrid1->SelectedRows->Count > 0))
		BitBtnOK->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormSelectPallet::BitBtnOKClick(TObject *Sender)
{
	int i;

	if (MyDBGrid1->SelectedRows->Count > 0) {
		MultiSelected = "";
		for (i = 0; i < MyDBGrid1->SelectedRows->Count; i++) {
			MyDBGrid1->DataSource->DataSet->GotoBookmark(MyDBGrid1->SelectedRows->Items[i]);
			if (i > 0) {
				MultiSelected += ", ";
			}
			MultiSelected += ADOQuery1ID_Pallet->Value;
		}
	}
}
//---------------------------------------------------------------------------

