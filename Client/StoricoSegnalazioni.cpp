//---------------------------------------------------------------------------

#pragma hdrstop

#include "StoricoSegnalazioni.h"
#include "InsAlpha.h"
#include "PLCTag.h"
#include "Socket.h"
#include "ClientData.h"
#include "DB.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TStoricoSegnalazioniForm *StoricoSegnalazioniForm;
//---------------------------------------------------------------------------
__fastcall TStoricoSegnalazioniForm::TStoricoSegnalazioniForm(TComponent* Owner)
	: TMDIChild(Owner)
{
}
//---------------------------------------------------------------------------

void TStoricoSegnalazioniForm::Aggiorna()
{
	TADOQuery *ADOQuery;
	AnsiString cond, da, a, strsql, SortString, FilterString;

	ADOQuery1->DisableControls();
	try {
		DBGrid1->StoreBookmark();
		SortString = ADOQuery1->Sort;
		FilterString = ADOQuery1->Filter;
		ADOQuery1->Close();
//		da = DateToStr(DateTimePicker1->Date) + " " + TimeToStr(DateTimePicker2->Time);
//		a = DateToStr(DateTimePicker3->Date) + " " + TimeToStr(DateTimePicker4->Time);
		da = FormatDateTime("yyyy-mm-dd", DateTimePicker1->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker2->Time);
		a = FormatDateTime("yyyy-mm-dd", DateTimePicker3->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker4->Time);
		cond.printf("WHERE (DataOra >= CAST('%s' AS DATETIME)) AND (DataOra <= CAST('%s' AS DATETIME))", da, a);
		strsql = "SELECT *, CASE WHEN Acquisito IS NULL THEN 0 ELSE 1 END AS Ack FROM Segnalazioni " + cond + " ORDER BY Ack, DataOra DESC";
		ADOQuery1->SQL->Text = strsql;
		ADOQuery1->Open();
		ADOQuery1->Sort = SortString;
		ADOQuery1->Filter = FilterString;
		DBGrid1->AutoFitAll();
		DBGrid1->RecallBookmark();
	} __finally {
		ADOQuery1->EnableControls();
	};
	StaticText1->Caption = "Tot.:" + IntToStr(ADOQuery1->RecordCount);
}
//---------------------------------------------------------------------------

void TStoricoSegnalazioniForm::AggiornaLog()
{
	TADOQuery *ADOQuery;
	AnsiString cond, da, a, strsql, SortString, FilterString;

	ADOQuery2->DisableControls();
	try {
		dbgLog->StoreBookmark();
		SortString = ADOQuery2->Sort;
		FilterString = ADOQuery2->Filter;
		ADOQuery2->Close();
		da = FormatDateTime("yyyy-mm-dd", DateTimePicker1->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker2->Time);
		a = FormatDateTime("yyyy-mm-dd", DateTimePicker3->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker4->Time);
		cond.printf("WHERE (DataOra >= CAST('%s' AS DATETIME)) AND (DataOra <= CAST('%s' AS DATETIME))", da, a);
		strsql = "SELECT * FROM Storico " + cond + " ORDER BY DataOra";
		ADOQuery2->SQL->Text = strsql;
		ADOQuery2->Open();
		ADOQuery2->Sort = SortString;
		ADOQuery2->Filter = FilterString;
		dbgLog->AutoFitAll();
		dbgLog->RecallBookmark();
	} __finally {
		ADOQuery2->EnableControls();
	};
	StaticText1->Caption = "Tot.:" + IntToStr(ADOQuery2->RecordCount);
}
//---------------------------------------------------------------------------

void TStoricoSegnalazioniForm::AggiornaProduzione()
{
	TADOQuery *ADOQuery;
	AnsiString cond, da, a, strsql, SortString, FilterString, FieldFilter;

	ADOQuery3->DisableControls();
	try {
		gbgProd->StoreBookmark();
		SortString = ADOQuery3->Sort;
		FilterString = ADOQuery3->Filter;
		ADOQuery3->Close();
		da = FormatDateTime("yyyy-mm-dd", DateTimePicker1->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker2->Time);
		a = FormatDateTime("yyyy-mm-dd", DateTimePicker3->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker4->Time);
		switch(rgFilter->ItemIndex) {
		case 0:
			FieldFilter = "DataOra";
			break;
		case 1:
			FieldFilter = "Inizio_Lavoro";
			break;
		case 2:
			FieldFilter = "Fine_Lavoro";
			break;
		}
		cond.printf("WHERE (%s >= CAST('%s' AS DATETIME)) AND (%s <= CAST('%s' AS DATETIME))", FieldFilter, da, FieldFilter, a);
		strsql.printf("SELECT S.DataOra, S.ID_Pallet, CAST(S.ID_Magazzino AS VARCHAR) + '/' + CAST(S.ID_Postazione AS VARCHAR) AS Posizione,            "
					  "	   S.Codice_Articolo, S.NumOrdine, M.Descrizione_Macchina, S.Programma_Macchina, S.Attivazione, S.Inizio_Lavoro, S.Fine_Lavoro,              "
					  "     Fine_Lavoro - Inizio_Lavoro AS T_Lavoro,                                                                                    "
					  "	   CAST(S.Magazzino_Destinazione AS VARCHAR) + '/' + CAST(S.Postazione_Destinazione AS VARCHAR) AS Destinazione                 "
					  "FROM StoricoProduzione AS S LEFT OUTER JOIN Macchine AS M ON S.ID_Macchina = M.ID_Macchina AND S.Tipo_Macchina = M.Tipo_Macchina "
					  "%s ORDER BY %s", cond, FieldFilter);
		ADOQuery3->SQL->Text = strsql;
		ADOQuery3->Open();
		ADOQuery3->Sort = SortString;
		ADOQuery3->Filter = FilterString;
		gbgProd->AutoFitAll();
		gbgProd->RecallBookmark();
	} __finally {
		ADOQuery3->EnableControls();
	};
	StaticText1->Caption = "Tot.:" + IntToStr(ADOQuery3->RecordCount);
}
//---------------------------------------------------------------------------

void TStoricoSegnalazioniForm::AggiornaEventiRobot1()
{
	AnsiString cond, da, a, strsql, SortString, FilterString;

	if (!DBDataModule->ADOConnection1->Connected) return;
	ADOQuery4->DisableControls();
	try {
		dbgEventiRobot1->StoreBookmark();
		SortString = ADOQuery4->Sort;
		FilterString = ADOQuery4->Filter;
		ADOQuery4->Close();
		da = FormatDateTime("yyyy-mm-dd", DateTimePicker1->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker2->Time);
		a = FormatDateTime("yyyy-mm-dd", DateTimePicker3->Date) + "T" + FormatDateTime("hh:nn:ss", DateTimePicker4->Time);
		cond.printf("WHERE (DataOra >= CAST('%s' AS DATETIME)) AND (DataOra <= CAST('%s' AS DATETIME)) AND SORGENTE = 'ROBOT1'", da, a);
		strsql = "SELECT * FROM EventiRobot " + cond + " ORDER BY DataOra DESC";
		ADOQuery4->SQL->Text = strsql;
		ADOQuery4->Open();
		ADOQuery4->Filter = FilterString;
		ADOQuery4->Sort = SortString;
		dbgEventiRobot1->AutoFitAll();
        dbgEventiRobot1->RecallBookmark();
	} __finally {
		ADOQuery4->EnableControls();
	};
    StaticText1->Caption = "Record: " + IntToStr(ADOQuery4->RecordCount);
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::FormCreate(TObject *Sender)
{
	DateTimePicker1->Date = Now();
	DateTimePicker3->Date = Now();
	PageControl1->ActivePageIndex = 0;
}
//---------------------------------------------------------------------------
void __fastcall TStoricoSegnalazioniForm::FormActivate(TObject *Sender)
{
	Timer1Timer(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::DBGrid1DrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column,
          TGridDrawState State)
{
	if (ADOQuery1->RecordCount == 0) {
		return;
	}
	if (ADOQuery1Acquisito->IsNull && ADOQuery1Allarme->AsInteger) {
		DBGrid1->Canvas->Font->Color = clWhite;
		DBGrid1->Canvas->Brush->Color = clRed;
	} else if (ADOQuery1Acquisito->IsNull && !ADOQuery1Allarme->AsInteger) {
		DBGrid1->Canvas->Font->Color = clBlack;
		DBGrid1->Canvas->Brush->Color = clYellow;
	}
}
//---------------------------------------------------------------------------




void __fastcall TStoricoSegnalazioniForm::Timer1Timer(TObject *Sender)
{
	switch(PageControl1->ActivePageIndex) {
	case 0:
		Aggiorna();
		break;
	case 1:
		AggiornaLog();
		break;
	case 2:
		AggiornaProduzione();
		break;
	case 3:
		AggiornaEventiRobot1();
		break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::PageControl1Change(TObject *Sender)
{
	switch(PageControl1->ActivePageIndex) {
	case 0:
		bEsporta->Visible = true;
		if (ADOQuery1->Active) {
			ADOQuery1->Close();
		}
		rgFilter->Visible = false;
		Aggiorna();
		break;
	case 1:
		bEsporta->Visible = true;
		if (ADOQuery2->Active) {
			ADOQuery2->Close();
		}
		rgFilter->Visible = false;
		AggiornaLog();
		ADOQuery2->Last();
		break;
	case 2:
		if (ADOQuery3->Active) {
			ADOQuery3->Close();
		}
		rgFilter->Visible = true;
		AggiornaProduzione();
		ADOQuery3->Last();
		bEsporta->Visible = true;
		break;
	case 3:
		if (ADOQuery4->Active) {
			ADOQuery4->Close();
		}
		rgFilter->Visible = true;
		AggiornaEventiRobot1();
		ADOQuery4->Last();
		bEsporta->Visible = true;
		break;
	}
	BitBtn1->Visible = (PageControl1->ActivePageIndex == 0);
}
//---------------------------------------------------------------------------

void __fastcall TStoricoSegnalazioniForm::bEsportaClick(TObject *Sender)
{
	AnsiString FileName;
	UnicodeString dt;
	TMyDBGrid *g;

	DateTimeToString(dt, L"yyyymmddhhnnss", Now());
	switch(PageControl1->ActivePageIndex) {
	case 0:
		FileName = "Allarmi_" + dt + ".csv";
		g = DBGrid1;
		SalvataggioStorico(g, FileName);
		break;
	case 1:
		FileName = "Log_" + dt + ".csv";
		g = dbgLog;
		SalvataggioStorico(g, SaveDialog1->FileName);
		break;
	case 2:
		FileName = "Produzione_" + dt + ".csv";
		g = gbgProd;
		SalvataggioStorico(g, FileName);
		break;
	case 3:
		FileName = "Lotti_" + dt + ".csv";
		g = dbgEventiRobot1;
		SalvataggioStorico(g, FileName);
		break;
	}
}
//---------------------------------------------------------------------------

void TStoricoSegnalazioniForm::SalvataggioStorico(TMyDBGrid *grid, AnsiString FileName)
{
	TADOQuery *q;
	AnsiString ln, val;
	int i;
	TStringList *Lines;

	ln = "";
	for(i = 0; i < grid->Columns->Count; i++) {
		ln = ln + '"' + grid->Columns->Items[i]->Title->Caption + '"';
		if (i < grid->Columns->Count - 1) {
			ln = ln + ";";
		}
	}
	Lines = new TStringList();
	Lines->Clear();
	Lines->Add(ln);
	q = (TADOQuery *)grid->DataSource->DataSet;
	q->DisableControls();
	try {
		q->First();
		while (!q->Eof) {
			ln = "";
			for(i = 0; i < grid->Columns->Count; i++) {
				val = q->FieldByName(grid->Columns->Items[i]->FieldName)->AsString;
				ln = ln + '"' + val + '"';
				if (i < grid->Columns->Count - 1) {
					ln = ln + ";";
				}
			}
			Lines->Add(ln);
			q->Next();
		}
	} catch(...) {}
	q->EnableControls();

	SaveDialog1->FileName = FileName;
	if (SaveDialog1->Execute()) {
		Lines->SaveToFile(SaveDialog1->FileName);
	}
	delete Lines;
}
//---------------------------------------------------------------------------


void __fastcall TStoricoSegnalazioniForm::BitBtn1Click(TObject *Sender)
{
	DBDataModule->AcquisisciSegnalazioni();
}
//---------------------------------------------------------------------------

