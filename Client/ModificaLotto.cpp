//---------------------------------------------------------------------------

#pragma hdrstop

#include "ModificaLotto.h"
#include "StringTableID.h"
#include "InsAlpha.h"
#include "Main.h"
#include "InsertLavorazioni.h"
#include "SelectPallet.h"
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormModificaLotto *FormModificaLotto;
//---------------------------------------------------------------------------
__fastcall TFormModificaLotto::TFormModificaLotto(TComponent* Owner)
	: TForm(Owner)
{
	ID_Lotto = 0;
	ID_Lavorazione = -1;
    m_IdArticolo = -1;
    m_PalletSelected = false;
    leIdPallet->ReadOnly = false;
}
//---------------------------------------------------------------------------

void TFormModificaLotto::ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue)
{
	UINT i;

	for (i = 0; i < rl.size(); i++) {
		if (rl[i][KeyField].ToIntDef(-1) == KeyValue ){
			cb->ItemIndex = i;
			return;
		}
	}
}
//---------------------------------------------------------------------------

void TFormModificaLotto::CaricaComboBoxArticoli()
{
	AnsiString tab, val;
	UINT i;

	tab.sprintf("Articoli ORDER BY Codice_Articolo");
	DBDataModule->CaricaTabella(tab, m_TabArticoli);
	cbArticoli->Clear();
	for (i = 0; i < m_TabArticoli.size(); i++) {
		val = m_TabArticoli[i]["Codice_Articolo"] + " - " + m_TabArticoli[i]["Descrizione_Articolo"];
		cbArticoli->Items->Add(val);
	}
}
//---------------------------------------------------------------------------

void TFormModificaLotto::ImpostaDati(int ID)
{
	TLavorazione lav;
	ID_Lotto = ID;

	CaricaComboBoxArticoli();
	if (ID >= 0)  {
		m_PalletSelected = ID;
    	//Modificando un lotto impedisco di modificare il pallet
		DBDataModule->LeggiLotto(ID, strLotto);
		ID_Lavorazione = strLotto.ID_Lavorazione;
		LE_Descrizione->Text = strLotto.Descrizione;
		LE_CodiceLotto->Text = strLotto.Codice;
		leQtaDaProdurre->Text = strLotto.Qta;
        TrackBar1->Position = strLotto.Priority;
        CheckBox1->Checked = !strLotto.Sospeso;
		leIdPallet->Text = strLotto.ID_Pallet;
		DBDataModule->LeggiLavorazione(strLotto.ID_Lavorazione, lav);
		leQtaProdotta->Text = strLotto.QtaProdotta;

        m_IdArticolo = lav.ID_Articolo;
		ImpostaComboBox(cbArticoli, m_TabArticoli, "ID_Articolo", m_IdArticolo);

		CaricaComboBoxLavorazioni(m_IdArticolo);
		ImpostaComboBox(CmbLavorazioni, m_TabLavorazioni, "ID_Lavorazione", strLotto.ID_Lavorazione);
		BitBtnOK->Enabled = !(ID_Lavorazione == -1 || !m_PalletSelected || leIdPallet->Text.IsEmpty() || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0);

        //Inibisco la modifica di alcuni campi durante l'editazione di un lotto
        cbArticoli->Enabled = false;
        CmbLavorazioni->Enabled = false;
        leIdPallet->Enabled = false;
	}
	else {
		ID_Lavorazione = -1;
		m_IdArticolo = -1;
        leIdPallet->Text = "";
		LE_Descrizione->Text = "";
		LE_CodiceLotto->Text = "";
		leQtaDaProdurre->Text = "";
		leQtaProdotta->Text = "";
		CmbLavorazioni->Clear();
        TrackBar1->Position = 50;
		CheckBox1->Checked = false;
		BitBtnOK->Enabled = false;
        m_PalletSelected = false;
        cbArticoli->Enabled = true;
        CmbLavorazioni->Enabled = true;
        leIdPallet->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void TFormModificaLotto::PreparaDati(int ID)
{
	TPallet p;
	TPezzo pz;
    TArticolo a;

	CaricaComboBoxArticoli();
	if (ID >= 0)  {
		leIdPallet->Text = ID;
		m_PalletSelected = true;
		ID_Lavorazione = -1;
		DBDataModule->LeggiPallet(ID, p);
		DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
        DBDataModule->LeggiArticolo(pz.ID_Articolo, a);
		m_IdArticolo = pz.ID_Articolo;
		LE_Descrizione->Text = a.Descrizione_Articolo;
		LE_CodiceLotto->Text = a.Codice_Articolo;
		leQtaDaProdurre->Text = "";
		leQtaProdotta->Text = "";
		CmbLavorazioni->Clear();
		TrackBar1->Position = 50;
		CheckBox1->Checked = false;
		ImpostaComboBox(cbArticoli, m_TabArticoli, "ID_Articolo", m_IdArticolo);
		CaricaComboBoxLavorazioni(m_IdArticolo);
		ImpostaComboBox(CmbLavorazioni, m_TabLavorazioni, "ID_Lavorazione", strLotto.ID_Lavorazione);
		BitBtnOK->Enabled = !(ID_Lavorazione == -1 || !m_PalletSelected || leIdPallet->Text.IsEmpty() || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0);
	}
	else {
		ID_Lavorazione = -1;
		m_IdArticolo = -1;
		leIdPallet->Text = "";
		LE_Descrizione->Text = "";
		LE_CodiceLotto->Text = "";
		leQtaDaProdurre->Text = "";
		leQtaProdotta->Text = "";
		CmbLavorazioni->Clear();
		TrackBar1->Position = 50;
		m_PalletSelected = false;
		CheckBox1->Checked = false;
		BitBtnOK->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLotto::BitBtnOKClick(TObject *Sender)
{
	int i, qta, prodotti, qtatot, resto, npallet = 0;
	std::vector<std::string> vectline;
	std::string line;
	AnsiString aline;
	TPallet p;
	TPezzo pz;
	String sMsg;
	TLotto l;

	//Non sono permesse lavorazioni con articolo non definito
	if (ID_Lavorazione == -1 || !m_PalletSelected || leIdPallet->Text.IsEmpty() || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0) {
		ModalResult = mrNone;
		return;
    }
	if (ID_Lotto >= 0) {
		// Verifico che il pallet non sia in una fase divera da quella iniziale
		DBDataModule->LeggiPallet(leIdPallet->Text.ToIntDef(0), p);
		DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
        //Controllo che ci sia il pezzo sul pallet
        if (p.ID_Pezzo == 0) {
            //Associo il pezzo appena creato al pallet
            p.ID_Pezzo = DBDataModule->CreaPezzo(pz);
            //Associo ID Pezzo
            pz.ID_Pezzo = p.ID_Pezzo;
            //Aggiorno il pallet
            DBDataModule->AggiornaPallet(p);
        }

        //Controllo che il pezzo sia associato l'articolo
        if (pz.ID_Articolo == 0) {
            //Associo l'articolo al pezzo
            pz.ID_Articolo = m_IdArticolo;
            //Aggiorno il pezzo
            DBDataModule->AggiornaPezzo(pz);
        }

		if (CheckBox1->Checked && (pz.Stato_Pezzo != 0)) {
			sMsg.printf(String::LoadStr(IDS_PALLETSTATE).w_str() , leIdPallet->Text.ToIntDef(0));
			Application->MessageBox(sMsg.w_str(), L"ERRORE", MB_OK);
			return;
		}
		// Modifica Lotto
		strLotto.ID_Lavorazione = ID_Lavorazione;
		strLotto.Codice = LE_CodiceLotto->Text;
		strLotto.Descrizione = LE_Descrizione->Text;
		strLotto.Qta = leQtaDaProdurre->Text.ToIntDef(0);
		strLotto.QtaProdotta = leQtaProdotta->Text.ToIntDef(0);
		strLotto.Priority = LE_Pri->Text.ToIntDef(50);
		strLotto.Sospeso = !CheckBox1->Checked;
		strLotto.ID_Pallet = leIdPallet->Text.ToIntDef(0);
		strLotto.ID_Lotto = ID_Lotto;
		DBDataModule->ModificaLotto(strLotto);
		DBDataModule->LeggiLotto(ID_Lotto, l);
		DBDataModule->AggiornaPrioritaLavoroInCorso(l.ID_Lavoro, l.Priority);
		if (CheckBox1->Checked) {
			DBDataModule->AttivaLavoroInCorso(l.ID_Lavoro);
		}
	} else {
		aline = leIdPallet->Text;
		line = aline.c_str();
		split(vectline, line, boost::is_any_of(","));
		npallet = vectline.size();
		qtatot = leQtaDaProdurre->Text.ToIntDef(0);
		prodotti = leQtaProdotta->Text.ToIntDef(0);
		qta = (qtatot - prodotti) / npallet;
		resto = qtatot % npallet;
		for (i = 0; i < npallet; i++) {
			aline = vectline[i].c_str();
			strLotto.ID_Pallet = aline.ToIntDef(0);

			// Verifico che il pallet non sia in una fase divera da quella iniziale
			DBDataModule->LeggiPallet(strLotto.ID_Pallet, p);

            //Leggo il pezzo associato al pallet
			DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);

            //Controllo che ci sia il pezzo sul pallet
            if (p.ID_Pezzo == 0) {
            	//Associo il pezzo appena creato al pallet
            	p.ID_Pezzo = DBDataModule->CreaPezzo(pz);
                //Associo ID Pezzo
                pz.ID_Pezzo = p.ID_Pezzo;
                //Aggiorno il pallet
                DBDataModule->AggiornaPallet(p);
            }

            //Controllo che il pezzo sia associato l'articolo
            if (pz.ID_Articolo == 0) {
            	//Associo l'articolo al pezzo
                pz.ID_Articolo = m_IdArticolo;
            	//Aggiorno il pezzo
                DBDataModule->AggiornaPezzo(pz);
            }

            //Controllo lo stato del pezzo che sia congruo
			if (CheckBox1->Checked && (pz.Stato_Pezzo != 0)) {
				sMsg.printf(String::LoadStr(IDS_PALLETSTATE).w_str() , strLotto.ID_Pallet);
				Application->MessageBox(sMsg.w_str(), L"ERRORE", MB_OK);
				continue;
			}
//			strLotto.Qta = qtatot - (qta * (npallet - 1));
			strLotto.Qta = qta;
			if (resto > 0) {
				strLotto.Qta += 1;
				resto--;
			}
			strLotto.ID_Lavorazione = ID_Lavorazione;
			strLotto.Codice = LE_CodiceLotto->Text;
			strLotto.Descrizione = LE_Descrizione->Text;
			strLotto.Priority = LE_Pri->Text.ToIntDef(50);
			strLotto.Sospeso = !CheckBox1->Checked;
			strLotto.QtaProdotta = leQtaProdotta->Text.ToIntDef(0);
			// Crea nuova Lotto
			strLotto.ID_Lotto = DBDataModule->CreaLotto(strLotto);
			if (strLotto.ID_Lotto == 0) {
				Application->MessageBox(String::LoadStr(IDS_ERRORCREALOTTO).w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
			}
			else {
				DBDataModule->LeggiLotto(strLotto.ID_Lotto, l);
				DBDataModule->AggiornaPrioritaLavoroInCorso(l.ID_Lavoro, l.Priority);
                //Controllo se il lotto � da attivare
				if (CheckBox1->Checked) {
                	//Attivo il lavoro associato al lotto
					DBDataModule->AttivaLavoroInCorso(l.ID_Lavoro);
                    //Log attivazione lotto dopo la creazione
					DBDataModule->LogAttivazioneLotto(l.ID_Lotto);
				}
			}
		}
	}
	ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLotto::cbArticoliChange(TObject *Sender)
{
	m_IdArticolo = m_TabArticoli[cbArticoli->ItemIndex]["ID_Articolo"].ToIntDef(0);
	CaricaComboBoxLavorazioni(m_IdArticolo);
	LE_CodiceLotto->Text = m_TabArticoli[cbArticoli->ItemIndex]["Codice_Articolo"];
	LE_Descrizione->Text = m_TabArticoli[cbArticoli->ItemIndex]["Descrizione_Articolo"];
	ID_Lavorazione = -1;
	BitBtnOK->Enabled = !(ID_Lavorazione == -1 || !m_PalletSelected || leIdPallet->Text.IsEmpty() || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0);
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLotto::CmbLavorazioniChange(TObject *Sender)
{
	ID_Lavorazione = m_TabLavorazioni[CmbLavorazioni->ItemIndex]["ID_Lavorazione"].ToIntDef(0);
	BitBtnOK->Enabled = !(ID_Lavorazione == -1 || !m_PalletSelected || leIdPallet->Text.IsEmpty() || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0);
}
//---------------------------------------------------------------------------

void TFormModificaLotto::CaricaComboBoxLavorazioni(int ID_Articolo)
{
	UINT i;
	AnsiString tab;

    tab.sprintf("Lavorazioni WHERE (ID_Articolo = %d) ", ID_Articolo);
    if (DBDataModule->CaricaTabella(tab, m_TabLavorazioni)) {
        CmbLavorazioni->Clear();
		for (i = 0; i < m_TabLavorazioni.size(); i++) {
            CmbLavorazioni->Items->Add(m_TabLavorazioni[i]["Codice"] + " - " + m_TabLavorazioni[i]["Descrizione"]);
        }
	}
	BitBtnOK->Enabled = !(ID_Lavorazione == -1 || !m_PalletSelected || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0);
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLotto::TrackBar1Change(TObject *Sender)
{
	LE_Pri->Text = TrackBar1->Position;
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLotto::leIdPalletClick(TObject *Sender)
{
	TLabeledEdit *l;
	AnsiString sqlstr, codart = "";
	TArticolo a;
	int i;

	l = (TLabeledEdit*)Sender;
	if (m_IdArticolo > 0) {
		DBDataModule->LeggiArticolo(m_IdArticolo, a);
		sqlstr.printf("SELECT * FROM ElencoPalletSenzaLotto WHERE Codice_Articolo = '%s' OR Codice_Articolo IS NULL ORDER BY ID_Pallet", a.Codice_Articolo.c_str());
	} else {
		Application->MessageBox(String::LoadStr(IDS_SELECTARTICOLO).w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
		return;
	}
	FormSelectPallet->ADOQuery1->SQL->Clear();
	FormSelectPallet->ADOQuery1->SQL->Add(sqlstr);
	FormSelectPallet->MyDBGrid1->Options << dgMultiSelect;
	if (FormSelectPallet->ShowModal() == IDOK) {
		if (FormSelectPallet->MultiSelected != "") {
			l->Text = FormSelectPallet->MultiSelected;
			m_PalletSelected = true;
		}
		else {
			//Controllo se un pallet non sia gi� associato ad un lotto
			if (DBDataModule->PalletAssociatoLotto(FormSelectPallet->Selected) == 0) {
				l->Text = FormSelectPallet->Selected;
				m_PalletSelected = true;
			}
			else {
				Application->MessageBox(String::LoadStr(IDS_PALLGIASULOTTO).w_str(), String::LoadStr(IDS_ATTENZIONE).w_str(), MB_OK);
			}
		}
	}
    else {
        m_PalletSelected = false;
    }
	BitBtnOK->Enabled = !(ID_Lavorazione == -1 || !m_PalletSelected || leIdPallet->Text.IsEmpty() || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0);
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaLotto::leQtaDaProdurreChange(TObject *Sender)
{
	BitBtnOK->Enabled = !(ID_Lavorazione == -1 || !m_PalletSelected || leIdPallet->Text.IsEmpty() || m_IdArticolo == -1 || leQtaDaProdurre->Text.ToIntDef(0) == 0);
}
//---------------------------------------------------------------------------

