//---------------------------------------------------------------------------

#ifndef ModificaFaseH
#define ModificaFaseH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include "DB.h"
//---------------------------------------------------------------------------
class TFormModificaFase : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TGroupBox *GroupBox1;
	TLabeledEdit *LE_Descrizione;
    TLabeledEdit *LE_CodiceFase;
	TComboBox *cbb1;
	TLabeledEdit *LE_PRG;
	TStaticText *txt1;
	TOpenDialog *dlgOpen1;
	TCheckBox *ChkBoxAbilitaFase;
	TStaticText *StaticText1;
	TComboBox *ComboTipiPallet;
	TLabeledEdit *LabelMessaggioOp;
	TComboBox *ComboCodOperazione;
	TStaticText *StaticText2;
	TComboBox *ComboNewTipiPallet;
	TStaticText *StaticText3;
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall OnChangeComboBoxMU(TObject *Sender);
	void __fastcall OnChangeComboBoxTipiPallet(TObject *Sender);
	void __fastcall OnChangeComboBoxOperazioni(TObject *Sender);
	void __fastcall OnPartProgramClick(TObject *Sender);
	void __fastcall OnChangeComboBoxNewTipiPallet(TObject *Sender);
	void __fastcall LE_CodiceFaseClick(TObject *Sender);
	void __fastcall LE_DescrizioneClick(TObject *Sender);
	void __fastcall LabelMessaggioOpClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormModificaFase(TComponent* Owner);
	void ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue);
	void ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue, AnsiString KeyField2, AnsiString KeyValue2);
	void ImpostaDati(int IndexLavorazione, int IndexFase);
	void CaricaComboBoxMacchine();
	void CaricaComboBoxTipiPallet();
	void CaricaComboBoxNewTipiPallet();
	void CaricaComboBoxOperazioni();
	void Aggiorna();

	TRecordList RecList;
	TRecordList RecListTipiPallet;
	TRecordList RecListOperazioni;
	int ID_Fase;
	int m_NumSequenza;
	TFase strFase;
	AnsiString m_PPFolderMU[6];
};
//---------------------------------------------------------------------------
extern PACKAGE TFormModificaFase *FormModificaFase;
//---------------------------------------------------------------------------
#endif
