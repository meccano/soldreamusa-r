//---------------------------------------------------------------------------

#pragma hdrstop

#include "InsAlpha.h"
#include "ModificaPallet.h"
#include "StringTableID.h"
#include "InsNum.h"
#include "Main.h"
#include "Define.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "MyDBGrid"
#pragma resource "*.dfm"
TFormModificaPallet *FormModificaPallet;
//---------------------------------------------------------------------------
__fastcall TFormModificaPallet::TFormModificaPallet(TComponent* Owner)
	: TForm(Owner)
{
	ID_Pallet = 0;
    m_bCheckValues = false;
}
//---------------------------------------------------------------------------

void TFormModificaPallet::CaricaComboBoxArticoli(int tpz)
{
	AnsiString tab, val;
	UINT i;

	if (tpz > 0) {
		tab.sprintf("Articoli WHERE ID_TipoPezzo = %d ORDER BY Codice_Articolo", tpz);
	} else {
		tab.sprintf("Articoli ORDER BY Codice_Articolo");
	}
	DBDataModule->CaricaTabella(tab, TabArticoli);
	cbArticoli->Clear();
	for (i = 0; i < TabArticoli.size(); i++) {
		val = TabArticoli[i]["Codice_Articolo"] + " - " + TabArticoli[i]["Descrizione_Articolo"];
		if (TabArticoli[i]["ID_TipoPezzo"].ToIntDef(-1) == tpz) {
			cbArticoli->Items->Add(val);
		}
	}
}
//---------------------------------------------------------------------------

void TFormModificaPallet::ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue)
{
	UINT i;
	AnsiString val;

	for (i = 0; i < rl.size(); i++) {
		val = rl[i][KeyField];
		if (val.ToIntDef(-1) == KeyValue) {
			cb->ItemIndex = i;
			return;
		}
	}
}
//---------------------------------------------------------------------------

void TFormModificaPallet::AggiornaComboTipiPezzi()
{
	UINT i;
	AnsiString sql;

	//Filtro i tipi pezzi in base al tipo pallet scelto
	sql.sprintf("TipiPezzi WHERE (ID_TipoPallet = %d OR ID_TipoPallet = 0)", TabTipiPallet[cbTipiPallet->ItemIndex]["ID_TipoPallet"].ToIntDef(0));
	DBDataModule->CaricaTabella(sql, TabTipiPezzi);
	cbTipiPezzi->Clear();
	for (i = 0; i < TabTipiPezzi.size(); i++) {
		cbTipiPezzi->Items->Add(TabTipiPezzi[i]["ID_TipoPezzo"] + " - " + TabTipiPezzi[i]["Descrizione_Pezzo"]);
	}

	//Se ho un solo elemento seleziono sempre il primo
	if (TabTipiPezzi.size() == 1) {
		cbTipiPezzi->ItemIndex = 0;
	}
}
//---------------------------------------------------------------------------

void TFormModificaPallet::ImpostaDati(int ID) {
	UINT i;
	AnsiString sqlstr, tipomacchina;
	int stato, nmag, npos, nmacchina;

	ID_Pallet = ID;

	DBDataModule->CaricaTabella("TipiPallet", TabTipiPallet);
	cbTipiPallet->Clear();
	for (i = 0; i < TabTipiPallet.size(); i++) {
		cbTipiPallet->Items->Add(TabTipiPallet[i]["ID_TipoPallet"] + " - " + TabTipiPallet[i]["Codice_Tipopallet"]);
	}

	//Se ho un solo elemento seleziono sempre il primo
	if (TabTipiPallet.size() == 1) {
		cbTipiPallet->ItemIndex = 0;
	}

	LE_ID_Pallet->Text = ID_Pallet;
	DBDataModule->LeggiPallet(ID_Pallet, p);
	leDescrizionePallet->Text = p.Descrizione_Pallet;
	ImpostaComboBox(cbTipiPallet, TabTipiPallet, "ID_TipoPallet", p.ID_TipoPallet);

	if ((DBDataModule->CercaPalletMagazzino(ID_Pallet, nmag, npos) == 0) ||
		(DBDataModule->CercaPalletMacchine(ID_Pallet, tipomacchina, nmacchina) == 0)) {
		//Pallet presente in magazzino o in macchina
		// Blocco modifica tipo pallet
		cbTipiPallet->Enabled = false;
	} else {
		cbTipiPallet->Enabled = true;
	}

	//Aggiorno la combo di selezione del tipo pezzo
	AggiornaComboTipiPezzi();

	//Dal pallet so ID_pezzo e ricavo i dati del pezzo associato al pallet
	DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);

	//Dal pezzo so ID_articolo e ricavo i dati dell'articolo associato al pezzo
	DBDataModule->LeggiArticolo(pz.ID_Articolo, a);
	ImpostaComboBox(cbTipiPezzi, TabTipiPezzi, "ID_TipoPezzo", a.ID_TipoPezzo);

	//Popolo il combo degli articoli
	CaricaComboBoxArticoli((TabTipiPezzi.size() == 1) ? 1 : a.ID_TipoPezzo);
	ImpostaComboBox(cbArticoli, TabArticoli, "ID_Articolo", pz.ID_Articolo);
	BitBtnSvuota->Enabled = (p.ID_Pezzo > 0);

    //Abilito la possibili� di selezionare un articolo se il pallet non � associato ad un lotto
	cbArticoli->Enabled = !DBDataModule->PalletAssociatoLotto(ID_Pallet) || (pz.ID_Articolo == 0);
	BitBtnSvuota->Enabled = (MainForm->pwdlevel > 0) && (!DBDataModule->PalletAssociatoLotto(ID_Pallet) || ((p.ID_Pezzo > 0) && (pz.ID_Articolo == 0)));

	// Popolo i checkbox dello stato
	if (p.ID_Pezzo) {
		rgStato->Enabled = true;
		rgErrore->Enabled = true;
		stato = pz.Stato_Pezzo % 100;
		rgStato->ItemIndex = stato;
		stato = pz.Stato_Pezzo / 100;
		rgErrore->ItemIndex = stato;
	} else {
		rgStato->Enabled = false;
		rgErrore->Enabled = false;
		rgStato->ItemIndex = -1;
		rgErrore->ItemIndex = -1;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::cbTipiPezziChange(TObject *Sender)
{
	CaricaComboBoxArticoli(TabTipiPezzi[cbTipiPezzi->ItemIndex]["ID_TipoPezzo"].ToIntDef(0));
	ImpostaComboBox(cbArticoli, TabArticoli, "ID_Articolo", pz.ID_Articolo);
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::BitBtnSvuotaClick(TObject *Sender)
{
    UINT i;
    
    if ((Application->MessageBox(String::LoadStr(IDS_EMPTYPAL).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
	    return;
        
    for (i = 0; i < m_PalletList.size(); i++) {
    	//Leggo il pallet
    	DBDataModule->LeggiPallet(m_PalletList[i], p);
        //Controllo che non ci sia un lotto associato al pallet
		if (DBDataModule->PalletAssociatoLotto(p.ID_Pallet) && (cbArticoli->ItemIndex >= 0)) {
			Application->MessageBox(String::LoadStr(IDS_NO_DELPEZZO).w_str(), String::LoadStr(IDS_ALLARME).w_str(), MB_OK);
        	break;    
        }
        DBDataModule->LeggiPallet(m_PalletList[i], p);
		DBDataModule->SvuotaPallet(p);
    }
    DBDataModule->LeggiPallet(m_PalletList[0], p);
    ImpostaDati(p.ID_Pallet);
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::cbArticoliChange(TObject *Sender)
{
	pz.ID_Articolo = TabArticoli[cbArticoli->ItemIndex]["ID_Articolo"].ToIntDef(0);
	rgStato->Enabled = true;
	rgErrore->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::BitBtnOKClick(TObject *Sender)
{
	TArticolo a;
	int tpz;
	UINT i;

	m_bCheckValues = CheckValues();
	if (!m_bCheckValues) {
		return;
	}

    for (i = 0; i < m_PalletList.size(); i++) {
		DBDataModule->LeggiPallet(m_PalletList[i], p);
		pz.ID_Articolo = TabArticoli[cbArticoli->ItemIndex]["ID_Articolo"].ToIntDef(0);
        tpz = TabTipiPezzi[cbTipiPezzi->ItemIndex]["ID_TipoPezzo"].ToIntDef(0);
        if ((pz.ID_Articolo == 0) && (cbArticoli->Text != "") && (tpz != 0)) {
            a.ID_TipoPezzo = tpz;
            a.Codice_Articolo = cbArticoli->Text;
            pz.ID_Articolo = DBDataModule->CreaArticolo(a);
        }
        if ((p.ID_Pezzo == 0) && (pz.ID_Articolo != 0)) {
            // Non ho il pezzo ma � stato selezionato un articolo: creo il pezzo e lo abbino al pallet
			pz.OffsetX = 0.0;
			pz.OffsetY = 0.0;
			pz.OffsetZ = 0.0;
			pz.OffsetA = 0.0;
			pz.OffsetB = 0.0;
			pz.OffsetC = 0.0;
			pz.OffsetROT = 0.0;
			pz.Gap = 0.0;
			pz.GapMin = 0.0;
			pz.GapMax = 0.0;
			pz.H = 0;
			pz.Misura_Eseguita = 0;
			pz.Stato_Pezzo = 0;
			pz.Descrizione = "";
            p.ID_Pezzo = DBDataModule->CreaPezzo(pz);
		}
		else if (p.ID_Pezzo != 0) {
			// C'� il pezzo: ne aggiorno i dati
			pz.ID_Pezzo = p.ID_Pezzo;
			pz.OffsetX = 0.0;
			pz.OffsetY = 0.0;
			pz.OffsetZ = 0.0;
			pz.OffsetA = 0.0;
			pz.OffsetB = 0.0;
			pz.OffsetC = 0.0;
			pz.OffsetROT = 0.0;
			pz.Gap = 0.0;
			pz.GapMin = 0.0;
			pz.GapMax = 0.0;
			pz.H = 0;
			pz.Misura_Eseguita = 0;
			rgStato->Enabled = true;
			rgErrore->Enabled = true;
			pz.Stato_Pezzo = rgErrore->ItemIndex * 100 + rgStato->ItemIndex;
			pz.Descrizione = "";
			DBDataModule->AggiornaPezzo(pz);
		}
		p.ID_TipoPallet = TabTipiPallet[cbTipiPallet->ItemIndex]["ID_TipoPallet"].ToIntDef(0);
		p.Descrizione_Pallet = leDescrizionePallet->Text;
		p.Stato_Pallet = (p.ID_Pezzo != 0) ? 1 : 0;
		DBDataModule->AggiornaPallet(p);
        //Log
		DBDataModule->Log("CLIENT", "PALLET", "Modifica pallet %d, Tipo %d, stato %d, pz %d, stato pz %d",p.ID_Pallet, p.ID_TipoPallet, p.Stato_Pallet, pz.ID_Pezzo, pz.Stato_Pezzo);
    }
    //Una volta confermato, svuoto l'array
    m_PalletList.clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::LE_XClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsNumForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsNumForm->Valore->Text = le->Text;
		if (InsNumForm->ShowModal() == IDOK) {
			le->Text = InsNumForm->Valore->Text;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::OnChangeTipoPallet(TObject *Sender)
{
	AggiornaComboTipiPezzi();
	BitBtnOK->Enabled = (cbTipiPallet->ItemIndex >= 0);
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::leDescrizionePalletClick(TObject *Sender)
{
	TLabeledEdit *le;

	if (MainForm->TouchPanel) {
		le = (TLabeledEdit*)Sender;
		InsAlphaForm->Valore->EditLabel->Caption = le->EditLabel->Caption;
		InsAlphaForm->Valore->Text = le->Text;
		if (InsAlphaForm->ShowModal() == IDOK) {
			le->Text = InsAlphaForm->Valore->Text;
		}
	}
}
//---------------------------------------------------------------------------

bool TFormModificaPallet::CheckValues()
{
	//Controllo se ho il tipo di pallet selezionato
	if (cbTipiPallet->ItemIndex < 0) {
        return false;
    }

	return true;
}
//---------------------------------------------------------------------------

void __fastcall TFormModificaPallet::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	//Se l'operatore ha premuto il tasto OK controllo se i dati sono corretti
	CanClose = (ModalResult == mrOk) ? m_bCheckValues : true;
}
//---------------------------------------------------------------------------

