//---------------------------------------------------------------------------

#ifndef MisuraH
#define MisuraH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
class TdmMisura : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TdmMisura(TComponent* Owner);
	int SetProgram(AnsiString ProgramName);
	AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, AnsiString Separatore);
	int ReadResults(double &X, double &Y, double &Z, double &A, double &B, double &C);
	AnsiString SelProgramsPath, CommandFile, ProgramsPath, ResultsFile;
	double ConvertDecimalSeparator(AnsiString ValStr);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmMisura *dmMisura;
//---------------------------------------------------------------------------
#endif
