//---------------------------------------------------------------------------

#ifndef SelectPezzoH
#define SelectPezzoH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "MyDBGrid.h"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TFormSelectPezzo : public TForm
{
__published:	// IDE-managed Components
	TGridPanel *GridPanel1;
	TMyDBGrid *MyDBGrid1;
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TDataSource *DataSource1;
	TADOQuery *ADOQuery1;
	TIntegerField *ADOQuery1ID_Pezzo;
	TStringField *ADOQuery1Descrizione_Articolo;
	TStringField *ADOQuery1Descrizione;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall MyDBGrid1CellClick(TColumn *Column);
	void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFormSelectPezzo(TComponent* Owner);
	int Selected;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormSelectPezzo *FormSelectPezzo;
//---------------------------------------------------------------------------
#endif
