//---------------------------------------------------------------------------

#ifndef PLCTagH
#define PLCTagH
//---------------------------------------------------------------------------
#define BIT0	0x0001
#define BIT1	0x0002
#define BIT2	0x0004
#define BIT3	0x0008
#define BIT4	0x0010
#define BIT5	0x0020
#define BIT6	0x0040
#define BIT7	0x0080
#define BIT8	0x0100
#define BIT9	0x0200
#define BIT10	0x0400
#define BIT11	0x0800
#define BIT12	0x1000
#define BIT13	0x2000
#define BIT14	0x4000
#define BIT15	0x8000

#define N_TAGS					50
#define TAG_RESET_ALLARMI		0
#define TAG_RIPRISTINO_MACCHINE	1
#define TAG_RESET_BAIA_1		2
#define TAG_RESET_BAIA_2		3
#define JOGp_TOTEM1             4
#define JOGp_TOTEM2             5
#define JOGm_TOTEM1             6
#define JOGm_TOTEM2             7
#define SETUP_TOTEM1            8
#define SETUP_TOTEM2            9
#define RESET_TOTEM1           10
#define RESET_TOTEM2           11
#define NOSCAN_TOTEM1          12
#define NOSCAN_TOTEM2          13
#define JOGp_TOTEM(n)          (3 + n)
#define JOGm_TOTEM(n)          (5 + n)
#define SETUP_TOTEM(n)	       (7 + n)
#define RESET_TOTEM(n)	       (9 + n)
#define NOSCAN_TOTEM(n)	       (11 + n)
#define M6PELCHKPAL			   14
#define M6PELCHKS3			   15
#define M6PELCHK148      	   16
#define M6PELCHK50      	   17
#define M1PELCHKS3     		   18
#define M2PELCHKPAL    		   19
#define M2PELCHKELE    		   20
#define M3PELCHKPAL    		   21
#define M3PELCHKELE    		   22
#define USETAG_TOTEM1           23
#define USETAG_TOTEM2           24
#define USETAG_TOTEM(n)	       (22 + n)
//---------------------------------------------------------------------------
extern int PLCTagList[N_TAGS][3];
#endif
