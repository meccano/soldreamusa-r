//---------------------------------------------------------------------------

#ifndef StazioniH
#define StazioniH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CHILDWIN.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.DBCtrls.hpp>
#include <Vcl.Mask.hpp>
#include "DB.h"
//---------------------------------------------------------------------------
class TStazioniForm : public TMDIChild
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TTabControl *TabControl1;
	TTimer *TimerUPD;
	TGroupBox *GroupBox4;
	TShape *sTapparellaAperta;
	TLabel *lblPortaOpAperta;
	TShape *sPresenzaPallet;
	TLabel *lblPresenzaPallet;
	TShape *sPalletInFase;
	TLabel *lblPalletInFase;
	TShape *sControlloAltezza;
	TLabel *lblControlloAltezza;
	TGroupBox *GroupBox1;
	TLabeledEdit *LE_ID_Pallet;
	TLabeledEdit *LE_TipoPallet;
	TGroupBox *GroupBox3;
	TLabeledEdit *LE_ID_Pezzo;
	TLabeledEdit *LE_Tipo_Pezzo;
	TLabeledEdit *LE_ID_Articolo;
	TEdit *LE_Descrizione_Tipo_Pezzo;
	TEdit *LE_Codice_Articolo;
	TEdit *LE_Descrizione_Articolo;
	TShape *sConfermaInserimento;
	TLabel *lblConfermaInserimento;
	TLabel *lblBaiaPrenotata;
	TShape *sBaiaPrenotata;
	TShape *sOKPrelievo;
	TShape *sOKDeposito;
	TLabel *Label1;
	TLabel *Label2;
	TShape *sAllarme;
	TLabel *Label3;
	TBitBtn *BitBtnInsert;
	TBitBtn *BitBtnSvuota;
	TBitBtn *BitBtnCancella;
	TBitBtn *BitBtnEditPallet;
	TBitBtn *BitBtnPick;
	TBitBtn *bitBtnResetBaia;
	TShape *sTapparellaChiusa;
	TLabel *Label4;
	TShape *sEmergenzaBaia;
	TLabel *Label5;
	TPanel *pnlButtonManualStation;
	void __fastcall BitBtnPickClick(TObject *Sender);
	void __fastcall TimerUPDTimer(TObject *Sender);
	void __fastcall BitBtnCreateClick(TObject *Sender);
	void __fastcall BitBtnSvuotaClick(TObject *Sender);
	void __fastcall BitBtnCancellaClick(TObject *Sender);
	void __fastcall BitBtnEditPalletClick(TObject *Sender);
	void __fastcall bitBtnResetBaiaMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall bitBtnResetBaiaMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
	void __fastcall pnlButtonManualStationClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TStazioniForm(TComponent* Owner);
	void Aggiorna();
	void ColoraShape(int stato, TShape *t, TColor ColorON = clLime, TColor ColorOFF = clGray);
	void ImpostaComboBox(int tp);
	TRecordList RecList;
	TIndexList TipiPezzi;
    int m_iBaiaManuale;
};
//---------------------------------------------------------------------------
extern PACKAGE TStazioniForm *StazioniForm;
//---------------------------------------------------------------------------
#endif



