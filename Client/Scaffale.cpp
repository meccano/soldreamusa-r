//---------------------------------------------------------------------------

//#include <math.h>
#pragma hdrstop

#include <inifiles.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <System.Math.hpp>
#include "Scaffale.h"
#include "Define.h"
#include "Magazzini.h"
#include "Util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "GLBaseClasses"
#pragma link "GLCoordinates"
#pragma link "GLGeomObjects"
#pragma link "GLObjects"
#pragma link "GLScene"
#pragma link "GLVectorFileObjects"
#pragma link "GLWin32Viewer"
#pragma link "GLFileobj"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

#define TRASPARENZA_PALLET	  	 0.15
#define COL_POS_BLOCCATA	     clrRed
#define COL_POS_PREN_PREL	     clrOrchid
#define COL_POS_PREN_DEP	     clrOrchid
#define COL_PALLET_NORMALE       clrAqua
#define COL_PEZZO_NORMALE        clrYellowGreen
#define COL_PEZZO_MISURATO		 clrSeaGreen
#define COL_TESTO		       	 clrLime
#define COL_TESTO2		       	 clrBlue
#define COL_PALLET_SELECTED  	 clrBlue
#define COL_PALLET_ONMOUSE		 clrGray15
#define COL_PALLET_ONMOUSE_SEL   clrGray30
//---------------------------------------------------------------------------

//clrOrchid
TFrameScaffale *FrameScaffale;       
//---------------------------------------------------------------------------
__fastcall TFrameScaffale::TFrameScaffale(TComponent* Owner, int ID)
	: TFrame(Owner)
{
	TIniFile *Ini;

	SendMessage(Handle, WM_SETREDRAW, FALSE, 0);
	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	demomode = Ini->ReadInteger("Impostazioni", "DemoMode", 0);
	delete Ini;
	Init(ID);
	SendMessage(Handle, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(Handle, NULL, 0, RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_NOINTERNALPAINT);
}
//---------------------------------------------------------------------------

void TFrameScaffale::Init(int ID) {
	int i, j, p, n, s, np, nn;
    int ID_DisegnoPallet;
	Extended x, y, z, base, orientamento_pallet;
	TGLCube *pallet;
	TGLCube *pezzo;
	TGLCylinder *palletC;
	TGLCylinder *pezzoC;
	TGLSpaceText *txt, *txt2;
	TPolygonMode pm;

	Tag = ID;
	oldpick = NULL;
	selected = NULL;
	Path = ExtractFilePath(Application->ExeName);
	IniFileName = Path + "Storage" + IntToStr(Tag) + ".ini";
	LoadConfigIni();
	Scaffale->LoadFromFile(ObjScaffale);
	Scaffale->Material->FrontProperties->Diffuse->Color = clrWhite;
	Scaffale->Material->BackProperties->Diffuse->Color = clrWhite;
	Scaffale->Material->BlendingMode = bmOpaque;
	pm = pmFill;
	n = 1;
    j = 0;
	for (p = 0; p < n_piani; p++) {
		nn = 0;
		for (s = 0; s < n_scaffali; s++) {
			np = atoi(m_PalletList[s][p].c_str());
			base = (s == 0) ? 0 : spazio_primo_scaffali + (s - 1) * spazio_scaffali;

            //Ciclo sul numero dei pallet presente su quel piano (p) dello scaffale (s)
			for (i = 0; i < np; i++, j++) {
            	//Carico i parametri per il disegnamento del pallet
                LoadDrawPalletParameter(m_PostazioniTable[j]["ID_DisegnoPallet"].ToIntDef(0));

				if (raggio_mag == 0) {
					x = X_base - i * passo_pallet - base;
                    y = RoundTo(atof(m_HPianoList[s][p].c_str()), -2);
					z = origine;
					orientamento_pallet = turn_angle;
				} else {
					x = X_base + Sin(System::Math::DegToRad(angolo_primo_pallet) + System::Math::DegToRad(angolo_tra_pallet) * nn) * raggio_mag;
                    y = RoundTo(atof(m_HPianoList[s][p].c_str()), -2);
					z = origine - Cos(System::Math::DegToRad(angolo_primo_pallet) + System::Math::DegToRad(angolo_tra_pallet) * nn) * raggio_mag;
					orientamento_pallet = -angolo_primo_pallet - angolo_tra_pallet * nn;
					nn++;
				}

				if (tipo_pallet == PALLET_RETTANGOLARE) {
					pallet = (TGLCube *)(Magazzino->AddNewChild(__classid(TGLCube)));
					pallet->Name = "PALLET" + IntToStr(n);
					pallet->Position->AsVector = PointMake(x, y, z);
					pallet->CubeWidth = base_pallet;
					pallet->CubeDepth = prof_pallet;
					pallet->CubeHeight = h_pallet;
					pallet->TurnAngle = orientamento_pallet;
					pallet->Material->FrontProperties->Diffuse->Color = COL_PALLET_NORMALE;
					pallet->Material->BackProperties->Diffuse->Color = COL_PALLET_NORMALE;
					pallet->Pickable = true;
					pallet->Material->PolygonMode = pm;
					pallet->Tag = n;
					pallet->Material->BlendingMode = bmTransparency;
					pallet->Material->FrontProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					pallet->Material->BackProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					pallet->Material->FaceCulling = fcCull;
					pezzo = (TGLCube *)(pallet->AddNewChild(__classid(TGLCube)));
					pezzo->Name = "Pz" + IntToStr(n);
					pezzo->Position->Y = h_pezzo / 2 + h_pallet / 2;
					pezzo->CubeWidth = base_pallet;
					pezzo->CubeDepth = prof_pallet;
					pezzo->CubeHeight = h_pezzo;
					pezzo->TurnAngle = 0;
					pezzo->Material->FrontProperties->Diffuse->Color = COL_PEZZO_NORMALE;
					pezzo->Material->BackProperties->Diffuse->Color = COL_PEZZO_NORMALE;
					pezzo->Pickable = true;
					pezzo->Material->PolygonMode = pm;
					pezzo->Tag = n;
					pezzo->Material->BlendingMode = bmTransparency;
					pezzo->Material->FrontProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					pezzo->Material->BackProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					pezzo->Material->FaceCulling = fcCull;
					txt = (TGLSpaceText *)(pallet->AddNewChild(__classid(TGLSpaceText)));
					txt2 = (TGLSpaceText *)(pallet->AddNewChild(__classid(TGLSpaceText)));
					palletmap[n] = pallet;
					pezzimap[n] = pezzo;
					txtmap[n] = txt;
					txt2map[n] = txt2;
					tipopalletmap[n] = tipo_pallet;
				} else if (tipo_pallet == PALLET_TONDO) {
					palletC = (TGLCylinder *)(Magazzino->AddNewChild(__classid(TGLCylinder)));
					palletC->Name = "PALLET" + IntToStr(n);
					palletC->Position->AsVector = PointMake(x, y, z);
					palletC->BottomRadius = base_pallet / 2;
					palletC->TopRadius = base_pallet / 2;
					palletC->Height = h_pallet;
					palletC->TurnAngle = turn_angle;
					palletC->Material->FrontProperties->Diffuse->Color = clrGray50;
					palletC->Material->BackProperties->Diffuse->Color = clrGray50;
					palletC->Pickable = true;
					palletC->Material->PolygonMode = pm;
					palletC->Tag = n;
					palletC->Material->BlendingMode = bmTransparency;
					palletC->Material->FrontProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					palletC->Material->BackProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					palletC->Material->FaceCulling = fcCull;
					pezzoC = (TGLCylinder *)(palletC->AddNewChild(__classid(TGLCylinder)));
					pezzoC->Name = "Pz" + IntToStr(n);
					pezzoC->Position->Y = h_pezzo / 2 + h_pallet / 2;
					pezzoC->BottomRadius = base_pallet / 2;
					pezzoC->TopRadius = base_pallet / 2;
					pezzoC->Height = h_pezzo;
					pezzoC->TurnAngle = 0;
					pezzoC->Material->FrontProperties->Diffuse->Color = COL_PALLET_NORMALE;
					pezzoC->Material->BackProperties->Diffuse->Color = COL_PALLET_NORMALE;
					pezzoC->Pickable = true;
					pezzoC->Material->PolygonMode = pm;
					pezzoC->Tag = n;
					pezzoC->Material->BlendingMode = bmTransparency;
					pezzoC->Material->FrontProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					pezzoC->Material->BackProperties->Diffuse->Alpha = TRASPARENZA_PALLET;
					pezzoC->Material->FaceCulling = fcCull;
					txt = (TGLSpaceText *)(palletC->AddNewChild(__classid(TGLSpaceText)));
					txt2 = (TGLSpaceText *)(palletC->AddNewChild(__classid(TGLSpaceText)));
					palletmap[n] = palletC;
					pezzimap[n] = pezzoC;
					txtmap[n] = txt;
					txt2map[n] = txt2;
					tipopalletmap[n] = tipo_pallet;
				} else {
					return;
				}
				txt->Name = "TXT" + IntToStr(n);
				txt->Text = IntToStr(n);
				txt->Position->Z = prof_pallet / 2 + 0.020;
				txt->Position->Y = h_pezzo * 2 / 3;
				txt->Extrusion = 0.020;
				txt->TextHeight = text_height;
				txt->Adjust->Horz = haCenter;
				txt->Adjust->Vert = vaCenter;
				txt->Material->FrontProperties->Diffuse->Color = COL_TESTO;
				txt->Material->BackProperties->Diffuse->Color = COL_TESTO;
				txt->Material->FaceCulling = fcCull;
				txt->Font->Style = TFontStyles() << fsBold;
				txt->Pickable = true;
				txt->Tag = n;
				txt2->Name = "TXT2_" + IntToStr(n);
				txt2->Text = "";
				txt2->Position->Z = prof_pallet / 2 + 0.020;
				txt2->Position->Y = 0;
				txt2->Extrusion = 0.020;
				txt2->TextHeight = h_pallet * 0.9;
				txt2->Adjust->Horz = haCenter;
				txt2->Adjust->Vert = vaCenter;
				txt2->Material->FrontProperties->Diffuse->Color = COL_TESTO2;
				txt2->Material->BackProperties->Diffuse->Color = COL_TESTO2;
				txt2->Material->FaceCulling = fcCull;
				txt2->Font->Style = TFontStyles() << fsBold;
				txt2->Pickable = true;
				txt2->Tag = n;
				n++;
            }
		}
	}
}
//---------------------------------------------------------------------------

void TFrameScaffale::UpdatedIniPallets() {
	int i, j, p, n, s, np, nn;
	int ID_DisegnoPallet;
	Extended x, y, z, base, orientamento_pallet;
	TGLCube *pallet;
	TGLCube *pezzo;
	TGLCylinder *palletC;
	TGLCylinder *pezzoC;
	TGLSpaceText *txt;

	n = 1;
    j = 0;
	for (p = 0; p < n_piani; p++) {
		nn = 0;
		for (s = 0; s < n_scaffali; s++) {
			np = atoi(m_PalletList[s][p].c_str());
			base = (s == 0) ? 0 : spazio_primo_scaffali + (s - 1) * spazio_scaffali;

			for (i = 0; i < np; i++, j++) {
            	//Carico i parametri per il disegnamento del pallet
                LoadDrawPalletParameter(m_PostazioniTable[j]["ID_DisegnoPallet"].ToIntDef(0));

				if (raggio_mag == 0) {
					x = X_base - i * passo_pallet - base;
                    y = RoundTo(atof(m_HPianoList[s][p].c_str()), -2);
					z = origine;
					orientamento_pallet = turn_angle;
				} else {
					x = X_base + Sin(System::Math::DegToRad(angolo_primo_pallet) + System::Math::DegToRad(angolo_tra_pallet) * nn) * raggio_mag;
                    y = RoundTo(atof(m_HPianoList[s][p].c_str()), -2);
					z = origine - Cos(System::Math::DegToRad(angolo_primo_pallet) + System::Math::DegToRad(angolo_tra_pallet) * nn) * raggio_mag;
					orientamento_pallet = -angolo_primo_pallet - angolo_tra_pallet * nn;
					nn++;
				}

				if (tipo_pallet == PALLET_RETTANGOLARE) {
					pallet = (TGLCube *)(Magazzino->FindChild("PALLET" + IntToStr(n), true));
					pallet->Position->AsVector = PointMake(x, y, z);
					pallet->CubeWidth = base_pallet;
					pallet->CubeDepth = prof_pallet;
					pallet->CubeHeight = h_pallet;
					pallet->TurnAngle = orientamento_pallet;
					pezzo = (TGLCube *)(pallet->FindChild("Pz" + IntToStr(n), true));
					pezzo->Position->Y = h_pezzo / 2 + h_pallet / 2;
					pezzo->CubeWidth = base_pallet;
					pezzo->CubeDepth = prof_pallet;
					pezzo->CubeHeight = h_pezzo;
					pezzo->TurnAngle = 0;
					txt = (TGLSpaceText *)(pallet->FindChild("TXT" + IntToStr(n), true));
					txt->Position->Z = prof_pallet / 2 + 0.010;
					txt->Position->Y = h_pezzo / 2;
					txt->Extrusion = 0.010;
					txt->TextHeight = text_height;
				} else if (tipo_pallet == PALLET_TONDO) {
					palletC = (TGLCylinder *)(Magazzino->FindChild("PALLET" + IntToStr(n), true));
					palletC->Position->AsVector = PointMake(x, y, z);
					palletC->BottomRadius = base_pallet / 2;
					palletC->TopRadius = base_pallet / 2;
					palletC->Height = h_pallet;
					palletC->TurnAngle = turn_angle;
					pezzoC = (TGLCylinder *)(palletC->FindChild("Pz" + IntToStr(n), true));
					pezzoC->Position->Y = h_pezzo / 2 + h_pallet / 2;
					pezzoC->BottomRadius = base_pallet / 2;
					pezzoC->TopRadius = base_pallet / 2;
					pezzoC->Height = h_pezzo;
					pezzoC->TurnAngle = 0;
					txt = (TGLSpaceText *)(palletC->FindChild("TXT" + IntToStr(n), true));
					txt->Position->Z = prof_pallet / 2 + 0.010;
					txt->Position->Y = h_pezzo / 2;
					txt->Extrusion = 0.010;
					txt->TextHeight = text_height;
				}
				n++;
			}
		}
	}
}
//---------------------------------------------------------------------------

void TFrameScaffale::LoadDrawPalletParameter(int ID_DisegnoPallet)
{
    tipo_pallet 	 	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["Type"], 0);
    X_base       	 	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["XBase"], 0);
    base_pallet      	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["BasePallet"], 0);
    prof_pallet      	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["ProfPallet"], 0);
    h_pallet         	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["HPallet"], 0);
    h_pezzo          	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["HPezzo"], 0);
    turn_angle       	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["TurnAngle"], 0);
    text_height     	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["TextHeight"], 0);
    angolo_primo_pallet = System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["AngoloPrimoPallet"], 0);
    angolo_tra_pallet 	= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["AngoloTraPallet"],  0);
    raggio_mag 			= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["RaggioMagazzino"], 0);
    origine 			= System::Sysutils::StrToFloatDef(m_DisegnoPalletTable[ID_DisegnoPallet]["Origine"], 0);
}
//---------------------------------------------------------------------------

void TFrameScaffale::SaveConfigIni() {
	TIniFile *Ini;

	Ini = new TIniFile(IniFileName);
	Ini->WriteString("Scaffale", "Scale"		 , FloatToStrF(Scaffale->Scale->X    , ffFixed, 7, 3 ));
	Ini->WriteString("Scaffale", "DirectionX"	 , FloatToStrF(Scaffale->PitchAngle  , ffFixed, 7, 3 ));
	Ini->WriteString("Scaffale", "DirectionY"	 , FloatToStrF(Scaffale->TurnAngle   , ffFixed, 7, 3 ));
	Ini->WriteString("Scaffale", "DirectionZ"	 , FloatToStrF(Scaffale->RollAngle   , ffFixed, 7, 3 ));
	Ini->WriteString("Scaffale", "PositionX"	 , FloatToStrF(Scaffale->Position->X , ffFixed, 7, 3 ));
	Ini->WriteString("Scaffale", "PositionY"	 , FloatToStrF(Scaffale->Position->Y , ffFixed, 7, 3 ));
	Ini->WriteString("Scaffale", "PositionZ"	 , FloatToStrF(Scaffale->Position->Z , ffFixed, 7, 3 ));
	Ini->WriteInteger("Pallets", "NScaffali"	 , n_scaffali);
	Ini->WriteInteger("Pallets", "Piani"		 , n_piani);
	Ini->WriteString("Pallets", "SpazioIScaffali", FloatToStrF(spazio_primo_scaffali, ffFixed, 7, 3 ));
	Ini->WriteString("Pallets", "SpazioScaffali" , FloatToStrF(spazio_scaffali, ffFixed, 7, 3 ));
	Ini->WriteString("Pallets", "SpazioUScaffali", FloatToStrF(spazio_ultimo_scaffali, ffFixed, 7, 3 ));
	Ini->WriteString("Pallets", "PassoPallet"	 , FloatToStrF(passo_pallet, ffFixed, 7, 3 ));
	// Altro
	Ini->WriteString("Camera", "PositionX", FloatToStrF(Camera->Position->X       , ffFixed, 7, 3 ));
	Ini->WriteString("Camera", "PositionY", FloatToStrF(Camera->Position->Y       , ffFixed, 7, 3 ));
	Ini->WriteString("Camera", "PositionZ", FloatToStrF(Camera->Position->Z       , ffFixed, 7, 3 ));
	Ini->WriteString("Target", "PositionX", FloatToStrF(Target->Position->X       , ffFixed, 7, 3 ));
	Ini->WriteString("Target", "PositionY", FloatToStrF(Target->Position->Y       , ffFixed, 7, 3 ));
	Ini->WriteString("Target", "PositionZ", FloatToStrF(Target->Position->Z       , ffFixed, 7, 3 ));
	Ini->WriteInteger("Frame", "Width", Width);
	Ini->WriteString("Light", "PositionX", FloatToStrF(Luce->Position->X       , ffFixed, 7, 3 ));
	Ini->WriteString("Light", "PositionY", FloatToStrF(Luce->Position->Y       , ffFixed, 7, 3 ));
	Ini->WriteString("Light", "PositionZ", FloatToStrF(Luce->Position->Z       , ffFixed, 7, 3 ));
	delete Ini;
}
//---------------------------------------------------------------------------

void TFrameScaffale::LoadConfigIni() {
	TIniFile *Ini;
	int s;
	AnsiString sLine, key;
	float scale, X, Y, Z;
	std::string line;
	std::vector<std::string> vectline;

	Ini = new TIniFile(IniFileName);

	// Scaffale
	ObjScaffale = Path + Ini->ReadString("Scaffale", "ObjFile", "MAG123.obj");
	scale = Ini->ReadFloat("Scaffale", "Scale", 0);
	Scaffale->Scale->SetVector(scale, scale, scale);
	Scaffale->PitchAngle = Ini->ReadFloat("Scaffale", "DirectionX", 0);
	Scaffale->TurnAngle = Ini->ReadFloat("Scaffale", "DirectionY", 0);
	Scaffale->RollAngle = Ini->ReadFloat("Scaffale", "DirectionZ", 0);
	X = Ini->ReadFloat("Scaffale", "PositionX", 0);
	Y = Ini->ReadFloat("Scaffale", "PositionY", 0);
	Z = Ini->ReadFloat("Scaffale", "PositionZ", 0);
	Scaffale->Position->SetPoint(X, Y, Z);
	// Pallets
	n_scaffali       		= Ini->ReadInteger("Pallets", "NScaffali", 0);
	n_piani          		= Ini->ReadInteger("Pallets", "Piani", 0);
	spazio_primo_scaffali	= Ini->ReadFloat("Pallets", "SpazioIScaffali", 0);
	spazio_scaffali  		= Ini->ReadFloat("Pallets", "SpazioScaffali", 0);
	spazio_ultimo_scaffali  = Ini->ReadFloat("Pallets", "SpazioUScaffali", 0);
	passo_pallet     		= Ini->ReadFloat("Pallets", "PassoPallet", 0);
	// Altro
	X = Ini->ReadFloat("Camera", "PositionX", 0);
	Y = Ini->ReadFloat("Camera", "PositionY", 0);
	Z = Ini->ReadFloat("Camera", "PositionZ", 0);
	Camera->Position->SetPoint(X, Y, Z);
	X = Ini->ReadFloat("Target", "PositionX", 0);
	Y = Ini->ReadFloat("Target", "PositionY", 0);
	Z = Ini->ReadFloat("Target", "PositionZ", 0);
	Target->Position->SetPoint(X, Y, Z);
	framewidth = Ini->ReadFloat("Frame", "Width", 500);
	Width = framewidth;
	X = Ini->ReadFloat("Light", "PositionX", 0);
	Y = Ini->ReadFloat("Light", "PositionY", 0);
	Z = Ini->ReadFloat("Light", "PositionZ", 0);
	Luce->Position->SetPoint(X, Y, Z);

	//Texture del magazzino
	TexturePaths = Path + Ini->ReadString("Scaffale", "TexturePaths", "TWSR+example+layout.mtl");

    //Carico la tabella dei pallet
    key.sprintf("Postazioni WHERE ID_Magazzino = %d ORDER BY ID_Postazione", Tag);
    DBDataModule->CaricaTabella(key.c_str(), m_PostazioniTable);
    
    //Carico la tabella dei pallet
    DBDataModule->CaricaTabella("DisegnoPallet", m_DisegnoPalletTable);
    
	//Pulisco la mappa
	m_PalletList.clear();
    m_HPianoList.clear();
	for (s = 0; s < n_scaffali; s++) {
		vectline.clear();
		//Numero di pallet per piano
		key.sprintf("NumPalletXPianoScaffale%d", s + 1);
		sLine = Ini->ReadString("Pallets", key.c_str() , "0");
		line = sLine.c_str();

		//Trovo il numero di pallet per piano
		split(vectline, line, boost::is_any_of("-"));
		m_PalletList.push_back(vectline);

		vectline.clear();
		//Distanza tra i piani
		key.sprintf("HPianiScaffale%d", s + 1);
		sLine = Ini->ReadString("Pallets", key.c_str() , "0");
		line = sLine.c_str();

		//Trovo l'altezza di ogni piano per ogni scaffale
		split(vectline, line, boost::is_any_of("-"));
		m_HPianoList.push_back(vectline);
	}

	delete Ini;
}
//---------------------------------------------------------------------------

void __fastcall TFrameScaffale::SpeedButtonSaveClick(TObject *Sender)
{
	SaveConfigIni();
}
//---------------------------------------------------------------------------

void TFrameScaffale::EvidenziaPallet(TGLCustomSceneObject *pick, TVector4f c) {
	TGLCustomSceneObject *pz;

	pick->Material->FrontProperties->Emission->Color = c;
	pick->Material->BackProperties->Emission->Color = c;
	pz = pezzimap[pick->Tag];
	pz->Material->FrontProperties->Emission->Color = c;
	pz->Material->BackProperties->Emission->Color = c;
}
//---------------------------------------------------------------------------

void TFrameScaffale::EvidenziaPalletSelezionato(TGLCustomSceneObject *pick) {
	TGLCustomSceneObject *pz;

	pz = pezzimap[pick->Tag];
	if (tipopalletmap[pick->Tag] == PALLET_RETTANGOLARE) {
		Selezione->Position->X = ((TGLCube*)pick)->Position->X ;
		Selezione->Position->Y = ((TGLCube*)pick)->Position->Y + ((TGLCube*)pz)->CubeHeight / 2;
		Selezione->Position->Z = ((TGLCube*)pick)->Position->Z ;
		Selezione->CubeWidth   = ((TGLCube*)pick)->CubeWidth * 1.01;
		Selezione->CubeDepth   = ((TGLCube*)pick)->CubeDepth * 1.01;
		Selezione->CubeHeight  = (((TGLCube*)pick)->CubeHeight + ((TGLCube*)pz)->CubeHeight)* 1.01;
		Selezione->Visible = true;
		SelezioneC->Visible = false;
	} else {
		SelezioneC->Position->X = ((TGLCylinder*)pick)->Position->X ;
		SelezioneC->Position->Y = ((TGLCylinder*)pick)->Position->Y + ((TGLCylinder*)pz)->Height / 2;
		SelezioneC->Position->Z = ((TGLCylinder*)pick)->Position->Z ;
		SelezioneC->BottomRadius = ((TGLCylinder*)pick)->BottomRadius * 1.01;
		SelezioneC->TopRadius = ((TGLCylinder*)pick)->BottomRadius * 1.01;
		SelezioneC->Height = (((TGLCylinder*)pick)->Height + ((TGLCylinder*)pz)->Height)* 1.01;
		SelezioneC->Visible = true;
		Selezione->Visible = false;
	}
	GLSceneViewer1->Refresh();
}
//---------------------------------------------------------------------------


void TFrameScaffale::ColoraPallet(TGLCustomSceneObject *pick, TVector4f c, TVector4f c2) {
	TGLCustomSceneObject *pz;

	pz = pezzimap[pick->Tag];
	pick->Material->FrontProperties->Diffuse->Color = c;
	pick->Material->BackProperties->Diffuse->Color = c;
	pz->Material->FrontProperties->Diffuse->Color = c2;
	pz->Material->BackProperties->Diffuse->Color = c2;
}
//---------------------------------------------------------------------------

void SetPalletAlpha(TGLCustomSceneObject *pick, float Alpha) {
	pick->Material->FrontProperties->Diffuse->Alpha = Alpha;
	pick->Material->BackProperties->Diffuse->Alpha = Alpha;
}
//---------------------------------------------------------------------------

void TFrameScaffale::ModificaTestoPallet(TGLCustomSceneObject *pick, AnsiString s) {
	TGLSpaceText *TXT;

	TXT = txtmap[pick->Tag];
	TXT->Text = s;
}
//---------------------------------------------------------------------------

void TFrameScaffale::ModificaTesto2Pallet(TGLCustomSceneObject *pick, AnsiString s) {
	TGLSpaceText *TXT;

	TXT = txt2map[pick->Tag];
	TXT->Text = s;
}
//---------------------------------------------------------------------------

void TFrameScaffale::ColoraTestoPallet(TGLCustomSceneObject *pick, TVector4f c) {
	TGLSpaceText *TXT;

	TXT = txtmap[pick->Tag];
	TXT->Material->FrontProperties->Diffuse->Color = c;
	TXT->Material->BackProperties->Diffuse->Color = c;
}
//---------------------------------------------------------------------------

void __fastcall TFrameScaffale::GLSceneViewer1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
	if (!SpeedButtonCurs->Down)
		return;
	if (oldpick) {
		selected = oldpick;
		MagazziniForm->SelectPosDBGrid(selected->Tag);
	} else {
		selected = NULL;
	}
	return;
}
//---------------------------------------------------------------------------

void __fastcall TFrameScaffale::GLSceneViewer1MouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
	TGLCustomSceneObject *pick;
	if (SpeedButtonCurs->Down) {
		// find what's under the mouse
		GLScene1->VisibilityCulling = vcObjectBased;
		pick = (TGLCustomSceneObject*)(GLSceneViewer1->Buffer->GetPickedObject(X, Y));
		GLScene1->VisibilityCulling = vcNone;
		if (pick == NULL) {
			if (oldpick) {
				EvidenziaPallet(oldpick, clrBlack);
				oldpick = NULL;
			}
			return;
		}
		pick = palletmap[pick->Tag];
		// if it has changed since last MouseMove...
		if (pick != oldpick) {
			// ...turn to black previous "hot" object...
			if (oldpick) {
				EvidenziaPallet(oldpick, clrBlack);
			}
			EvidenziaPallet(pick, COL_PALLET_ONMOUSE);
			Hint = pick->Hint;
			if (Hint != "") {
				ShowHint = true;
				Application->ActivateHint(ClientToScreen(TPoint(X, Y)));
			} else {
				ShowHint = false;
			}
			// ...and don't forget it !
			oldpick = pick;
		}
	} else if (SpeedButtonNav->Down && (Shift.Contains(ssLeft))) {
		Camera->Position->Y -= (my - Y) / 50.0;
		Camera->MoveAroundTarget(0, (mx - X) / 10.0);
	} else if (SpeedButtonPan->Down && (Shift.Contains(ssLeft))) {
		Target->Position->X -= (mx - X) / 220.0;
		Target->Position->Y -= (my - Y) / 220.0;
	}
	mx=X;
	my=Y;
	return;
}
//---------------------------------------------------------------------------

void __fastcall TFrameScaffale::SpeedButtonLoadClick(TObject *Sender)
{
	LoadConfigIni();
	UpdatedIniPallets();
}
//---------------------------------------------------------------------------

void TFrameScaffale::Zoom(int MouseDelta) {
	float Delta;

	if (SpeedButtonCurs->Down) {
		return;
	}
	Delta = Camera->DistanceToTarget() * (0.05 * MouseDelta);
	if ((Camera->DistanceToTarget() - Delta) > 1.0) {
		Camera->MoveInEyeSpace(Delta, 0, 0);
	}
}
//---------------------------------------------------------------------------

void TFrameScaffale::SelectPos(int n)
{
	TGLCustomSceneObject *pick;

	pick = palletmap[n];
	if (pick) {
		EvidenziaPalletSelezionato(pick);
		selected = pick;
	}
}
//---------------------------------------------------------------------------

void TFrameScaffale::SetLabel(int n, AnsiString s)
{
	TGLCustomSceneObject *pick;

	pick = palletmap[n];
	if (pick) {
		ModificaTestoPallet(pick, s);
	}
}
//---------------------------------------------------------------------------

void TFrameScaffale::SetLabel2(int n, AnsiString s)
{
	TGLCustomSceneObject *pick;

	pick = palletmap[n];
	if (pick) {
		ModificaTesto2Pallet(pick, s);
	}
}
//---------------------------------------------------------------------------

void TFrameScaffale::AggiornaPresenza(int n, int idpallet, int tipopallet, int idpezzo, int stato, int stato_pezzo, int stato_pallet, int misurato, int macchina, int lotto, AnsiString Alias, AnsiString Hint)
{
	TGLCustomSceneObject *pick;
	TGLCustomSceneObject *pz;
	float a1, a2;
	TVector4f colore_pallet;

	pick = palletmap[n];
	pz = pezzimap[n];
    if (!pick || !pz) {
        return;
    }
	ModificaTesto2Pallet(pick, idpallet ? IntToStr(idpallet) : (UnicodeString)"");
	pick->Hint = Hint;
	switch(stato) {
		case POS_BLOCCATA:
			pick->Material->PolygonMode = pmLines;
			pz->Material->PolygonMode = pmLines;
			ColoraPallet(pick, COL_POS_BLOCCATA, COL_POS_BLOCCATA);
			SetPalletAlpha(pick, 1.0);
			SetPalletAlpha(pz, 1.0);
			break;
		case POS_PRENOTATA_DEPOSITO:
			pick->Material->PolygonMode = pmLines;
			pz->Material->PolygonMode = pmLines;
			ColoraPallet(pick, COL_POS_PREN_DEP, COL_POS_PREN_DEP);
			SetPalletAlpha(pick, 1.0);
			SetPalletAlpha(pz, 1.0);
			break;
		default:
			if (stato == POS_PRENOTATA_PRELIEVO) {
				colore_pallet = COL_POS_PREN_PREL;
			}
            else if (lotto != 0) {
				colore_pallet = COL_PALLET_NORMALE;
			}
            else {
				colore_pallet = COL_NO_LOTTO;
			}
			a1 = (idpallet) ? 1.0 : TRASPARENZA_PALLET;
			a2 = (idpezzo) ? 1.0 : TRASPARENZA_PALLET;
			pick->Material->PolygonMode = pmFill;
			pz->Material->PolygonMode = pmFill;
			if (stato_pezzo >= STATO_PEZZO_SCARTO) {
				ColoraPallet(pick, colore_pallet, COL_PEZZO_ERRORE);
			}
            else {
				switch(stato_pezzo) {
                case STATO_PEZZO_GREZZO:
                    ColoraPallet(pick, colore_pallet, COL_PEZZO_GREZZO);
                    break;
				case STATO_PEZZO_ATTREZZATO:
					ColoraPallet(pick, colore_pallet, COL_PEZZO_ATTREZZATO);
					break;
                case STATO_PEZZO_LAVORATO:
                    ColoraPallet(pick, colore_pallet, COL_PEZZO_LAVORATO);
                    break;
                case STATO_PEZZO_LOTTO_SOSPESO:
                    ColoraPallet(pick, colore_pallet, COL_LOTTO_SOSPESO);
                    break;
                default:
                    ColoraPallet(pick, colore_pallet, clrWhite);
                    break;
				}
				switch(macchina) {
				case 1:
					ColoraTestoPallet(pick, COL_PEZZO_A_MU1);
					break;
				case 2:
					ColoraTestoPallet(pick, COL_PEZZO_A_MU2);
					break;
				case 3:
					ColoraTestoPallet(pick, COL_PEZZO_A_MU12);
					break;
                default:
                    ColoraTestoPallet(pick, COL_TESTO);
                    break;
				}
			}
			SetPalletAlpha(pick, a1);
			SetPalletAlpha(pz, a2);
			break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFrameScaffale::GLSceneViewer1BeforeRender(TObject *Sender)
{
	GL()->LineWidth(3.0);
}
//---------------------------------------------------------------------------

void __fastcall TFrameScaffale::GLSceneViewer1DblClick(TObject *Sender)
{
	if (!SpeedButtonCurs->Down)
		return;
	MagazziniForm->BitBtnEditClick(Sender);
}
//---------------------------------------------------------------------------

