//---------------------------------------------------------------------------

#ifndef NewJobH
#define NewJobH
//---------------------------------------------------------------------------
#include "DB.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <System.Classes.hpp>
#include <vector>
//---------------------------------------------------------------------------
class TFormNewJob : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GroupBox1;
	TLabeledEdit *LE_ID_Pallet;
	TPanel *Panel1;
	TBitBtn *BitBtnOK;
	TBitBtn *BitBtnCANCEL;
	TLabeledEdit *LE_PRG;
	TLabeledEdit *LE_Pri;
	TStaticText *StaticText1;
	TComboBox *ComboBox2;
	TStaticText *StaticText2;
	TTrackBar *TrackBar1;
	TComboBox *ComboBox1;
	TOpenDialog *OpenDialog1;
	TCheckBox *CheckBox1;
	void __fastcall LE_ID_PalletClick(TObject *Sender);
	void __fastcall LE_PRGClick(TObject *Sender);
	void __fastcall ComboBox1Change(TObject *Sender);
	void __fastcall BitBtnOKClick(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall ComboBox2Change(TObject *Sender);
	void __fastcall ComboBox2Select(TObject *Sender);
	void __fastcall BitBtnCANCELClick(TObject *Sender);
private:	// User declarations
	bool ProgrammaRichiesto;
public:		// User declarations
	__fastcall TFormNewJob(TComponent* Owner);
	void ImpostaComboBox(TComboBox *cb, TRecordList rl, AnsiString KeyField, int KeyValue);
	void CaricaComboBoxMacchine(int tp);
	void SelectPallet(int idpallet, int TM = 0, int MU = 0);
	void Aggiorna();
	TRecordList RecList;
	int ID_Pallet;
	TPallet p;
	TJob j;
	bool PalletSelected;
	bool MachineSelected;
	std::vector<int> m_PalletList;
	bool ProgramSelected;
	AnsiString PPFolderMU[6];
};
//---------------------------------------------------------------------------
extern PACKAGE TFormNewJob *FormNewJob;
//---------------------------------------------------------------------------
#endif
