// ---------------------------------------------------------------------
#pragma hdrstop
#include <tchar.h>
#include "Splash.h"
// ---------------------------------------------------------------------
#include <vcl.h>
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
// ---------------------------------------------------------------------

USEFORM("Robot.cpp", RobotForm);
USEFORM("SelectPezzo.cpp", FormSelectPezzo);
USEFORM("SelectPallet.cpp", FormSelectPallet);
USEFORM("Scaffale.cpp", FrameScaffale); /* TFrame: File Type */
USEFORM("NewJob.cpp", FormNewJob);
USEFORM("MSG.cpp", MSGForm);
USEFORM("ModificaTempi.cpp", FormModificaTempi);
USEFORM("Password.cpp", PasswordDlg);
USEFORM("Pallet.cpp", PalletForm);
USEFORM("NewLavorazioni.cpp", FormNewLavorazioni);
USEFORM("Socket.cpp", SocketDataModule); /* TDataModule: File Type */
USEFORM("StoricoSegnalazioni.cpp", StoricoSegnalazioniForm);
USEFORM("Stazioni.cpp", StazioniForm);
USEFORM("Splash.cpp", SplashForm);
USEFORM("Jobs.cpp", JobsForm);
USEFORM("InsNum.cpp", InsNumForm);
USEFORM("InsertPallet.cpp", FormInsertPallet);
USEFORM("Lotti.cpp", LottiForm);
USEFORM("Lavorazioni.cpp", LavorazioniForm);
USEFORM("Chiusura.cpp", ChiusuraForm);
USEFORM("ChildWin.cpp", MDIChild);
USEFORM("Articoli.cpp", ArticoliForm);
USEFORM("InsertLavorazioni.cpp", FormInsertLavorazioni);
USEFORM("InsAlpha.cpp", InsAlphaForm);
USEFORM("DB.cpp", DBDataModule); /* TDataModule: File Type */
USEFORM("Macchine.cpp", MacchineForm);
USEFORM("ModificaFase.cpp", FormModificaFase);
USEFORM("ModificaArticolo.cpp", FormModificaArticolo);
USEFORM("ModificaPallet.cpp", FormModificaPallet);
USEFORM("ModificaLotto.cpp", FormModificaLotto);
USEFORM("ModificaLavorazione.cpp", FormModificaLavorazione);
USEFORM("Misura.cpp", dmMisura); /* TDataModule: File Type */
USEFORM("Main.cpp", MainForm);
USEFORM("Magazzini.cpp", MagazziniForm);
//---------------------------------------------------------------------------
WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int) {
	HANDLE mutex;
	AnsiString MutexName = ExtractFileName(Application->ExeName).UpperCase();

	mutex = OpenMutex(MUTEX_ALL_ACCESS, false, MutexName.c_str());
	if (mutex == NULL) {
		mutex = CreateMutex(NULL, true, MutexName.c_str());
	}
	else {
		exit(EXIT_SUCCESS);
	}
	Application->Initialize();
	SplashForm = new TSplashForm(Application);
	if (!FindWindowA("TAppBuilder", NULL)) {
		SplashForm->FormStyle = fsStayOnTop;
	}
	else {
		SplashForm->FormStyle = fsNormal;
	}

	SplashForm->Show();
	SplashForm->Update();
	FormatSettings.DecimalSeparator = '.';
	FormatSettings.ThousandSeparator = ',';
	FormatSettings.TimeSeparator = ':';
	Application->CreateForm(__classid(TDBDataModule), &DBDataModule);
		Application->CreateForm(__classid(TSocketDataModule), &SocketDataModule);
		Application->CreateForm(__classid(TMainForm), &MainForm);
		Application->CreateForm(__classid(TMagazziniForm), &MagazziniForm);
		Application->CreateForm(__classid(TInsAlphaForm), &InsAlphaForm);
		Application->CreateForm(__classid(TInsNumForm), &InsNumForm);
		Application->CreateForm(__classid(TPasswordDlg), &PasswordDlg);
		Application->CreateForm(__classid(TChiusuraForm), &ChiusuraForm);
		Application->CreateForm(__classid(TMSGForm), &MSGForm);
		Application->CreateForm(__classid(TFormInsertPallet), &FormInsertPallet);
		Application->CreateForm(__classid(TFormInsertLavorazioni), &FormInsertLavorazioni);
		Application->CreateForm(__classid(TFormModificaArticolo), &FormModificaArticolo);
		Application->CreateForm(__classid(TFormSelectPallet), &FormSelectPallet);
		Application->CreateForm(__classid(TFormModificaPallet), &FormModificaPallet);
		Application->CreateForm(__classid(TFormNewJob), &FormNewJob);
		Application->CreateForm(__classid(TdmMisura), &dmMisura);
		Application->CreateForm(__classid(TFormModificaLavorazione), &FormModificaLavorazione);
		Application->CreateForm(__classid(TFormNewLavorazioni), &FormNewLavorazioni);
		Application->CreateForm(__classid(TFormModificaFase), &FormModificaFase);
		Application->CreateForm(__classid(TFormModificaLotto), &FormModificaLotto);
		Application->CreateForm(__classid(TFormSelectPezzo), &FormSelectPezzo);
		Application->CreateForm(__classid(TFormModificaTempi), &FormModificaTempi);
		SplashForm->Close();
	delete SplashForm;
	Application->MainForm->Visible = true;
//	ShowWindow(Application->MainForm->Handle, SW_MAXIMIZE);
	Application->Run();
	CloseHandle(mutex);
	return 0;
}
// ---------------------------------------------------------------------

