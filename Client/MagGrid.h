//---------------------------------------------------------------------------

#ifndef MagGridH
#define MagGridH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.DBGrids.hpp>
#include <Vcl.Grids.hpp>
#include "u_TEnhDBGrid.hpp"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
//---------------------------------------------------------------------------
class TFrameDBGrid : public TFrame
{
__published:	// IDE-managed Components
	TEnhDBGrid *EnhDBGrid1;
	TADOConnection *ADOConnection1;
	TADOQuery *ADOQuery1;
	TDataSource *DataSource1;
private:	// User declarations
public:		// User declarations
	__fastcall TFrameDBGrid(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFrameDBGrid *FrameDBGrid;
//---------------------------------------------------------------------------
#endif
