//---------------------------------------------------------------------------
#pragma hdrstop

#include <inifiles.hpp>
#include "Magazzini.h"
#include "Main.h"
#include "NewJob.h"
#include "ModificaPallet.h"
#include "InsertPallet.h"
#include "StringTableID.h"
#include "ClientData.h"
#include "PLCTag.h"
#include "Socket.h"
#include "insnum.h"
#include "Robot.h"
#include "NewLavorazioni.h"
#include "ModificaLotto.h"
#include "Util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CHILDWIN"
#pragma link "GLScene"
#pragma link "GLCoordinates"
#pragma link "GLCrossPlatform"
#pragma link "GLObjects"
#pragma link "GLWin32Viewer"
#pragma link "GLSpaceText"
#pragma link "GLCoordinates"
#pragma link "GLCrossPlatform"
#pragma link "GLObjects"
#pragma link "GLScene"
#pragma link "GLSpaceText"
#pragma link "GLWin32Viewer"
#pragma resource "*.dfm"
#pragma link "MyDBGrid"
//---------------------------------------------------------------------------
#define POS_LIBERA				0
#define POS_PRENOTATA_PRELIEVO	1
#define POS_PRENOTATA_DEPOSITO	2
#define POS_BLOCCATA			3
//---------------------------------------------------------------------------
TMagazziniForm *MagazziniForm;
//---------------------------------------------------------------------------
__fastcall TMagazziniForm::TMagazziniForm(TComponent* Owner)
	: TMDIChild(Owner)
{
	Storage1->Visible = false;
	if (!MainForm->onlyelectrode) {
		FrameScaffale1 = (TFrameScaffale *)new TFrameScaffale(Storage1, 1);
		FrameScaffale1->Parent = Storage1;
		FrameScaffale1->Name = "FrameScaffale1";
		FrameScaffale1->Align = alClient;
		FrameScaffale1->GLSceneViewer1->OnMouseEnter = &FrameScaffale1GLSceneViewer1MouseEnter;
	}
	else {
		PageControl1->Pages[0]->TabVisible = false;
	}
	ImpostaAliases(1);
	SelectedPage = -1;
	if (!MainForm->onlyelectrode) {
		PageControl1->ActivePageIndex = 0;
	} else {
		PageControl1->ActivePageIndex = 4;
    }
	PageControl2->ActivePageIndex = 0;
	changing_override = false;
	magprel = 0;
	posprel = 0;
	magdep = 0;
	posdep = 0;
	idpallet = 0;
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	try {
		ADOQuery1->Close();
	} catch(...) {}
	TimerUpd->Enabled = false;
	TimerUpd2->Enabled = false;
	ShowWindow(Handle, SW_MINIMIZE);
	ShowWindow(Handle, SW_MINIMIZE);
	ShowWindow(Handle, SW_HIDE);
	ShowWindow(Handle, SW_HIDE);
	SetInvisible();
	Action = caNone;
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::ApplicationEvents1Message(tagMSG &Msg, bool &Handled)
{
	int MouseDelta;
	float Delta;

	if (Msg.message == WM_MOUSEWHEEL) {
		MouseDelta = GET_WHEEL_DELTA_WPARAM(Msg.wParam) / WHEEL_DELTA;
		switch (PageControl1->ActivePageIndex) {
		case 0:
			if (FrameScaffale1->Focused()) {
				FrameScaffale1->Zoom(MouseDelta);
				Handled = true;
			}
			break;
		}
	}
}
//---------------------------------------------------------------------------

void TMagazziniForm::Aggiorna()
{
	Storage1->Visible = false;
	Aggiorna3d();
	TimerShow3d->Enabled = true;
}
//---------------------------------------------------------------------------

void TMagazziniForm::ColoraShape(int stato, TShape *t, TColor ColorON, TColor ColorOFF)
{
	if (stato) {
		t->Brush->Color = ColorON;
	} else {
		t->Brush->Color = ColorOFF;
	}
}
//---------------------------------------------------------------------------

void TMagazziniForm::AggiornaBottoni()
{
	AnsiString s;

	bbMissione->Enabled = ((magprel > 0) && (magdep > 0));
	bPrelievo->Enabled = (ADOQuery1ID_Pallet->Value > 0);
	bDeposito->Enabled = (ADOQuery1ID_Pallet->Value == 0);
	if (magprel > 0) {
		s.printf("(%d,%d)", magprel, posprel);
		stPrelievo->Caption = s;
	}
    else {
		stPrelievo->Caption = "-";
	}
	if (magdep > 0) {
		s.printf("(%d,%d)", magdep, posdep);
		stDeposito->Caption = s;
	}
    else {
		stDeposito->Caption = "-";
	}
	if (PageControl2->ActivePageIndex > 1) {
		BitBtnBlocca->Visible = false;
		BitBtnSblocca->Visible = false;
		BitBtnSvuota->Visible = false;
		BitBtnInsert->Visible = false;
		BitBtnEdit->Visible = false;
		BitBtnPick->Visible = false;
		BitBtnJobCreate->Visible = false;
        gbMissione->Visible = false;
	}
    else {
		if ((MainForm->pwdlevel == 0) && (PageControl2->ActivePageIndex > 3))
			PageControl2->ActivePageIndex = 0;
		BitBtnEdit->Visible = true;
		BitBtnPick->Visible = true;
		BitBtnJobCreate->Visible = true;
        gbMissione->Visible = (MainForm->pwdlevel > 1);
		BitBtnBlocca->Visible = (ADOQuery1Stato_Postazione->Value == 0);
		BitBtnSblocca->Visible = (ADOQuery1Stato_Postazione->Value > 0);
		BitBtnSvuota->Visible = (ADOQuery1ID_Pallet->Value > 0);
		BitBtnInsert->Visible = (ADOQuery1ID_Pallet->Value == 0);
		BitBtnEdit->Enabled = (ADOQuery1ID_Pallet->Value > 0);
		BitBtnPick->Enabled = (ADOQuery1ID_Pallet->Value > 0);
		BitBtnJobCreate->Enabled = (ADOQuery1ID_Pallet->Value > 0);
	}
 }
//---------------------------------------------------------------------------

void TMagazziniForm::Aggiorna3d()
{
	AnsiString strsql, alias, TableName, messaggio;
	int n, p, tp, pz, stpost, stpz, stplt, m, tollko, lotto, miss;
	UINT i;
	TFrameScaffale *s;
	int GridRow, TotalRow;
	double ox, oy, oz, dx, dy, dz, g, gm, gM, h;

	TableName.printf("Magazzino WHERE ID_Magazzino = %d ORDER BY ID_Postazione", PageControl1->ActivePageIndex + 1);
	DBDataModule->CaricaTabella(TableName, RecList);
	DBDataModule->CaricaTabellaK("Lotti WHERE Sospeso = 0 ORDER BY ID_Pallet", "ID_Pallet", TabLotti);
	TableName.printf("SELECT ID_Pallet, COUNT(*) AS N FROM (SELECT ID_Pallet FROM Jobs UNION SELECT ID_Pallet FROM Missioni) AS t GROUP BY ID_Pallet");
	DBDataModule->CaricaTabellaKDaQuery(TableName, "ID_Pallet", TabMiss);

	switch(PageControl1->ActivePageIndex) {
	case 0:
		s = FrameScaffale1;
		s->pToolBar->Visible = (MainForm->pwdlevel > 1);
		break;
	}
	if (PageControl1->ActivePageIndex < 6) {
		for (i = 0; i < RecList.size(); i++) {
			n = RecList[i]["ID_Postazione"].ToIntDef(0);
			p = RecList[i]["ID_Pallet"].ToIntDef(0);
            tp = RecList[i]["ID_TipoPallet"].ToIntDef(0);
			pz = RecList[i]["ID_Pezzo"].ToIntDef(0);
			stpost = RecList[i]["Stato_Postazione"].ToIntDef(0);
			stpz = RecList[i]["Stato_Pezzo"].ToIntDef(0);
			stplt = RecList[i]["Stato_Pallet"].ToIntDef(0);
			m = RecList[i]["Misura_Eseguita"].ToIntDef(0);
			miss = TabMiss[RecList[i]["ID_Pallet"]]["N"].ToIntDef(0);
			lotto = TabLotti[RecList[i]["ID_Pallet"]]["ID_Lotto"].ToIntDef(0);
			messaggio = RecList[i]["Descrizione_Pallet"];
            alias = RecList[i]["Alias"];
			if (n) {
				s->AggiornaPresenza(n, p, tp, pz, stpost, stpz, stplt, m, miss, lotto, alias, messaggio);
			}
		}
	}
	try {
		ADOQuery1->DisableControls();
		if (SelectedPage != PageControl1->ActivePageIndex) {
			ADOQuery1->Close();
			strsql.printf("SELECT * FROM MAGAZZINO WHERE ID_MAGAZZINO = %d ORDER BY ID_Postazione", PageControl1->ActivePageIndex + 1);
			ADOQuery1->SQL->Text = strsql;
			SelectedPage = PageControl1->ActivePageIndex;
			ADOQuery1->Open();
			MyDBGrid1->AutoFitAll();
		} else {
			MyDBGrid1->AutoFitAll();
			MyDBGrid1->RefreshDataSet();
		}
	} catch(...) {}
	try {
		ADOQuery1->EnableControls();
	} catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::PageControl1Change(TObject *Sender)
{
	MyDBGrid1->SelectedRows->Clear();
	MyDBGrid1->ClearFilter();
	ADOQuery1->Filtered = false;
	ADOQuery1->Sort = "";
	ADOQuery1->First();
	idpallet = 0;
	Aggiorna();
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::FrameScaffale1GLSceneViewer1MouseEnter(TObject *Sender)
{
	if (FrameScaffale1) {
		FrameScaffale1->SetFocus();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::MyDBGrid1MouseEnter(TObject *Sender)
{
	MyDBGrid1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::ADOQuery1AfterScroll(TDataSet *DataSet)
{
	int n;

	if (ADOQuery1->ControlsDisabled()) {
		return;
	}
	if (ADOQuery1ID_Postazione->IsNull) {
		return;
	}

	n = ADOQuery1ID_Postazione->Value;
	switch (PageControl1->ActivePageIndex) {
	case 0:
		FrameScaffale1->SelectPos(n);
		break;
	}
	AggiornaBottoni();
}
//---------------------------------------------------------------------------

void TMagazziniForm::SelectPosDBGrid(int pos) {
	TLocateOptions Opts;

	MyDBGrid1->SelectedRows->Clear();
	Opts.Clear();
	Opts << loPartialKey;
	ADOQuery1->Locate("ID_Postazione", IntToStr(pos), Opts);
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::FormActivate(TObject *Sender)
{
	TolleranzaElettrodi = 0;
	TolleranzaGAP = 0;
	TolleranzaGAPMax = 0;
	RiutilizziElettrodi = 0;
	Aggiorna();
	ADOQuery1->First();
	TimerUpd->Enabled = true;
	TimerUpd2->Enabled = true;
	magprel = 0;
	posprel = 0;
	magdep = 0;
	posdep = 0;
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::TimerShow3dTimer(TObject *Sender)
{
	TimerShow3d->Enabled = false;
	switch (PageControl1->ActivePageIndex) {
	case 0:
		PageControl1->Width = FrameScaffale1->framewidth + 12;
		Storage1->Visible = true;
		break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::TimerUpdTimer(TObject *Sender)
{
	TimerUpd->Enabled = false;
	Aggiorna3d();
	TimerUpd->Enabled = true;
}
//---------------------------------------------------------------------------

void TMagazziniForm::GeneraMissionePrelievoPallet(int ID_Pallet, int baia)
{
	TJob j;
	TPallet p;
	TPezzo pz;

	//Leggo i dati del pallet
	DBDataModule->LeggiPallet(ID_Pallet, p);

	//Leggo i dati del pezzo
	DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);

	memset(&j, 0, sizeof(TJob));
	j.ID_Pallet = ID_Pallet;
	j.Tipo_Missione = "DROP";
	j.Tipo_Macchina = "S";
	j.ID_Macchina = baia;
	j.ID_Pezzo = pz.ID_Pezzo;
	j.Programma_Macchina = p.Programma;
	j.ID_Articolo = pz.ID_Articolo;
	j.Priorita = 50;
    //Controllo che non sia gi� stata generato un job o una missione identica
    if (!DBDataModule->EsisteGiaJobPallet(j.Tipo_Missione, j.Tipo_Macchina, ID_Pallet) && 
    	!DBDataModule->EsisteGiaMissionePallet(j.Tipo_Missione, j.Tipo_Macchina, ID_Pallet)) {
        //Creo il JOB
        j.ID_Job = DBDataModule->CreaJob(j);
        //Attivo il JOB
        DBDataModule->AttivaJob(j.ID_Job);
        // LOG
        DBDataModule->Log("CLIENT", "MAGAZZINI", "PC %d: Prelievo Pallet = %d dal magazzino e depositato baia %d", MainForm->stationnumber, ID_Pallet, baia);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::BitBtnPickClick(TObject *Sender)
{
	TBitBtn *btn = (TBitBtn*)Sender;
    
    //Chiedo conferma all'operatore
	if ((Application->MessageBox(String::LoadStr(IDS_EXTPALLET).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;

	GeneraMissionePrelievoPallet(ADOQuery1ID_Pallet->Value, 0);
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::BitBtnEditClick(TObject *Sender)
{
    FormModificaPallet->m_PalletList.clear();
    FormModificaPallet->ImpostaDati(ADOQuery1ID_Pallet->Value);
    FormModificaPallet->m_PalletList.push_back(ADOQuery1ID_Pallet->Value);
    if (ADOQuery1ID_Pallet->Value) {
        FormModificaPallet->ShowModal();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::BitBtnJobCreateClick(TObject *Sender)
{
	FormNewJob->m_PalletList.clear();
	FormNewJob->m_PalletList.push_back(ADOQuery1ID_Pallet->Value);
	FormNewJob->SelectPallet(ADOQuery1ID_Pallet->Value);
	FormNewJob->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::BitBtnBloccaClick(TObject *Sender)
{
	int i;

	DBDataModule->CambiaStatoPostazione(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, POS_BLOCCATA);
	if (MyDBGrid1->SelectedRows->Count > 0) {
		for (i = 0; i < MyDBGrid1->SelectedRows->Count; i++) {
			MyDBGrid1->DataSource->DataSet->GotoBookmark(MyDBGrid1->SelectedRows->Items[i]);
			DBDataModule->CambiaStatoPostazione(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, POS_BLOCCATA);
		}
		MyDBGrid1->SelectedRows->Clear();
	}
	Aggiorna3d();
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::BitBtnSbloccaClick(TObject *Sender)
{
	int i;

	DBDataModule->CambiaStatoPostazione(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, POS_LIBERA);
	DBDataModule->ResettaErrorePezzo(ADOQuery1ID_Pezzo->Value);
	DBDataModule->AggiornaDescPallet(ADOQuery1ID_Pallet->Value, "");
	if (MyDBGrid1->SelectedRows->Count > 0) {
		for (i = 0; i < MyDBGrid1->SelectedRows->Count; i++) {
			MyDBGrid1->DataSource->DataSet->GotoBookmark(MyDBGrid1->SelectedRows->Items[i]);
			DBDataModule->CambiaStatoPostazione(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, POS_LIBERA);
			DBDataModule->ResettaErrorePezzo(ADOQuery1ID_Pezzo->Value);
			DBDataModule->AggiornaDescPallet(ADOQuery1ID_Pallet->Value, "");
			// LOG
			logstr.sprintf("PC %d: release position %d of storage %d", MainForm->stationnumber, ADOQuery1ID_Postazione->Value, ADOQuery1ID_Magazzino->Value);
			DBDataModule->Log("CLIENT", "MAGAZZINI", logstr);
		}
		MyDBGrid1->SelectedRows->Clear();
	}
	// LOG
	logstr.sprintf("PC %d: release position %d of storage %d", MainForm->stationnumber, ADOQuery1ID_Postazione->Value, ADOQuery1ID_Magazzino->Value);
	DBDataModule->Log("CLIENT", "STORAGE", logstr);
	Aggiorna3d();
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::BitBtnSvuotaClick(TObject *Sender)
{
	TPezzo pz;
	int i;


	if ((Application->MessageBox(String::LoadStr(IDS_EMPTYPLACE).w_str(), String::LoadStr(IDS_CONFOP).w_str(), MB_YESNO)) != ID_YES)
		return;
	// LOG
	logstr.sprintf("PC %d: delete pallet = %d article %s from position %d storage %d", MainForm->stationnumber, ADOQuery1ID_Pallet->Value, ADOQuery1Codice_Articolo->AsAnsiString, ADOQuery1ID_Postazione->Value, ADOQuery1ID_Magazzino->Value);
	DBDataModule->Log("CLIENT", "MAGAZZINI", logstr);
	if (ADOQuery1ID_Pallet->Value) {
		DBDataModule->CancellaJobPallet(ADOQuery1ID_Pallet->Value);
	}
	DBDataModule->PalletInMagazzino(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, 0);
	DBDataModule->CambiaStatoPostazione(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, POS_BLOCCATA);
	if (MyDBGrid1->SelectedRows->Count > 0) {
		for (i = 0; i < MyDBGrid1->SelectedRows->Count; i++) {
			MyDBGrid1->DataSource->DataSet->GotoBookmark(MyDBGrid1->SelectedRows->Items[i]);
			// LOG
			logstr.sprintf("PC %d: delete pallet = %d article %s from position %d storage %d", MainForm->stationnumber, ADOQuery1ID_Pallet->Value, ADOQuery1Codice_Articolo->AsAnsiString, ADOQuery1ID_Postazione->Value, ADOQuery1ID_Magazzino->Value);
			DBDataModule->Log("CLIENT", "MAGAZZINI", logstr);
			if (ADOQuery1ID_Pallet->Value) {
				DBDataModule->CancellaJobPallet(ADOQuery1ID_Pallet->Value);
			}
			DBDataModule->PalletInMagazzino(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, 0);
			DBDataModule->CambiaStatoPostazione(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, POS_BLOCCATA);
		}
	}
	DBDataModule->CleanupPezzi();
	Aggiorna3d();
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::BitBtnInsertClick(TObject *Sender)
{
	int res, idpallet;
	TPallet p;

	switch (PageControl1->ActivePageIndex) {
	case 0:
		FormInsertPallet->UnFilterPalletType();
		break;
	}
	res = FormInsertPallet->ShowModal();
	if (res == IDOK) {
		idpallet = FormInsertPallet->Selected;
		DBDataModule->LeggiPallet(idpallet, p);
		if (p.ID_Pallet > 0) {
			DBDataModule->PalletInMagazzino(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, idpallet);
			DBDataModule->CambiaStatoPostazione(ADOQuery1ID_Magazzino->Value, ADOQuery1ID_Postazione->Value, POS_BLOCCATA);
			// LOG
			logstr.sprintf("PC %d: insert pallet = %d in position %d storage %d", MainForm->stationnumber, idpallet, ADOQuery1ID_Postazione->Value, ADOQuery1ID_Magazzino->Value);
			DBDataModule->Log("CLIENT", "MAGAZZINI", logstr);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::MyDBGrid1DrawColumnCell(TObject *Sender, const TRect &Rect, int DataCol, TColumn *Column, TGridDrawState State)
{
	if (ADOQuery1->RecordCount == 0) {
		return;
	}

	if (ADOQuery1Stato_Postazione->AsInteger > 3) {
		MyDBGrid1->Canvas->Font->Color = clWhite;
		MyDBGrid1->Canvas->Brush->Color = clMaroon;
	}
    else if (ADOQuery1Stato_Postazione->AsInteger == 3) {
		MyDBGrid1->Canvas->Font->Color = clWhite;
		MyDBGrid1->Canvas->Brush->Color = clRed;
	}
    else if (ADOQuery1Stato_Postazione->AsInteger != 0) {
		MyDBGrid1->Canvas->Font->Color = clWhite;
		MyDBGrid1->Canvas->Brush->Color = (TColor)0x00D1A4FF; // (TColor)0x001A88D9;
	}
    else if (ADOQuery1Stato_Postazione->AsInteger == 3) {
		MyDBGrid1->Canvas->Font->Color = clWhite;
		MyDBGrid1->Canvas->Brush->Color = clRed;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::GLSceneViewer1BeforeRender(TObject *Sender)
{
	GL()->LineWidth(2.0);
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::TimerUpd2Timer(TObject *Sender)
{
	TimerUpd2->Enabled = false;
	AggiornaBottoni();
	TimerUpd2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (Shift.Contains(ssCtrl) && (Key == VK_DELETE))
		Key = 0; // ignore
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::ADOQuery1CalcFields(TDataSet *DataSet)
{
//	ADOQuery1FuoriTolleranza->Value = 0;
    ADOQuery1Label->Value = (ADOQuery1Alias->AsString == "") ? ADOQuery1ID_Postazione->AsString : ADOQuery1Alias->AsString;
}
//---------------------------------------------------------------------------

void TMagazziniForm::EvidenziaDBEdit(TDBEdit *e, bool val)
{
	if (val) {
		e->Color = clRed;
		e->Font->Color = clWhite;
	} else {
		e->Color = clGradientInactiveCaption;
		e->Font->Color = clBlack;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::bPrelievoClick(TObject *Sender)
{
	idpallet = ADOQuery1ID_Pallet->Value;
	if (idpallet > 0) {
		magprel = ADOQuery1ID_Magazzino->Value;
		posprel = ADOQuery1ID_Postazione->Value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::bDepositoClick(TObject *Sender)
{
	ADOQuery1ID_Pallet->Value;
	if (ADOQuery1ID_Pallet->Value == 0) {
		magdep = ADOQuery1ID_Magazzino->Value;
		posdep = ADOQuery1ID_Postazione->Value;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMagazziniForm::bbMissioneClick(TObject *Sender)
{
	TJob j;
	TPallet p;

	if ((magprel > 0) && (idpallet > 0) && (magdep > 0)) {
		j.ID_Pallet = idpallet;
		j.Tipo_Missione = "DROP";
		j.Tipo_Macchina = IntToStr(magdep);
		j.ID_Macchina = posdep;
		j.Priorita = 99;
		DBDataModule->LeggiPallet(j.ID_Pallet, p);
		j.ID_Pezzo = p.ID_Pezzo;
		j.ID_Job = DBDataModule->CreaJob(j);
		DBDataModule->AttivaJob(j.ID_Job);
	}
}
//---------------------------------------------------------------------------

void TMagazziniForm::ImpostaAliases(int nmag)
{
	AnsiString TableName;
	UINT i;
    int n;
	TFrameScaffale *s;
	AnsiString label;

	TableName.printf("Postazioni WHERE ID_Magazzino = %d ORDER BY ID_Postazione", nmag);
	DBDataModule->CaricaTabella(TableName, RecList);
	switch(nmag) {
	case 1:
		s = FrameScaffale1;
		break;
	}
    for (i = 0; i < RecList.size(); i++) {
        n = RecList[i]["ID_Postazione"].ToIntDef(0);
        label = RecList[i]["Alias"];
        if (n && (label != "")) {
            s->SetLabel(n, label);
        }
    }
}
//---------------------------------------------------------------------------

