//---------------------------------------------------------------------------
#ifndef ClientDataH
#define ClientDataH
//---------------------------------------------------------------------------
#include "define.h"
//---------------------------------------------------------------------------
struct tool {
	int PotNumber;	//Numero del bicchiere su cui � inserito l'utensile
	int ToolName;	//Nome utensile
	int ToolGroup;	//Nome Gruppo
	int MaxLife;	//Vita massima gruppo/utensile
	int ActLife;	//Vita attuale gruppo/utensile
	int CountLife;	//Vita resituda gruppo/utensile
	float Raggio;
	float Lunghezza;
	float CorrRadius;
	float CorrLength;
	int KitCode;	//Codice del KIT (vedi DMG)
	int Broken;		//Utensile/Gruppo rotto
};
//---------------------------------------------------------------------------

typedef struct {
	int Comando_START;
	int Comando_CallMaster;
	int ComandoHOLD;
	int ComandoRESET;
	int Errore;
	int Automatico;
	int Running;
	int Hold;
	int Vita;
	int Emergenza;
	int Play;
	int Top_Master;
	int HOME;
	int Presenza_pinza;
	int Pinza_aperta;
	int Pinza_chiusa;
	int Presenza_pallet;
	int Pinza_bassa;
	int Pinza_alta;
} TStatoROBOT;
//---------------------------------------------------------------------------

typedef struct {
	TStatoROBOT Stato;
	int Override;
	int Pinza;
	int ErrAltezzaBaia[N_STAZIONI_OP];
	SHORT R[120];
} TDatiROBOT;
//---------------------------------------------------------------------------

typedef struct {
	int Tipo_prelievo;	// 2=MU, 3=Baia, 4=Mag.elettrodi, 6=Magazzino
	int N_locazione_magazzino_pre;
	int N_magazzino_pre;
	int N_staz_operatore_pre;
	int N_MU_pre;
	int Pos_MU_pre;
    int Pos_staz_operatore_pre;
	int Tipo_deposito;	// 2=MU, 3=Baia, 4=Mag.elettrodi, 6=Magazzino
	int N_locazione_magazzino_dep;
	int N_magazzino_dep;
	int N_staz_operatore_dep;
	int N_MU_dep;
	int Pos_MU_dep;
    int Pos_staz_operatore_dep;
	int Strobe;
	int N_pallet;
	int Tipo_pinza;
    int TipoMissioneRBT;
	int intParam[5];
} TMissione;
//---------------------------------------------------------------------------

typedef struct {
	int Override;
	int ErrAltezzaBaia[N_STAZIONI_OP];
	WORD MW[5000];
} TDatiTRASLO;
//---------------------------------------------------------------------------

typedef struct {
	unsigned char DB[255][255];
} TDatiPLC;
//---------------------------------------------------------------------------

typedef struct {
	int aut;
	int run;
	int motion;
	int running_prg;
	int main_prg;
	int P1Fuori;
	int P2Fuori;
	int PortaAperta;
	int SetUp;
	int alarm;
	int emergency;
	int EndCycle;
	int Rottura_Utensile;
	int N_Utensile_Rotto;
	int Errore_Misura;
	tool ToolsTab[N_TOOLS + 1];
} TDatiMacchina;
//---------------------------------------------------------------------------

typedef struct {
	int aut;
	int run;
	int motion;
	int running_prg;
	int main_prg;
	int alarm;
	int emergency;
	int FineCicloPartProgram;
	int FineCicloRotazione;
	int ForcaCambioARiposo;
	int P1Fuori;
	int P2Fuori;
	int MacchinaInAllarme;
	int PalletScarto;
	int CicloInEsecuzione;
	int PresenzaPallet;
	tool ToolsTab[N_TOOLS + 1];
} TDatiMacchinaFanuc;
//---------------------------------------------------------------------------

typedef struct {
	int ProgramStatus;
	char SelectedProgram[200];
	char ActiveProgram[200];
	int	npallet;
	int opt;
    int grip;
} TDatiMacchinaHeidenhein;
//---------------------------------------------------------------------------

typedef struct {
	int IDValue;
	int IDLast;
} TDatiRFID;
//---------------------------------------------------------------------------

typedef struct {
	// Inizio pacchetto per sincronismo trasmissione dati
	char Sync[5];
	// Generali
	int watchdog;
	bool PLC_Connected;
//	bool Modbus_Connected;
	bool Robot_Connected;
	bool M_Connected[N_MACCHINE_UT];
//	bool C_Connected[N_MACCHINE_UT];
	// Missione in corso
	TMissione Missione;
	// Robot
	TDatiROBOT DatiRBT[N_ROBOTS];
	// Cartesiano
//	TDatiTRASLO DatiTRS;
	// PLC
	TDatiPLC PLC;
	TDatiMacchinaFanuc DatiMU[N_MACCHINE_UT];
//	int Forzatura_Macchina[N_MACCHINE_UT];
} TClientData;
//---------------------------------------------------------------------------

extern TClientData ClientData;
//---------------------------------------------------------------------------
#endif
