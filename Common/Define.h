//---------------------------------------------------------------------------
#ifndef DEFINE_H
#define DEFINE_H
//---------------------------------------------------------------------------
#define BIT0							0x0001
#define BIT1							0x0002
#define BIT2							0x0004
#define BIT3							0x0008
#define BIT4							0x0010
#define BIT5							0x0020
#define BIT6							0x0040
#define BIT7							0x0080
#define BIT8							0x0100
#define BIT9							0x0200
#define BIT10							0x0400
#define BIT11							0x0800
#define BIT12							0x1000
#define BIT13							0x2000
#define BIT14							0x4000
#define BIT15							0x8000
//---------------------------------------------------------------------------

#define N_STAZIONI_OP					2	//Numero di baie operatore
#define N_MACCHINE_UT					3	//Numero di macchine utensili
#define N_ROBOTS						1	//Numero di Robot antropomorfi
//---------------------------------------------------------------------------

#define GRIP_NONE						0
#define GRIP_PALLET						1	//Pinza pallet
#define GRIP_TOOL						2
//---------------------------------------------------------------------------

#define TPALLET_500						1
//---------------------------------------------------------------------------

#define PAR_DROP_PALLET_STATION_MU1		1	//Indice del parametro per il deposito del pallet in baia una volta uscita dalla macchina utensile 1
#define PAR_DROP_PALLET_STATION_MU2		2	//Indice del parametro per il deposito del pallet in baia una volta uscita dalla macchina utensile 2
#define PAR_DROP_PALLET_STATION_MU3		3	//Indice del parametro per il deposito del pallet in baia una volta uscita dalla macchina utensile 3
#define PAR_DROP_PALLET_STATION_MU(i)	(PAR_DROP_PALLET_STATION_MU1 + i - 1)
//---------------------------------------------------------------------------

//Definizioni posizioni magazzino
#define POS_LIBERA						0
#define POS_PRENOTATA_PRELIEVO			1
#define POS_PRENOTATA_DEPOSITO			2
#define POS_BLOCCATA					3
#define POS_BLOCCATA_CHECK				4
//---------------------------------------------------------------------------

//Definizioni dello stato dei pezzi
#define STATO_PEZZO_GREZZO				0
#define STATO_PEZZO_ATTREZZATO			1
#define STATO_PEZZO_LAVORATO			2
#define STATO_PEZZO_LOTTO_SOSPESO	    3
#define STATO_PEZZO_ERRORE_PEL			100
#define STATO_PEZZO_SCARTO				200
#define STATO_PEZZO_ERRORE_QUOTE		300
#define STATO_PEZZO_NON_LAVORABILE		400
//---------------------------------------------------------------------------

#define COL_PEZZO_GREZZO				clrDimGray
#define COL_PEZZO_ATTREZZATO			clrYellowGreen
#define COL_PEZZO_LAVORATO				clrGreen
#define COL_PEZZO_ERRORE				clrRed
#define COL_PEZZO_A_MU1			 		clrPurple
#define COL_PEZZO_A_MU2			 		clrBlue
#define COL_PEZZO_A_MU12		 		clrBlack
#define COL_NO_LOTTO					clrGray50
#define COL_LOTTO_SOSPESO				clrSlateBlue
#define COL_MU1							clrSlateBlue
#define COL_MU2							clrOrange
#define COL_MU3							clrSalmon
//---------------------------------------------------------------------------

//Definizioni missioni PC
#define TPMISS_MU						2
#define TPMISS_BAIA						3
#define TPMISS_MAG						6
//---------------------------------------------------------------------------

#define OVERRIDE   						10
//---------------------------------------------------------------------------

#define MAX_NUM_TOOLNAME				10000	//Indica il numero massimo con cui si pu� rappresentare il nome di un utensile
#define N_TOOLS							200		//Indica il numero di bicchieri presenti in catena della macchina utensile
//---------------------------------------------------------------------------

#define DB_ALLARMI						12		//Indica il numero della DB associata agli allarmi
#define DB_PLC_MU_OUT					14		//Indica il numero della DB associata allo scambio dati tra PC e MU in uscita
#define DB_PLC_MU_IN					15		//Indica il numero della DB associata allo scambio dati tra PC e MU in ingresso
#define DB_STAZIONI						30		//Indica il numero della DB associata alle baie
#define DB_TIPOALLARMI					31		//Indica se l'allarme corrispondente e un allarme o un warning
//---------------------------------------------------------------------------

//Definizioni tipi CN Fanuc
#define CN_FANUC_31i					1
#define CN_FANUC_16i					2
//---------------------------------------------------------------------------

enum LSV2_RUNINFO_TYPE
{
  LSV2_RUNINFO_AXES_CONFIG = 20,
  LSV2_RUNINFO_AXES_POSITION = 21,
  LSV2_RUNINFO_CUTTER_LOCATION = 22,
  LSV2_RUNINFO_EXECUTION_MODE = 23,
  LSV2_RUNINFO_EXECUTION_POINT = 24,
  LSV2_RUNINFO_OVERRIDES_INFO = 25,
  LSV2_RUNINFO_PROGRAM_STATUS = 26,
  LSV2_RUNINFO_FIRST_ERROR = 27,
  LSV2_RUNINFO_NEXT_ERROR = 28,
  LSV2_RUNINFO_DNC_MODE = 29,

  LSV2_RUNINFO_NC_UPTIME = 30,
  LSV2_RUNINFO_MACHINE_UPTIME = 31,
  LSV2_RUNINFO_MACHINE_RUNNINGTIME = 32,
  LSV2_RUNINFO_PLC_TIME0 = 33,
  LSV2_RUNINFO_PLC_TIME1,
  LSV2_RUNINFO_PLC_TIME2,
  LSV2_RUNINFO_PLC_TIME3,
  LSV2_RUNINFO_PLC_TIME4,
  LSV2_RUNINFO_PLC_TIME5,
  LSV2_RUNINFO_PLC_TIME6,
  LSV2_RUNINFO_PLC_TIME7,
  LSV2_RUNINFO_PLC_TIME8,
  LSV2_RUNINFO_PLC_TIME9,
  LSV2_RUNINFO_PLC_TIME10,
  LSV2_RUNINFO_PLC_TIME11,
  LSV2_RUNINFO_PLC_TIME12,

  LSV2_RUNINFO_PCCT  = 50   // PositionControlCycleTime
};
//---------------------------------------------------------------------------

enum LSV2_EXEC_MODE
{
  LSV2_EXEC_MANUAL = 0,
  LSV2_EXEC_MDI = 1,
  LSV2_EXEC_RPF = 2,
  LSV2_EXEC_SINGLESTEP = 3,
  LSV2_EXEC_AUTOMATIC = 4,
  LSV2_EXEC_OTHER = 5
};
//---------------------------------------------------------------------------

enum LSV2_PROGRAM_STATUS
{
	LSV2_PROGRAM_STATUS_STARTED,
	LSV2_PROGRAM_STATUS_STOPPED,
	LSV2_PROGRAM_STATUS_FINISHED,
	LSV2_PROGRAM_STATUS_CANCELED,
	LSV2_PROGRAM_STATUS_INTERRUPTED,
	LSV2_PROGRAM_STATUS_ERROR,
	LSV2_PROGRAM_STATUS_ERROR_CLEARED,
	LSV2_PROGRAM_STATUS_IDLE
};
//---------------------------------------------------------------------------

static AnsiString DescProgramStatus[LSV2_PROGRAM_STATUS_IDLE + 1] = {
	"STARTED",
	"STOPPED",
	"FINISHED",
	"CANCELED",
	"INTERRUPTED",
	"ERROR",
	"ERROR_CLEARED",
	"IDLE"
};
//---------------------------------------------------------------------------

#endif
