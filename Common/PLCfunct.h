//---------------------------------------------------------------------------

#ifndef PLCfunctH
#define PLCfunctH

#define BYTE0	0x000000FF
#define BYTE1	0x0000FF00
#define BYTE2	0x00FF0000
#define BYTE3	0xFF000000

//---------------------------------------------------------------------------
// ModBus
long int GetDWord(WORD *MW, int md);
float GetReal(WORD *MW, int md);
bool GetBit(WORD *MW, int mx, int bit);
AnsiString GetString(WORD *MW, int address, int length);
// Siemens
int DBD(char *buffer);
int DBW(char *buffer);
void FormatDBD(int n, char *buffer);
void FormatDBW(int n, char *buffer);
char GetBit(unsigned char value, unsigned char bit);
double GetTimer(char *buffer);
void FormatTimer(double t, char *buffer);
float GetPlcReal(char *buffer);
long GetPlcDWord(char *buffer);
int GetPlcWord(char *buffer);
//---------------------------------------------------------------------------

#endif
