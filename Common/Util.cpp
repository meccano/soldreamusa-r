//---------------------------------------------------------------------------

#pragma hdrstop

#include <time.h>
#include <stdio.h>
#include <System.IOUtils.hpp>
#include "Util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

void TGetFiles::GetFileList(TStringDynArray &fileslist) {
	fileslist = files;
}
//---------------------------------------------------------------------------

__fastcall TGetFiles::TGetFiles(bool CreateSuspended, AnsiString fn)
	: TThread(CreateSuspended)
{
	FolderName = fn;
}
//---------------------------------------------------------------------------
void __fastcall TGetFiles::Execute()
{
	time_t t;

	NameThreadForDebugging((AnsiString)("GetFiles"), ThreadID);
	//---- Place thread code here ----
	t = clock();
	while (!Terminated) {
		try {
			if ((clock() - t) > 20000) {
				if (DirectoryExists(FolderName))
					files = TDirectory::GetFiles(FolderName, "*.H", TSearchOption::soAllDirectories);
				t = clock();
            }
		} catch (...) {};
		Sleep(1000);
	}
	Suspend();
}
//---------------------------------------------------------------------------

AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, int NumCaratteri)
{
	int inizio, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	inizio = tg.Pos(NomeCampo);
	if (inizio > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, inizio + cl - 1);
		res = tg.SubString(1, NumCaratteri);
	}
	return res;
}
//---------------------------------------------------------------------------

AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma)
{
	int i, inizio, fine, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	inizio = tg.Pos(NomeCampo);
	if (inizio > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, inizio + cl - 1);
		for(i = 1; i < tg.Length(); i++) {
			if (!isdigit((wchar_t)(tg[i]))) {
				fine = i - 1;
				break;
			}
		}
		if (fine <= 0) {
			fine = tg.Length() - 1;
		}
		if (fine > 0) {
			res = tg.SubString(1, fine);
		}
	}
	return res;
}
//---------------------------------------------------------------------------

TColor Color4fToByte(TVector4f BytesToWrap)
{
    System::Uitypes::TColor C;
    C = (System::Uitypes::TColor)RGB(Trunc(BytesToWrap.X * 255), Trunc(BytesToWrap.Y * 255), Trunc(BytesToWrap.Z * 255));

	return C;
}
//---------------------------------------------------------------------------

AnsiString Parser(AnsiString NomeCampo, AnsiString Telegramma, AnsiString Separatore)
{
	int inizio, fine, cl;
	AnsiString tg, res;

	res = "";
	tg = Telegramma;
	inizio = tg.Pos(NomeCampo);
	if (inizio > 0) {
		cl = NomeCampo.Length();
		tg.Delete(1, inizio + cl - 1);
		fine = tg.Pos(Separatore);
		if (fine <= 0) {
			fine = tg.Length();
		}
		if (fine > 0)
			res = tg.SubString(1, fine - 1);
	}
	return res;
}
//---------------------------------------------------------------------------

int NumeroProgramma(AnsiString filename) {
	FILE *f;
	char s[256];
	AnsiString field;
	int res, val;

	res = -2;
	val = 0;
	try {
		f = fopen(filename.c_str(), "r");
		if (f) {
			while (fgets(s, 256, f) && !val) {
				field = Parser("O", s);
				if (field.IsEmpty()) {
					field = Parser(":", s);
				}
				val = atoi(field.c_str());
			}
			fclose(f);
			res = val;
		}
	} catch (...) {
		res = -1;
	}
	return res;
}
//---------------------------------------------------------------------------

char DecimalToBcd(char decimal) {
    return (char) ((decimal / 10)*16)+(decimal % 10);
}
//---------------------------------------------------------------------------

double ltod(long value)
{
	return ((double)value)/1000.0;
}
//---------------------------------------------------------------------------

long dtol(double value)
{
	double tmp = value*1000.0;
	return (long)tmp;
}
//---------------------------------------------------------------------------

AnsiString AutDesc(int n) {
	switch(n) {
	case 1 : return "MEMORY";
	case 2 : return "****";
	case 3 : return "EDIT";
	case 4 : return "HANDLE";
	case 5 : return "JOG";
	case 6 : return "Teach in JOG";
	case 7 : return "Teach in HANDLE";
	case 8 : return "INC feed";
	case 9 : return "REFERENCE";
	case 10 : return "REMOTE";
	}
	return "?";
}
//---------------------------------------------------------------------------

AnsiString RunDesc(int n) {
	switch(n) {
	case 0 : return "****(reset)";
	case 1 : return "STOP";
	case 2 : return "HOLD";
	case 3 : return "START";
	case 4 : return "MSTR";
	}
	return "?";
}
//---------------------------------------------------------------------------

