//---------------------------------------------------------------------------

#ifndef LOGTXTH
#define LOGTXTH
//---------------------------------------------------------------------------

extern void LogTxt(char *s, ...);
extern void LogTxt(AnsiString s);
extern void CancellaLogVecchi(int giorni = 14);

//---------------------------------------------------------------------------
#endif

