/PROG  _PC_N_EVE
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "Pc events";
PROG_SIZE	= 2288;
CREATE		= DATE 19-04-11  TIME 08:21:54;
MODIFIED	= DATE 19-04-13  TIME 07:35:42;
FILE_NAME	= PC_N_ALL;
VERSION		= 0;
LINE_COUNT	= 84;
MEMORY_SIZE	= 2580;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*; 
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !------------------------------- ;
   2:  ! SEND EVENT TO PC ;
   3:  ! N. EVENT ;
   4:  !------------------------------- ;
   5:  !AR[1]:n.event ;
   6:   ;
   7:  R[29:PC-N.Evento]=AR[1]    ;
   8:   ;
   9:  !su PC modificare ;
  10:  !C:/meccano/.../eventirobot2 ;
  11:  !e riavviare applicativo ;
  12:   ;
  13:  !------------------------ ;
  14:  !0: ...null.......  ;
  15:  !1: restart after abort ;
  16:  !2: access request ;
  17:  !3: ;
  18:  !4: pallet locked ;
  19:  !5: pallet unlocked ;
  20:  !6: pallet unlocked manually ;
  21:  !7: pallet locked manually ;
  22:  !8: mission waiting ;
  23:  !9: mission recived ;
  24:   ;
  25:  !11: manual L-Homing  ;
  26:  !12: manual J-Homing ;
  27:  !13: ... ;
  28:  !14: ... ;
  29:   ;
  30:  !21: Switch to T1 Mode ;
  31:  !22: Switch to T2 Mode ;
  32:  !23: Switch to AUTO Mode ;
  33:  !24: ... ;
  34:   ;
  35:  !31: unload pallet from Mach.1 ;
  36:  !32: unload pallet from Mach.2 ;
  37:  !33: unload pallet from Mach.3 ;
  38:  !34: unload pallet from Stat.1 ;
  39:  !35: unload pallet from Stat.2 ;
  40:   ;
  41:  !41: load pallet to Mach.1 ;
  42:  !42: load pallet to Mach.2 ;
  43:  !43: load pallet to Mach.3 ;
  44:  !44: load pallet to Stat.1 ;
  45:  !45: load pallet to Stat.2 ;
  46:   ;
  47:  !51: check height Stat.1 ;
  48:  !52: check height Stat.2 ;
  49:   ;
  50:  !61: open Stat.1 ;
  51:  !62: open Stat.2 ;
  52:   ;
  53:  !71: close Stat.1 ;
  54:  !72: close Stat.2 ;
  55:   ;
  56:  !81: lock pallet on Mach.1 ;
  57:  !82: lock pallet on Mach.2 ;
  58:  !83: lock pallet on Mach.3 ;
  59:   ;
  60:  !91: unlock pallet on Mach.1 ;
  61:  !92: unlock pallet on Mach.2 ;
  62:  !93: unlock pallet on Mach.3 ;
  63:   ;
  64:  !101: control PEL on Mach.1 OK ;
  65:  !102: control PEL on Mach.2 OK ;
  66:  !103: control PEL on Mach.3 OK ;
  67:   ;
  68:  !105: control PEL on Mach.1 KO ;
  69:  !106: control PEL on Mach.2 KO ;
  70:  !107: control PEL on Mach.3 KO ;
  71:   ;
  72:   ;
  73:   ;
  74:  !111: end load Machine 1 ;
  75:  !112: end load Machine 2 ;
  76:  !113: end load Machine 3 ;
  77:   ;
  78:  !141: check position free ;
  79:  !142: check position not free ;
  80:   ;
  81:  !151: point modification allowed ;
  82:   ;
  83:  ! ;
  84:   ;
/POS
/END
