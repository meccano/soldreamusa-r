BACKUP DATABASE [Soldream]
  TO DISK = 'C:\MECCANO\Database\Soldream.bak'
  WITH INIT, NOUNLOAD
  , name = 'Soldream-Complete Database Backup'
  , NOSKIP
  , STATS = 10
  , Description = 'script per il backup del database Soldream con MSDE/SQL Server'
  , NOFORMAT
GO

QUIT