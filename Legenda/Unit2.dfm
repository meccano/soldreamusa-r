object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 636
  ClientWidth = 783
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GLSceneViewer1: TGLSceneViewer
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 777
    Height = 630
    Camera = Camera
    FieldOfView = 92.794364929199220000
    Align = alClient
    TabOrder = 0
  end
  object GLScene1: TGLScene
    Left = 568
    Top = 16
    object Luce: TGLLightSource
      Ambient.Color = {0000803F0000803F0000803F0000803F}
      ConstAttenuation = 1.000000000000000000
      Position.Coordinates = {00007A440000FA4500C0DAC50000803F}
      LightStyle = lsOmni
      SpotCutOff = 180.000000000000000000
      SpotDirection.Coordinates = {00000000000000000000000000000000}
    end
    object Camera: TGLCamera
      DepthOfView = 300.000000000000000000
      FocalLength = 300.000000000000000000
      NearPlaneBias = 0.100000001490116100
      TargetObject = Target
      Position.Coordinates = {0000000000004040000020C10000803F}
      Direction.Coordinates = {00000000000000000000803F00000000}
      Up.Coordinates = {000000000000803F0000008000000000}
    end
    object Target: TGLDummyCube
      Position.Coordinates = {000000000000803F000000000000803F}
      Pickable = False
      CubeSize = 1.000000000000000000
      EdgeColor.Color = {A9A5253FB1A8283EB1A8283E0000803F}
      object glcGrezzo: TGLCube
        Material.FrontProperties.Diffuse.Color = {0000803F0000803F000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03F9A99993F000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCC4C3ECDCC4C3E}
        object GLSpaceText5: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Pezzi da attrezzare')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcAttrezzato1: TGLCube
        Material.FrontProperties.Diffuse.Color = {000000000000803F000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03F3333733F000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCC4C3ECDCC4C3E}
        object GLSpaceText1: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Invia a lavorazione')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcLavorato1: TGLCube
        Material.FrontProperties.Diffuse.Color = {00000000F8FEFE3E0000803F0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03F3333333F000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCC4C3ECDCC4C3E}
        object GLSpaceText13: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Pronto per lavorazione')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcAttrezzato2: TGLCube
        Material.FrontProperties.Diffuse.Color = {000000000000003F000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03F6666E63E000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCC4C3ECDCC4C3E}
        object GLSpaceText14: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Lavorato')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcLavorato2: TGLCube
        Material.FrontProperties.Diffuse.Color = {0000803F00000000000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03FCDCC4C3E000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCC4C3ECDCC4C3E}
        object GLSpaceText15: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Scarto')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcScarto: TGLCube
        Material.FrontProperties.Diffuse.Color = {0000003F0000003F0000003F0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03FCDCC4CBD000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCCCC3DCDCC4C3E}
        object GLSpaceText7: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Pallet senza lotto')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcNoLotto: TGLCube
        Material.FrontProperties.Diffuse.Color = {000000000000803F0000803F0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03F9A9999BE000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCCCC3DCDCC4C3E}
        object GLSpaceText16: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Con lotto attivo')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcPrenotatoPrel: TGLCube
        Material.FrontProperties.Diffuse.Color = {E4DB5B3FEBE0E03EE4DB5B3F0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03FCDCC0CBF000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCCCC3DCDCC4C3E}
        object GLSpaceText3: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Pallet in prelievo')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcPrenotatoDep: TGLCube
        Material.FrontProperties.Diffuse.Color = {E4DB5B3FEBE0E03EE4DB5B3F0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Material.PolygonMode = pmLines
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03FCDCC4CBF000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCC4C3ECDCC4C3E}
        object GLSpaceText2: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Pallet in deposito')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object glcBloccato: TGLCube
        Material.FrontProperties.Diffuse.Color = {0000803F00000000000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Material.PolygonMode = pmLines
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {0000C03F666686BF000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3ECDCC4C3ECDCC4C3E}
        object GLSpaceText4: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {9A9919BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Postazione bloccata')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLSpaceText6: TGLSpaceText
        Material.FrontProperties.Diffuse.Color = {00000000000000000000803F0000803F}
        Direction.Coordinates = {6BB092BE00000000B44475BF00000000}
        Position.Coordinates = {CDCC0C3F666626BF000000000000803F}
        Scale.Coordinates = {9A99993F9A99993F9A99993F00000000}
        TurnAngle = 180.000000000000000000
        Visible = False
        Extrusion = 0.050000000745058060
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Lines.Strings = (
          '3')
        AspectRatio = 0.699999988079071000
        TextHeight = 0.100000001490116100
        object GLSpaceText8: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Position.Coordinates = {5C8F423E0AD723BDCDCCCCBD0000803F}
          Scale.Coordinates = {52B85E3F52B85E3F52B85E3F00000000}
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Destinato Makino')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLSpaceText9: TGLSpaceText
        Material.FrontProperties.Diffuse.Color = {0000803F000000000000803F0000803F}
        Direction.Coordinates = {6BB092BE00000000B44475BF00000000}
        Position.Coordinates = {CDCC0C3F333373BF000000000000803F}
        Scale.Coordinates = {9A99993F9A99993F9A99993F00000000}
        TurnAngle = 180.000000000000000000
        Visible = False
        Extrusion = 0.050000000745058060
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Lines.Strings = (
          '2')
        AspectRatio = 0.699999988079071000
        TextHeight = 0.100000001490116100
        object GLSpaceText10: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Position.Coordinates = {5C8F423E0AD723BDCDCCCCBD0000803F}
          Scale.Coordinates = {52B85E3F52B85E3F52B85E3F00000000}
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Destinato Hermle')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLSpaceText11: TGLSpaceText
        Material.FrontProperties.Diffuse.Color = {0000000000000000000000000000803F}
        Direction.Coordinates = {6BB092BE00000000B44475BF00000000}
        Position.Coordinates = {CDCC0C3F0000A0BF000000000000803F}
        Scale.Coordinates = {9A99993F9A99993F9A99993F00000000}
        TurnAngle = 180.000000000000000000
        Visible = False
        Extrusion = 0.050000000745058060
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Lines.Strings = (
          '6')
        AspectRatio = 0.699999988079071000
        TextHeight = 0.100000001490116100
        object GLSpaceText12: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Position.Coordinates = {5C8F423E0AD723BDCDCCCCBD0000803F}
          Scale.Coordinates = {52B85E3F52B85E3F52B85E3F00000000}
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Destinato sia Makino che Hermle')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
    end
  end
end
