object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 640
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GLSceneViewer1: TGLSceneViewer
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 629
    Height = 634
    Camera = Camera
    FieldOfView = 92.703445434570310000
    Align = alClient
    TabOrder = 0
  end
  object GLScene1: TGLScene
    Left = 552
    Top = 24
    object Luce: TGLLightSource
      Ambient.Color = {0000803F0000803F0000803F0000803F}
      ConstAttenuation = 1.000000000000000000
      Position.Coordinates = {00007A440000FA4500C0DAC50000803F}
      LightStyle = lsOmni
      SpotCutOff = 180.000000000000000000
      SpotDirection.Coordinates = {00000000000000000000000000000000}
    end
    object Camera: TGLCamera
      DepthOfView = 300.000000000000000000
      FocalLength = 300.000000000000000000
      NearPlaneBias = 0.100000001490116100
      TargetObject = Target
      Position.Coordinates = {0000000000004040000020C10000803F}
      Direction.Coordinates = {00000000000000000000803F00000000}
      Up.Coordinates = {000000000000803F0000008000000000}
    end
    object Target: TGLDummyCube
      Position.Coordinates = {000000000000803F000000000000803F}
      Pickable = False
      CubeSize = 1.000000000000000000
      EdgeColor.Color = {A9A5253FB1A8283EB1A8283E0000803F}
      object GLCube1: TGLCube
        Material.FrontProperties.Diffuse.Color = {9A99193FCDCC4C3FACC8483E0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F6666663F000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText1: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Part finished')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube5: TGLCube
        Material.FrontProperties.Diffuse.Color = {EC51B83ECDCC4C3EEC51B83D0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F9A99993F000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText5: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Raw part')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube7: TGLCube
        Material.FrontProperties.Diffuse.Color = {0000803F00000000000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F9A9999BE000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText7: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'End life electrode')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube6: TGLCube
        Material.FrontProperties.Diffuse.Color = {938C0C3E938E0E3FDCD6D63E0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F9A99193F000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText6: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Measured part')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube2: TGLCube
        Material.FrontProperties.Diffuse.Color = {9A99593F14AE073FCDCCCC3D0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Material.PolygonMode = pmLines
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F9A9919BF000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText2: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Reserved for drop')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube3: TGLCube
        Material.FrontProperties.Diffuse.Color = {9A99593F14AE073FCDCCCC3D0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F666666BF000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText3: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Reserved for pick')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube4: TGLCube
        Material.FrontProperties.Diffuse.Color = {0000803F00000000000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Material.PolygonMode = pmLines
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F9A9999BF000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText4: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Blocked position')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube8: TGLCube
        Material.FrontProperties.Diffuse.Color = {0000803F0000803F000000000000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F9A99993E000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText8: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Electrode out of tolerance')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
      object GLCube9: TGLCube
        Material.FrontProperties.Diffuse.Color = {CFBC3C3EA19E9E3ECFBC3C3E0000803F}
        Material.FrontProperties.Emission.Color = {CDCCCC3DCDCCCC3DCDCCCC3D0000803F}
        Direction.Coordinates = {EE83843E00000000EA46773F00000000}
        Position.Coordinates = {CDCC0C3F00000000000000000000803F}
        TurnAngle = 15.000000000000000000
        Pickable = False
        CubeSize = {CDCC4C3E0000803ECDCC4C3E}
        object GLSpaceText9: TGLSpaceText
          Material.FrontProperties.Diffuse.Color = {938C0C3E938C0C3E938E0E3F0000803F}
          Direction.Coordinates = {2EBDBBB300000000000080BF00000000}
          Position.Coordinates = {EC5138BE0AD723BDCDCCCCBD0000803F}
          TurnAngle = 180.000000000000000000
          Extrusion = 0.009999999776482582
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Used electrode')
          AspectRatio = 0.699999988079071000
          TextHeight = 0.100000001490116100
        end
      end
    end
  end
end
