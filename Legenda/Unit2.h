//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "GLBaseClasses.hpp"
#include "GLCoordinates.hpp"
#include "GLCrossPlatform.hpp"
#include "GLObjects.hpp"
#include "GLScene.hpp"
#include "GLSpaceText.hpp"
#include "GLWin32Viewer.hpp"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TGLSceneViewer *GLSceneViewer1;
	TGLScene *GLScene1;
	TGLLightSource *Luce;
	TGLCamera *Camera;
	TGLDummyCube *Target;
	TGLCube *glcGrezzo;
	TGLSpaceText *GLSpaceText5;
	TGLCube *glcAttrezzato1;
	TGLSpaceText *GLSpaceText1;
	TGLCube *glcPrenotatoDep;
	TGLSpaceText *GLSpaceText2;
	TGLCube *glcPrenotatoPrel;
	TGLSpaceText *GLSpaceText3;
	TGLCube *glcBloccato;
	TGLSpaceText *GLSpaceText4;
	TGLCube *glcScarto;
	TGLSpaceText *GLSpaceText7;
	TGLSpaceText *GLSpaceText6;
	TGLSpaceText *GLSpaceText8;
	TGLSpaceText *GLSpaceText9;
	TGLSpaceText *GLSpaceText10;
	TGLSpaceText *GLSpaceText11;
	TGLSpaceText *GLSpaceText12;
	TGLCube *glcLavorato1;
	TGLSpaceText *GLSpaceText13;
	TGLCube *glcAttrezzato2;
	TGLSpaceText *GLSpaceText14;
	TGLCube *glcLavorato2;
	TGLSpaceText *GLSpaceText15;
	TGLCube *glcNoLotto;
	TGLSpaceText *GLSpaceText16;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
