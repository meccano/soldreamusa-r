//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "BaseClasses.hpp"
#include "GLCoordinates.hpp"
#include "GLCrossPlatform.hpp"
#include "GLObjects.hpp"
#include "GLScene.hpp"
#include "GLSpaceText.hpp"
#include "GLWin32Viewer.hpp"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TGLSceneViewer *GLSceneViewer1;
	TGLScene *GLScene1;
	TGLLightSource *Luce;
	TGLCamera *Camera;
	TGLDummyCube *Target;
	TGLCube *GLCube5;
	TGLSpaceText *GLSpaceText5;
	TGLCube *GLCube1;
	TGLSpaceText *GLSpaceText1;
	TGLCube *GLCube6;
	TGLSpaceText *GLSpaceText6;
	TGLCube *GLCube2;
	TGLSpaceText *GLSpaceText2;
	TGLCube *GLCube3;
	TGLSpaceText *GLSpaceText3;
	TGLCube *GLCube4;
	TGLSpaceText *GLSpaceText4;
	TGLCube *GLCube7;
	TGLSpaceText *GLSpaceText7;
	TGLCube *GLCube8;
	TGLSpaceText *GLSpaceText8;
	TGLCube *GLCube9;
	TGLSpaceText *GLSpaceText9;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
