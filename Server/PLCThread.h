//---------------------------------------------------------------------------

#ifndef PLCThreadH
#define PLCThreadH
//---------------------------------------------------------------------------
#define BCCWIN
#define LITTLE_ENDIAN
#include <System.Classes.hpp>
#include <System.SyncObjs.hpp>
#include <time.h>
#include "nodave.h"
#include "openSocket.h"
#include "PLCFunct.h"
#include "DB.H"
#include <vector>
//---------------------------------------------------------------------------

#define	PLCPort             102
#define	PLCSlot             0
#define	PLCRack             0
#define	MPILocal            1
#define	MPIRemote			2
//---------------------------------------------------------------------------

#define	WR_BYTE  			1
#define	WR_WORD             2
#define	WR_DWORD            3
#define	SET_BIT             4
#define	RST_BIT             5
#define	WR_BLOCK            6
//---------------------------------------------------------------------------

#pragma pack(1)

struct PLCWriteCom {
	int db;
	int add;
	int n;
	int cmd;
	char unsigned data[512];
};

class TPLCThread : public TThread
{            
private:
protected:
	void __fastcall Execute();
public:
	__fastcall TPLCThread(bool CreateSuspended = false);
	void InizializzaPLC();
	void ConnettiPLC();
   	void DisconnettiPLC();
	void StopThread();
	void WriteData(int db, int add, int N, int cmd, unsigned char *data = NULL);
	void WriteByte(int db, int add, unsigned char value);
	void WriteWord(int db, int add, int value);
	void WriteDWord(int db, int add, long int value);
	void WriteBlock(int db, int add, int lenght, unsigned char *value);
	void SetBit(int db, int add, int bit);
	void ResetBit(int db, int add, int bit);
	void WriteTimer(int db, int add, double value);
	unsigned char* DataBlock(int db);
	void LeggiTestiAllarmi();
	int NextActiveAlarm(int actall);
	void LeggiTestiAllarmi(AnsiString FileName);
	void CaricaTestiAllarmi(int lang);
	void AggiornaAllarmi(int db, int dw);
    void EraseMission();
    void InviaMissione(TMissione m);
	void __fastcall SegnalaAllarme();
	void __fastcall SegnalaWarning();
	void __fastcall AcquisisciSegnalazione();
    void __fastcall LogMissione();
	bool initialized, connected;
	unsigned char DB[255][255];
	daveInterface *di;
	daveConnection *dc;
	_daveOSserialType fds;
	bool Stopped;
	AnsiString PLCAddress;
	std::vector<PLCWriteCom> AsyncCom;
	int tentativi;
	int PLCError;
	bool firstscan;
    bool m_SetupPageActive;
	AnsiString alltxt[512];
	bool allbak[512];
	int SYNCHROnall;
    int m_AlarmNum;
	clock_t t1, t2;
	AnsiString CycleTime;
    TMissione m_MissionRobot;
};
//----------------------------------------------------------------------------
extern TPLCThread *PLCThread;
//----------------------------------------------------------------------------
#pragma	pack()
#endif
 
