﻿//---------------------------------------------------------------------------
#pragma hdrstop

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <System.Syncobjs.hpp>
#include <Math.hpp>
#include <inifiles.hpp>
#include "madExcept.hpp"
#include "DB.h"
#include "Fanuc.h"
#include "LogTxt.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
/*
R:  "R,a"
DI: "Q1,a"    parte da bit a per taglen bits
DO: "I1,a"    parte da bit a per taglen bits
RI: "Q1,a+5000,n"
RO: "Ix,a+5000,n"
SI: "Qx,a+7000,n"
SO: "Ix,a+7000,n"
*/
TFanuc *Fanuc[N_ROBOTS];
//---------------------------------------------------------------------------

__fastcall TFanuc::TFanuc(int n)
	: TThread(true)
{
	ID = n;
	NameThreadForDebugging("FanucThread"  + IntToStr(ID), ThreadID);

	if (RegisterIdCommBase("DG0-109-LF3") == false) {
		IdCommGetError(&err1, &err2);
		//…segnalazione dei codici di errore err1 e err2…
		Synchronize(SegnalaErroreRegistrazioneIdComm);
	}

	if (RegisterIdCommProtocol("DG0-109-LF3-09120", 57) == false) {
		IdCommGetError(&err1, &err2);
		//…segnalazione dei codici di errore err1 e err2…
		Synchronize(SegnalaErroreRegistrazioneProtocolloFanuc);
	}

    //Inizializzazione variabili
	Init();

	//Carico da INI le aree di memoria
    LoadIniFile();
}
//---------------------------------------------------------------------------

void TFanuc::Connect() {
	AnsiString devicestr;
    AnsiString sTcpIp, sGrp;
	UINT i;

    sTcpIp.sprintf("TCPIP000%d", ID + 1);
    sGrp.sprintf("GRP%d", ID + 1);
	if (localport == RobotPort[ID]) {
		localport = RobotPort[ID] + 1;
	} else {
		localport = RobotPort[ID];
	}

    if (m_pTag) {
    	delete [] m_pTag;    
        m_pTag = NULL;
    }
    m_pTag = new PTAG[m_Taglist.size()];

    localport += ID * 1000;
	devicestr = RobotAddress[ID] + "," + IntToStr(RobotPort[ID]) + "," + IntToStr(localport);
	comport = DeviceOn(sTcpIp.c_str(), sGrp.c_str());
	if (comport == NULL) {
		IdCommGetError(&err1, &err2);
		LogTxt("DeviceOn[%d] fallita: err1 = %d, err2 = %d", ID, err1, err2);
		// …segnalazione dei codici di errore err1 e err2…
		if ((err1 == 400) && (err2 == 2)) {
			// Libreria IDCOMM non registrata, la registro
			RegisterIdCommBase("DG0-109-LF3");
			RegisterIdCommProtocol("DG0-109-LF3-09120", 57);
		}
		return;
	}

	if (DeviceInit(comport, devicestr.c_str(), "100,500") == false) {
		IdCommGetError(&err1, &err2);
		LogTxt("DeviceInit[%d] fallita: err1 = %d, err2 = %d", ID, err1, err2);
		// …segnalazione dei codici di errore err1 e err2…
		IdCommCloseAll();
		return;
	}

	plc = IdOn("IDC25_SNPX", 8, 0, comport);
	if (plc == NULL) {
		IdCommGetError(&err1, &err2);
		LogTxt("IdOn[%d] fallita: err1 = %d, err2 = %d", ID, err1, err2);
		// segnalazione dei codici di errore err1 e err2…
		IdCommCloseAll();
		return;
	}

	// Importante impostare al valore presente sul Robot
	if (IdInit(plc, WAITFORRESUME, " *,5000", 1) == FALSE) {
		IdCommGetError(&err1, &err2);
		LogTxt("IdInit[%d] fallita: err1 = %d, err2 = %d", ID, err1, err2);
		// …segnalazione dei codici di errore err1 e err2…
		IdCommCloseAll();
		return;
	}

	for(i = 0; i < m_Taglist.size(); i++) {
		m_pTag[i] = TagOn(plc, (char*)m_Taglist[i].c_str(), (USHORT)atoi(m_Taglen[i].c_str()), 0, 50, NULL);
		if (m_pTag[i] == NULL) {
			IdCommGetError(&err1, &err2);
			LogTxt("TagON[%d] fallita: err1 = %d, err2 = %d", ID, err1, err2);
			// …segnalazione dei codici di errore err1 e err2…
			IdCommCloseAll();
			return;
		}
	}
	LogTxt("ROBOT [%d] CONNESSO", ID);
	Connected = true;
}
//---------------------------------------------------------------------------

void TFanuc::Disconnect() {
	LogTxt("ROBOT[%d] DISCONNESSO", ID);
	IdCommCloseAll();
	Connected = false;
    if (m_pTag) {
    	delete [] m_pTag;
        m_pTag = NULL;
    }

	Sleep(1000);
}
//---------------------------------------------------------------------------

void TFanuc::StopThread() {
	TInterlocked::Exchange(Stopped, 1);
	/* Wait for this thread to finish executing */
	this->WaitFor();
}
//---------------------------------------------------------------------------

void TFanuc::Init() {
	err1 = 0;
	err2 = 0;
    step = 0;
	Connected = false;
	TxQueue.clear();
	localport = 0;
	Stopped = false;
    m_pTag = NULL;
    m_Taglist.clear();
    m_Taglen.clear();
}
//---------------------------------------------------------------------------

void TFanuc::LoadIniFile()
{
	TIniFile *pIni;
    AnsiString s;
	std::string line, subline;
    std::size_t found;
    std::vector<std::string> Tag;

	pIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));

    //Indirizzo IP di comunicazione del robot
	RobotAddress[ID] = pIni->ReadString("ROBOT" + IntToStr(ID + 1), "RobotAddress", "127.0.0.1");

    //Porta di comunicazione del robot
    RobotPort[ID] = pIni->ReadInteger("ROBOT" + IntToStr(ID + 1), "RobotPort", 18245);

    //Ricavo le aree da andare a leggere
    s = pIni->ReadString("ROBOT" + IntToStr(ID + 1), "Tag", "0");
    line = s.c_str();
	split(Tag, line, boost::is_any_of("-"));

    for (UINT i = 0; i < Tag.size(); i++) {
    	line = Tag[i];
        found = line.find("[");
        if (found != std::string::npos) {
        	//Ricavo la variabile da leggere/scrivere
        	subline = line.substr(0, found);
			m_Taglist.push_back(subline);

            //Ricavo la dimensione dell'area di memoria da andare a leggere/scrivere
            found++;
        	subline = line.substr(found, (line.length() - 1) - found);
			m_Taglen.push_back(subline);
        }
    }

	delete pIni;
}
//---------------------------------------------------------------------------

bool TFanuc::ReadTagSnpx(UINT iTag)
{
    SHORT w[MAXLEN];

    //Resetto l'array in modo da non avere dei valori incongruenti
    memset(&w, -1, sizeof(w));

    if (ReadTag(m_pTag[iTag], &w) == FALSE) {
        IdCommGetError(&err1, &err2);
        LogTxt("ReadTag %d fallita: ID = %d, err1 = %d, err2 = %d", iTag, ID, err1, err2);
        //Attendo nMillisecondi per poter dare modo alla libreria di ripartire
        Sleep(WAITFORRESUME * 1000 + 250);
        return false;
    }

    //Copio la struttura appena letta
    return (memcpy_s(&m_wReadData, sizeof(m_wReadData), &w, sizeof(w)) == 0);
}
//---------------------------------------------------------------------------

void __fastcall TFanuc::Execute()
{
	int j, bit;
	time_t t;
	SHORT val;

	TxQueue.clear();
	while (!Stopped) {
		Sleep(35);
		try {
			if (!Connected) {
				Connect();
			}
            else if	(TxQueue.empty()) {
				switch (step) {
				case TAGR_R1_14:
					if (ReadTagSnpx(step)) {
						for (j = 1; j <= atoi(m_Taglen[TAGR_R1_14].c_str()); j++) {
							R[j] = m_wReadData[j - 1];
						}
					}
					step++;
					break;
				case TAGR_R100_120:   // sinottico uscite robot
					if (ReadTagSnpx(step)) {
						for (j = 100; j < (atoi(m_Taglen[TAGR_R100_120].c_str()) + 100); j++) {
							R[j] = m_wReadData[j - 100];

							// Se cade l'automatico metto override a 10
                            if (!RobotInAuto()) {
								val = 10;
                                WriteShort(TAGW_OVERRIDE, &val);
                            }
						}
					}
					step++;
					break;
                default:
					step = 0;
					t2 = clock();
					t = t2 - t1;
					t1 = t2;
					CycleTime.printf("%d ms", t);
                    break;
                }
            }
            else { 
                switch (TxQueue[0].command) {
               	case COM_WRITE_INT:
					if ((WriteTag(TxQueue[0].Tag, TxQueue[0].TagShort) == FALSE) && (TxQueue[0].tentativi > 0)) {
						//…segnalazione dei codici di errore err1 e err2…
						IdCommGetError(&err1, &err2);
						SetFunctionError((float)(TxQueue[0].command), 0);
						LogTxt("WriteShort fallita: err1 = %d, err2 = %d", err1, err2);
						//Attendo nMillisecondi per poter dare modo alla libreria di ripartire
						Sleep(WAITFORRESUME * 1000 + 250);
                        continue;
					}
					TxQueue.pop_front();
					break;
               	case COM_WRITE_PULSE:
                	//Alzo il registro a 1
					if ((WriteTag(TxQueue[0].Tag, TxQueue[0].TagShort) == FALSE) && (TxQueue[0].tentativi > 0)) {
						//…segnalazione dei codici di errore err1 e err2…
						IdCommGetError(&err1, &err2);
						SetFunctionError((float)(TxQueue[0].command), 0);
						LogTxt("WriteShort fallita: err1 = %d, err2 = %d", err1, err2);
						//Attendo nMillisecondi per poter dare modo alla libreria di ripartire
						Sleep(WAITFORRESUME * 1000 + 250);
                        continue;
					}

                    //Attendo i millesecondi preimpostati
                    Sleep(TxQueue[0].Wait);

                    //Resetto il registro
                    val = 0;
					if ((WriteTag(TxQueue[0].Tag, &val) == FALSE) && (TxQueue[0].tentativi > 0)) {
						//…segnalazione dei codici di errore err1 e err2…
						IdCommGetError(&err1, &err2);
						SetFunctionError((float)(TxQueue[0].command), 0);
						LogTxt("WriteShort fallita: err1 = %d, err2 = %d", err1, err2);
						//Attendo nMillisecondi per poter dare modo alla libreria di ripartire
						Sleep(WAITFORRESUME * 1000 + 250);
						continue;
					}
                    TxQueue.pop_front();
					break;
				case COM_WRITE_REAL:
					if ((WriteTag(TxQueue[0].Tag, TxQueue[0].TagFloat) == FALSE) && (TxQueue[0].tentativi > 0)) {
						//…segnalazione dei codici di errore err1 e err2…
						IdCommGetError(&err1, &err2);
						SetFunctionError((float)(TxQueue[0].command), 0);
						LogTxt("WriteReal fallita: err1 = %d, err2 = %d", err1, err2);
						//Attendo nMillisecondi per poter dare modo alla libreria di ripartire
						Sleep(WAITFORRESUME * 1000 + 250);
						continue;
					}
					TxQueue.pop_front();
					break;
                }
            }
		}
        catch (...) {
			HandleException();
		}
	}
	Disconnect();
}
//---------------------------------------------------------------------------

bool TFanuc::WritePulse(int TagNumber, int Wait)
{
	RbtWriteCom c;
	int i, len;

    len = atoi(m_Taglen[TagNumber].c_str());
    //Controllo che la dimensione non superi il valore massimo
    if (len > MAXLEN) {
        return false;
    }

    memset(&c, 0, sizeof(c));

    //Imposto il comando
    c.command = COM_WRITE_PULSE;
    c.Wait = Wait;

	c.Tag = m_pTag[TagNumber];
	for (i = 0; i < len; i++) {
		c.TagShort[i] = 1;
	}

	c.tentativi = 10;
	TxQueue.push_back(c);

    return true;
}
//---------------------------------------------------------------------------

bool TFanuc::WriteShort(int TagNumber, SHORT *TagValues) {
	RbtWriteCom c;
	int i, len;

    len = atoi(m_Taglen[TagNumber].c_str());
    //Controllo che la dimensione non superi il valore massimo
    if (len > MAXLEN) {
        return false;
    }

    memset(&c, 0, sizeof(c));

    //Imposto il comando
    c.command = COM_WRITE_INT;

	c.Tag = m_pTag[TagNumber];
	for (i = 0; i < len; i++) {
		c.TagShort[i] = TagValues[i];
	}
	c.tentativi = 10;
	TxQueue.push_back(c);

    return true;
}
//---------------------------------------------------------------------------

bool TFanuc::WriteReal(int TagNumber, float *TagValues) {
	RbtWriteCom c;
	int i, len;

    len = atoi(m_Taglen[TagNumber].c_str());
    //Controllo che la dimensione non superi il valore massimo
    if (len > MAXLEN) {
        return false;
    }

    memset(&c, 0, sizeof(c));
    //Imposto il comando
    c.command = COM_WRITE_REAL;

	c.Tag = m_pTag[TagNumber];
	for (i = 0; i < len; i++) {
		c.TagFloat[i] = TagValues[i];
	}
	c.tentativi = 10;
	TxQueue.push_back(c);

    return true;
}
//---------------------------------------------------------------------------

void TFanuc::StartCycleMU(int MU)
{
	switch (MU) {
    case 1:
    	WritePulse(TAGW_START_MU1, 1000);
    	break;
    case 2:
    	WritePulse(TAGW_START_MU2, 1000);
    	break;
    case 3:
    	WritePulse(TAGW_START_MU3, 1000);
    	break;
    }
}
//---------------------------------------------------------------------------

void TFanuc::AggiornaDatiMissione(TMissione &m)
{
	int tipomiss, locmag, nmag, nbaia, nmu, posmu;

	//Resetto la struttura
	memset(&m ,0, sizeof(m));

	tipomiss 	 = (int)R[TIPO_MISSIONE];
	locmag 		 = (int)R[LOC_MAGAZZINO];
	nmag 		 = (int)R[N_MAGAZZINO];
	nbaia 		 = (int)R[N_BAIA];
	m.Tipo_pinza = (int)R[TIPO_PINZA];
	m.Strobe	 = (int)R[STROBE];
	m.N_pallet 	 = (int)R[ID_PALLET];
	nmu 		 = (int)R[N_MACCHINA];
	posmu 		 = (int)R[N_POSTAZIONE];

    m.TipoMissioneRBT = tipomiss;
    switch (tipomiss) {
	case TPM_PRELIEVO_MACCHINA:
    	m.Tipo_prelievo = TPMISS_MU;
        m.N_MU_pre = nmu;
        m.Pos_MU_pre = posmu;
    	break;    
	case TPM_PRELIEVO_BAIA:
    	m.Tipo_prelievo = TPMISS_BAIA;
        m.N_staz_operatore_pre = nbaia;
    	break;    
	case TPM_PRELIEVO_MAGAZZINO:
    	m.Tipo_prelievo = TPMISS_MAG;
        m.N_locazione_magazzino_pre = locmag;
        m.N_magazzino_pre = nmag;
    	break;
	case TPM_DEPOSITO_MACCHINA:
    	m.Tipo_deposito = TPMISS_MU;
        m.N_MU_dep = nmu;
        m.Pos_MU_dep = posmu;
    	break;    
	case TPM_DEPOSITO_BAIA:
    	m.Tipo_deposito = TPMISS_BAIA;
        m.N_staz_operatore_dep = nbaia;
    	break;    
	case TPM_DEPOSITO_MAGAZZINO:
    	m.Tipo_deposito = TPMISS_MAG;
        m.N_locazione_magazzino_dep = locmag;
        m.N_magazzino_dep = nmag;
        break;

	}
}
//---------------------------------------------------------------------------

void TFanuc::InviaMissione(TMissione m) {

	switch(m.Tipo_prelievo) {
	case TPMISS_MU:
		SendMission(TPM_PRELIEVO_MACCHINA, 0, 0, 0, m.Tipo_pinza, m.N_MU_pre, m.Pos_MU_pre, m.N_pallet);
		break;
	case TPMISS_BAIA:
		SendMission(TPM_PRELIEVO_BAIA, 0, 0, m.N_staz_operatore_pre, m.Tipo_pinza, 0, m.Pos_staz_operatore_pre, m.N_pallet);
		break;
	case TPMISS_MAG:
		SendMission(TPM_PRELIEVO_MAGAZZINO, m.N_locazione_magazzino_pre, m.N_magazzino_pre, 0, m.Tipo_pinza, m.N_MU_dep, 0, m.N_pallet);
		break;
	case 0:
		switch(m.Tipo_deposito) {
		case TPMISS_MU:
			SendMission(TPM_DEPOSITO_MACCHINA, 0, 0, 0, m.Tipo_pinza, m.N_MU_dep, m.Pos_MU_dep, m.N_pallet);
			break;
		case TPMISS_BAIA:
			SendMission(TPM_DEPOSITO_BAIA, 0, 0, m.N_staz_operatore_dep, m.Tipo_pinza, 0, m.Pos_staz_operatore_dep, m.N_pallet);
			break;
		case TPMISS_MAG:
			SendMission(TPM_DEPOSITO_MAGAZZINO, m.N_locazione_magazzino_dep, m.N_magazzino_dep, 0, m.Tipo_pinza, 0, 0, m.N_pallet);
			break;

		}
		break;
	}
}
//---------------------------------------------------------------------------

void TFanuc::SendMission(int tipomiss, int locmag, int nmag, int nbaia, int tipo_pinza, int nmu, int posmu, int idpallet)
{
	SHORT val[10];

	// Resetto flag extra-altezza alla baia
	memset(val, 0, sizeof(val));
    if (nbaia) {
		WriteShort((nbaia == 1) ? TAGW_EXTRAH_S1 : TAGW_EXTRAH_S2, val);
    }

	val[0] = (SHORT)tipomiss;
	val[1] = (SHORT)locmag;
	val[2] = (SHORT)nmag;
	val[3] = (SHORT)nbaia;
	val[4] = (SHORT)tipo_pinza;
	val[5] = 1;
	val[6] = (SHORT)idpallet;
	val[7] = (SHORT)nmu;
	val[8] = 0;
	if (nmag) {
		// Se missione magazzino al robot non serve in numero postazione macchina, uso R[10] per memorizzarmi la postazione del magazzino
		val[9] = (SHORT)locmag;
	}
    else {
		val[9] = (SHORT)posmu;
	}
	memcpy(R + 1, val, 10 * sizeof(SHORT));
	WriteShort(TAGW_MISSIONE, val);

	memset(&m_MissionRobot, 0, sizeof(TMissioneROBOT));
    m_MissionRobot.Tipo_missione = tipomiss;
    m_MissionRobot.N_locazione_magazzino = locmag;
    m_MissionRobot.N_magazzino = nmag;
    m_MissionRobot.N_staz_Operatore = nbaia;
    m_MissionRobot.Tipo_pinza = tipo_pinza;
    m_MissionRobot.ID = idpallet;
    m_MissionRobot.N_MU = nmu;
    m_MissionRobot.N_staz_MU = posmu;
    Synchronize(LogMissione);
}
//---------------------------------------------------------------------------

void TFanuc::EraseMission()
{
	SHORT val[10];

	memset(val, 0, sizeof(val));
	memcpy(R + 1, val, 10 * sizeof(SHORT));
	WriteShort(TAGW_MISSIONE, val);
/*
	//Resetto flag extra-altezza alla baia
	WriteShort(TAGW_EXTRAH, val);
*/    
}
//---------------------------------------------------------------------------

void __fastcall TFanuc::LogMissione() {
	DBDataModule->LogMissRobot(m_MissionRobot.Tipo_missione,
    					   m_MissionRobot.N_locazione_magazzino,
                           m_MissionRobot.N_magazzino,
                           m_MissionRobot.N_staz_Operatore,
                           m_MissionRobot.Tipo_pinza,
                           m_MissionRobot.N_MU,
                           m_MissionRobot.N_staz_MU,
                           m_MissionRobot.ID);
}
//---------------------------------------------------------------------------

void __fastcall TFanuc::SegnalaErroreRegistrazioneIdComm() {
	DBDataModule->Segnalazione(100, "Impossibile registrare libreria IDCOMM", 1);
}
//---------------------------------------------------------------------------

void __fastcall TFanuc::SegnalaErroreRegistrazioneProtocolloFanuc() {
	DBDataModule->Segnalazione(101, "Impossibile registrare protocollo GE FANUC SNPX", 1);
}
//---------------------------------------------------------------------------

void TFanuc::SetFunctionError(float comando, int errore)
{
	//Valorizzo i dati per comporre la stringa di errore
	FunctionError = comando;
	ErrorNumber = errore;

	//Sincronizzo la scrittura dell'errore a DB
	Synchronize(WriteFunctionError);

    //Scrivo anche sul file di LOG
	LogTxt("FANUC: Comando %d, errore %d", comando, errore);

	//Decremento il numero di retry
	TxQueue[0].tentativi--;

	//Controllo se devo disconnettere la comunicazione
	if (TxQueue[0].tentativi <= 0) {
		Disconnect();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFanuc::WriteFunctionError()
{
	DBDataModule->Log("SERVER", "FANUC", "Function: %g, ErrorNumber: %d", FunctionError, ErrorNumber);
}
//---------------------------------------------------------------------------
