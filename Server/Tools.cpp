//---------------------------------------------------------------------------
// Per le DOOSAN con lettura della tabella utensili da file di testo esportata
// da Overmach il campo "CountLife" � una percentuale di tempo decrementale
// ogni qualvolta l'utensile si utilizza. Per cui gestiscono gi� loro il fatto
// di ricavare il tempo di vita rimasto.
// Per le altre come DMG, MITSUBISHI o leggere direttamente dal controllo FANUC
// si deve gestire invece "MaxLife - ActLife"
//---------------------------------------------------------------------------

#pragma hdrstop

#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Xml.Win.msxmldom.hpp>
#include <IniFiles.hpp>
#include <algorithm>
#include <fstream.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "Tools.h"
#include "DB.h"
#include "LogTxt.h"
#include "util.h"
#include "System.IOUtils.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdmTools *dmTools;
//---------------------------------------------------------------------------

#define VITA_RESIDUA(max, act) ((max > act) ? (max - act) : 0)
//---------------------------------------------------------------------------

__fastcall TdmTools::TdmTools(TComponent* Owner)
	: TDataModule(Owner)
{
	TIniFile *ini;
    int i;

    //Resetto la struttura
    memset(&m_ToolsTabMU, 0, sizeof(ToolsTabMU)*N_MACCHINE_UT);

    //Inizializzo il file INI
	ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));

    //Ciclo su tutte le macchine utensili
    for (i = 0; i < N_MACCHINE_UT; i++) {
        m_ToolsTabMU[i].TypeMU = ini->ReadInteger("MU" + IntToStr(i + 1), "TypeMU", 0);
        m_ToolsTabMU[i].TableName = ini->ReadString("MU" + IntToStr(i + 1), "NameTabUtensili", "Utensili.txt");
        m_ToolsTabMU[i].TablePath = ini->ReadString("MU" + IntToStr(i + 1), "PathTabUtensili", "C:\\Meccano\\TabUtensili\\");
    }
	delete ini;
}
//---------------------------------------------------------------------------

int TdmTools::ElencoUtensili(AnsiString PartProgram, std::vector<int>*utensili)
{
	FILE *f;
	char s[256];
	AnsiString field;
	int res, val, i;

	res = -2;
	try {
		f = fopen(PartProgram.c_str(), "r");
		if (f) {
			field = "";
			i = 0;
			utensili->clear();
			while (fgets(s, 256, f)) {
				field = Parser("T", s, "M6");
				if (field.Length() > 0) {
					val = field.Trim().ToIntDef(0);
					if (val > 0) {
						// Selezione utensile Txxxx
						utensili->push_back(val);
					}
				}
			}
			fclose(f);
			res = 0;
		}
	} catch (...) {
		res = -1;
	}
	return res;
}
//---------------------------------------------------------------------------

int TdmTools::GetIndex(tool *pToolsTab, int id)
{
	if (IsGroup(id)) {
    	return GetGroupIndex(pToolsTab, id);
    }
    else {
        return GetToolIndex(pToolsTab, id);
    }
}
//---------------------------------------------------------------------------

int TdmTools::GetToolIndex(tool *pToolsTab, int idutensile) {
	int i, res = -1;

	//Scorro tutti gli utensili
	for(i = 0; (i <= N_TOOLS) && (res < 0); i++) {
		// Verifico se � l'utensile che cerco
		if (pToolsTab[i].ToolName == idutensile) {
			res = i;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

int TdmTools::GetGroupIndex(tool *pToolsTab, int idutensile)
{
	int i, res = -1;

	//Scorro tutti gli utensili
	for(i = 0; (i <= N_TOOLS) && (res < 0); i++) {
		// Verifico se � l'utensile che cerco
		if (pToolsTab[i].ToolGroup == idutensile) {
			res = i;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

int TdmTools::GetLife(tool *pToolsTab, int id)
{
	if (IsGroup(id)) {
        return GetGroupLife(pToolsTab, id);
    }
    else {
    	return GetToolLife(pToolsTab, id);
    }
}
//---------------------------------------------------------------------------

int TdmTools::GetToolLife(tool *pToolsTab, int Tool)
{
	int i, VitaResidua = 0;
    bool bFound = false;

    for (i = 0; i <= N_TOOLS && !bFound; i++) {
    	bFound = (pToolsTab[i].ToolName == Tool);
		if (bFound) {
			VitaResidua = VITA_RESIDUA(pToolsTab[i].MaxLife, pToolsTab[i].ActLife);
		}
	}
	return VitaResidua;
}
//---------------------------------------------------------------------------

int TdmTools::GetGroupLife(tool *pToolsTab, int Gruppo)
{
	int i, VitaResiduaTot = 0;

	for (i = 0; i <= N_TOOLS; i++) {
		if (pToolsTab[i].ToolGroup == Gruppo) {
			VitaResiduaTot += VITA_RESIDUA(pToolsTab[i].MaxLife, pToolsTab[i].ActLife);
		}
	}
	return VitaResiduaTot;
}
//---------------------------------------------------------------------------

int TdmTools::GetActLife(tool *pToolsTab, int id)
{
	if (IsGroup(id)) {
        return GetActLifeGroup(pToolsTab, id);
    }
    else {
    	return GetActLifeTool(pToolsTab, id);
    }
}
//---------------------------------------------------------------------------

int TdmTools::GetActLifeTool(tool *pToolsTab, int Tool)
{
	int i, VitaResidua = 0;
    bool bFound = false;

    for (i = 0; i <= N_TOOLS && !bFound; i++) {
    	bFound = (pToolsTab[i].ToolName == Tool);
		if (bFound) {
			VitaResidua = pToolsTab[i].ActLife;
		}
	}
	return VitaResidua;
}
//---------------------------------------------------------------------------

int TdmTools::GetActLifeGroup(tool *pToolsTab, int Gruppo)
{
	int VitaResiduaTot = 0;
	int i;

	for (i = 0; i <= N_TOOLS; i++) {
		if (pToolsTab[i].ToolGroup == Gruppo) {
			VitaResiduaTot += pToolsTab[i].ActLife;
		}
	}
	return VitaResiduaTot;
}
//---------------------------------------------------------------------------

int TdmTools::GetCountLife(tool *pToolsTab, int id)
{
	if (IsGroup(id)) {
        return GetCountLifeGroup(pToolsTab, id);
    }
    else {
    	return GetCountLifeTool(pToolsTab, id);
    }
}
//---------------------------------------------------------------------------

int TdmTools::GetCountLifeTool(tool *pToolsTab, int Tool)
{
	int i, VitaResidua = 0;
    bool bFound = false;

    for (i = 0; i <= N_TOOLS && !bFound; i++) {
    	bFound = (pToolsTab[i].ToolName == Tool);
		if (bFound) {
			VitaResidua = pToolsTab[i].CountLife;
		}
	}
	return VitaResidua;
}
//---------------------------------------------------------------------------

int TdmTools::GetCountLifeGroup(tool *pToolsTab, int Gruppo)
{
	int VitaResiduaTot = 0;
	int i;

	for (i = 0; i <= N_TOOLS; i++) {
		if (pToolsTab[i].ToolGroup == Gruppo) {
			VitaResiduaTot += pToolsTab[i].CountLife;
		}
	}
	return VitaResiduaTot;
}
//---------------------------------------------------------------------------

bool TdmTools::Exists(tool *pToolsTab, int id)
{
	if (IsGroup(id)) {
    	return GroupExists(pToolsTab, id);
    }
    else {
        return ToolExists(pToolsTab, id);
    }
}
//---------------------------------------------------------------------------

bool TdmTools::GroupExists(tool *pToolsTab, int Gruppo)
{
	bool bToolPresent = false;
	int i;

	for (i = 0; i <= N_TOOLS && !bToolPresent; i++) {
		bToolPresent = ((pToolsTab[i].ToolGroup == Gruppo) && (pToolsTab[i].Broken == 0));
	}
	return bToolPresent;
}
//---------------------------------------------------------------------------

bool TdmTools::ToolExists(tool *pToolsTab, int Utensile)
{
	bool bToolPresent = false;
	int i;

	for (i = 0; i <= N_TOOLS && !bToolPresent; i++) {
		bToolPresent = ((pToolsTab[i].ToolName == Utensile) && (pToolsTab[i].Broken == 0));
	}
	return bToolPresent;
}
//---------------------------------------------------------------------------

bool TdmTools::IsGroup(int Utensile)
{
	return (Utensile >= MAX_NUM_TOOLNAME);
}
//---------------------------------------------------------------------------

int TdmTools::VerificaUtensili(int MU, AnsiString Programma, AnsiString &commento)
{
	std::vector<int> utensili;
	int nprog, VitaTot;

	nprog = NumeroProgramma(Programma);

	//Carico la lista degli utensili utilizzati per effettuare il programma
	if (ElencoUtensili(Programma, &utensili) != 0) {
    	commento = "Errore durante lettura programma di lavoro";
		return -1;
	}

	for(UINT i = 0; i < utensili.size(); i++) {
        //Controllo che l'utensile/gruppo nel partprogram sia pres
        if (!Exists(m_ToolsTabMU[MU - 1].TableTools, utensili[i])) {
        	commento.sprintf("Utensile %d non trovato o rotto", utensili[i]);
            return -2;
        }
        else {
            VitaTot = GetCountLife(m_ToolsTabMU[MU - 1].TableTools, utensili[i]);
            if ((VitaTot <= 0) || !DBDataModule->ToolPresentInTableLife(i, VitaTot, nprog)) {
				commento.sprintf("Utensile %d vita insufficiente", utensili[i]);
				return -3;
            }
        }
	}

    return 0;
}
//---------------------------------------------------------------------------

void TdmTools::AggiornaViteUtensili(int MU, AnsiString Programma)
{
	std::vector<int>utensili;
	int nprog;

	nprog = NumeroProgramma(Programma);

	//Carico la lista degli utensili utilizzati per effettuare il programma
	if (ElencoUtensili(Programma, &utensili) == -2) {
		DBDataModule->Log("SERVER", "TOOLS", "AggiornaViteUtensili: Programma di lavorazione %s non trovato", Programma.c_str());
		return;
	}

	for(UINT i = 0; i < utensili.size(); i++) {
		//Cerco l'indice in tabella utensili
		if (GetIndex(m_ToolsTabMU[MU - 1].TableTools, utensili[i]) >= 0) {
			// Se trovato in tabella aggiorno
			DBDataModule->MemorizzaVitaUtensile(utensili[i], GetCountLife(m_ToolsTabMU[MU - 1].TableTools, utensili[i]), nprog);
		}
	}
}
//---------------------------------------------------------------------------

void TdmTools::AggiornaUsoUtensili(int MU, AnsiString Programma)
{
	std::vector<int>utensili;
	int nprog;

	nprog = NumeroProgramma(Programma);

	//Carico la lista degli utensili utilizzati per effettuare il programma
	if (ElencoUtensili(Programma, &utensili) == -2) {
		DBDataModule->Log("SERVER", "TOOLS", "AggiornaUsoUtensili: Programma di lavorazione %s non trovato", Programma.c_str());
		return;
	}

	for(UINT i = 0; i < utensili.size(); i++) {
		//Cerco l'indice in tabella utensili
		if (GetIndex(m_ToolsTabMU[MU - 1].TableTools, utensili[i]) >= 0) {
			// Se trovato in tabella aggiorno
			DBDataModule->RicalcolaUsoUtensile(utensili[i], GetCountLife(m_ToolsTabMU[MU - 1].TableTools, utensili[i]), nprog);
		}
	}
}
//---------------------------------------------------------------------------

void TdmTools::LogUtensiliMancanti(tool *pToolsTab, AnsiString PartProgram)
{
	UINT i;
	AnsiString tool;
	TStringList *ListMissedTools = new TStringList;
	std::vector<int> utensili;

	//Leggo tutti gli utensili contenuti nel programma
	if (ElencoUtensili(PartProgram, &utensili) == -2) {
		return;
	}

	//Verifica presenza utensle richiesto in tabella utensile
	for(i = 0; i < utensili.size(); i++) {
		if (!Exists(pToolsTab, utensili[i])) {
			tool.sprintf("%d, ", utensili[i]);
			ListMissedTools->Add(tool);
		}
	}

	//Compongo il file per la lista degli utensili assenti
	if (ListMissedTools->Count > 0) {
		ListMissedTools->SaveToFile(ExtractFilePath(PartProgram) + "ListaUtensili.csv");
	}
	delete ListMissedTools;
}
//---------------------------------------------------------------------------

void __fastcall TdmTools::TimerReadToolsTimer(TObject *Sender)
{
	int i;
    AnsiString NomeFile, NomeFileNew;

	TimerReadTools->Enabled = false;
    for (i = 0; i < N_MACCHINE_UT; i++) {
    	//Controllo che il percorso abbia il delimitatore finale
        if (!m_ToolsTabMU[i].TablePath.IsPathDelimiter(m_ToolsTabMU[i].TablePath.Length())) {
        	m_ToolsTabMU[i].TablePath += "\\";
        }
        //Nome del file da caricare
        NomeFile = m_ToolsTabMU[i].TablePath + m_ToolsTabMU[i].TableName;
        NomeFileNew = m_ToolsTabMU[i].TablePath + TPath::GetFileNameWithoutExtension(NomeFile) + ".new";

        //Controllo che il file esista e il controllo abilitato
        if (FileExists(NomeFile) && (DBDataModule->LeggiAbilitazioneVerificaUtensili(i + 1))) {
            //Verifico l'esistenza del vecchio file
            if (FileExists(NomeFileNew)) {
                DeleteFile(NomeFileNew);
            }

            //Rinomino il file in .OLD
            if (RenameFile(NomeFile, ChangeFileExt(NomeFile, ".new"))) {
            	if (m_ToolsTabMU[i].TypeMU == TAB_TOOL_TYPE_DOOSAN) {
                    //Leggo la tabella utensili in base alla formattazione del file
                    LeggiTabellaUtensiliOvermach(m_ToolsTabMU[i].TableTools, NomeFileNew);
                }
            	if (m_ToolsTabMU[i].TypeMU == TAB_TOOL_TYPE_MORISEIKI) {
                    //Leggo la tabella utensili in base alla formattazione del file
                    LeggiTabellaUtensiliMori(m_ToolsTabMU[i].TableTools, NomeFileNew);
                }
            }
        }
    }
	TimerReadTools->Enabled = true;	
}
//---------------------------------------------------------------------------

void TdmTools::LeggiTabellaUtensiliOvermach(tool *pToolsTab, AnsiString FileName)
{
	UINT j;
	int pos;
	ifstream f;
	char str[_MAX_PATH];
	std::vector<std::string> vectline;
 	IndexList ToolsList;

    //Resetto la struttura
    memset(pToolsTab, 0, sizeof(pToolsTab));

    //Eseguo la lettura della tabella utensili
    f.open(FileName.c_str(), std::ifstream::in);

    //Resetto l'array
    ToolsList.clear();

    // Lettura dati
    while (!f.eof()) {
        //Resetto il vettore
        vectline.clear();

        //Lo resetto solo per debug della stringa
        memset(str, 0, _MAX_PATH);

        // Leggo la stringa dei valori degli utensili
        f.getline(str, _MAX_PATH);

        //Controllo che non abbiano lasciato uno spazio tra una riga
        //e la successiva oppure alla fine del file
        if (str[0] != NULL) {
            //Trovo i campi del file con attraverso il token
            split(vectline, str, boost::is_any_of("\t"));
            ToolsList.push_back(vectline);
        }
    }
    f.close();

    for (j = 0; j < ToolsList.size(); j++) {
        //CAMPO 4: POS
        pos = atoi(ToolsList[j][3].c_str());
        if ((pos >= 0) && (pos < N_TOOLS + 1)) {
            //CAMPO 4: numero bicchiere
            pToolsTab[pos].PotNumber = pos;

            //CAMPO 1: Numero utensile
            pToolsTab[pos].ToolName = atoi(ToolsList[j][0].c_str());

            // CAMPO 2: gruppo
            pToolsTab[pos].ToolGroup = atoi(ToolsList[j][1].c_str());

            // CAMPO 3: conteggio decrementale percentuale vita
            pToolsTab[pos].CountLife = atoi(ToolsList[j][2].c_str());

            // CAMPO 6: H
            pToolsTab[pos].Lunghezza = atoi(ToolsList[j][5].c_str());

            // CAMPO 7: D
            pToolsTab[pos].Raggio = atoi(ToolsList[j][6].c_str());

            // CAMPO 12: Available
            pToolsTab[pos].Broken = atoi(ToolsList[j][11].c_str());
        }
    }
}
//---------------------------------------------------------------------------

void TdmTools::LeggiTabellaUtensiliMori(tool *pToolsTab, AnsiString FileName)
{
	TXMLDocument *xmlDoc;
	IXMLNode *Root;
	IXMLNodeList *Nodi, *SottoNodi, *SottoSottoNodi;
	int i, j, z, val, gruppo, idutensile;
	AnsiString strval;

	// Questo serve perch� i numeri float nell'xml hanno usano il punto mentre nella localizzazione italiana
	// si usa la virgola, quindi darebbe errore di conversione ( usando la StrToFloatDef(xxx, 0) da 0
	FormatSettings.DecimalSeparator = '.';
    //Resetto la struttura
    memset(pToolsTab, 0, sizeof(pToolsTab));
	// Creo l'oggetto TXMLDocument
	xmlDoc = new TXMLDocument(this);
	// Carico il file nell'oggetto TXMLDocument
	xmlDoc->LoadFromFile(FileName);
	// Ricavo il puntatore al nodo principale
	Root = xmlDoc->DocumentElement;
	// Ricavo la lista di tutti i nodi figli del principale
	Nodi = Root->ChildNodes;
	// Ciclo su tutti i nodi della lista
	for(i = 0; (i < Nodi->Count) && (i <= N_TOOLS); i++) {
    	//Leggo PotNumber che mi identifica il numero del bicchiere
		val = Nodi[0].Get(i)->ChildNodes->FindNode("PotNumber")->GetText().ToIntDef(0);
        pToolsTab[i].PotNumber = val;
		// Leggo campo ToolID
		// � un numero di 8 cifre
		// le prime quattro sono il numero utensile
		// le ultime quattro sono il gruppo
		val = Nodi[0].Get(i)->ChildNodes->FindNode("ToolID")->GetText().ToIntDef(0);
		// Isolo le quattro cifre a SX per ricavare il numero utensile
		idutensile = val / 10000;
		// Isolo le quattro DX cifre per ricavare il numero utensile
		gruppo = val % 10000;
		pToolsTab[i].ToolName = idutensile;
		pToolsTab[i].ToolGroup = gruppo;
		// Mi sono rotto il cazzo di commentare ogni riga!!! Il resto si capisce!!!
		val = Nodi[0].Get(i)->ChildNodes->FindNode("Status")->GetText().ToIntDef(0);
		pToolsTab[i].Broken = ((val & BIT10) || (val & BIT8));
		//TOOLS_TAB[i].Broken = (val == 1096);
		// Vabb�, questo non si capirebbe...
		// Alcuni dei dati non sono contenuti nmei nodi figli del principale,
		// ma in nodi figli dei figli del principale
		// Quindi faccio un ciclo nel nodo corrente per scorrere tutti i nodi figli
		SottoNodi = Nodi[0].Get(i)->ChildNodes;
		for(j = 0; j < SottoNodi->Count; j++) {
			if (SottoNodi[0].Get(j)->NodeName == "OffsetInfoMain") {
				// OffsetInfoMain/LifeSettingVal contiene la vita max
				val = SottoNodi[0].Get(j)->ChildNodes->FindNode("LifeSettingVal")->GetText().ToIntDef(0);
				pToolsTab[i].MaxLife = val;
				// OffsetInfoMain/LifeCurrentVal contiene la vita attuale
				val = (SottoNodi[0].Get(j)->ChildNodes->FindNode("LifeCurrentVal")->GetText().ToIntDef(0)) / 60;
				pToolsTab[i].ActLife = val;
				// Ricavo la vita residua
				pToolsTab[i].CountLife = pToolsTab[i].MaxLife - pToolsTab[i].ActLife;
				//Ricalcolo il broken
				if (pToolsTab[i].CountLife <= 0) {
					pToolsTab[i].Broken = 1;
				}
				// A questo punto scorro i nodi figli per ricercare i nodi OffsetInfo con
				// attributo SHAPE uguale a X e Z
				SottoSottoNodi = SottoNodi[0].Get(j)->ChildNodes;
				for(z = 0; z < SottoSottoNodi->Count; z++) {
					if (SottoSottoNodi[0].Get(z)->NodeName == "OffsetInfo") {
						if (SottoSottoNodi[0].Get(z)->GetAttribute("SHAPE") == "X") {
							// OffsetInfo SHAPE = X contiene la raggio utenzile
							strval = SottoSottoNodi[0].Get(z)->ChildNodes->FindNode("Value")->GetText();
							pToolsTab[i].Raggio = (float)System::Sysutils::StrToFloatDef(strval, 0);
						}
                        else if (SottoSottoNodi[0].Get(z)->GetAttribute("SHAPE") == "Z") {
							// OffsetInfo SHAPE = Z contiene il lunghezza utensile
							strval = SottoSottoNodi[0].Get(z)->ChildNodes->FindNode("Value")->GetText();
							pToolsTab[i].Lunghezza = (float)System::Sysutils::StrToFloatDef(strval, 0);
						}
					}
				}
			}
            else if (SottoNodi[0].Get(j)->NodeName == "UserArea") {
				if (SottoNodi[0].Get(j)->GetAttribute("NUMBER") == "1") {
					// UserArea NUMBER = 1 contiene il correttore lunghezza
					strval = SottoNodi[0].Get(j)->ChildNodes->FindNode("Value")->GetText();
					pToolsTab[i].CorrLength = (float)System::Sysutils::StrToFloatDef(strval, 0);
				}
                else if (SottoNodi[0].Get(j)->GetAttribute("NUMBER") == "2") {
					// UserArea NUMBER = 2 contiene il correttore raggio
					strval = SottoNodi[0].Get(j)->ChildNodes->FindNode("Value")->GetText();
					pToolsTab[i].CorrRadius = (float)System::Sysutils::StrToFloatDef(strval, 0);
				}
                else if (SottoNodi[0].Get(j)->GetAttribute("NUMBER") == "3") {
					// UserArea NUMBER = 3 contiene il codice kit
					val = SottoNodi[0].Get(j)->ChildNodes->FindNode("Value")->GetText().ToIntDef(0);
					pToolsTab[i].KitCode = val;
				}
			}
		}
	}
	delete xmlDoc;
}
//---------------------------------------------------------------------------

