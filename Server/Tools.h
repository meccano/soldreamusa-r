//---------------------------------------------------------------------------

#ifndef ToolsH
#define ToolsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <vector>
#include "clientdata.h"
//---------------------------------------------------------------------------

//Definizioni del tipo di macchina utensile che mi esporata una tabella utensile diversa
#define	TAB_TOOL_TYPE_MORISEIKI		1
#define	TAB_TOOL_TYPE_DOOSAN		2
#define	TAB_TOOL_TYPE_GMV_MIT		3
//---------------------------------------------------------------------------

struct ToolsTabMU {
	int TypeMU;
    AnsiString TableName;
    AnsiString TablePath;
    tool TableTools[N_TOOLS + 1];
};
//---------------------------------------------------------------------------

typedef std::vector<std::vector<std::string> > IndexList;
//---------------------------------------------------------------------------

class TdmTools : public TDataModule
{
__published:	// IDE-managed Components
	TTimer *TimerReadTools;
	void __fastcall TimerReadToolsTimer(TObject *Sender);
private:	// User declarations
	__fastcall TdmTools(TComponent* Owner);
    int ElencoUtensili(AnsiString PartProgram, std::vector<int>*utensili);

	int GetIndex(tool *pToolsTab, int id);
	int GetToolIndex(tool *pToolsTab, int idutensile);
	int GetGroupIndex(tool *pToolsTab, int idutensile);

	int GetLife(tool *pToolsTab, int id);
	int GetToolLife(tool *pToolsTab, int Tool);
	int GetGroupLife(tool *pToolsTab, int Gruppo);

	int GetActLife(tool *pToolsTab, int id);
	int GetActLifeTool(tool *pToolsTab, int Tool);
	int GetActLifeGroup(tool *pToolsTab, int Gruppo);

	int GetCountLife(tool *pToolsTab, int id);
	int GetCountLifeTool(tool *pToolsTab, int Tool);
	int GetCountLifeGroup(tool *pToolsTab, int Gruppo);

	bool Exists(tool *pToolsTab, int id);
	bool GroupExists(tool *pToolsTab, int Gruppo);
	bool ToolExists(tool *pToolsTab, int Utensile);

	bool IsGroup(int Utensile);

public:		// User declarations
    void LeggiTabellaUtensiliOvermach(tool *pToolsTab, AnsiString FileName);
    void LeggiTabellaUtensiliMori(tool *pToolsTab, AnsiString FileName);
    int VerificaUtensili(int MU, AnsiString Programma, AnsiString &commento);
    void AggiornaViteUtensili(int MU, AnsiString Programma);
    void AggiornaUsoUtensili(int MU, AnsiString Programma);
    void LogUtensiliMancanti(tool *pToolsTab, AnsiString PartProgram);

    ToolsTabMU m_ToolsTabMU[N_MACCHINE_UT];
};
//---------------------------------------------------------------------------
extern PACKAGE TdmTools *dmTools;
//---------------------------------------------------------------------------
#endif

