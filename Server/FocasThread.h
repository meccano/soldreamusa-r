//---------------------------------------------------------------------------

#ifndef FocasThreadH
#define FocasThreadH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#define FS30D;
#include "FwLib32.h"
#include <time.h>
#include <deque>
#include <IdFTP.hpp>
#include <IdAllFTPListParsers.hpp>
#include "define.h"
#include "clientdata.h"
//---------------------------------------------------------------------------
#define BUFSIZE	(8192 * 1024)
#define PROGRAMMA_ROTAZIONE_TAVOLA	9999
//---------------------------------------------------------------------------
enum CommandFocas
{
    COM_WRITED = 1,
    COM_WRITE_PROGRAM3,
    COM_WRITE_PROGRAM4,
    COM_READ_PROGRAM3,
    COM_READ_PROGRAM4,
    COM_WRITE_MACRO,
	COM_SELECT_PROGRAM,
	COM_WRITE_PROGRAM_DS,
    COM_DELETE_ALL_DS,
    COM_WRITER,
    COM_SET_BIT_R,
    COM_RESET_BIT_R,
    COM_PULSE_BIT_R,
    COM_START_PROGRAM,
    COM_FINE_CICLO,
    COM_DELETE_FILE_DS,
    COM_WRITE_MACRO_R,
    COM_DEL_FILE_I30,
    COM_DEL_FILE_0I_15IB,
    COM_WRITE_MACRO_DOUBLE,
	COM_SEND_SETUP,
    COM_FIND_AND_DELETE,
    COM_WRITE_CORRETTORI
};
//---------------------------------------------------------------------------

#define TIMEOUT_START 				600
//---------------------------------------------------------------------------
struct WriteCom {
	int comando;
	int intp[32];
	char stringp[5][256];
    int LapsCounter;
};
//---------------------------------------------------------------------------
class TFocasThread : public TThread
{
private:
	void ReadToolsData(tool *tab);
	bool DL3(int nprog, char *s);
	bool DL4(int nprog, char *s, char *path);
	bool DL_DS(AnsiString SourceFile, AnsiString DestFile);
	bool DelAll_DS();
	bool DelFile_DS(AnsiString filename);
	bool ReadFile(AnsiString FileNameStr, char *buf);
	bool WriteFile(AnsiString FileNameStr, char *buf);
	bool UL3(char *s, int nprog);
	bool UL4(char *s, char *file_name);
	void __fastcall InvioStartMU1();
	IODBPMC d, r;
	IODBMR m;
	ODBALMMSG almmsg;
	char buffer[BUFSIZE];
protected:
	void __fastcall Execute();
public:
	__fastcall TFocasThread(int n, bool CreateSuspended = true);
	__fastcall ~TFocasThread();
	void __fastcall WriteFunctionError();
	void __fastcall ScriviAllarme();
	void __fastcall Ok();
    void __fastcall LogMission();
 	void __fastcall StartCycle();
	void StopThread();
	void Resettone();
	void Connect();
	void Disconnect();
	void SetBitR(int address, int bit);
	void ResetBitR(int address, int bit);
	void PulseBitR(int address, int bit, int wait);
	void WriteD(int address, int value);
	void WriteR(int address, int value);
	void WriteMacro(int address, int value);
	void WriteMacroR(int address, int value);
	void WriteMacroDouble(int address, double value);
	void WriteProgram3(AnsiString filename, int nprogram = -1);
	void WriteProgram4(AnsiString filename, int nprogram = -1, AnsiString CncPath = "");
	void WriteProgram_DS(AnsiString SourceFile, AnsiString DestFile);
    void ReadProgram3(int nprogram, AnsiString PathSalvataggio);
    void ReadProgram4(int nprogram, AnsiString PathSalvataggio, AnsiString CncPath = "");
    void ReadProgram4(AnsiString nprogram, AnsiString PathSalvataggio, AnsiString CncPath = "");
    void ReadProgram4(AnsiString FileName, AnsiString CncPath = "");
	void SelectProgram(int nprogram, int commandstart);
	void StartProgram();
	void InviaFineCiclo();
	bool FindFile_DS(AnsiString sFileNamePRG);
    void FindAndDelete_DS(int nProgram);
	void DeleteAll_DS();
	void DeleteFile_DS(AnsiString filename);
	void ImpostaAbilitaGestioneUtensili(int Value);
	void SendSETUP();
	void ImpostaTABELLA_UTENSILI_LETTA(bool Value);
	bool LeggiTABELLA_UTENSILI_LETTA();
	void CancellaFile30I(int Programma, AnsiString CncPath = "");
	void CancellaFile0I_15IB(int Programma);
    void SetFunctionError(float comando, int errore, AnsiString strlog = "", bool rileggi = true, bool disconnetti = false);
    void SetCorrettori(int Gruppo, int Num_Utensile, int H, int R, int Stato);
	bool CancellaFileDaMacchina(int nProgram);
	float FunctionError;
    int ErrorNumber;
   	AnsiString LogMsg;
	int Stopped;
	bool Connected;
	int ID;
    int TypeCN;
	TCriticalSection *Lock;
	std::deque<WriteCom> TxQueue;
	AnsiString Address;
	int SETUP_Attivo;
    int m_M21;
    int test;
	int Port;
	int Step;
	AnsiString PPFolder;
	AnsiString DataServerAddress;
	int DataServerPort;
	AnsiString DataServerUser;
	AnsiString DataServerPassword;
	unsigned short h;
	clock_t t1, t2, TimeoutStart;
	AnsiString CycleTime;
	ODBST statinfo16i;
	ODBST2 statinfo;
	ODBPRO prg;
	bool forza_rilettura_stato, TABELLA_UTENSILI_LETTA;
	int AbilitaGestioneUtensili;
	int Mancanza_Utensile;
	int Rottura_Utensile;
	int N_Utensile_Rotto;
	int PortaAperta;
	int P1Fuori;
	int P2Fuori;
	int m_bEnabled;
	int FineCicloPartProgram;
	int RobotAbilitato;
};
//---------------------------------------------------------------------------
extern TFocasThread *Focas[N_MACCHINE_UT];

#endif
