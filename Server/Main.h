//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "trayicon.h"
#include <Vcl.ImgList.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.AppEvnts.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdExplicitTLSClientServerBase.hpp>
#include <IdFTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
	TPopupMenu *PopupMenu1;
	TMenuItem *Exit1;
	TMenuItem *Apri1;
	TPanel *Panel1;
	TStaticText *StaticText1;
	TGroupBox *GroupBox1;
	TGroupBox *GroupBox2;
	TMemo *Memo1;
	TPanel *Panel2;
	TBitBtn *BitBtn1;
	TImageList *ImageList1;
	TImage *Image5;
	TTimer *Timer1;
	TApplicationEvents *ApplicationEvents1;
	TImage *Image1;
	TTrayIcon *TrayIcon1;
	TGridPanel *GridPanel1;
	TLabel *lblDB;
	TLabel *lblRobot;
	TLabel *lblPLC;
	TLabel *lblM1;
	TLabel *lblM2;
	TShape *shpDB;
	TShape *shpRobot;
	TShape *shpPLC;
	TShape *shpM1;
	TShape *shpM2;
	TLabel *Label1;
	TLabel *TRobot;
	TLabel *TPLC;
	TLabel *T1;
	TLabel *T2;
	TLabel *Label4;
	TShape *shpM3;
	TLabel *T3;
	void __fastcall Exit1Click(TObject *Sender);
	void __fastcall Apri1Click(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall ApplicationEvents1Minimize(TObject *Sender);
	void __fastcall ApplicationEvents1Restore(TObject *Sender);
	void __fastcall TrayIcon1DblClick(TObject *Sender);
	void __fastcall Image5Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Image1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TMainForm(TComponent* Owner);
	void  AccendiLed(int n, TColor c);
	void  VisTCiclo(int n, AnsiString val);
	bool  Richiesta_chiusura;
	bool  Avvio;
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
