﻿//---------------------------------------------------------------------------

#ifndef FanucH
#define FanucH
//---------------------------------------------------------------------------
#define BORLANDCPP
#include <deque>
#include <vector>
#include "idtypes25.h"
#include "idfunc25.h"
//---------------------------------------------------------------------------

//Definizioni missioni Robot
#define TPM_DEPOSITO_MACCHINA			1
#define TPM_PRELIEVO_MACCHINA			2
#define TPM_PRELIEVO_BAIA				3
#define TPM_DEPOSITO_BAIA				4
#define TPM_DEPOSITO_MAGAZZINO			5
#define TPM_PRELIEVO_MAGAZZINO			6
#define TPM_DEPOSITO_ATTREZZATURA_MU	7
#define TPM_PRELIEVO_ATTREZZATURA_MU	8
#define TPM_PULIZIA						9
#define TPM_HOME						15
//---------------------------------------------------------------------------

//Registri missioni Robot
#define TIPO_MISSIONE					1
#define LOC_MAGAZZINO					2
#define N_MAGAZZINO						3
#define N_BAIA							4
#define TIPO_PINZA						5
#define STROBE							6
#define ID_PALLET						7
#define N_MACCHINA						8
#define NUM_ALLARME                     9    // non gestito
#define N_POSTAZIONE					10
// Override gestito separatamente
#define PINZA_SU_ROBOT					12
#define EXTRA_ALTEZZA					13  // concetto diverso per gestione magazzino e macchina
#define CONTROLLO_DATI_ROB_OK			15  // come MHT, controllo che tutto ok da parte robot per fine missione
//---------------------------------------------------------------------------

#define MAXLEN							50
//---------------------------------------------------------------------------

//Numero intero che indica il numero di secondi che la libreria attende prima di far
//eseguire una nuova operazione di lettura o scrittura sull�Id dopo che si � verificato un errore
#define WAITFORRESUME		1
//---------------------------------------------------------------------------

enum TagNumbers
{
	TAGR_R1_14 = 0,
	TAGR_R100_120,
	TAGW_MISSIONE,
	TAGW_OVERRIDE,
	TAGW_EXTRAH_S1,
	TAGW_EXTRAH_S2,
	TAGW_START_MU1,
	TAGW_START_MU2,
	TAGW_START_MU3
};
//---------------------------------------------------------------------------

enum CommandFanuc
{
    COM_WRITE_INT = 1,
    COM_WRITE_REAL,
    COM_WRITE_PULSE
};
//---------------------------------------------------------------------------

struct RbtWriteCom
{
	int command;
	PTAG Tag;
	SHORT TagShort[MAXLEN];
	float TagFloat[MAXLEN];
	int tentativi;
    int Wait;
};
//---------------------------------------------------------------------------

typedef struct {
	int Tipo_missione;
	int N_locazione_magazzino;
	int N_magazzino;
	int N_staz_Operatore;
	int Tipo_pinza;
	int Strobe;
	int ID;
	int N_MU;
	int Messaggi;
	int N_staz_MU;
} TMissioneROBOT;
//---------------------------------------------------------------------------

class TFanuc : public TThread
{
private:
protected:
	void __fastcall Execute();
public:
	__fastcall TFanuc(int n);
	void Connect();
	void Disconnect();
	void Init();
    bool ReadTagSnpx(UINT iTag);
	bool WriteShort(int TagNumber, SHORT *TagValues);
    bool WriteReal(int TagNumber, float *TagValues);
    bool WritePulse(int TagNumber, int Wait);
	void StopThread();
    void LoadIniFile();
	void SendMission(int tipomiss, int locmag, int nmag, int nbaia, int tipo_pinza, int nmu, int posmu, int idpallet = 0);
	void EraseMission();
    void InviaMissione(TMissione m);
    void AggiornaDatiMissione(TMissione &m);
    void StartCycleMU(int MU);
	void __fastcall SegnalaErroreRegistrazioneIdComm();
	void __fastcall SegnalaErroreRegistrazioneProtocolloFanuc();
	void __fastcall LogMissione();
	void SetFunctionError(float comando, int errore);
	void __fastcall WriteFunctionError();
	std::deque<RbtWriteCom> TxQueue;
    std::vector<std::string> m_Taglist;
    std::vector<std::string> m_Taglen;
    int ID;
    int step;
	int Stopped;
	bool Connected;
	clock_t t1, t2;
	AnsiString CycleTime;
	SHORT err1;
	SHORT err2;
	int localport;
    int RobotPort[N_ROBOTS];
    AnsiString RobotAddress[N_ROBOTS];
	PDEVICE comport;
	PID plc;
    PTAG *m_pTag;
    SHORT m_wReadData[MAXLEN];
	SHORT R[200];
    TMissioneROBOT m_MissionRobot;
	float FunctionError;
	int ErrorNumber;

    bool RobotInError()		{ return (R[110] == 1); };
    bool RobotInAuto()		{ return (R[111] == 1); };
    bool RobotInRun()		{ return (R[112] == 1); };
    bool RobotInHome()		{ return (R[113] == 1); };
    bool IsPalletPresent()	{ return (R[116] == 1); };
};
//---------------------------------------------------------------------------
extern TFanuc *Fanuc[N_ROBOTS];

#endif
