//---------------------------------------------------------------------------

#ifndef DBH
#define DBH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include "ClientData.h"
#include <ExtCtrls.hpp>
#include <Data.DB.hpp>
#include <map>
//---------------------------------------------------------------------------
typedef struct {
	int ID_Pallet;
	int ID_TipoPallet;
	int ID_Pezzo;
	int Stato_Pallet;
	int Extra_Altezza;
	AnsiString Descrizione_Pallet;
	AnsiString Codice;
	AnsiString Programma;
	AnsiString Alias;
} TPallet;
//---------------------------------------------------------------------------
typedef struct {
	int ID_Pezzo;
	int ID_Articolo;
	int Stato_Pezzo;
	int Abilita_Lavaggio;
	int Lavaggio_Eseguito;
	int Programma_Lavaggio;
	int Abilita_Misura;
	int Misura_Eseguita;
	double ZeroX;
	double ZeroY;
	double OffsetX;
	double OffsetY;
	double OffsetZ;
	double OffsetA;
	double OffsetB;
	double OffsetC;
	double OffsetROT;
	double Gap;
	double GapMin;
	double GapMax;
	double H;
	AnsiString Descrizione;
} TPezzo;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Missione;
	int ID_Pallet;
	AnsiString Tipo_Missione;
	AnsiString Tipo_Macchina;
	int ID_Macchina;
	AnsiString Programma_Macchina;
	int ID_Articolo;
	int Priorita;
	int ID_Job;
	AnsiString Commento;
	AnsiString GruppoFase;
	AnsiString GruppoPallet;
	int ID_Pezzo;
	int AzionePallet;
	AnsiString GruppoLavorazione;
	AnsiString Descrizione_Op;
    int ID_Lotto;
    int ID_Lavoro;
    int IndiceLavoro;
} TMissioneDB;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Job;
	int ID_Pallet;
	AnsiString Tipo_Missione;
	AnsiString Tipo_Macchina;
	int ID_Macchina;
	AnsiString Programma_Macchina;
	int ID_Articolo;
	int Priorita;
	AnsiString Active;
	AnsiString Commento;
	AnsiString GruppoFase;
	AnsiString GruppoPallet;
	int ID_Pezzo;
	int AzionePallet;
	AnsiString GruppoLavorazione;
    AnsiString Descrizione_Op;
    int ID_Lotto;
    int ID_Lavoro;
    int IndiceLavoro;
} TJob;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Articolo;
	int ID_TipoPezzo;
	AnsiString Codice_Articolo;
	AnsiString Descrizione_Articolo;
	float DiamentroPezzo_Grezzo;
	float DiamentroPezzo_Semilavorato;
	float AltezzaPezzo;
	int ftc;
	int Faccia_Pinza;
	int TStazionamento;
} TArticolo;
//---------------------------------------------------------------------------

typedef struct {
	AnsiString 	Codice			;
	int 		PezziPerPallet	;
	double 		Attrezzatura	;
	double 		Controlli		;
	double 		Altro			;
	double 		Produzione		;
	int 		Quantita		;
	int 		Finiti			;
	int 		Esecuzione		;
	int 		Completato		;
} dati_articolo;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lavorazione;
	AnsiString Codice;
	AnsiString Descrizione;
	int ID_Articolo;
} TLavorazione;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lavorazione;
	int ID_Fase;
	AnsiString Codice;
	AnsiString Descrizione;
	int ID_Operazione;
	int ID_Macchina;
	AnsiString Tipo_Macchina;
	AnsiString Programma_MU;
	int ID_TipoPallet;
	AnsiString MessaggioOp;
	int OrdineSeq;
	int Abilitazione;
	int ID_TipoPalletXCambio;
} TFase;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lavoro;
	int Indice;
	int ID_Lavorazione;
	int ID_Fase;
	int LavoroAttivo;
	int ID_Pallet;
	int FaseCompletata;
	int ID_Job;
	int ID_Missione;
	int Priorita;
} TLavoriInCorso;
//---------------------------------------------------------------------------

typedef struct {
	int ID_Lotto;
	AnsiString Codice;
	AnsiString Descrizione;
	int ID_Lavorazione;
	int Qta;
	int QtaProdotta;
	int Attivo;
	int Priority;
	int In_Prod;
} TLotto;
//---------------------------------------------------------------------------

typedef std::map<int, std::map<AnsiString, AnsiString> > TRecordList;
//---------------------------------------------------------------------------
typedef std::map<AnsiString, std::map<AnsiString, AnsiString> > TIndexList;
//---------------------------------------------------------------------------
class TDBDataModule : public TDataModule
{
__published:	// IDE-managed Components
	TADOConnection *ADOConnection1;
	TTimer *TimerPuliziaStorici;
	TTimer *TimerConnect;
	void __fastcall TimerPuliziaStoriciTimer(TObject *Sender);
	void __fastcall TimerConnectTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TDBDataModule(TComponent* Owner);
	void PuliziaStorici();
	void ConnettiDB();
	void DisconnettiDB();
	int verificaConnessioneDB();
	void CaricaTabella(AnsiString TableName, TRecordList &RecList);
	void CaricaTabellaK(AnsiString TableName, AnsiString KeyField, TIndexList &RecList);
	int CaricaTabellaQ(AnsiString Query, TRecordList &RecList);
	void Segnalazione(int n, AnsiString msg, int all, int popup = 0, int popupstation = 0);
    bool SegnalazioneAcquisita(int nall);
	void AcquisisciSegnalazioneAttiva(int nmsg);
    void Log(AnsiString Sorgente, AnsiString Tipo, char *s, ...);
	void Log(AnsiString Sorgente, AnsiString Tipo, AnsiString Evento);
	int PosizioneMacchina(AnsiString TipoMacchina, int ID_Macchina, int ID_TipoPallet);
	int AdattatoreRichiesto(AnsiString TipoMacchina, int ID_Macchina, int ID_TipoPallet);
	int LeggiAdattatoreInMacchina(AnsiString Tipo_Macchina, int ID_Macchina);
	int TipoPinza(int ID_TipoPallet);
	int TipoPinzaPezzi(int ID_TipoPezzo);
	bool LeggiPallet(int ID_Pallet, TPallet &p);
	void LeggiPalletDaPezzo(int ID_Pezzo, TPallet &p);
	void CancellaPallet(int ID_Pallet);
	int NewPalletID();
	int CreaPallet(TPallet p);
	int CreaNuovoPallet(int TipoPallet, TPallet &newpallet);
	int AggiornaPallet(TPallet p);
	int PalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet);
	int UtensileInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Id_Utensile);
	int AdattatoreInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet);
	int LeggiPalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, TPallet &p);
	int LeggiIDPezzoInMacchina(int ID_Macchina, AnsiString Tipo_Macchina);
	int CercaDestinazioneMagazzino(int Tipo_Pallet, int &nmag, int &npos, int gruppo = 0, int extrah = 0);
	int CercaAliasMagazzino(AnsiString alias, int Tipo_Pallet, int &nmag, int &npos, int extrah = 0);
	int CellaTotemPrecedente(int pos);
	int CellaTotemSuccessiva(int pos);
	int CercaDestinazioneTotem(int extralarge, int &nmag, int &npos);
	int CercaPalletMagazzino(int Tipo_Pallet, int &nmag, int &npos);
	int LeggiPalletMagazzino(int Magazzino, int Postazione, TPallet &p);
	int PalletInMagazzino(int Magazzino, int Postazione, int ID_Pallet);
	int LeggiProxMissione(TMissioneDB &m);
	int LeggiMissione(int ID_Missione, TMissioneDB &m);
	int LeggiMissionePallet(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, TMissioneDB &m);
	int LeggiMissionePallet(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int numero_macchina, TMissioneDB &m);
	int CancellaMissione(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int ID_Macchina);
	int CancellaMissione(int ID_Missione);
	int CambiaStatoPostazione(int Magazzino, int Postazione, int stato);
	int LeggiJob(int ID_Job, TJob &j);
	int CreaJob(TJob j);
    int AttivaJob(int ID_Job);
	int CreaMissione(TMissioneDB m);
	int CreaMissionePrelievoMacchina(AnsiString tpmacc, int nmacc, int pri = 50);
	int CreaMissioneDepositoMacchina(int ID_Pallet, AnsiString tpmacc, int nmacc, int pri = 50);
	int AggiornaCampoTabella(AnsiString Tabella, AnsiString CampoChiave, int ValoreChiave, AnsiString CampoModifica, AnsiString ValoreModifica, bool isstring);
	int LeggiCampoTabella(AnsiString Tabella, AnsiString CampoDaLeggere, AnsiString ClauslaWhere, AnsiString &Valore);
	int SetProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, AnsiString Programma_Macchina, int Programma_Attivo = 0);
	int SetStatoProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Programma_Attivo);
	int LeggiProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, AnsiString &Programma_Macchina);
	void CancellaJob(int ID_Job);
	void InoltraJobs();
	bool JobInoltrabile(TJob j);
	bool JobInoltrabilePerPalletControlloPezzo(TJob j);
	bool JobInoltrabilePerPezzo(TJob j, int ID_Pallet);
	bool JobInoltrabilePerCambioPallet(TJob j);
	bool NonEsisteJobConPriMaggiore(TJob j);
	bool LeggiArticolo(int ID_Articolo, TArticolo &a);
	void LeggiArticoloDaIdPezzo(int ID_Pezzo, TArticolo &a);
	void SvuotaPallet(TPallet &p);
	bool ControlloPalletDaLavare(int ID_Pallet);
	int ImpostaPezzoLavato(int ID_Pezzo, int lavato);
	int CercaPalletInBaia(int idPallet);
	int AggiornaIdPezzoJob(int ID_Pezzo, AnsiString Gruppo);
	int ProgrammaLavaggioPallet(int ID_Pallet);
	int GetSelfGenID(AnsiString TableName);
	int AggiornaStatoPezzo(int ID_Pezzo, int NuovoStato = -1);
	int AggiornaPezzoSuPallet(int idpallet, int idpezzo);
	int ResettaErrorePezzo(int ID_Pezzo);
	int CreaPezzo(TPezzo pz);
	int LeggiPezzo(int ID_Pezzo, TPezzo &p);
	int ImpostaPresetPezzo(TPezzo p);
	int EsisteGiaMissione(AnsiString tpmiss, AnsiString tpmacc, int nmacc);
	int EsisteGiaJob(AnsiString tpmiss, AnsiString tpmacc, int nmacc);
	int EsisteMissioneGenerica(AnsiString tpmiss,int ID_P);
	int LeggiUtensileInMacchina(AnsiString Tipo_Macchina, int ID_Macchina);
	int CreaMissionePrelievoUtensileMacchina(AnsiString tpmacc, int nmacc, int pri = 50);
	int AddPresetMacchina(AnsiString TipoMacchina, int ID_Macchina, int ID_TipoPallet, TPezzo &pz);
	int AggiornaDescPallet(int id, AnsiString desc);
	int AggiornaProgrammaPallet(int id, AnsiString programma);
	void CancellaPezzo(int ID_Pezzo);
	int CommentoJob(int JobID, AnsiString commento);
	int CommentoMissione(int MissID, AnsiString commento);
	void LogDestProd(int ID_Job, int nmag, int npos);
	int MissioneInviata();
	void LeggiArticoloInProduzione(AnsiString codice, int MU, dati_articolo &d);
	void LavoraArticolo(AnsiString codice, int MU);
	void CompletaArticolo(AnsiString codice, int MU);
	void CancellaArticoloCompletato(AnsiString codice, int MU);
	int ProssimoArticolo(dati_articolo &d, double t, int MU);
	void AggiornaArticolo(AnsiString codice, int MU);
	int CercaPalletAttrezzatoInMagazzino();
	int LeggiMaxArticoliMacchina(AnsiString Tipo_Macchina, int ID_Macchina);
	int LeggiNArticoliAttiviMacchina(int ID_Macchina);
	int LeggiNArticoliProgrammatiMacchina(int ID_Macchina);
	int ContaPalletInGiro(int MU);
	int CercaPalletVuotoInMagazzino(int MU);
	int CercaPalletAttrezzatoInMagazzino(int MU);
	int CercaPalletFinitoInMagazzino(int MU);
	int LeggiProgrammaPallet(int id);
	int LogPrelievoMissione();
	int LogDepositoMissione();
	int AggiornaBaieFasi(AnsiString GruppoFase, int ID_Macchina);
	int LeggiAbilitazioneVerificaUtensili(int MU);
	int LeggiAbilitazioneControlloBalluff();
	int CheckIdPalletFromGruppoPallet(AnsiString Gruppo, int &RowCount);
	int LeggiFase(int ID_Lavorazione, int ID_Fase, TFase &f);
	int CercaTipoPalletMagazzino(int ID_TipoPallet, int &ID_Pallet);
	void AggiornaExtraAltezzaPallet(int ID_Pallet);
	int VerificaUtensili(AnsiString prg, tool *STR_TOOLS_TAB, AnsiString &commento);
	bool ToolPresentInTableLife(int tool, int VitaResidua, int Programma);
	int MemorizzaVitaUtensile(int tool, int VitaResidua, int Programma);
	int RicalcolaUsoUtensile(int tool, int vita_attuale, int Programma);
	bool GetGruppoLavorazioneID(int Lav, AnsiString &ID);
    int LeggiLavorazione(int ID_Lavorazione, TLavorazione &strLavor);
    bool GetGruppoFaseID(int Lav, int Fase, AnsiString &ID);
    bool GetGruppoPalletID(int Lav, int Fase, AnsiString &ID);
	int CompletaFaseLavoriInCorso(int ID_Lavoro, int Indice);
	int AggiornaJobLavoriInCorso(int ID_Job, int ID_Lavoro, int Indice);
	int AggiornaMissioneLavoriInCorso(int ID_Job, int ID_Missione);
    int PalletAssociatoLotto(int ID_Pallet);
    int RiattivaLavoriInCorso(int ID_Lavoro);
    int EliminaLavoriInCorso(int ID_Lavoro);
	int EliminaLotto(int ID_Lavoro);
	int AggiornaQtaLotto(int ID_Lavoro);
    int QtaMancanteFineLotto(int ID_Lavoro);
    int AggiornaLavoriInCorso(int ID_Lavoro, int ID_Pallet);
    void CompletaAzionePallet(TMissioneDB m, TPallet p);
    int CercaLavoroDaEseguire(int ID_Pallet);
    int CreaJobDaFase(TJob &j);
	int CreaJobPrelievoMacchina(AnsiString tpmacc, int nmacc, int pri);
    int CreaJobDepositoMacchina(int ID_Pallet, AnsiString tpmacc, int nmacc, int pri);
	void LogMissRobot(int tipomiss, int locmag, int nmag, int nbaia, int tipo_pinza, int nmu, int posmu, int idpallet);
    void LogMiss(AnsiString str, int ID_Pallet, AnsiString tipomiss, AnsiString tipomacc, int nmacc, int ID_Job = 0, int ID_Missione = 0);
	int NewIDLavoro();
	int CreaLavoriInCorso(int ID_Pallet, int ID_Lavorazione, int LavoroAttivo, int priorita = 50);
	int CreaLavoro(TLavoriInCorso lav);
	int EsisteLavoriInCorso(int ID_Lavorazione, int ID_Pallet);
	void AttivaLavoroInCorso(int ID_Lavoro);
	int ResettaLavoriInCorso(int ID_Lavoro, int Indice);
	int VerificaPalletBloccatoInMagazzino(int ID_Pallet);
	int EsisteGiaJobAttivo(AnsiString tpmiss, AnsiString tpmacc, int nmacc);
    int EsisteGiaMissionePallet(AnsiString tpmiss, AnsiString tpmacc, int ID_Pallet);
	int EsisteGiaMissionePallet(AnsiString tpmiss, AnsiString tpmacc, int nmacc, int ID_Pallet);
    int EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int ID_Pallet);
	int EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int nmacc, int ID_Pallet);
	int EsisteGiaJobAttivoPallet(int pallet, AnsiString tpmiss, AnsiString tpmacc, int nmacc);
	void LogFineLavoro(AnsiString codice, int qta, bool iniziato = true);
	void LogQtaLavoro(AnsiString codice, int qta);
	void LogProdInizioLavoro(int ID_Pallet, int ID_Macchina, AnsiString CodiceArticolo, AnsiString Programma);
	void LogLavoroProd(AnsiString campo, int ID_Pallet, int ID_Macchina);
	void LogProdLavoro(AnsiString campo, int ID_Pallet, AnsiString Tipo_Macchina, int ID_Macchina);
	int LeggiAbilitazioneStandby(int MU);
    int AggiornaStatoMacchinaUtensile(int MU, int iStato);
	int CercaPallet(int ID_Pallet, AnsiString Tipo_Macchina, TPallet &p);
    int LeggiAbilitazioneParametro(int index);
	void EventoRobot(AnsiString Sorgente, AnsiString Tipo, char *s, ...);
	void EventoRobot(AnsiString Sorgente, AnsiString Tipo, AnsiString Evento);
	TDateTime t;
};

//---------------------------------------------------------------------------
extern PACKAGE TDBDataModule *DBDataModule;
extern AnsiString logstr;
//---------------------------------------------------------------------------


#endif
