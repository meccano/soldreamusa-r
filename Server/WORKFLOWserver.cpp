//---------------------------------------------------------------------------
#pragma hdrstop
//---------------------------------------------------------------------------
#include <vcl.h>
//---------------------------------------------------------------------------
USEFORM("Main.cpp", MainForm);
USEFORM("Tools.cpp", dmTools); /* TDataModule: File Type */
USEFORM("Socket.cpp", SocketDataModule); /* TDataModule: File Type */
USEFORM("DB.cpp", DBDataModule); /* TDataModule: File Type */
USEFORM("Events.cpp", EventsDM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	HANDLE mutex;
	AnsiString MutexName = ExtractFileName(Application->ExeName).UpperCase();

	mutex = OpenMutex(MUTEX_ALL_ACCESS, false, MutexName.c_str());
	if (mutex == NULL) {
		mutex = CreateMutex(NULL, true, MutexName.c_str());
	} else {
		exit(EXIT_SUCCESS);
	}
	try
	{
		FormatSettings.DecimalSeparator = '.';
		FormatSettings.ThousandSeparator = ',';
		FormatSettings.TimeSeparator = ':';
		FormatSettings.DateSeparator = '/';
		FormatSettings.ShortDateFormat = "dd/mm/yyyy";
		Application->Initialize();
		Application->CreateForm(__classid(TDBDataModule), &DBDataModule);
		Application->CreateForm(__classid(TMainForm), &MainForm);
		Application->CreateForm(__classid(TdmTools), &dmTools);
		Application->CreateForm(__classid(TSocketDataModule), &SocketDataModule);
		Application->CreateForm(__classid(TEventsDM), &EventsDM);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	ReleaseMutex(mutex);
	return 0;
}
//---------------------------------------------------------------------------
