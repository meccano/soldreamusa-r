﻿//---------------------------------------------------------------------------
#pragma hdrstop

#include <fstream>
#include <iostream>
#include <string>
#include "math.h"
#include <IniFiles.hpp>
#include "System.Math.hpp"
#include "Events.h"
#include "Fanuc.h"
#include "PLCThread.h"
#include "FocasThread.h"
#include "LogTxt.h"
#include "Tools.h"
#include "Util.h"
#include "Define.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TEventsDM *EventsDM;
//---------------------------------------------------------------------------

__fastcall TEventsDM::TEventsDM(TComponent* Owner)
	: TDataModule(Owner)
{
	memset(Eventi, 0, sizeof(Eventi));
	memset(EventiBAK, 0, sizeof(Eventi));

	//Leggo gli eventi legati ai robot
	LeggiTestiEventiRobot();

	//Leggo gli allarmi legati ai robot
	LeggiTestiAllarmiRobot();
}
//---------------------------------------------------------------------------

__fastcall TEventsDM::~TEventsDM()
{
}
//---------------------------------------------------------------------------

void TEventsDM::NewEvent(int IDev, int *intParam) {
	TEvento e;

	e.IDEvento = IDev;
	if (intParam != NULL) {
		memcpy(e.intParam, intParam, sizeof(int) * 10);
	}
	e.t = clock();
	EventList.push_back(e);
}
//---------------------------------------------------------------------------

void TEventsDM::NewEvent(int IDev, AnsiString strParam) {
	TEvento e;

	e.IDEvento = IDev;
	e.strParam = strParam;
	e.t = clock();
	EventList.push_back(e);
}
//---------------------------------------------------------------------------

bool TEventsDM::ExistsEvent(int IDev) {
	UINT i;
	bool res = false;

	for(i = 0; (i < EventList.size()) && !res; i++) {
		if (EventList[i].IDEvento == IDev) {
			res = true;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

void TEventsDM::EraseEvent(int n) {
	EventList.erase(EventList.begin() + n);
}
//---------------------------------------------------------------------------

void __fastcall TEventsDM::TimerScanEventsTimer(TObject *Sender)
{
	UINT i;
	TPallet p;
	TPezzo pz;
    AnsiString prg;
	int param[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    //Se la comunicazione col traslo è interrotta o non siamo collegati al DB
    //non eseguo nemmeno la scansione degli eventi
	if (!DebugHook && (!DBDataModule->ADOConnection1->Connected || !Fanuc[0]->Connected || !PLCThread->connected)) {
		return;
	}

	TimerScanEvents->Enabled = false;
	// Rilevo eventi
	try {
		//Aggiorno i dati delle missione in corso
        Fanuc[0]->AggiornaDatiMissione(m_MissionRun);

        //Copio gli eventi "passati"
		memcpy(EventiBAK, Eventi, sizeof(Eventi));

		// Controllo se perso evento EV_ROBOT_IN_ATTESA lo riattivo
		if (EventiBAK[EV_ROBOT_IN_ATTESA] && AttesaMissione() && !ExistsEvent(EV_ROBOT_IN_ATTESA)) {
			EventiBAK[EV_ROBOT_IN_ATTESA] = 0;
		}

        //Attesa missione
		Eventi[EV_ROBOT_IN_ATTESA] = AttesaMissione() && (m_MissionRun.Strobe == 0);
		if (!EventiBAK[EV_ROBOT_IN_ATTESA] && Eventi[EV_ROBOT_IN_ATTESA]) {
			if (!ExistsEvent(EV_ROBOT_IN_ATTESA)) {
				NewEvent(EV_ROBOT_IN_ATTESA);
				DBDataModule->Log("SERVER", "EVENT", "Robot: Evento pronto per nuova missione");
			} else {
				DBDataModule->Log("SERVER", "EVENT", "Robot: Evento pronto per nuova missione scartato perchè già presente");
			}
		}

        //Fine missione
		Eventi[EV_FINE_MISSIONE_ROBOT] = (m_MissionRun.Strobe == 4);
		if (!EventiBAK[EV_FINE_MISSIONE_ROBOT] && Eventi[EV_FINE_MISSIONE_ROBOT]) {
			NewEvent(EV_FINE_MISSIONE_ROBOT);
			DBDataModule->Log("SERVER", "EVENT", "Robot: Evento operazione completata");
		}

		//Missione abortita
		Eventi[EV_MISSIONE_ABORTITA] = (m_MissionRun.Strobe == 5);
		if (!EventiBAK[EV_MISSIONE_ABORTITA] && Eventi[EV_MISSIONE_ABORTITA]) {
			NewEvent(EV_MISSIONE_ABORTITA);
			DBDataModule->Log("SERVER", "EVENT", "Robot: Evento missione abortita");
		}

        //Missione errata
		Eventi[EV_MISSIONE_ERRATA] = (m_MissionRun.Strobe == 20);
		if (!EventiBAK[EV_MISSIONE_ERRATA] && Eventi[EV_MISSIONE_ERRATA]) {
			NewEvent(EV_MISSIONE_ERRATA);
			DBDataModule->Log("SERVER", "EVENT", "Robot: Evento dati missione errati");
		}

		//Missione di prelievo del pallet dalla baia 1
        Eventi[EV_PRELEVA_PALLET_BAIA1] = (PLCThread->DB[DB_STAZIONI][4] & BIT3);
        if (!EventiBAK[EV_PRELEVA_PALLET_BAIA1] && Eventi[EV_PRELEVA_PALLET_BAIA1]) {
        	param[0] = 1;
            NewEvent(EV_PRELEVA_PALLET_BAIA1);
            DBDataModule->Log("SERVER", "EVENT", "Baia1: Evento prelievo pallet");
        }

		//Missione di prelievo del pallet dalla baia 2
        Eventi[EV_PRELEVA_PALLET_BAIA2] = (PLCThread->DB[DB_STAZIONI][10] & BIT3);
        if (!EventiBAK[EV_PRELEVA_PALLET_BAIA2] && Eventi[EV_PRELEVA_PALLET_BAIA2]) {
        	param[0] = 2;
            NewEvent(EV_PRELEVA_PALLET_BAIA2);
            DBDataModule->Log("SERVER", "EVENT", "Baia2: Evento prelievo pallet");
        }

        //Ciclo su tutte le macchine
        for (i = 1; i <= N_MACCHINE_UT; i++) {
            //Missione di prelievo del pallet dalla macchina utensile
            Eventi[EV_FINE_PRG_MU(i)] = (PLCThread->DB[DB_PLC_MU_IN][i - 1] & BIT3);
            if (!EventiBAK[EV_FINE_PRG_MU(i)] && Eventi[EV_FINE_PRG_MU(i)]) {
                param[0] = i;
                NewEvent(EV_FINE_PRG_MU(i));
                DBDataModule->Log("SERVER", "EVENT", "MU%d: Evento prelievo pallet", i);
            }
        }

		// Log Eventi robot
		Eventi[EV_EVENT_RBT1] = Fanuc[0]->R[29];
		if ((EventiBAK[EV_EVENT_RBT1] != Eventi[EV_EVENT_RBT1]) && (Eventi[EV_EVENT_RBT1] != 0)) {
			//Scrivo il messaggio
			LogEventoRobot(1, Eventi[EV_EVENT_RBT1]);
		}

		// Log Allarmi robot
		Eventi[EV_ALARM_RBT1] = Fanuc[0]->R[9];
		if ((EventiBAK[EV_ALARM_RBT1] != Eventi[EV_ALARM_RBT1]) && (Eventi[EV_ALARM_RBT1] != 0)) {
			//Scrivo il messaggio
			LogAllarmeRobot(1, Eventi[EV_ALARM_RBT1]);
		}

		//Controllo se posso generare delle missioni per i job attivi
		DBDataModule->InoltraJobs();

		// Eseguo eventi
		for(i = 0; i < EventList.size(); i++) {
			switch(EventList[i].IDEvento) {
			case EV_ROBOT_IN_ATTESA:
				// Aggiorno tabella missioni in memoria
				DBDataModule->CaricaTabella("Missioni ORDER BY Priorita DESC, Creazione, ID_Missione", TabMissioni);
				if (GeneraMissione() == 0) {
					EraseEvent(i--);
				}
				break;
			case EV_FINE_MISSIONE_ROBOT:
                switch(Fanuc[0]->R[TIPO_MISSIONE]) {
                case TPM_PRELIEVO_MACCHINA:
                case TPM_PRELIEVO_BAIA:
                case TPM_PRELIEVO_MAGAZZINO:
                    if (GestisciPrelievo() == 0) {
                        Fanuc[0]->EraseMission();
                        EraseEvent(i--);
                    }
                    break;
                case TPM_DEPOSITO_MACCHINA:
                case TPM_DEPOSITO_BAIA:
                case TPM_DEPOSITO_MAGAZZINO:
                    if (GestisciFineMissione() == 0) {
                        Fanuc[0]->EraseMission();
                        EraseEvent(i--);
                    }
                    break;
                }
				break;
			case EV_MISSIONE_ABORTITA:
			case EV_MISSIONE_ERRATA:
				Fanuc[0]->EraseMission();
				EraseEvent(i--);
				break;
			case EV_PRELEVA_PALLET_BAIA1:
			case EV_PRELEVA_PALLET_BAIA2:
				if (DBDataModule->CreaMissionePrelievoMacchina("S", param[0], 90)) {
					EraseEvent(i--);
				}
				break;
			case EV_FINE_PRG_MU1:
			case EV_FINE_PRG_MU2:
			case EV_FINE_PRG_MU3:
            	//Comando per scaricare la macchina utensile
				ScaricaMacchina(param[0]);
                //Leggo il pallet in macchina
                DBDataModule->LeggiPalletInMacchina(param[0], "M", p);
                //Log produzione
                DBDataModule->LogLavoroProd("Fine_Lavoro", p.ID_Pallet, param[0]);
                //Cancello evento
                EraseEvent(i--);
				break;
			default:
				EraseEvent(i--);
				break;
			}
		}
	} catch (...) {}
	TimerScanEvents->Enabled = true;
}
//---------------------------------------------------------------------------

bool TEventsDM::AttesaMissione()
{
	return ((!Fanuc[0]->R[STROBE]) &&		// Verifico di non avere altre missioni sul robot
		    (Fanuc[0]->TxQueue.empty()) && 	// Controllo anche di non avere già scritture da eseguire perchè potrei già aver inviato un'altra missione
			(Fanuc[0]->RobotInAuto()) &&	// AUTO
			(Fanuc[0]->RobotInRun()) &&		// RUNNING
			(Fanuc[0]->RobotInHome()));		// HOME
}
//---------------------------------------------------------------------------

bool TEventsDM::OKPrelievoInBaia(int nbaia)
{
	TPallet p;

    switch (nbaia) {
    case 1:
        // Verifico baia OK PRELIEVO
        if (!(PLCThread->DB[DB_STAZIONI][4] & BIT0) && (PLCThread->DB[DB_STAZIONI][4] & BIT3)) {
            return false;
        }
        break;
    case 2:
        // Verifico baia OK PRELIEVO
        if (!(PLCThread->DB[DB_STAZIONI][10] & BIT0) && (PLCThread->DB[DB_STAZIONI][10] & BIT3)) {
            return false;
        }
        break;
    default:
    	return false;
    }

	// Verifico se la stazione é occupata
	DBDataModule->LeggiPalletInMacchina(nbaia, "S", p);
	if (p.ID_Pallet == 0)
		return false;

	// OK, allora posso inviare missione
	return true;
}
//---------------------------------------------------------------------------

bool TEventsDM::OKDepositoInBaia(int nbaia)
{
	TPallet p;

    switch (nbaia) {
    case 1:
        // Verifico baia OK deposito
        if (!(PLCThread->DB[DB_STAZIONI][4] & BIT1)) {
            return false;
        }
        break;
    case 2:
        // Verifico baia OK Deposito
        if (!(PLCThread->DB[DB_STAZIONI][10] & BIT1)) {
            return false;
        }
        break;
    default:
    	return false;
    }

	// Verifico se la stazione é occupata
	DBDataModule->LeggiPalletInMacchina(nbaia, "S", p);
	if (p.ID_Pallet != 0)
		return false;

	// OK, allora posso inviare missione
	return true;
}
//---------------------------------------------------------------------------

bool TEventsDM::OKPrelievoMacchina(int MU, AnsiString &mess)
{
	// Verifico se la macchina utensile è in automatico
	if (!(PLCThread->DB[DB_PLC_MU_IN][MU - 1] & BIT0)) {
    	mess = "Machine not in automatic mode";
        return false;
    }

	// Verifico se il pallet in macchina è bloccato
	if (!(PLCThread->DB[DB_PLC_MU_IN][MU - 1] & BIT1)) {
    	mess = "Machine table clamp not lock";
        return false;
    }

	// Verifico se la porta della macchina utensile è aperta
	if (!(PLCThread->DB[DB_PLC_MU_IN][MU - 1] & BIT4)) {
    	mess = "Machine door not open";
        return false;
    }

	return true;
}
//---------------------------------------------------------------------------

bool TEventsDM::OKDepositoMacchina(int MU, AnsiString &mess)
{
	// Verifico se la macchina utensile è in automatico
	if (!(PLCThread->DB[DB_PLC_MU_IN][MU - 1] & BIT0)) {
    	mess = "Machine not in automatic mode";
        return false;
    }

	// Verifico se la tavola in macchina è bloccata
	if (!(PLCThread->DB[DB_PLC_MU_IN][MU - 1] & BIT2)) {
    	mess = "Machine table clamp not lock";
        return false;
    }

	// Verifico se la porta della macchina utensile è aperta
	if (!(PLCThread->DB[DB_PLC_MU_IN][MU - 1] & BIT4)) {
    	mess = "Machine door not open";
        return false;
    }

	return true;
}
//---------------------------------------------------------------------------

int TEventsDM::GestisciPrelievo() {
	TPallet p, pi;
	TPezzo pz;
	TMissioneDB m;
	int nmacc, nbaia, tmiss, nmag, nloc, xh, nProgramma;

    tmiss = Fanuc[0]->R[TIPO_MISSIONE];
	nmacc = Fanuc[0]->R[N_MACCHINA];
	nbaia = Fanuc[0]->R[N_BAIA];
    nmag = Fanuc[0]->R[N_MAGAZZINO];
    nloc = Fanuc[0]->R[LOC_MAGAZZINO];

	switch(tmiss) {
	case TPM_PRELIEVO_MACCHINA:
		if (DBDataModule->LeggiPalletInMacchina(nmacc, "M", p) != 0)
			return -1;
		if (p.ID_Pallet == 0)
			return -2;
		// Scrivo pallet su robot
		if (DBDataModule->PalletInMacchina(7, "R", p.ID_Pallet) == 0)
			return -3;
		// Cancella pallet in Macchina
		DBDataModule->PalletInMacchina(nmacc, "M", 0);
		// Leggo la missione
		DBDataModule->LeggiMissionePallet(p.ID_Pallet, "PICK", "M", nmacc, m);
		// Cancello Missione
		DBDataModule->CancellaMissione(m.ID_Missione);
		// Cancello part program dala macchina
		//Ottengo il numero del programma del pallet in macchina
		nProgramma = NumeroProgramma(p.Programma);
        //Seleziono un programma fittizio per eseguire la cancellazione
        Focas[nmacc - 1]->SelectProgram(50, 0);
        //Cancello il programma
		Focas[nmacc - 1]->CancellaFile30I(nProgramma);
		break;
	case TPM_PRELIEVO_BAIA:
		// Leggo pallet in stazione
		if (DBDataModule->LeggiPalletInMacchina(nbaia, "S", p) != 0)
			return -1;
		if (p.ID_Pallet == 0)
			return -2;
		// Verifico Extra_Altezza
		xh = (nbaia == 1) ? Fanuc[0]->R[13] : Fanuc[0]->R[14];
		p.Extra_Altezza = (xh ? 1 : 0);
		if (xh != 2) {
			// Scrivo pallet su robot
			if (DBDataModule->PalletInMacchina(7, "R", p.ID_Pallet) == 0)
				return -3;
			// Cancella pallet in stazione
			DBDataModule->PalletInMacchina(nbaia, "S", 0);
		}
		// Imposta pezzo come attrezzato
		DBDataModule->AggiornaStatoPezzo(p.ID_Pezzo, STATO_PEZZO_ATTREZZATO);
		// Leggo la missione associata al pallet
		DBDataModule->LeggiMissionePallet(p.ID_Pallet, "PICK", "S", nbaia, m);
		// Cancello Missione
		DBDataModule->CancellaMissione(m.ID_Missione);
		if (xh == 2) {
            //Visualizzo un allarme
			DBDataModule->Segnalazione(1002, "Pallet hight for the storage!", 1);
            //Log
			DBDataModule->Log("SERVER", "EVENT", "Pallet %d troppo alto per il deposito in Magazzino!", p.ID_Pallet);
		}
		break;
	case TPM_PRELIEVO_MAGAZZINO:
		// Leggo pallet in magazzino
		if (DBDataModule->LeggiPalletMagazzino(nmag, nloc, p) != 0)
			return -1;
		if (p.ID_Pallet == 0)
			return -2;
		// Scrivo pallet su robot
		if (DBDataModule->PalletInMacchina(7, "R", p.ID_Pallet) == 0)
			return -3;
		// Cancella pallet in mag
		DBDataModule->PalletInMagazzino(nmag, nloc, 0);
		// Tolgo prenotazione dal magazzino
		DBDataModule->CambiaStatoPostazione(nmag, nloc, POS_LIBERA);
		break;
	}
	DBDataModule->LogPrelievoMissione();
	return 0;
}
//---------------------------------------------------------------------------

int TEventsDM::GestisciFineMissione() {
	TPallet p, pi, pm;
    TJob j;
    TPezzo pz;
	TMissioneDB m;
	TArticolo a;
	int nmacc, nbaia, tmiss, nmag, nloc;

    tmiss = Fanuc[0]->R[TIPO_MISSIONE];
	nmacc = Fanuc[0]->R[N_MACCHINA];
	nbaia = Fanuc[0]->R[N_BAIA];
    nmag = Fanuc[0]->R[N_MAGAZZINO];
    nloc = Fanuc[0]->R[LOC_MAGAZZINO];

	switch(tmiss) {
	case TPM_DEPOSITO_MACCHINA:
        if (DBDataModule->LeggiPalletInMacchina(7, "R", p) != 0)
            return -1;
        if (p.ID_Pallet == 0) {
            // Significa che mi sono perso l'evento di prelievo
            GestisciPrelievo();
            return -2;
        }
        // Scrivo pallet in macchina
        if (DBDataModule->PalletInMacchina(nmacc, "M", p.ID_Pallet) == 0)
            return -3;
		// Resetto lo stato del pezzo
		DBDataModule->ResettaErrorePezzo(p.ID_Pezzo);
		DBDataModule->AggiornaStatoPezzo(p.ID_Pezzo, STATO_PEZZO_ATTREZZATO);
		DBDataModule->AggiornaDescPallet(p.ID_Pallet, "");
		// Cancella pallet su Robot
		DBDataModule->PalletInMacchina(7, "R", 0);
        // Leggo la missione
		DBDataModule->LeggiMissionePallet(p.ID_Pallet, "DROP", "M", nmacc, m);
        // Prima però mi memorizzo il programma da attivare in macchina
        DBDataModule->AggiornaProgrammaPallet(p.ID_Pallet, m.Programma_Macchina);
		// INVIO PROGRAMMI MACCHINA
		if (!m.Programma_Macchina.IsEmpty()) {
        	//Scrivo il programma in macchina
        	Focas[nmacc - 1]->WriteProgram4(m.Programma_Macchina, NumeroProgramma(m.Programma_Macchina));
        	//Seleziono il programma
			Focas[nmacc - 1]->SelectProgram(NumeroProgramma(m.Programma_Macchina), 1);
        }
		// Log
        DBDataModule->Log("SERVER", "EVENT", "MU%d: DROP Pallet %d complete", nmacc, p.ID_Pallet);
		// Ok, adesso cancello la missione
		DBDataModule->CancellaMissione(m.ID_Missione);
		//Record nel DB per l'inizio del lavoro
		DBDataModule->LeggiArticoloDaIdPezzo(p.ID_Pezzo, a);
		DBDataModule->LogProdInizioLavoro(p.ID_Pallet, nmacc, a.Codice_Articolo, p.Programma);
		break;
	case TPM_DEPOSITO_BAIA:
		// Leggo pallet su Robot
		if (DBDataModule->LeggiPalletInMacchina(7, "R", p) != 0)
			return -1;
		if (p.ID_Pallet == 0) {
			// Significa che mi sono perso l'evento di prelievo
			GestisciPrelievo();
			return -2;
		}
		// Scrivo pallet su stazione
		if (DBDataModule->PalletInMacchina(nbaia, "S", p.ID_Pallet) == 0)
			return -3;
		// Cancella pallet su Robot
		DBDataModule->PalletInMacchina(7, "R", 0);
		//Controllo se devo svuotare il pallet per un ribaltamento manuale
		DBDataModule->LeggiMissionePallet(p.ID_Pallet, "DROP", "S", m);
        //In base all'azione del pallet eseguo istruzioni diverse
        DBDataModule->CompletaAzionePallet(m, p);
        //Aggiorno, se esiste, la fase in corso
        DBDataModule->CompletaFaseLavoriInCorso(m.ID_Lavoro, m.IndiceLavoro);
		// Cancello Missione
		DBDataModule->CancellaMissione(m.ID_Missione);
		break;
	case TPM_DEPOSITO_MAGAZZINO:
		// Leggo pallet su Robot
		if (DBDataModule->LeggiPalletInMacchina(7, "R", p) != 0)
			return -1;
		if (p.ID_Pallet == 0) {
			// Significa che mi sono perso l'evento di prelievo
			GestisciPrelievo();
			return -2;
		}

		// Scrivo pallet in magazzino
		if (DBDataModule->PalletInMagazzino(nmag, nloc, p.ID_Pallet) == 0) {
            //Scrivo un log
            DBDataModule->Log("SERVER", "EVENT", "Fallito inserimento in magazzino del pallet");
			return -3;
        }
		// Cancella pallet su Robot
		DBDataModule->PalletInMacchina(7, "R", 0);
		// Tolgo prenotazione dal magazzino
		DBDataModule->LeggiPezzo(p.ID_Pezzo, pz);
		DBDataModule->CambiaStatoPostazione(nmag, nloc, (pz.Stato_Pezzo >= STATO_PEZZO_ERRORE_PEL) ? POS_BLOCCATA : POS_LIBERA);
		// Imposta pezzo come attrezzato (modifica ROEN per i cicli in automatico dato che loro non considerano lo stato)
		DBDataModule->AggiornaStatoPezzo(p.ID_Pezzo, STATO_PEZZO_ATTREZZATO);
		// Leggo la missione
		DBDataModule->LeggiMissionePallet(p.ID_Pallet, "DROP", IntToStr(nmag), nloc, m);
		// LOG Produzione
		DBDataModule->LogDestProd(m.ID_Job, nmag, nloc);
		// Log
		DBDataModule->Log("SERVER", "EVENT", "MAG%d-%d: DROP Pallet %d complete", nmag, nloc, p.ID_Pallet);
		// Cancello Missione
		DBDataModule->CancellaMissione(m.ID_Missione);
		break;
	}
	DBDataModule->LogDepositoMissione();
	return 0;
}
//---------------------------------------------------------------------------

void TEventsDM::GestisciMissioneAbortita()
{
	int tmiss, nmag, nloc;

    tmiss = Fanuc[0]->R[TIPO_MISSIONE];
    nmag = Fanuc[0]->R[N_MAGAZZINO];
    nloc = Fanuc[0]->R[LOC_MAGAZZINO];

	switch(tmiss) {
	case TPM_PRELIEVO_MAGAZZINO:
		// Tolgo prenotazione dal magazzino
		DBDataModule->CambiaStatoPostazione(nmag, nloc, POS_LIBERA);
		break;
	case TPM_DEPOSITO_MAGAZZINO:
		// Tolgo prenotazione dal magazzino
		DBDataModule->CambiaStatoPostazione(nmag, nloc, POS_LIBERA);
		break;
	}
}
//---------------------------------------------------------------------------

int TEventsDM::CercaDeposito(TMissione &m) {
	UINT i;
	int nmag, npos;
	TPallet p;

    //Leggo il pallet
	DBDataModule->LeggiPallet(m.N_pallet, p);
    for (i = 0; i < TabMissioni.size(); i++) {
		if (TabMissioni[i]["Tipo_Missione"] == "DROP") {
			if (TabMissioni[i]["ID_Pallet"].ToIntDef(0) == m.N_pallet) {
				if (GeneraMissioneDeposito(i, p.ID_TipoPallet, m) == 0) {
                    return 0;
                }
            }
		}
	}
    // Non ho trovato alcuna postazione di deposito del pallet, allora lo posiziono in magazzino
	if (DBDataModule->CercaAliasMagazzino(p.Alias, p.ID_TipoPallet, nmag, npos, p.Extra_Altezza) == 0) {
		m.Tipo_deposito = TPMISS_MAG;
		m.N_magazzino_dep = nmag;
		m.N_locazione_magazzino_dep = npos;
		m.Tipo_pinza = DBDataModule->TipoPinza(p.ID_TipoPallet);
		return 0;
	}
	//Cerco dove depositare il pallet in quale posizine di un magazzino
	if (DBDataModule->CercaDestinazioneMagazzino(p.ID_TipoPallet, nmag, npos, 0, p.Extra_Altezza) == 0) {
		m.Tipo_deposito = TPMISS_MAG;
    	m.N_magazzino_dep = nmag;
    	m.N_locazione_magazzino_dep = npos;
    	m.Tipo_pinza = DBDataModule->TipoPinza(p.ID_TipoPallet);
    	return 0;
	}
	DBDataModule->Segnalazione(1002, "Nessuna destinazione disponibile", 0);
	return -1;
}
//---------------------------------------------------------------------------

int TEventsDM::CercaMacchinaGruppo(int gruppo, AnsiString tmacc, int ID_TipoPallet) {
	UINT i;
	int nmacc, ID_Pallet, npos, res = 0;
	bool bOkDeposito = false;
	AnsiString tabsql, errmess;
	TRecordList TabMacchine;

	tabsql.printf("Macchine WHERE Tipo_Macchina = '%s' AND Gruppo = %d", tmacc.c_str(), gruppo);
	DBDataModule->CaricaTabella(tabsql, TabMacchine);
	for (i = 0; i < TabMacchine.size() && (res == 0); i++) {
		ID_Pallet = TabMacchine[i]["ID_Pallet"].ToIntDef(0);
		nmacc = TabMacchine[i]["ID_Macchina"].ToIntDef(0);

		npos = DBDataModule->PosizioneMacchina(tmacc, nmacc, ID_TipoPallet);
        if (tmacc == "M") {
            bOkDeposito = ((npos > 0) && OKDepositoMacchina(nmacc, errmess));
        }
        else if (tmacc == "S") {
			bOkDeposito = ((npos > 0) && OKDepositoInBaia(nmacc));
		}

		if ((ID_Pallet == 0) && bOkDeposito &&
			(!(DBDataModule->EsisteGiaJob("DROP", tmacc, nmacc)) || !(DBDataModule->EsisteGiaMissione("DROP", tmacc, nmacc)))) {
			res = nmacc;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

int TEventsDM::CercaBaiaDeposito(int ID_TipoPallet) {
	int npos, res = 0, i;

    //Ciclo sul numero di stazioni dell'impianto
	for (i = 1; i <= N_STAZIONI_OP && !res; i++) {
    	//Posizione tipo pallet 
		npos = DBDataModule->PosizioneMacchina("S", i, ID_TipoPallet);

		// Esiste posizione per questo tipo pallet in baia 'i', ho il consenso per il deposito e non ci sono giá missioni
		if ((npos > 0) && (OKDepositoInBaia(i)) &&
        	(!(DBDataModule->EsisteGiaJob("DROP", "S", i)) || !(DBDataModule->EsisteGiaMissione("DROP", "S", i)))) {
			res = i;
		}
	}
	return res;
}
//-----------------------------------------------------------------------

int TEventsDM::CercaBaiaPrelievo(int ID_Pallet) {
	int npos, res = 0, i;
	TPallet p;

	for (i = 1; (i <= N_STAZIONI_OP) && (res == 0); i++) {
		DBDataModule->LeggiPalletInMacchina(i, "S", p);
		// Esiste posizione per questo tipo pallet in baia 'i' e ho il consenso per il deposito
		npos = DBDataModule->PosizioneMacchina("S", i, p.ID_TipoPallet);
		if ((npos > 0) && (p.ID_Pallet == ID_Pallet)) {
			res = i;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

int TEventsDM::CercaPrelievo(TMissione &m) {
	int nmag, npos;

	// OK cerco pallet richiesto in magazzino
	if (DBDataModule->CercaPalletMagazzino(m.N_pallet, nmag, npos) == 0) {
		m.Tipo_prelievo = TPMISS_MAG;
		m.N_magazzino_pre = nmag;
		m.N_locazione_magazzino_pre = npos;
		return 0;
	}
	return -1;
}
//---------------------------------------------------------------------------

int TEventsDM::CercaPalletInBaia(TMissione &m , int NPallet)
{
	TPallet p, pb;
	int i, npos = -1;

    DBDataModule->LeggiPallet(NPallet, pb);
    if (pb.ID_Pallet > 0) {
        for (i = 1; i <= N_STAZIONI_OP; i++) {
            npos = DBDataModule->PosizioneMacchina("S", i, pb.ID_TipoPallet);
            DBDataModule->LeggiPalletInMacchina(i, "S", p);
            if ((npos > 0) && (p.ID_Pallet == pb.ID_Pallet) && (OKPrelievoInBaia(i))) {
                m.Tipo_prelievo = TPMISS_BAIA;
                m.N_staz_operatore_pre = i;
                m.Tipo_pinza = DBDataModule->TipoPinza(p.ID_TipoPallet);
                m.Pos_staz_operatore_pre = npos;
                return i;
            }
        }
	}
	return -1;
}
//---------------------------------------------------------------------------

int TEventsDM::GeneraMissione() {
	UINT i;
	int nmacc, nmiss = 0, id_pallet = 0, id_pezzo = 0, adattatore_in_macchina = 0;
	AnsiString tmacc, errmess;
	AnsiString Programma;
	TPallet p, pm, pr;
	TMissione m;

	// Nessun'altra missione, automatico, running e home
	if (!AttesaMissione())
		return 0;

	// Leggo dal DB il pallet sul robot
	DBDataModule->LeggiPalletInMacchina(7, "R", pr);

    //Pinza pallet
    if (Fanuc[0]->R[12] == GRIP_PALLET) {
    	//Controllo presenza pallet in pinza
        if ((!Fanuc[0]->IsPalletPresent() && (pr.ID_Pallet != 0)) ||
            (Fanuc[0]->IsPalletPresent() && (pr.ID_Pallet == 0))) {
            DBDataModule->Segnalazione(1001, "Errore stato presenza pallet su robot", 1);
            return -1;
        }
    }

	// Se ho il pallet a bordo devo fare una missione di solo deposito
	if (pr.ID_Pallet != 0) {
		for (i = 0; i < TabMissioni.size(); i++) {
			if (TabMissioni[i]["Tipo_Missione"] == "DROP") {
				nmiss = TabMissioni[i]["ID_Missione"].ToIntDef(0);
				// Verifico se è una missione per il pallet che ho sopra al robot
				if (TabMissioni[i]["ID_Pallet"].ToIntDef(0) == pr.ID_Pallet) {
                    memset(&m, 0, sizeof(m));
					if (GeneraMissioneDeposito(i, pr.ID_TipoPallet, m) == 0) {
						// OK, genero missione di solo deposito
						Fanuc[0]->InviaMissione(m);
						DBDataModule->CommentoMissione(nmiss, "OK");
						return 0;
					}
				}
			}
		}
		// Se arrivo qua vuol dire che non ho missioni per il pallet che ho sul robot
		memset(&m, 0, sizeof(m));
		m.N_pallet = pr.ID_Pallet;
		if (CercaDeposito(m) == 0) {
			// Genero missione solo deposito in magazzino
			m.Tipo_pinza = DBDataModule->TipoPinza(pr.ID_TipoPallet);
			Fanuc[0]->InviaMissione(m);
			DBDataModule->CambiaStatoPostazione(m.N_magazzino_dep, m.N_locazione_magazzino_dep, POS_PRENOTATA_DEPOSITO);
			return 0;
		}
		else if (nmiss) {
			DBDataModule->CommentoMissione(nmiss, "Nessuna destinazione disponibile");
		}
	}
	else {
		// Se arrivo qui significa che il robot è vuoto: in questo caso faccio una missione di prelievo e deposito (normale)
		// 1) Verifico se esiste una missione prelievo (MU o BAIA)
		for (i = 0; i < TabMissioni.size(); i++) {
			// Se esiste controllo la posizione di prelievo
			if (TabMissioni[i]["Tipo_Missione"] == "PICK") {
				memset(&m, 0, sizeof(m));
				id_pallet = TabMissioni[i]["ID_Pallet"].ToIntDef(0);
				if (id_pallet != 0) {
					DBDataModule->LeggiPallet(id_pallet, p);
					nmacc = TabMissioni[i]["ID_Macchina"].ToIntDef(0);
					tmacc = TabMissioni[i]["Tipo_Macchina"];
					nmiss = TabMissioni[i]["ID_Missione"].ToIntDef(0);

                    if (nmacc == 0) {
                        // C'è un pallet diverso da quello da prelevare
                        DBDataModule->CommentoMissione(nmiss, "Machine number absent");
                        continue;
                    }

                    //Leggo il pallet in macchina da prelevare
                    if (DBDataModule->LeggiPalletInMacchina(nmacc, tmacc, pm) != 0) {
                        // In baia c'è un pallet diverso da quello da prelevare
                        DBDataModule->CommentoMissione(nmiss, "Reading pallet " + IntToStr(id_pallet) + " wrong!");
                        continue;
                    }
                    
					// Prelievo da macchina
					if (tmacc == "M") {
                    	//Controllo se devo prelevare l'adattatore
						if (p.ID_Pallet > 10000) {
                            //Controllo se ho l'adattarore in macchina
                            adattatore_in_macchina = DBDataModule->LeggiAdattatoreInMacchina(tmacc, nmacc);

                        	//Controllo se l'adattatore da prelevare é quello richiesto
							if (adattatore_in_macchina == 0) {
                                // C'è un pallet diverso da quello da prelevare
                                DBDataModule->CommentoMissione(nmiss, "nessun adattatore presente in macchina");
                                continue;
                            }

                        	//Controllo se l'adattatore da prelevare é quello richiesto
                            if (p.ID_Pallet != adattatore_in_macchina) {
                                // C'è un pallet diverso da quello da prelevare
                                DBDataModule->CommentoMissione(nmiss, "é presente un adattatore diverso da quello richiesto");
                                continue;
                            }
                        }
                        else {
                            //Controllo se è lo stesso numero pallet
                            if (pm.ID_Pallet != p.ID_Pallet) {
                                // C'è un pallet diverso da quello da prelevare
                                DBDataModule->CommentoMissione(nmiss, "é presente un pallet diverso da quello richiesto");
                                continue;
                            }
                        }

                        // Verifico se ho l'ok prelievo in macchina
                        if (!OKPrelievoMacchina(nmacc, errmess)) {
                            // non ho ok prelievo in macchina
                            DBDataModule->CommentoMissione(nmiss, errmess);
                            continue;
                        }

                        m.Tipo_prelievo = TPMISS_MU;
                        m.N_MU_pre = nmacc;
                        m.Pos_MU_pre = DBDataModule->PosizioneMacchina(tmacc, nmacc, p.ID_TipoPallet);
					}
					else if (tmacc == "S") {
                        //Controllo se è lo stesso numero pallet
                        if (pm.ID_Pallet != p.ID_Pallet) {
                            // C'è un pallet diverso da quello da prelevare
							DBDataModule->CommentoMissione(nmiss, "é presente un pallet diverso da quello richiesto");
                            continue;
                        }

						// Prelievo da stazione operatore cerco stazione giusta
						nmacc = CercaBaiaPrelievo(p.ID_Pallet);
						if (nmacc == 0) {
							DBDataModule->CommentoMissione(nmiss, "Pallet not found in bay");
							continue;
						}

						// Verifico se ho l'ok prelievo in baia
						if (!OKPrelievoInBaia(nmacc)) {
							// non ho ok prelievo in baia
							DBDataModule->CommentoMissione(nmiss, "OK pick miss from station");
							continue;
						}

						m.Tipo_prelievo = TPMISS_BAIA;
						m.N_staz_operatore_pre = nmacc;
                        m.Pos_staz_operatore_pre = DBDataModule->PosizioneMacchina(tmacc, nmacc, p.ID_TipoPallet);
					}

					// Adesso cerco la postazione a magazzino dove depositare il pallet
					m.Tipo_pinza = DBDataModule->TipoPinza(p.ID_TipoPallet);
					m.N_pallet = p.ID_Pallet;
					if (CercaDeposito(m) == 0) {
						Fanuc[0]->InviaMissione(m);
						DBDataModule->CommentoMissione(nmiss, "OK");
						return 0;
					}
					else {
						DBDataModule->CommentoMissione(nmiss, "No position free");
					}
				}
			}
		}
		// Altrimenti verifico se esiste una missione deposito (MU o BAIA) o pulizia
		for (i = 0; i < TabMissioni.size(); i++) {
			if (TabMissioni[i]["Tipo_Missione"] == "DROP") {
				id_pallet = TabMissioni[i]["ID_Pallet"].ToIntDef(0);
				if (id_pallet != 0) {
					memset(&m, 0, sizeof(m));
					DBDataModule->LeggiPallet(id_pallet, p);
					if (GeneraMissioneDeposito(i, p.ID_TipoPallet, m) == 0) {
						nmiss = TabMissioni[i]["ID_Missione"].ToIntDef(0);
						// OK cerco pallet richiesto in magazzino
                        if (CercaPrelievo(m) == 0) {
                            Fanuc[0]->InviaMissione(m);
                            DBDataModule->CommentoMissione(nmiss, "OK");
                            DBDataModule->CambiaStatoPostazione(m.N_magazzino_pre, m.N_locazione_magazzino_pre, POS_PRENOTATA_PRELIEVO);
                            return 0;
                        }
						else if (CercaPalletInBaia(m, id_pallet) > 0) {
                            Fanuc[0]->InviaMissione(m);
                            DBDataModule->CommentoMissione(nmiss, "OK");
                            return 0;
						}
                        else {
                            DBDataModule->CommentoMissione(nmiss, "Pallet not finded");
                        }
					}
				}
			}
		}
	}
	return -4;
}
//---------------------------------------------------------------------------

int TEventsDM::GeneraMissioneDeposito(int IndexRecordList, int ID_TipoPallet, TMissione &m) {
	TPallet pm;
	int idplt, nmaccgruppo, nmag, npos, nposmacc, nmacc, nmiss, adattatore_richiesto, adattatore_in_macchina;
	AnsiString tmacc, errmess;

	// Prima controllo che dalla macchina sia stato tolto il pallet precedente
	nmacc = TabMissioni[IndexRecordList]["ID_Macchina"].ToIntDef(0);
	tmacc = TabMissioni[IndexRecordList]["Tipo_Macchina"];
	nmiss = TabMissioni[IndexRecordList]["ID_Missione"].ToIntDef(0);
    idplt = TabMissioni[IndexRecordList]["ID_Pallet"].ToIntDef(0);

    //Controllo che se devo depositare in macchina l'attrezzaggio sia montato in macchina
	if (tmacc == "M") {
		if (nmacc > 1000) {
			nmaccgruppo = CercaMacchinaGruppo(nmacc, tmacc, ID_TipoPallet);
			if (nmaccgruppo) {
				nmacc = nmaccgruppo;
				DBDataModule->AggiornaCampoTabella("Missioni", "ID_Missione", nmiss, "ID_Macchina", nmacc, false);
			}
			else {
				// Pallet già presente in macchina, guardo altra missione
				DBDataModule->CommentoMissione(nmiss, "Machine not ready for drop");
				return 1;
			}
		}

        //Controllo se in macchina è presente un altro pallet
		DBDataModule->LeggiPalletInMacchina(nmacc, tmacc, pm);
        if (pm.ID_Pallet > 0) {
            // Pallet già presente in macchina, guardo altra missione
            DBDataModule->CommentoMissione(nmiss, "In the machine is present another pallet");
            return 1;
        }

		// Nessun pallet in macchina!!! Verifico se posso depositare in macchina o é richiesto l'adattatore
		adattatore_richiesto = DBDataModule->AdattatoreRichiesto("M", nmacc, ID_TipoPallet);
		adattatore_in_macchina = DBDataModule->LeggiAdattatoreInMacchina("M", nmacc);
        //Controllo se in macchina ho un adattatore ed è diverso da quello richiesto
        if (adattatore_richiesto != adattatore_in_macchina) {
            // Adattatore pallet richiesto
			if (adattatore_in_macchina) {
				if (OKPrelievoMacchina(nmacc, errmess)) {
					//Controllo se posso prelevare l'adattatore dalla macchina utensile
					//Invio missione al robot di prelevare l'adattatore dalla macchina
					Fanuc[0]->SendMission(TPM_PRELIEVO_MACCHINA, 0 /* Pos Mag */, 0 /* N Mag */, 0 /* Op Stat */, GRIP_PALLET /* pinza */, nmacc /* N MU */, 1 /* Pos */, adattatore_in_macchina /* ID pallet */);
					//Commento Missione
					DBDataModule->CommentoMissione(nmiss, "OK Prelievo Adattatore");
					//Log
					DBDataModule->Log("SERVER", "EVENT", "MU%d: Invio missione Robot prelievo adattatore. Missione %d", nmacc, nmiss);
					return 1;
				}
                else {
					//Commento Missione
					DBDataModule->CommentoMissione(nmiss, errmess);
					return 1;
               }
            }
			else if (DBDataModule->CercaPalletMagazzino(adattatore_richiesto, nmag, npos) == 0) {
                // Ho trovato l'adattatore, genero una missione di deposito in macchina e prelievo a magazzino
                DBDataModule->CreaMissioneDepositoMacchina(adattatore_richiesto, "M", nmacc, 99);

                //Vecchia gestione per depositare l'adattatore in macchina utensile prima di depositare il pallet
//				DBDataModule->CambiaStatoPostazione(nmag, npos, POS_PRENOTATA_PRELIEVO);
//				Fanuc[0]->SendMission(TPM_PRELIEVO_MAGAZZINO, npos /* Pos Mag */, nmag /* N Mag */, 0 /* Op Stat */, 1 /* pinza */, 0 /* N MU */, 0 /* Pos */, adattatore_richiesto /* ID pallet */);

				//In questo modo uscendo dalla funzione viene richiamata la "CercaPrelievo", in modo da
                //prelevare l'adattatore e depositarlo in macchina
				m.Tipo_pinza = GRIP_PALLET;
				m.N_pallet = adattatore_richiesto;
                return 0;
            }
        }

        //Controllo se ho un adattatore in macchina e un altro da depositare
        //Arrivato qui dovrebbe già essere stato prelevato nella parte di codice sopra.
        if ((idplt > 10000) && (adattatore_in_macchina > 0)) {
            // Adattatore già presente in macchina, guardo altra missione
            DBDataModule->CommentoMissione(nmiss, "Altro adattatore già presente in macchina");
            return 1;
        }

        // Controllo anche che non sia presente un adattatore
        if (adattatore_richiesto != adattatore_in_macchina) {
            // Adattatore presente in macchina diverso da quello richiesto, guardo altra missione
            DBDataModule->CommentoMissione(nmiss, "Adattatore presente in macchina diverso da quello richiesto");
            return 1;
        }

        //Controllo se ho l'ok al deposito dalla macchina utensile
        if (!OKDepositoMacchina(nmacc, errmess)) {
            DBDataModule->CommentoMissione(nmiss, errmess);
            return 1;
        }

        //Controllo che per quel tipo pallet ci sia la posizione disponibile in macchina
        nposmacc = DBDataModule->PosizioneMacchina(tmacc, nmacc, ID_TipoPallet);
        if (!nposmacc) {
            DBDataModule->CommentoMissione(nmiss, "Machine position not avaible!");
            return 1;
        }

		m.N_pallet = idplt;
		m.Tipo_deposito = TPMISS_MU;
		m.Tipo_pinza = DBDataModule->TipoPinza(ID_TipoPallet);
		m.N_MU_dep = nmacc;
		m.Pos_MU_dep = nposmacc;
        return 0;
	}
    else if (tmacc == "S") {
		if (nmacc != 0) {
            if (nmacc > 1000) {
                nmaccgruppo = CercaMacchinaGruppo(nmacc, tmacc, ID_TipoPallet);
                if (nmaccgruppo) {
                    nmacc = nmaccgruppo;
                    DBDataModule->AggiornaCampoTabella("Missioni", "ID_Missione", nmiss, "ID_Macchina", nmacc, false);
                }
                else {
                    // Pallet già presente in macchina, guardo altra missione
                    DBDataModule->CommentoMissione(nmiss, "Machine not ready for drop");
                    return 1;
                }
            }
            else {
                npos = nmacc;
                if (!OKDepositoInBaia(npos)) {
                    // Non ho OK DEPOSITO in baia quindi non eseguo nemmeno il prelievo
                    DBDataModule->CommentoMissione(nmiss, "Ok drop miss from station");
                    return 1;
                }
            }
		}
		else {
			npos = CercaBaiaDeposito(ID_TipoPallet);
			if (npos == 0) {
				// Nessuna baia disponibile
				DBDataModule->CommentoMissione(nmiss, "Station not avaible");
				return 1;
			}
			else {
				//Aggiorno il numero della baia per tutte le missioni di quella fase
				DBDataModule->AggiornaBaieFasi(TabMissioni[IndexRecordList]["GruppoFase"], npos);
				// Aggiorno tabella missioni in memoria
				DBDataModule->CaricaTabella("Missioni ORDER BY Priorita DESC, Creazione, ID_Missione", TabMissioni);
			}
		}

        //Controllo che per quel tipo pallet ci sia la posizione disponibile in macchina
        nposmacc = DBDataModule->PosizioneMacchina(tmacc, npos, ID_TipoPallet);
        if (!nposmacc) {
            DBDataModule->CommentoMissione(nmiss, "Bay position not avaible!");
            return 1;

        }

		m.N_pallet = idplt;
		m.Tipo_deposito = TPMISS_BAIA;
		m.Tipo_pinza = DBDataModule->TipoPinza(ID_TipoPallet);
		m.N_staz_operatore_dep = npos;
        m.Pos_staz_operatore_dep = nposmacc;
		return 0;
	}
	else if (tmacc.ToIntDef(0) > 0) {
		// Missione a magazzino generata manualmente dall'operatore
		m.N_pallet = idplt;
		m.Tipo_deposito = TPMISS_MAG;
		m.Tipo_pinza = DBDataModule->TipoPinza(ID_TipoPallet);
		m.N_magazzino_dep = tmacc.ToIntDef(0);
		m.N_locazione_magazzino_dep = nmacc;
		return 0;
	}
	return 1;
}
//---------------------------------------------------------------------------

int TEventsDM::ScaricaMacchina(int MU)
{
	int stato;
	TPallet pm;

    //Leggo lo stato del check di abilitazione deposito del pallet in stazione una volta uscito della macchina utensile
    stato = DBDataModule->LeggiAbilitazioneParametro(PAR_DROP_PALLET_STATION_MU1);

	//Verifico se effettivamente ho un pallet da prelevare sulla tavola esterna
	DBDataModule->LeggiPalletInMacchina(MU, "M", pm);

    //Controllo che non ci siano già missioni di prelievo in corso per quel pallet
    if (stato && !DBDataModule->EsisteGiaJobPallet("DROP", "S", pm.ID_Pallet)) {
        //Creo Job di drop in stazione, se baia impegnata va a magazzino e appena
        //libera la baia preleva da magazzino verso baia
        DBDataModule->CreaJobDepositoMacchina(pm.ID_Pallet, "S", 0, 50);
    }

	//Creazione missione di prelievo dalla macchina
	DBDataModule->CreaJobPrelievoMacchina("M", MU, 50);

    //Log
	DBDataModule->Log("SERVER", "EVENT", "MU%d: Generato prelievo pallet %d", MU, pm.ID_Pallet);

	return 0;
}
//---------------------------------------------------------------------------

int TEventsDM::LeggiTestiEventiRobot()
{
	AnsiString Path, IniFileName, TestoEvento;
	std::string line;
    std::size_t found;
	UINT i;
	FILE *f;
	char s[256];
	int idevento, res = 0;;

	for (i = 0; i < 1000; i++) {
		EventiRobot_txt[0][i] = "";
	}

	for (i = 0; i < N_ROBOTS; i++) {
        Path = ExtractFilePath(Application->ExeName);
        IniFileName.sprintf("%sEventiRobot%d.txt", Path, i + 1);

        res = -2;
        try {
            f = fopen(IniFileName.c_str(), "r");
            if (f) {
                while (fgets(s, 256, f)) {
                    line = s;
                    found = line.find(";");
                    if (found != std::string::npos) {
						//Ricavo la variabile da leggere/scrivere
						idevento = atoi(line.substr(0, found).c_str());

                        //Ricavo la dimensione dell'area di memoria da andare a leggere/scrivere
						found++;
						TestoEvento = line.substr(found, (line.length() - 1) - found).c_str();

						EventiRobot_txt[0][idevento] = TestoEvento;
					}
                }
                fclose(f);
                res = 0;
            }
        }
        catch (...) {
            res = -1;
        }
    }
	return res;
}
//---------------------------------------------------------------------------

int TEventsDM::LeggiTestiAllarmiRobot()
{
	AnsiString Path, IniFileName, TestoAllarme;
	std::string line;
    std::size_t found;
	UINT i;
	FILE *f;
	char s[256];
	int idallarme, res = 0;

	for (i = 0; i < 1000; i++) {
		AllarmiRobot_txt[0][i] = "";
	}

	for (i = 0; i < N_ROBOTS; i++) {
		Path = ExtractFilePath(Application->ExeName);
        IniFileName.sprintf("%sAllarmiRobot%d.txt", Path, i + 1);

        res = -2;
        try {
            f = fopen(IniFileName.c_str(), "r");
            if (f) {
                while (fgets(s, 256, f)) {
                    line = s;
                    found = line.find(";");
                    if (found != std::string::npos) {

						//Ricavo la variabile da leggere/scrivere
						idallarme = atoi(line.substr(0, found).c_str());

                        //Ricavo la dimensione dell'area di memoria da andare a leggere/scrivere
						found++;
						TestoAllarme = line.substr(found, (line.length() - 1) - found).c_str();

						AllarmiRobot_txt[0][idallarme] = TestoAllarme;
					}
                }
                fclose(f);
                res = 0;
            }
        }
        catch (...) {
            res = -1;
        }
    }
	return res;
}
//---------------------------------------------------------------------------

void TEventsDM::LogEventoRobot(int iNumRobot, int n)
{
    AnsiString sDescr;

	if ((n <= 0) || (n > 999)) {
		return;
	}
	sDescr = EventiRobot_txt[iNumRobot - 1][n];
	if (sDescr.IsEmpty()) {
		sDescr.printf("%d: Testo evento mancante", n);
	}
	//Evento Robot
	DBDataModule->EventoRobot("ROBOT" + IntToStr(iNumRobot), "EVENTO", "%d: %s", n, sDescr.c_str());
}
//---------------------------------------------------------------------------

void TEventsDM::LogAllarmeRobot(int iNumRobot, int n)
{
    AnsiString sDescr;

	if ((n <= 0) || (n > 999)) {
		return;
	}
	sDescr = AllarmiRobot_txt[iNumRobot - 1][n];
	if (sDescr.IsEmpty()) {
		sDescr.printf("%d: Testo allarme mancante", n);
	}
	//Evento Robot
    DBDataModule->EventoRobot("ROBOT" + IntToStr(iNumRobot), "ALLARME", "%d: %s", n, sDescr.c_str());
}
//---------------------------------------------------------------------------


