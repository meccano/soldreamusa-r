//	****************************************************************************
//				Sintesi S.r.l.
//  			Viale Virgilio, 58/H - 41100 MODENA - ITALY
//				Tel. +39-59-847070
//				Fax. +39-59-848728
//
//  			Tutti i diritti sono riservati.
//				Ogni copia od utilizzo anche parziale delle informazioni
//				contenute in questo documento per scopi diversi da quelli
//				previsti nella licenza d'uso e' vietato e protetto dalle leggi
//				sulla regolamentazione del diritto d'autore.
//				Ogni modifica al contenuto di questo file, se non autorizzata e
//				suggerita da Sintesi, potrebbe causare malfunzionamenti al
//				programma ed e' compiuta sotto la completa responsabilita'
//				dell'utente.
//
// *****************************************************************************
//	MODULE NAME		: IDTYPES25.H
// IDCOMM VERSION	: 2.0
//	DESCRIPTION		: IDCOMM PUBLIC HEADER FILE
//
//  ****************************************************************************

#ifndef __WIDTYPES25_H
#define __WIDTYPES25_H

#if !defined(_WIN32_WINNT)
#define _WIN32_WINNT 0x0400
#endif
#include <windows.h>
#include <time.h>

typedef struct __DEVICE {					// Logical Device Structure
   struct __ELEM *	idList;				// testa della lista degli id sul device
   struct __ELEM *	idTail; 				// coda della lista degli id sul device
	HANDLE   mutex;				  			// reserved
	LPSTR 	devName;							// phisical device name ("COM1", "COM2", ...)
	LPSTR 	devGroup;						// device group used to manage twin device
	WORD 	   devType;							// device type (SERIAL, NETWORK, ...)
	LPVOID 	device;							// device structure pointer (SERDEVICE *,NETDEVICE *,...)
	struct __DEVICE	*	twinDevice; 	// twin device structure pointer
	WORD 		status;							// status flags
	WORD 		error;							// error
} DEVICE;

typedef	DEVICE *	PDEVICE;

typedef struct __ID {						// Industrial Device Structure
	PDEVICE	device;						// Pointer to Logical Device
	struct	__PROTOCOL	*	protocol;	// Pointer to Protocol structure in use
	WORD	   model;							// Id Model
	SHORT		waitTime;						// Number of seconds during which the ID is paused after a comunication error
	time_t	resumeTime;						// Current resume time after a comunication error
	LPVOID	params;							// Pointer to current ID parameter structure
	BYTE		mode;								// Working ID mode (NORMAL, DEMO, SUSPENDED ...)
	BYTE		status;							// ID internal status (OK, ERROR ... )
	DWORD		daemonThread;					// Protocol-Level Thread Handle
   BOOL		daemonAlive;					// Protocol-Level Thread Status
   BOOL		modemConnection;				// Connection via modem
	WORD     modemConnectionType;			// MODEM_CALL o MODEM_ANSWER
   LPSTR		modemInitString;				// Modem initialization string
   LPSTR		modemTelNumber;				// Telefon Number to dial
   WORD		numTags;							// Not used in 2.0 version - Only for 1.0 Compatibility
   struct	__IDTAG		** tagPool;		// Not used in 2.0 version - Only for 1.0 Compatibility
   struct __ELEM *	tagList;			  	// testa della lista dei tag sull'Id
   struct __ELEM *	tagTail;			  	// coda della lista dei tag sull'Id
} ID;

typedef	ID *	PID;

typedef struct __IDTAG {		// Tag Structure
	ID			*	id;				// Pointer to ID
	LPVOID	address;				// Pointer to address specific structure
	WORD		conversion;			// Protocol specific conversion for this Tag
	ULONG		readIdleTime;		// Minimum idle time between two consecutive read (during this time all read return the last value)
   ULONG 	readNextTime;		// Next time to execute a read
	LPVOID 	imgProcess;			// Pointer to image process area holding the last Tag value
	ULONG		imgSize;				// Tag size in bytes
	WORD		unitSize;			// Tag's unit size in bytes
	WORD		repetition;			// Number of elements whose type is referenced in address.

} IDTAG;

typedef	IDTAG *	PTAG;

//
//	Level 2 (Protocol) Function Structure
//
// ##FG 14/05/2004. Aggiunta TagOn per avere l'ID che nella SetTag non c'e'
typedef struct __PROTOCOL {
	HMODULE	hProtocol;														// DLL Protcol Handle
	BOOL	(*	On)( ID *, PSHORT );											// Specific Protocol "On" function
	BOOL	(*	Init)( ID *, LPSTR, PSHORT );								// Specific Protocol "Init" function
	VOID	(* FreeParams)( LPVOID );										// Specific Protocol "FreeParams" function
	VOID	(* FreeAddress)( LPVOID );										// Specific Protocol "FreeAddress" function
	BOOL	(* SetTag)( LPVOID *, PSHORT, LPSTR, WORD, PSHORT);	// Specific Protocol "SetTag" function
	BOOL	(* ReadTag)( IDTAG *, PSHORT);								// Specific Protocol "ReadTag" function
	BOOL	(* WriteTag)( IDTAG *, PSHORT);								// Specific Protocol "WriteTag" function
	BOOL	(*	Off)( ID *, PSHORT );										// Specific Protocol "Off" function
   DWORD	(* Daemon)( LPDWORD param );									// Protocol Background Function
	BOOL	(* TagOn)( ID *, LPVOID *, PSHORT, LPSTR, WORD, PSHORT);	// Specific Protocol "TagOn" function
} PROTOCOL;

// *****************************************************************************
// 	LIST SECTION: Pointers to Devices, Ids and Tags.
// *****************************************************************************
typedef struct __ELEM {
	struct	__ELEM	*	next;				// Pointer to next element in List
	union {
   	LPVOID	item;			 				// Dev, Id o Tag pointer
		DEVICE	*	dev;					   // Device element
		ID			*	id;						// ID element
		IDTAG		*	tag;						// TAG element
	} ref;
} ELEM;

typedef	ELEM	*	PELEM;

#define	pId	ref.id
#define	pDev	ref.dev
#define	pTag	ref.tag
#define  pItem ref.item

// *****************************************************************************
// DEBUG MESSAGE TYPE CONSTANTS
// *****************************************************************************
#define	DMSG_WARNING		1
#define	DMSG_ERROR			2
#define	DMSG_INFORMATION	3
#define	DMSG_RED				4
#define	DMSG_CYAN			5
#define	DMSG_GRAY			6
#define	DMSG_GREEN			7
#define	DMSG_BLUE			8
#define	DMSG_WHITE			9
#define	DMSG_YELLOW			10
#define	DMSG_PLUS			11
#define	DMSG_MINUS			12
#define	DMSG_REDCROSS		13
#define	DMSG_BLUEARROW		14
#define	DMSG_GREENCONF		15
#define	DMSG_REDCONF  		16

// *****************************************************************************
// ERROR CODE SECTION
// *****************************************************************************
#define	ERR_NONE							0
#define	ERR_INTERNAL					1
#define	ERR_IDCOMMBADINSTALL			2
#define	ERR_IDCOMMSERIAL 				3
#define	ERR_IDCOMMBADPROTINSTALL	4
#define 	ERR_EVALMODE					5

#define	ERR_DEV_INGROUPEXCEED		100
#define	ERR_DEV_ALREADYDEFINED		101
#define	ERR_DEV_INVALIDNAME			102
#define	ERR_DEV_CANNOTCREATE			103
#define	ERR_DEV_RESET		 			104
#define	ERR_DEV_BADPARAM		 		105
#define	ERR_DEV_TIMEOUT	 			106
#define	ERR_DEV_INVALID	 			107
#define	ERR_DEV_CLOSE			 		108
#define	ERR_DEV_CANTCONTROL			109
#define  ERR_MODEM_BUSY					110
#define  ERR_MODEM_NOCARRIER			111
#define  ERR_MODEM_TIMEOUT				112
#define	ERR_DAEMON_CREATE				113

#define	ERR_PROT_INSMEM				200
#define	ERR_PROT_BADADDRESS			201
#define	ERR_PROT_TIMEOUT				202
#define	ERR_PROT_BADCKSM				203
#define	ERR_PROT_BADPARAM				204
#define 	ERR_PROT_DEVACCESS			205
#define 	ERR_PROT_BADANSWER			206
#define 	ERR_PROT_BADMODEL				207
#define	ERR_PROT_DATAOUTOFRANGE		208
#define	ERR_PROT_BADREQUEST	  		209
#define 	ERR_PROT_ADDROUTOFBOUNDS	210
#define 	ERR_PROT_INVALIDOP			211
#define 	ERR_PROT_BADINIT				212

// OFFSET OF PROTOCOL-SPECIFIC ERROR CODE
#define	ERR_SPECIFIC					250

#define	ERR_DEVICEON 	 				400
#define	ERR_DEVICEINIT					401
#define	ERR_DEVICEOFF	 				402
#define	ERR_IDON			 				403
#define	ERR_IDINIT		 				404
#define	ERR_IDOFF		 				405
#define	ERR_TAGON		 				406
#define	ERR_IDREADTAG					407
#define	ERR_IDWRITETAG					408
#define	ERR_IDPEEKTAG					409
#define	ERR_IDSETTAG		 			410
#define  ERR_TAGUNITSIZE				411
#define	ERR_DEVICEIOCTL	 			412
#define	ERR_TAGOFF			 			413
#define	ERR_READTAG						414
#define	ERR_WRITETAG					415
#define	ERR_PEEKTAG						416
#define	ERR_IDMODEMINIT				417
#define	ERR_REGISTERIDCOMMBASE		418
#define	ERR_REGISTERIDCOMMPROTOCOL	419

#define 	ERR_WAITFORRESUME				450
#define	ERR_INSMEM						452
#define	ERR_CANTFINDTAG				453
#define	ERR_BADREF						459
#define	ERR_INVALIDPROTOCOL			460
#define	ERR_INVALIDPROTOCOLVERSION 461
#define 	ERR_MUTEX_ALLOC				462
#define 	ERR_BADTAGIDX					463
#define 	ERR_BADTAGPOOL					464
#define 	ERR_CANNOTREDEFINETAG		465
#define 	ERR_CANNOTCREATETAG			466
#define 	ERR_MUTEXTIMEOUT				467
#define 	ERR_MUTEXFAILED				468
#define 	ERR_MUTEXABANDONED			469
#define	ERR_CANTCLOSEID				470
#define	ERR_CANTCLOSETAG				471

// *****************************************************************************
// 	ID OPERATION MODE CODE SECTION
// *****************************************************************************
#define NORMALMODE						0x01
#define DEMOMODE							0x02
#define SUSPENDEDMODE					0x03

// *****************************************************************************
// 	CALLER SECTION
// *****************************************************************************
#define IDC_IDCOMM_CALLER				0x01
#define IDC_SCADA_CALLER				0x02

// *****************************************************************************
//	DEVICE CONTROL OPERATION CODES FOR DevIoctl()
// *****************************************************************************
#define DEV_RESET						1
#define DEV_SET_PARAMS				2
#define DEV_SET_TIMEOUTS			3
#define DEV_IQ_DRAIN					4
#define DEV_SET_MODEM_TIMEOUTS   5
#define DEV_USE_MODEM_TIMEOUTS   6
#define DEV_USE_TIMEOUTS   		7
#define DEV_SER_BASE					100
#define DEV_NET_BASE					200
#define DEV_NUL_BASE					300

#define NET_RESET						DEV_NET_BASE + 1
#define NET_SET_PARAMS				DEV_SET_PARAMS

#define NUL_SET_PARAMS				DEV_NET_BASE + 1
#define NUL_GET_PARAMS				DEV_NET_BASE + 2

#define SER_RESET						DEV_RESET
#define SER_SET_PARAMS				DEV_SET_PARAMS
#define SER_SET_TIMEOUTS			DEV_SET_TIMEOUTS
#define SER_SET_MODEM_TIMEOUTS	DEV_SET_MODEM_TIMEOUTS
#define SER_USE_MODEM_TIMEOUTS	DEV_USE_MODEM_TIMEOUTS
#define SER_USE_TIMEOUTS			DEV_USE_TIMEOUTS
#define SER_IQ_DRAIN					DEV_IQ_DRAIN
#define SER_SET_DTR					DEV_SER_BASE + 1
#define SER_CLR_DTR					DEV_SER_BASE + 2
#define SER_SET_RTS					DEV_SER_BASE + 3
#define SER_CLR_RTS					DEV_SER_BASE + 4
#define SER_RTS_ENABLE 				DEV_SER_BASE + 5
#define SER_RTS_DISABLE				DEV_SER_BASE + 6
#define SER_RTS_HANDSHAKE			DEV_SER_BASE + 7
#define SER_RTS_TOGGLE				DEV_SER_BASE + 8
#define SER_DTR_ENABLE 				DEV_SER_BASE + 9
#define SER_DTR_DISABLE				DEV_SER_BASE + 10
#define SER_DTR_HANDSHAKE			DEV_SER_BASE + 11
#define SER_SET_BREAK				DEV_SER_BASE + 12
#define SER_CLR_BREAK				DEV_SER_BASE + 13
#define SER_OQ_DRAIN					DEV_SER_BASE + 14
#define SER_DSR_MONITOR				DEV_SER_BASE + 15
#define SER_CTS_MONITOR				DEV_SER_BASE + 16
#define SER_OQ_FLUSH					DEV_SER_BASE + 17
#define SER_SET_XONXOFF				DEV_SER_BASE + 18
#define SER_CLR_XONXOFF				DEV_SER_BASE + 19
#define SER_GET_MODEM_STATUS     DEV_SER_BASE + 20
#define SER_MODEM_TEST_CD        DEV_SER_BASE + 21



// *****************************************************************************
//	COMMON CONTROL CHARACTERS CODES
// *****************************************************************************
#define	SOH	0x01
#define	STX	0x02
#define	ETX	0x03
#define	EOT	0x04
#define	ENQ	0x05
#define	ACK	0x06
#define	BEL	0x07
#define	BS 	0x08
#define	HT 	0x09
#define	LF 	0x0A
#define	VT 	0x0B
#define	FF 	0x0C
#define	CR 	0x0D
#define	SO 	0x0E
#define	SI 	0x0F
#define	DLE	0x10
#define	DC1	0x11
#define	DC2	0x12
#define	DC3	0x13
#define	DC4	0x14
#define	NAK	0x15
#define	SYN	0x16
#define	ETB	0x17
#define	CAN	0x18
#define	EM 	0x19
#define	SUB	0x1A
#define	ESC	0x1B
#define	FS 	0x1C
#define	GS 	0x1D
#define	RS 	0x1E
#define	US 	0x1F
#define	XONCHAR	0x11
#define	XOFFCHAR	0x13

// *****************************************************************************
//	TEST OPERATIONS CODES FOR DevTest()
// *****************************************************************************
#define TEST_IQSIZE		1	// Test Intput queue lenght
#define TEST_OQSIZE		2	// Test Output queue lenght
#define TEST_BREAK		3	// Test device break state (serial)


#endif	// __IDTYPES25_H

