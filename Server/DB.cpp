//---------------------------------------------------------------------------

#pragma hdrstop

#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <inifiles.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include "DB.h"
#include "LogTxt.h"
#include "inifiles.hpp"
#include "time.h"
#include "Events.h"
#include "Fanuc.h"
#include "Tools.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDBDataModule *DBDataModule;
AnsiString logstr;
//---------------------------------------------------------------------------

Variant ReadField(TADOQuery *q, AnsiString f)
{
	Variant res = "";

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->Value;
	return res;
}
//---------------------------------------------------------------------------

int ReadInt(TADOQuery *q, AnsiString f)
{
	int res = 0;

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->Value;
	return res;
}
//---------------------------------------------------------------------------

AnsiString ReadString(TADOQuery *q, AnsiString f)
{
	AnsiString res = "";

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = q->FieldByName(f)->AsString;
	return res;
}
//---------------------------------------------------------------------------

float ReadFloat(TADOQuery *q, AnsiString f)
{
	float res = 0;

	if (!(q->IsEmpty() || q->Eof || q->FieldByName(f)->IsNull))
		res = (float)q->FieldByName(f)->AsFloat;
	return res;
}
//---------------------------------------------------------------------------

__fastcall TDBDataModule::TDBDataModule(TComponent* Owner) : TDataModule(Owner)
{
	TIniFile * pIni;
	AnsiString ConnectionString;

	t = 0;
	pIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	ConnectionString = pIni->ReadString("Database", "ConnectionString", "");
	delete pIni;
	if (!ConnectionString.IsEmpty())
		ADOConnection1->ConnectionString = ConnectionString;
	TimerConnect->Enabled = true;
	TimerPuliziaStorici->Enabled = true;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaCampoTabella(AnsiString Tabella, AnsiString CampoChiave, int ValoreChiave, AnsiString CampoModifica, AnsiString ValoreModifica, bool isstring) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;
	AnsiString Val;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		if (isstring) {
			Val = "'" + ValoreModifica + "'";
		} else {
			Val = ValoreModifica;
		}
		strsql.printf("UPDATE %s SET %s = %s WHERE %s = %d",
			Tabella.c_str(),
			CampoModifica.c_str(),
			Val.c_str(),
			CampoChiave.c_str(),
			ValoreChiave
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CaricaTabellaQ(AnsiString Query, TRecordList &RecList)
{
	AnsiString campo;
	TADOQuery *ADOQuery;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = Query.c_str();
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[i][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {}
   	delete ADOQuery;
    return i;
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaTabella(AnsiString TableName, TRecordList &RecList) {
	AnsiString strsql, campo;
	TADOQuery *ADOQuery;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM %s", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[i][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {}
   	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CaricaTabellaK(AnsiString TableName, AnsiString KeyField, TIndexList &RecList) {
	AnsiString strsql, campo, chiave;
	TADOQuery *ADOQuery;
	int j, i = 0;

	RecList.clear();
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM %s", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		while (!ADOQuery->Eof) {
			for (j = 0; j < ADOQuery->FieldCount; j++) {
				chiave = ADOQuery->FieldByName(KeyField)->AsString;
				campo = ADOQuery->FieldList->Fields[j]->FieldName;
				RecList[chiave][campo] = ADOQuery->FieldList->Fields[j]->AsString;
			}
			ADOQuery->Next();
			i++;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

bool TDBDataModule::GetGruppoLavorazioneID(int Lav, AnsiString &ID)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	long i;
	bool bFound = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		for (i = 0; i < LONG_MAX && !bFound; i++) {
			ID.sprintf("1-%d-%ld", Lav, i);
			strsql.printf("SELECT TOP (1) * FROM Jobs WHERE GruppoLavorazione = '%s'", ID.c_str());
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			bFound = ADOQuery->Eof;
			ADOQuery->Close();
		}
	} catch(...) {};
	delete ADOQuery;

	return bFound;
}
//---------------------------------------------------------------------------

void __fastcall TDBDataModule::TimerPuliziaStoriciTimer(TObject *Sender)
{
	TDateTime adesso;
	WORD adessoY, adessoM, adessoD, Y, M, D;

	TimerPuliziaStorici->Enabled = false;
	try {
		adesso = Now();
		DecodeDate(adesso, adessoY, adessoM, adessoD);
		DecodeDate(t, Y, M, D);
		if ((adessoD != D) || (adessoM != M) || (adessoY != Y)) {
			// Cambio giorno
			PuliziaStorici();
			t = adesso;
		}
	} catch(...) {}
	TimerPuliziaStorici->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TDBDataModule::TimerConnectTimer(TObject *Sender)
{
	TimerConnect->Enabled = false;
	if (ADOConnection1->Connected) {
		if (!verificaConnessioneDB())
			DisconnettiDB();
	} else {
		ConnettiDB();
	}
	TimerConnect->Enabled = true;
}
//---------------------------------------------------------------------------

void TDBDataModule::ConnettiDB()
{
	try  {
		ADOConnection1->Open();
		LogTxt("Database connected");
	} catch(...) {
		ADOConnection1->Connected = false;
	}
}
//---------------------------------------------------------------------------

void TDBDataModule::DisconnettiDB()
{
	ADOConnection1->Close();
	ADOConnection1->Connected = false;
	LogTxt("Database disconnected");
}
//---------------------------------------------------------------------------

int TDBDataModule::verificaConnessioneDB()
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "SELECT * FROM TipiPezzi";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		ADOQuery->Close();
		res = 1;
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiLavorazione(int ID_Lavorazione, TLavorazione &strLavor)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	memset(&strLavor, 0, sizeof(TLavorazione));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lavorazioni WHERE ID_Lavorazione = %d", ID_Lavorazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			strLavor.ID_Lavorazione = ReadInt(ADOQuery, "ID_Lavorazione");
			strLavor.ID_Articolo    = ReadInt(ADOQuery, "ID_Articolo");
			strLavor.Codice			= ReadString(ADOQuery, "Codice");
			strLavor.Descrizione	= ReadString(ADOQuery, "Descrizione");
			res = 1;
		}
		ADOQuery->Close();
	}
	catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

bool TDBDataModule::GetGruppoPalletID(int Lav, int Fase, AnsiString &ID)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	long i;
	bool bFound = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		for (i = 0; i < LONG_MAX && !bFound; i++) {
			ID.sprintf("1-%d-%d-%ld", Lav, Fase, i);
			strsql.printf("SELECT TOP (1) * FROM Jobs WHERE GruppoPallet = '%s'", ID.c_str());
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			bFound = ADOQuery->Eof;
			ADOQuery->Close();
		}
	} catch(...) {};
	delete ADOQuery;

	return bFound;
}
//---------------------------------------------------------------------------

bool TDBDataModule::GetGruppoFaseID(int Lav, int Fase, AnsiString &ID)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	long i;
	bool bFound = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		for (i = 0; i < LONG_MAX && !bFound; i++) {
			ID.sprintf("1-%d-%d-%ld", Lav, Fase, i);
			strsql.printf("SELECT TOP (1) * FROM Jobs WHERE GruppoFase = '%s'", ID.c_str());
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			bFound = ADOQuery->Eof;
			ADOQuery->Close();
		}
	} catch(...) {};
	delete ADOQuery;

	return bFound;
}
//---------------------------------------------------------------------------

bool TDBDataModule::SegnalazioneAcquisita(int nall) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP (1) * FROM Segnalazioni WHERE (N = %d) AND (Acquisito IS NULL)", nall);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			// Messaggio gi� inviato
			ADOQuery->Close();
			delete ADOQuery;
			return false;
		}
	}
	catch(...) {};
	delete ADOQuery;
	return true;
}
//---------------------------------------------------------------------------

void TDBDataModule::Segnalazione(int nall, AnsiString msg, int all, int popup, int popupstation)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
//		strsql.printf("SELECT TOP (1) * FROM Segnalazioni WHERE (Messaggio = '%s') AND ((Acquisito IS NULL) OR (GETDATE() - Acquisito < 1.0 / 24.0 / 20.0))", msg);
		strsql.printf("SELECT TOP (1) * FROM Segnalazioni WHERE (Messaggio = '%s') AND (Acquisito IS NULL)", msg.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			// Messaggio gi� inviato
			ADOQuery->Close();
			delete ADOQuery;
			return;
		}
		ADOQuery->Close();
		strsql.printf("INSERT INTO Segnalazioni (N, Messaggio, Allarme, Popup, StationPopup) VALUES (%d, '%s', %d, %d, %d)", nall, msg.c_str(), all, popup, popupstation);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::Log(AnsiString Sorgente, AnsiString Tipo, char *s, ...)
{
	va_list argptr;
	AnsiString logstr;

    //Costruisco la stringa
    va_start(argptr, s);
    logstr.vprintf(s, argptr);
    va_end(argptr);

    //Log
    Log(Sorgente, Tipo, logstr);
}
//---------------------------------------------------------------------------

void TDBDataModule::Log(AnsiString Sorgente, AnsiString Tipo, AnsiString Evento)
{
	TADOQuery *ADOQuery;
	AnsiString strsql, ev;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP (1) * FROM Storico ORDER BY DataOra DESC");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			// Messaggio gi� loggato
			ev = ReadString(ADOQuery, "Evento");
			if (ev == Evento) {
				ADOQuery->Close();
				delete ADOQuery;
				return;
			}
		}
		ADOQuery->Close();
		strsql.printf("INSERT INTO Storico (Sorgente, Tipo, Evento) VALUES ('%s', '%s', '%s')", Sorgente.c_str(), Tipo.c_str(), Evento.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::AcquisisciSegnalazioneAttiva(int nmsg) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Segnalazioni SET Acquisito = GetDate() WHERE (N = '%d') AND Acquisito IS NULL", nmsg);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::PuliziaStorici() {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Segnalazioni WHERE (DataOra < (GETDATE() - 30))");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		strsql.printf("DELETE FROM Storico WHERE (DataOra < (GETDATE() - 30))");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		strsql.printf("DELETE FROM StoricoMissioni WHERE (Inizio < (GETDATE() - 30))");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
//		strsql.printf("DELETE FROM StoricoLavori WHERE (Attivazione < (GETDATE() - 30))");
//		ADOQuery->SQL->Text = strsql;
//		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::PosizioneMacchina(AnsiString TipoMacchina, int ID_Macchina, int ID_TipoPallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int npos = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM PosizioniMacchine WHERE Tipo_Macchina = '%s' AND ID_Macchina = %d AND ID_TipoPallet = %d",
			TipoMacchina.c_str(), ID_Macchina, ID_TipoPallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			npos = ReadInt(ADOQuery, "Posizione");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return npos;
}
//---------------------------------------------------------------------------

int TDBDataModule::AdattatoreRichiesto(AnsiString TipoMacchina, int ID_Macchina, int ID_TipoPallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int a = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM PosizioniMacchine WHERE Tipo_Macchina = '%s' AND ID_Macchina = %d AND ID_TipoPallet = %d",
			TipoMacchina.c_str(), ID_Macchina, ID_TipoPallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			a = ReadInt(ADOQuery, "Adattatore");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return a;
}
//---------------------------------------------------------------------------

int TDBDataModule::AddPresetMacchina(AnsiString TipoMacchina, int ID_Macchina, int ID_TipoPallet, TPezzo &pz) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	double X, Y, Z, A, B, C, ROT, ZX, ZY;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM PosizioniMacchine WHERE Tipo_Macchina = '%s' AND ID_Macchina = %d AND ID_TipoPallet = %d",
			TipoMacchina.c_str(), ID_Macchina, ID_TipoPallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			X	= ReadFloat(ADOQuery, "PresetX");
			Y	= ReadFloat(ADOQuery, "PresetY");
			Z	= ReadFloat(ADOQuery, "PresetZ");
			A	= ReadFloat(ADOQuery, "PresetA");
			B	= ReadFloat(ADOQuery, "PresetB");
			C	= ReadFloat(ADOQuery, "PresetC");
			ROT	= ReadFloat(ADOQuery, "PresetROT");
			ZX	= ReadFloat(ADOQuery, "ZeroX");
			ZY	= ReadFloat(ADOQuery, "ZeroY");
			pz.OffsetX += X;
			pz.OffsetY += Y;
			pz.OffsetZ += Z;
			pz.OffsetA += A;
			pz.OffsetB += B;
			pz.OffsetC += C;
			pz.OffsetROT += ROT;
			pz.ZeroX += ZX;
			pz.ZeroY += ZY;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return 0;
}
//---------------------------------------------------------------------------


int TDBDataModule::LeggiAdattatoreInMacchina(AnsiString Tipo_Macchina, int ID_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine "
					  "WHERE (ID_Macchina = %d) "
					  "AND (Tipo_Macchina = '%s') ", ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "Adattatore");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiUtensileInMacchina(AnsiString Tipo_Macchina, int ID_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine "
					  "WHERE (ID_Macchina = %d) "
					  "AND (Tipo_Macchina = '%s') ", ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Utensile");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::TipoPinza(int ID_TipoPallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int p = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM TipiPallet WHERE ID_TipoPallet = %d", ID_TipoPallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p = ReadInt(ADOQuery, "Pinza");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return p;
}
//---------------------------------------------------------------------------

int TDBDataModule::TipoPinzaPezzi(int ID_TipoPezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int p = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM TipiPezzi WHERE ID_TipoPezzo = %d", ID_TipoPezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p = ReadInt(ADOQuery, "Pinza");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return p;
}
//---------------------------------------------------------------------------

bool TDBDataModule::LeggiPallet(int ID_Pallet, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	TPezzo pz;
	TArticolo a;
	bool bRes = false;

	memset(&p, 0, sizeof(TPallet));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pallet		= ReadInt(ADOQuery, "ID_Pallet");
			p.ID_TipoPallet	= ReadInt(ADOQuery, "ID_TipoPallet");
			p.ID_Pezzo		= ReadInt(ADOQuery, "ID_Pezzo");
			p.Stato_Pallet	= ReadInt(ADOQuery, "Stato_Pallet");
			p.Extra_Altezza	= ReadInt(ADOQuery, "Extra_Altezza");
			p.Descrizione_Pallet = ReadString(ADOQuery, "Descrizione_Pallet");
			p.Codice = "";
			if (p.ID_Pezzo != 0) {
				if (LeggiPezzo(p.ID_Pezzo, pz) == 0) {
                    if (pz.ID_Articolo) {
                        LeggiArticolo(pz.ID_Articolo, a);
                        p.Codice = a.Codice_Articolo;
                    }
                }
			}
			p.Programma = ReadString(ADOQuery, "Programma");
			p.Alias = ReadString(ADOQuery, "Alias");
            bRes = true;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
    return bRes;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiPalletDaPezzo(int ID_Pezzo, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	memset(&p, 0, sizeof(TPallet));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT P.*, A.Codice_Articolo "
					  "FROM Articoli AS A INNER JOIN "
					  "Pezzi AS Pz ON A.ID_Articolo = Pz.ID_Articolo RIGHT OUTER JOIN "
					  "Pallet AS P ON Pz.ID_Pezzo = P.ID_Pezzo "
					  "WHERE P.ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pallet		= ReadInt(ADOQuery, "ID_Pallet");
			p.ID_TipoPallet	= ReadInt(ADOQuery, "ID_TipoPallet");
			p.ID_Pezzo		= ReadInt(ADOQuery, "ID_Pezzo");
			p.Stato_Pallet	= ReadInt(ADOQuery, "Stato_Pallet");
			p.Extra_Altezza	= ReadInt(ADOQuery, "Extra_Altezza");
			p.Descrizione_Pallet = ReadString(ADOQuery, "Descrizione_Pallet");
			p.Codice = ReadString(ADOQuery, "Codice_Articolo");
			p.Programma = ReadString(ADOQuery, "Programma");
			p.Alias = ReadString(ADOQuery, "Alias");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaPallet(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Pallet WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::NewPalletID() {
	AnsiString strsql, testo;
	TADOQuery *ADOQuery;
	int id, newid = -1;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "SELECT * FROM Pallet ORDER BY ID_Pallet";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		newid = 1;
		if (ADOQuery->Eof) {
			res = newid;
		} else while (!ADOQuery->Eof) {
			id = ReadInt(ADOQuery, "ID_Pallet");
			if (newid == id) {
				newid++;
				ADOQuery->Next();
			} else {
				res = newid;
				break;
			}
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaPallet(TPallet p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Pallet (ID_Pallet, ID_TipoPallet, ID_Pezzo, Stato_Pallet, Extra_Altezza) VALUES (%d, %d, %d, %d, %d)",
			p.ID_Pallet,
			p.ID_TipoPallet,
			p.ID_Pezzo,
			p.Stato_Pallet,
			p.Extra_Altezza
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaNuovoPallet(int TipoPallet, TPallet &newpallet) {
	TPallet p;
	int res = 0;

	p.ID_Pallet = NewPalletID();
	p.ID_TipoPallet = TipoPallet;
	p.ID_Pezzo = 0;
	p.Stato_Pallet = 0;
	p.Extra_Altezza = 0;
	if (p.ID_Pallet > 0) {
		CreaPallet(p);
		newpallet = p;
	}
	return p.ID_Pallet;
}
//---------------------------------------------------------------------------

int TDBDataModule::PalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Id_Pallet = %d, LastDrop = GetDate() WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'", ID_Pallet, ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::UtensileInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Id_Utensile) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Id_Utensile = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'", Id_Utensile, ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AdattatoreInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Adattatore = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'", ID_Pallet, ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::SetProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, AnsiString Programma_Macchina, int Programma_Attivo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Programma_Macchina = '%s', Programma_Attivo = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'",
			Programma_Macchina.c_str(),
			Programma_Attivo,
			ID_Macchina,
			Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, AnsiString &Programma_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		Programma_Macchina = "";
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'",
			ID_Macchina,
			Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			Programma_Macchina = ReadString(ADOQuery, "Programma_Macchina");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::SetStatoProgrammaMacchina(int ID_Macchina, AnsiString Tipo_Macchina, int Programma_Attivo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Macchine SET Programma_Attivo = %d WHERE ID_Macchina = %d AND Tipo_Macchina = '%s'",
			Programma_Attivo,
			ID_Macchina,
			Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiPalletInMacchina(int ID_Macchina, AnsiString Tipo_Macchina, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&p, 0, sizeof(TPallet));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT M.ID_Macchina, M.Tipo_Macchina, M.ID_Pallet, P.* "
					  "FROM Macchine AS M           "
					  "LEFT OUTER JOIN Pallet AS P  "
					  "ON M.ID_Pallet = P.ID_Pallet "
					  "WHERE (M.ID_Macchina = %d) 	"
					  "AND (M.Tipo_Macchina = '%s') ", ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pallet	= ReadInt(ADOQuery, "ID_Pallet");
			p.ID_TipoPallet	= ReadInt(ADOQuery, "ID_TipoPallet");
			p.ID_Pezzo	= ReadInt(ADOQuery, "ID_Pezzo");
			p.Stato_Pallet = ReadInt(ADOQuery, "Stato_Pallet");
			p.Extra_Altezza = ReadInt(ADOQuery, "Extra_Altezza");
			p.Codice = ReadString(ADOQuery, "Codice");
			p.Programma = ReadString(ADOQuery, "Programma");
			p.Alias = ReadString(ADOQuery, "Alias");
			p.Descrizione_Pallet = ReadString(ADOQuery, "Descrizione_Pallet");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiIDPezzoInMacchina(int ID_Macchina, AnsiString Tipo_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine "
					  "WHERE (ID_Macchina = %d) "
					  "AND (Tipo_Macchina = '%s') ", ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Pallet");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaPallet(TPallet p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET ID_TipoPallet = %d, ID_Pezzo = %d, Stato_Pallet = %d, "
					  "	Extra_Altezza = %d, Descrizione_Pallet = '%s' WHERE ID_Pallet = %d",
			p.ID_TipoPallet,
			p.ID_Pezzo,
			p.Stato_Pallet,
			p.Extra_Altezza,
			p.Descrizione_Pallet.c_str(),
			p.ID_Pallet
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaPezzoSuPallet(int idpallet, int idpezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET ID_Pezzo = %d WHERE ID_Pallet = %d",
			idpezzo,
			idpallet
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaDescPallet(int id, AnsiString desc) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	if (id > 10000) {
    	return res;
	}

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET Descrizione_Pallet = '%s' WHERE ID_Pallet = %d", desc.c_str(), id);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaProgrammaPallet(int id, AnsiString programma) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pallet SET Programma = '%s' WHERE ID_Pallet = %d", programma.c_str(), id);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaDestinazioneMagazzino(int Tipo_Pallet, int &nmag, int &npos, int gruppo, int extrah) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT M.ID_Magazzino, M.ID_TipoPallet, M.Gruppo, P.*    "
					  "FROM Magazzini AS M										"
					  "LEFT OUTER JOIN Postazioni AS P							"
					  "ON M.ID_Magazzino = P.ID_Magazzino						"
					  "WHERE ((M.ID_TipoPallet = %d) OR	(M.ID_TipoPallet = 0))	"
					  "AND (M.Gruppo = %d)                                      "
					  "AND (P.ID_Pallet IS NULL OR P.ID_Pallet = 0)				"
					  "AND ((P.Stato_Postazione IS NULL) OR (P.Stato_Postazione = 0)) "
					  "AND (P.Extra_Altezza >= %d)								"
					  "ORDER BY P.Extra_Altezza, P.ID_Postazione				"
					  , Tipo_Pallet, gruppo, extrah);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			nmag = ReadInt(ADOQuery, "ID_Magazzino");
			npos = ReadInt(ADOQuery, "ID_Postazione");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaAliasMagazzino(AnsiString alias, int Tipo_Pallet, int &nmag, int &npos, int extrah) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

    try {
        ADOQuery = new TADOQuery(NULL);
        ADOQuery->Connection = ADOConnection1;
        strsql.printf("SELECT M.ID_Magazzino, M.ID_TipoPallet, P.*        			"
                      "FROM Magazzini AS M									    	"
                      "LEFT OUTER JOIN Postazioni AS P						    	"
                      "ON M.ID_Magazzino = P.ID_Magazzino					    	"
                      "WHERE ((M.ID_TipoPallet = %d) OR	(M.ID_TipoPallet = 0))		"
                      "AND (P.Alias = '%s')									      	"
                      "AND (P.ID_Pallet IS NULL OR P.ID_Pallet = 0)			    	"
                      "AND ((P.Stato_Postazione IS NULL) OR (P.Stato_Postazione = 0)) "
                      "AND (P.Extra_Altezza >= %d)									"
                      "ORDER BY P.Extra_Altezza, P.ID_Postazione					"
                      , Tipo_Pallet, alias.c_str(), extrah);
        ADOQuery->SQL->Text = strsql;
        ADOQuery->Open();
        if (!ADOQuery->Eof) {
            nmag = ReadInt(ADOQuery, "ID_Magazzino");
            npos = ReadInt(ADOQuery, "ID_Postazione");
            res = 0;
        }
        ADOQuery->Close();
    } catch(Exception &E) {};
    delete ADOQuery;
    return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CellaTotemPrecedente(int pos) {
	int res = -1;

	if ((pos % 30) == 1) {
		res = pos + 30;
	} else {
		res = pos - 1;
	}
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CellaTotemSuccessiva(int pos) {
	int res = -1;

	if ((pos % 30) == 0) {
		res = pos - 29;
	} else {
		res = pos + 1;
	}
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaDestinazioneTotem(int extralarge, int &nmag, int &npos) {
	AnsiString strsql, TableName;
	TIndexList TabPostazioni;
	int mag, i, iprev, inext, res = -1;

	for (mag = 4; mag <= 4; mag++) {
		TableName.printf("Magazzino WHERE ID_Magazzino = %d", mag);
		CaricaTabellaK(TableName, "ID_Postazione", TabPostazioni);
		for (i = 1; i <= 150; i++) {
			if ((TabPostazioni[i]["ID_Pezzo"].ToIntDef(0) == 0) &&
				(TabPostazioni[i]["Stato_Postazione"].ToIntDef(0) == 0)) {
				// Ok, postazione vuota e sbloccata
				if (!extralarge) {
					// Se � un elettrodo normale controllo che le postazioni adiacenti non contengano un elettrodo largo
					iprev = CellaTotemPrecedente(i);
					inext = CellaTotemSuccessiva(i);
					if ((TabPostazioni[iprev]["ID_Pezzo"].ToIntDef(0) != 0) &&
						(TabPostazioni[iprev]["Extra_H_Pallet"].ToIntDef(0) != 0)) {
						// Nella pos precedente c'� un elettrodo largo
						continue;
					}
					if ((TabPostazioni[inext]["ID_Pezzo"].ToIntDef(0) != 0) &&
						(TabPostazioni[inext]["Extra_H_Pallet"].ToIntDef(0) != 0)) {
						// Nella pos successiva c'� un elettrodo largo
						continue;
					}
					// Se arrivo qui la postazione pu� essere usata
					nmag = mag;
					npos = i;
					return 0;
				} else {
					// Se � un elettrodo largo controllo che le postazioni adiacenti non contengano un elettrodo
					// e quelle pi� in l� non contengano un elettrodo largo
					iprev = CellaTotemPrecedente(i);
					inext = CellaTotemSuccessiva(i);
					if ((TabPostazioni[iprev]["ID_Pezzo"].ToIntDef(0) != 0) ||
						(TabPostazioni[iprev]["Stato_Postazione"].ToIntDef(0) == POS_BLOCCATA)) {
						// Nella pos precedente c'� un elettrodo
						continue;
					}
					if ((TabPostazioni[inext]["ID_Pezzo"].ToIntDef(0) != 0) ||
						(TabPostazioni[inext]["Stato_Postazione"].ToIntDef(0) == POS_BLOCCATA)) {
						// Nella pos successiva c'� un elettrodo
						continue;
					}
					/*
					iprev = CellaTotemPrecedente(iprev);
					inext = CellaTotemSuccessiva(inext);
					if ((TabPostazioni[iprev]["ID_Pezzo"].ToIntDef(0) != 0) &&
						(TabPostazioni[iprev]["Extra_H_Pallet"].ToIntDef(0) != 0)) {
						// Nella pos precedente alla precedente c'� un elettrodo largo
						continue;
					}
					if ((TabPostazioni[inext]["ID_Pezzo"].ToIntDef(0) != 0) &&
						(TabPostazioni[inext]["Extra_H_Pallet"].ToIntDef(0) != 0)) {
						// Nella pos successiva alla successiva c'� un elettrodo largo
						continue;
					}
					*/
					// Se arrivo qui la postazione pu� essere usata
					nmag = mag;
					npos = i;
					return 0;
				}
			}
		}
	}
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPalletMagazzino(int ID_Pallet, int &nmag, int &npos) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;
	TPallet p;
	TPezzo pz;
	TArticolo a;

	try {
		LeggiPallet(ID_Pallet, p);
		LeggiPezzo(p.ID_Pezzo, pz);
		LeggiArticolo(pz.ID_Articolo, a);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		//Prima controllo se si sta cercando un pallet con sopra un pezzo.
		//se questo � presente nei magazzini di raffreddamento non lo prelevo
		//finch� non � trascoro il tempo di raffreddamento
		strsql.printf("SELECT * FROM Postazioni AS Pos"
					  "	INNER JOIN Pallet AS P ON Pos.ID_Pallet = P.ID_Pallet"
					  "	INNER JOIN Pezzi AS Pz ON P.ID_Pezzo = Pz.ID_Pezzo"
					  "	INNER JOIN Articoli AS A on pz.ID_Articolo = A.ID_Articolo"
					  "	WHERE"
					  "	(pos.ID_Pallet = %d) AND"
					  "	((Stato_Postazione IS NULL) OR (Stato_Postazione = 0)) AND"
					  "	((DATEdiff(MINUTE, POS.ORADEPOSITO, GETDATE()) > A.TStazionamento) OR (Pos.OraDeposito IS NULL))",
					  ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (ADOQuery->Eof) {
			//Il pallet richiesto non � nel magazzino di raffreddamento oppure non � ancora passato il tempo
			//Per darmi la possibilit� di portarlo via
			ADOQuery->Close();
			strsql.printf("	SELECT * FROM Postazioni "
						  "	WHERE ID_Pallet = %d "
						  "	AND ((Stato_Postazione IS NULL) OR (Stato_Postazione = 0))"
						  , ID_Pallet);
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			if (!ADOQuery->Eof) {
				nmag = ReadInt(ADOQuery, "ID_Magazzino");
				npos = ReadInt(ADOQuery, "ID_Postazione");
				res = 0;
			}
			ADOQuery->Close();
		} else {
			nmag = ReadInt(ADOQuery, "ID_Magazzino");
			npos = ReadInt(ADOQuery, "ID_Postazione");
			res = 0;
        }
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::VerificaPalletBloccatoInMagazzino(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;

		strsql.printf("SELECT * FROM Postazioni "
					  "WHERE (ID_Pallet = %d) AND (Stato_Postazione IS NOT NULL) AND (Stato_Postazione <> 0)",
					  ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiPalletMagazzino(int Magazzino, int Postazione, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT *	FROM Magazzino   "
					  "WHERE (ID_Magazzino = %d) "
					  "AND (ID_Postazione = %d)  ", Magazzino, Postazione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pallet	= ReadInt(ADOQuery, "ID_Pallet");
			p.ID_TipoPallet	= ReadInt(ADOQuery, "ID_TipoPallet");
			p.ID_Pezzo	= ReadInt(ADOQuery, "ID_Pezzo");
			p.Stato_Pallet = ReadInt(ADOQuery, "Stato_Pallet");
			p.Extra_Altezza = ReadInt(ADOQuery, "Extra_Altezza");
			p.Alias = ReadString(ADOQuery, "Alias");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::PalletInMagazzino(int Magazzino, int Postazione, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Postazioni SET ID_Pallet = %d, OraDeposito = GetDate() WHERE ID_Magazzino = %d AND ID_Postazione = %d", ID_Pallet, Magazzino, Postazione);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CambiaStatoPostazione(int Magazzino, int Postazione, int stato) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Postazioni SET Stato_Postazione = %d WHERE ID_Magazzino = %d AND ID_Postazione = %d", stato, Magazzino, Postazione);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiProxMissione(TMissioneDB &m) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&m, 0, sizeof(TMissioneDB));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni ORDER BY Priorita DESC, ID_Missione");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			m.ID_Missione		   = ReadInt(ADOQuery, 		"ID_Missione");
			m.ID_Pallet            = ReadInt(ADOQuery, 		"ID_Pallet");
			m.ID_Macchina          = ReadInt(ADOQuery, 		"ID_Macchina");
			m.Tipo_Macchina        = ReadString(ADOQuery, 	"Tipo_Macchina");
			m.Tipo_Missione        = ReadString(ADOQuery, 	"Tipo_Missione");
			m.Programma_Macchina   = ReadString(ADOQuery, 	"Programma_Macchina");
			m.ID_Articolo          = ReadInt(ADOQuery, 		"ID_Articolo");
			m.Priorita             = ReadInt(ADOQuery, 		"Priorita");
			m.ID_Job               = ReadInt(ADOQuery, 		"ID_Job");
 			m.GruppoFase		   = ReadString(ADOQuery, 	"GruppoFase");
			m.GruppoPallet		   = ReadString(ADOQuery, 	"GruppoPallet");
			m.ID_Pezzo             = ReadInt(ADOQuery, 		"ID_Pezzo");
			m.AzionePallet         = ReadInt(ADOQuery, 		"AzionePallet");
			m.GruppoLavorazione	   = ReadString(ADOQuery, 	"GruppoLavorazione");
			m.Commento			   = ReadString(ADOQuery, 	"Commento");
			m.Descrizione_Op	   = ReadString(ADOQuery, 	"Descrizione_Op");
            m.ID_Lotto   		   = ReadInt(ADOQuery, 		"ID_Lotto");
            m.ID_Lavoro   		   = ReadInt(ADOQuery, 		"ID_Lavoro");
            m.IndiceLavoro 		   = ReadInt(ADOQuery, 		"IndiceLavoro");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiMissione(int ID_Missione, TMissioneDB &m)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&m, 0, sizeof(TMissioneDB));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni WHERE ID_Missione = %d", ID_Missione);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			m.ID_Missione		   = ReadInt(ADOQuery, 		"ID_Missione");
			m.ID_Pallet            = ReadInt(ADOQuery, 		"ID_Pallet");
			m.ID_Macchina          = ReadInt(ADOQuery, 		"ID_Macchina");
			m.Tipo_Macchina        = ReadString(ADOQuery, 	"Tipo_Macchina");
			m.Tipo_Missione        = ReadString(ADOQuery, 	"Tipo_Missione");
			m.Programma_Macchina   = ReadString(ADOQuery, 	"Programma_Macchina");
			m.ID_Articolo          = ReadInt(ADOQuery, 		"ID_Articolo");
			m.Priorita             = ReadInt(ADOQuery, 		"Priorita");
			m.ID_Job               = ReadInt(ADOQuery, 		"ID_Job");
			m.GruppoFase		   = ReadString(ADOQuery, 	"GruppoFase");
			m.GruppoPallet		   = ReadString(ADOQuery, 	"GruppoPallet");
			m.ID_Pezzo             = ReadInt(ADOQuery, 		"ID_Pezzo");
			m.AzionePallet         = ReadInt(ADOQuery, 		"AzionePallet");
			m.GruppoLavorazione	   = ReadString(ADOQuery, 	"GruppoLavorazione");
 			m.Commento			   = ReadString(ADOQuery, 	"Commento");
			m.Descrizione_Op	   = ReadString(ADOQuery, 	"Descrizione_Op");
            m.ID_Lotto   		   = ReadInt(ADOQuery, 		"ID_Lotto");
            m.ID_Lavoro   		   = ReadInt(ADOQuery, 		"ID_Lavoro");
            m.IndiceLavoro 		   = ReadInt(ADOQuery, 		"IndiceLavoro");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiMissionePallet(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, TMissioneDB &m) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&m, 0, sizeof(TMissioneDB));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE ID_Pallet = %d "
					  "AND Tipo_Missione = '%s' "
					  "AND Tipo_Macchina = '%s' "
					  "ORDER BY Priorita DESC, ID_Missione", ID_Pallet, Tipo_Missione.c_str(), Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			m.ID_Missione		   = ReadInt(ADOQuery, 		"ID_Missione");
			m.ID_Pallet            = ReadInt(ADOQuery, 		"ID_Pallet");
			m.ID_Macchina          = ReadInt(ADOQuery, 		"ID_Macchina");
			m.Tipo_Macchina        = ReadString(ADOQuery, 	"Tipo_Macchina");
			m.Tipo_Missione        = ReadString(ADOQuery, 	"Tipo_Missione");
			m.Programma_Macchina   = ReadString(ADOQuery, 	"Programma_Macchina");
			m.ID_Articolo          = ReadInt(ADOQuery, 		"ID_Articolo");
			m.Priorita             = ReadInt(ADOQuery, 		"Priorita");
			m.ID_Job               = ReadInt(ADOQuery, 		"ID_Job");
			m.GruppoFase		   = ReadString(ADOQuery, 	"GruppoFase");
			m.GruppoPallet		   = ReadString(ADOQuery, 	"GruppoPallet");
			m.ID_Pezzo             = ReadInt(ADOQuery, 		"ID_Pezzo");
			m.AzionePallet         = ReadInt(ADOQuery, 		"AzionePallet");
			m.GruppoLavorazione	   = ReadString(ADOQuery, 	"GruppoLavorazione");
 			m.Commento			   = ReadString(ADOQuery, 	"Commento");
			m.Descrizione_Op	   = ReadString(ADOQuery, 	"Descrizione_Op");
            m.ID_Lotto   		   = ReadInt(ADOQuery, 		"ID_Lotto");
            m.ID_Lavoro   		   = ReadInt(ADOQuery, 		"ID_Lavoro");
            m.IndiceLavoro 		   = ReadInt(ADOQuery, 		"IndiceLavoro");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiMissionePallet(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int numero_macchina, TMissioneDB &m) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&m, 0, sizeof(TMissioneDB));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE ID_Pallet = %d "
					  "AND Tipo_Missione = '%s' "
					  "AND Tipo_Macchina = '%s' "
					  "AND ID_Macchina = %d "
					  "ORDER BY Priorita DESC, ID_Missione", ID_Pallet, Tipo_Missione.c_str(), Tipo_Macchina.c_str(), numero_macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			m.ID_Missione		   = ReadInt(ADOQuery, 		"ID_Missione");
			m.ID_Pallet            = ReadInt(ADOQuery, 		"ID_Pallet");
			m.ID_Macchina          = ReadInt(ADOQuery, 		"ID_Macchina");
			m.Tipo_Macchina        = ReadString(ADOQuery, 	"Tipo_Macchina");
			m.Tipo_Missione        = ReadString(ADOQuery, 	"Tipo_Missione");
			m.Programma_Macchina   = ReadString(ADOQuery, 	"Programma_Macchina");
			m.ID_Articolo          = ReadInt(ADOQuery, 		"ID_Articolo");
			m.Priorita             = ReadInt(ADOQuery, 		"Priorita");
			m.ID_Job               = ReadInt(ADOQuery, 		"ID_Job");
			m.GruppoFase		   = ReadString(ADOQuery, 	"GruppoFase");
			m.GruppoPallet		   = ReadString(ADOQuery, 	"GruppoPallet");
			m.ID_Pezzo             = ReadInt(ADOQuery, 		"ID_Pezzo");
			m.AzionePallet         = ReadInt(ADOQuery, 		"AzionePallet");
			m.GruppoLavorazione	   = ReadString(ADOQuery, 	"GruppoLavorazione");
			m.Commento			   = ReadString(ADOQuery, 	"Commento");
			m.Descrizione_Op	   = ReadString(ADOQuery, 	"Descrizione_Op");
            m.ID_Lotto   		   = ReadInt(ADOQuery, 		"ID_Lotto");
            m.ID_Lavoro   		   = ReadInt(ADOQuery, 		"ID_Lavoro");
            m.IndiceLavoro 		   = ReadInt(ADOQuery, 		"IndiceLavoro");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CancellaMissione(int ID_Pallet, AnsiString Tipo_Missione, AnsiString Tipo_Macchina, int ID_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
        //Log
        LogMiss("Delete Mission", ID_Pallet, Tipo_Missione, Tipo_Macchina, ID_Macchina, 0, 0);

		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Missioni "
					  "WHERE ID_Pallet = %d "
					  "AND Tipo_Missione = '%s' "
					  "AND Tipo_Macchina = '%s' "
					  "AND ID_Macchina = %d ",
					  ID_Pallet, Tipo_Missione.c_str(), Tipo_Macchina.c_str(), ID_Macchina);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
    return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CancellaMissione(int ID_Missione) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
    TMissioneDB m;
	int res = -1;

	try {
    	//Leggo la missione da cancellare
		if (LeggiMissione(ID_Missione, m) == 0) {
        	//Log
            LogMiss("Delete Mission", m.ID_Pallet, m.Tipo_Missione, m.Tipo_Macchina, m.ID_Macchina, m.ID_Job, m.ID_Missione);
        }
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Missioni WHERE ID_Missione = %d", ID_Missione);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
    return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiJob(int ID_Job, TJob &j)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	memset(&j, 0, sizeof(TJob));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs WHERE ID_Job = %d", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			j.ID_Job             = ReadInt(ADOQuery, "ID_Job");
			j.ID_Pallet          = ReadInt(ADOQuery, "ID_Pallet");
			j.ID_Macchina        = ReadInt(ADOQuery, "ID_Macchina");
			j.Tipo_Macchina      = ReadString(ADOQuery, "Tipo_Macchina");
			j.Tipo_Missione      = ReadString(ADOQuery, "Tipo_Missione");
			j.Programma_Macchina = ReadString(ADOQuery, "Programma_Macchina");
			j.ID_Articolo        = ReadInt(ADOQuery, "ID_Articolo");
			j.Priorita           = ReadInt(ADOQuery, "Priorita");
			j.GruppoFase		 = ReadString(ADOQuery, "GruppoFase");
			j.GruppoPallet		 = ReadString(ADOQuery, "GruppoPallet");
			j.ID_Pezzo			 = ReadInt(ADOQuery, "ID_Pezzo");
			j.AzionePallet		 = ReadInt(ADOQuery, "AzionePallet");
			j.GruppoLavorazione  = ReadString(ADOQuery, "GruppoLavorazione");
			j.Descrizione_Op	 = ReadString(ADOQuery, "Descrizione_Op");
			j.Commento			 = ReadString(ADOQuery, "Commento");
            j.ID_Lotto			 = ReadInt(ADOQuery, "ID_Lotto");
            j.ID_Lavoro			 = ReadInt(ADOQuery, "ID_Lavoro");
            j.IndiceLavoro		 = ReadInt(ADOQuery, "IndiceLavoro");
			res = 0;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaJobDepositoMacchina(int ID_Pallet, AnsiString tpmacc, int nmacc, int pri) {
	TJob j;
	TPallet p;
    TPezzo pz;

	if (!ID_Pallet) {
		return 0;
	}
	memset(&j, 0, sizeof(TJob));
	LeggiPallet(ID_Pallet, p);
	LeggiPezzo(p.ID_Pezzo, pz);
	j.ID_Pallet = ID_Pallet;
	j.Tipo_Missione = "DROP";
	j.Tipo_Macchina = tpmacc;
	j.ID_Macchina = nmacc;
    j.ID_Pezzo = pz.ID_Pezzo;
    j.ID_Articolo = pz.ID_Articolo;
	j.Programma_Macchina = p.Programma;
	j.Priorita = pri;
    j.ID_Lotto = PalletAssociatoLotto(p.ID_Pallet);
	if ((ID_Pallet < 10000) && EsisteGiaMissionePallet(j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, p.ID_Pallet))
		return 1;
    //Controllo se non � gi� stato generato un job
	if (EsisteGiaJobPallet(j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, p.ID_Pallet))
		return 1;
	j.ID_Job = CreaJob(j);
    if (j.ID_Job) {
        return AttivaJob(j.ID_Job);
    }

    return 0;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaJobPrelievoMacchina(AnsiString tpmacc, int nmacc, int pri) {
	TJob j;
	TPallet p;
	TArticolo a;

	LeggiPalletInMacchina(nmacc, tpmacc, p);
	if (!p.ID_Pallet) {
		return 0;
	}
	memset(&j, 0, sizeof(TJob));
	//Cerco Articolo dal pezzo
	LeggiArticoloDaIdPezzo(p.ID_Pezzo, a);
	j.ID_Pallet = p.ID_Pallet;
	j.Tipo_Missione = "PICK";
	j.Tipo_Macchina = tpmacc;
	j.ID_Macchina = nmacc;
	j.Priorita = pri;
	j.ID_Pezzo = p.ID_Pezzo;
	j.ID_Articolo = a.ID_Articolo;
    j.ID_Lotto = PalletAssociatoLotto(p.ID_Pallet);
    //Controllo se non � gi� stata generata una missione
	if (EsisteGiaMissionePallet(j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, p.ID_Pallet))
		return 2;
    //Controllo se non � gi� stato generato un job
	if (EsisteGiaJobPallet(j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, p.ID_Pallet))
		return 1;
	j.ID_Job = CreaJob(j);
    if (j.ID_Job) {
		//Attivo il JOB
        return AttivaJob(j.ID_Job);
    }

    return 0;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaJob(TJob j) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Jobs "
					  "(ID_Pallet, ID_Macchina, Tipo_Macchina, Tipo_Missione, Programma_Macchina, ID_Articolo, Priorita, GruppoFase, GruppoPallet, ID_Pezzo, AzionePallet, GruppoLavorazione, Commento, ID_Lotto, ID_Lavoro, IndiceLavoro, Descrizione_Op) "
					  "VALUES (%d, %d, '%s', '%s', '%s', %d, %d, '%s', '%s', %d, %d, '%s', '%s', %d, %d, %d, '%s') ",
			j.ID_Pallet,
			j.ID_Macchina,
			j.Tipo_Macchina.c_str(),
			j.Tipo_Missione.c_str(),
			j.Programma_Macchina.c_str(),
			j.ID_Articolo,
			j.Priorita,
			j.GruppoFase.c_str(),
			j.GruppoPallet.c_str(),
			j.ID_Pezzo,
			j.AzionePallet,
			j.GruppoLavorazione.c_str(),
			j.Commento.c_str(),
            j.ID_Lotto,
            j.ID_Lavoro,
            j.IndiceLavoro,
            j.Descrizione_Op.c_str()
			);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
        j.ID_Job = GetSelfGenID("Jobs");
        if (j.ID_Job) {
        	LogMiss("Create Job", j.ID_Pallet, j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, j.ID_Job, 0);
        }
	} catch(Exception &E) {};
	delete ADOQuery;
	return j.ID_Job;
}
//---------------------------------------------------------------------------

int TDBDataModule::AttivaJob(int ID_Job)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Jobs SET Active = GetDate() WHERE ID_Job = %d", ID_Job);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
    return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaMissione(TMissioneDB m) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Missioni "
					  "(ID_Pallet, ID_Macchina, Tipo_Macchina, Tipo_Missione, Programma_Macchina, ID_Articolo, Priorita, ID_Job, GruppoFase, GruppoPallet, ID_Pezzo, AzionePallet, GruppoLavorazione, Commento, Descrizione_Op, ID_Lotto, ID_Lavoro, IndiceLavoro) "
					  "VALUES (%d, %d, '%s', '%s', '%s', %d, %d, %d, '%s', '%s', %d, %d, '%s', '%s', '%s', %d, %d, %d) ",
			m.ID_Pallet,
			m.ID_Macchina,
			m.Tipo_Macchina.c_str(),
			m.Tipo_Missione.c_str(),
			m.Programma_Macchina.c_str(),
			m.ID_Articolo,
			m.Priorita,
			m.ID_Job,
			m.GruppoFase.c_str(),
			m.GruppoPallet.c_str(),
			m.ID_Pezzo,
			m.AzionePallet,
			m.GruppoLavorazione.c_str(),
			m.Commento.c_str(),
			m.Descrizione_Op.c_str(),
            m.ID_Lotto,
            m.ID_Lavoro,
            m.IndiceLavoro
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
        if (res) {
            res = GetSelfGenID("Missioni");
        	LogMiss("Create Mission", m.ID_Pallet, m.Tipo_Missione, m.Tipo_Macchina, m.ID_Macchina, m.ID_Job, res);
        }
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogMissRobot(int tipomiss, int locmag, int nmag, int nbaia, int tipo_pinza, int nmu, int posmu, int idpallet) {
	AnsiString res;

	res = IntToStr(tipomiss);
	switch(tipomiss) {
	case TPM_DEPOSITO_MACCHINA:
		res += " - Machine drop";
		logstr.sprintf("Mission to robot: %s, machine=%d, pos=%d, pallet=%d, grip=%d", res.c_str(), nmu, posmu, idpallet, tipo_pinza);
		break;
	case TPM_PRELIEVO_MACCHINA:
		res += " - Machine pick";
		logstr.sprintf("Mission to robot: %s, machine=%d, pos=%d, pallet=%d, grip=%d", res.c_str(), nmu, posmu, idpallet, tipo_pinza);
		break;
	case TPM_DEPOSITO_BAIA:
		res += " - Station drop";
		logstr.sprintf("Mission to robot: %s, station=%d, pos=%d, pallet=%d, grip=%d", res.c_str(), nbaia, posmu, idpallet, tipo_pinza);
		break;
	case TPM_PRELIEVO_BAIA:
		res += " - Station pick";
		logstr.sprintf("Mission to robot: %s, station=%d, pos=%d, pallet=%d, grip=%d", res.c_str(), nbaia, posmu, idpallet, tipo_pinza);
		break;
	case TPM_DEPOSITO_MAGAZZINO:
		res += " - Storage drop";
		logstr.sprintf("Mission to robot: %s, storage=%d, loc=%d, pallet=%d, grip=%d", res.c_str(), nmag, locmag, idpallet, tipo_pinza);
		break;
	case TPM_PRELIEVO_MAGAZZINO:
		res += " - Storage pick";
		logstr.sprintf("Mission to robot: %s, storage=%d, loc=%d, pallet=%d, grip=%d", res.c_str(), nmag, locmag, idpallet, tipo_pinza);
		break;
	}
	DBDataModule->Log("SERVER", "ROBOT", logstr);
}
//---------------------------------------------------------------------------

void TDBDataModule::LogMiss(AnsiString str, int ID_Pallet, AnsiString tipomiss, AnsiString tipomacc, int nmacc, int ID_Job, int ID_Missione) {
	AnsiString res;

    res = str + " ";
	logstr.sprintf("Mission: %s, pallet=%d, tipomiss=%s, tipomacc=%s, nmacc=%d, ID_Job=%d, ID_Missione=%d", res.c_str(), ID_Pallet, tipomiss.c_str(), tipomacc.c_str(), nmacc, ID_Job, ID_Missione);
	DBDataModule->Log("SERVER", "DB", logstr);
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaMissione(AnsiString tpmiss, AnsiString tpmacc, int nmacc) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s'",
			nmacc, tpmacc.c_str(), tpmiss.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Missione");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaMissionePallet(AnsiString tpmiss, AnsiString tpmacc, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND ID_Pallet = %d", tpmacc.c_str(), tpmiss.c_str(), ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Missione");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaMissionePallet(AnsiString tpmiss, AnsiString tpmacc, int nmacc, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND ID_Pallet = %d",
			nmacc, tpmacc.c_str(), tpmiss.c_str(), ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Missione");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJob(AnsiString tpmiss, AnsiString tpmacc, int nmacc) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s'",
			nmacc, tpmacc.c_str(), tpmiss.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Job");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND ID_Pallet = %d", tpmacc.c_str(), tpmiss.c_str(), ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJobPallet(AnsiString tpmiss, AnsiString tpmacc, int nmacc, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND ID_Pallet = %d",
			nmacc, tpmacc.c_str(), tpmiss.c_str(), ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJobAttivoPallet(int pallet, AnsiString tpmiss, AnsiString tpmacc, int nmacc) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE ID_Pallet = %d AND ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s'  AND (Active IS NOT NULL)",
			pallet, nmacc, tpmacc.c_str(), tpmiss.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteMissioneGenerica(AnsiString tpmiss,int ID_P) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni "
					  "WHERE ID_Pallet = %d AND Tipo_Missione = '%s'",
			ID_P, tpmiss.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Missione");;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaMissionePrelievoMacchina(AnsiString tpmacc, int nmacc, int pri) {
	TMissioneDB m;
	TPallet p;
	TArticolo a;

	LeggiPalletInMacchina(nmacc, tpmacc, p);
	if (!p.ID_Pallet) {
		return 1;
	}
	//Cerco Articolo dal pezzo
	LeggiArticoloDaIdPezzo(p.ID_Pezzo, a);
	memset(&m, 0, sizeof(TMissioneDB));
	m.ID_Pallet = p.ID_Pallet;
	m.Tipo_Missione = "PICK";
	m.Tipo_Macchina = tpmacc;
	m.ID_Macchina = nmacc;
	m.Priorita = pri;
	m.Programma_Macchina = p.Programma;
	m.ID_Pezzo = p.ID_Pezzo;
	m.ID_Articolo = a.ID_Articolo;
    m.ID_Lotto = PalletAssociatoLotto(p.ID_Pallet);
	if (EsisteGiaMissione(m.Tipo_Missione, m.Tipo_Macchina, m.ID_Macchina))
		return 2;
	return CreaMissione(m);
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaMissionePrelievoUtensileMacchina(AnsiString tpmacc, int nmacc, int pri) {
	TMissioneDB m;
	TPallet p;
	int idelettrodo;

	memset(&m, 0, sizeof(TMissioneDB));
	idelettrodo = LeggiUtensileInMacchina("M", nmacc);
	if (!idelettrodo) {
		return 0;
	}
	LeggiPallet(idelettrodo, p);
	m.ID_Pallet = p.ID_Pallet;
	m.Tipo_Missione = "PICK";
	m.Tipo_Macchina = tpmacc;
	m.ID_Macchina = nmacc;
	m.Priorita = pri;
    m.ID_Lotto = PalletAssociatoLotto(p.ID_Pallet);
	if (EsisteGiaMissione(m.Tipo_Missione, m.Tipo_Macchina, m.ID_Macchina))
		return 1;
	return CreaMissione(m);
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaMissioneDepositoMacchina(int ID_Pallet, AnsiString tpmacc, int nmacc, int pri) {
	TMissioneDB m;
	TPallet p;
    TPezzo pz;

	memset(&m, 0, sizeof(TMissioneDB));
	if (!ID_Pallet) {
		return 0;
	}
	LeggiPallet(ID_Pallet, p);
	LeggiPezzo(p.ID_Pezzo, pz);
	m.ID_Pallet = ID_Pallet;
	m.Tipo_Missione = "DROP";
	m.Tipo_Macchina = tpmacc;
	m.ID_Macchina = nmacc;
    m.ID_Pezzo = pz.ID_Pezzo;
    m.ID_Articolo = pz.ID_Articolo;
	m.Programma_Macchina = p.Programma;
	m.Priorita = pri;
    m.ID_Lotto = PalletAssociatoLotto(p.ID_Pallet);
	if ((ID_Pallet < 10000) && EsisteGiaMissione(m.Tipo_Missione, m.Tipo_Macchina, m.ID_Macchina))
		return 1;
	return CreaMissione(m);
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaJob(int ID_Job) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
    TJob j;
	int res = -1;

	try {
    	//Leggo la missione da cancellare
		if (LeggiJob(ID_Job, j) == 0) {
        	//Log
            LogMiss("Delete Job", j.ID_Pallet, j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina, j.ID_Job, 0);
        }
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Jobs WHERE ID_Job = %d", ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

bool TDBDataModule::JobInoltrabile(TJob j) {
	AnsiString strsql, commento;
	TADOQuery *ADOQuery;
	bool res = false;
	TPallet p;
	int n;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Missioni WHERE ((ID_Pallet = %d) AND ((Tipo_Missione = 'PICK') OR (Tipo_Missione = 'DROP')))", j.ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
        //Non ci sono missioni in corso
		if (ADOQuery->Eof) {
        	//Controllo se il pallet in esame non sia dentro alla macchina utensile
        	if (LeggiAdattatoreInMacchina(j.Tipo_Macchina, j.ID_Macchina) == j.ID_Pallet) {
                ADOQuery->Close();
				CommentoJob(j.ID_Job, "Pallet sulla tavola interna della macchina utensile");
                delete ADOQuery;
                return res;
            }
        	//Controllo se � un deposito in macchina utensile
			if ((j.Tipo_Macchina == "M") && (j.Tipo_Missione == "DROP")) {
            	//Leggo se ho un pallet in macchina
				LeggiPalletInMacchina(j.ID_Macchina, "M", p);

				//Se in macchina c'� un pallet o se la macchina fa parte di un gruppo e ho la disponibilit� della macchina
				if ((p.ID_Pallet != 0) || (j.ID_Macchina > 1000 && !EventsDM->CercaMacchinaGruppo(j.ID_Macchina, j.Tipo_Macchina, p.ID_TipoPallet))) {
					ADOQuery->Close();
					CommentoJob(j.ID_Job, "Another pallet in the machine");
					delete ADOQuery;
					return res;
				}

				// Se il pallet da portare in macchina � in baia ma non ho l'ok prelievo non inoltro
				n = CercaPalletInBaia(j.ID_Pallet);
				if ((n != 0) && !EventsDM->OKPrelievoInBaia(n)) {
					ADOQuery->Close();
					CommentoJob(j.ID_Job, "Pallet in bay waiting for the operator confirmation");
					delete ADOQuery;
					return res;
				}

				if (VerificaPalletBloccatoInMagazzino(j.ID_Pallet) != 0) {
					ADOQuery->Close();
					CommentoJob(j.ID_Job, "Pallet in storage but blocked");
					delete ADOQuery;
					return res;
				}

                //Controllo se ho gi� missioni attive per quel tipo macchina
				if (EsisteGiaMissione(j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina)) {
					ADOQuery->Close();
					CommentoJob(j.ID_Job, "Already exixst mission for this Machine tool");
					delete ADOQuery;
					return res;
                }

                //Controllo tabella utensili
/*				if ((!j.Programma_Macchina.IsEmpty()) && LeggiAbilitazioneVerificaUtensili(j.ID_Macchina)) {
					if (dmTools->VerificaUtensili(j.ID_Macchina, p.Programma, commento) != 0) {
						ADOQuery->Close();
						delete ADOQuery;
                        CommentoJob(j.ID_Job, commento);
						LeggiPallet(j.ID_Pallet, p);
						AggiornaStatoPezzo(p.ID_Pezzo, STATO_PEZZO_NON_LAVORABILE);
						AggiornaDescPallet(p.ID_Pallet, commento);
						return false;
					}
					else
						res = true;
				} */
                else
					res = true;
			}
/*            else if ((j.Tipo_Macchina == "S") && (j.Tipo_Missione == "DROP")) {
                //Controllo se ho gi� missioni attive per quel tipo macchina
                if (EsisteGiaMissione(j.Tipo_Missione, j.Tipo_Macchina, j.ID_Macchina)) {
					ADOQuery->Close();
					delete ADOQuery;
					CommentoJob(j.ID_Job, "Baia gi� prenotata da un'altra missione");
					return false;
                }
                else
					res = true;
			}*/
            else 
				res = true;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

bool TDBDataModule::JobInoltrabilePerPalletControlloPezzo(TJob j) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	bool res = false;
	TPallet p;

	try {
		//Leggo il pallet
		LeggiPallet(j.ID_Pallet, p);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		//Se ho un pezzo
		if (p.ID_Pezzo > 0) {
			strsql.printf("SELECT Missioni.*, Pallet.ID_Pezzo FROM Missioni             "
						  "	INNER JOIN Pallet ON Missioni.ID_Pezzo = Pallet.ID_Pezzo  "
						  "	WHERE (Missioni.ID_Pezzo = %d) ", p.ID_Pezzo);
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			if (ADOQuery->Eof) {
				res = true;
			}
			ADOQuery->Close();
		} else {
			res = true;
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

bool TDBDataModule::JobInoltrabilePerPezzo(TJob j, int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	bool res = false;
	TPallet p;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		//Verifico di non avere missioni in corso per il pallet che contiene quel pezzo
		strsql.printf("SELECT * FROM Missioni WHERE (ID_Pallet = %d) "
					  "OR ((Tipo_Macchina = '%s') AND (ID_Macchina = %d) "
					  "AND ((Tipo_Missione = 'DROP_PZ') OR (Tipo_Missione = 'PICK_PZ') OR (Tipo_Missione = 'PICK_PZ_TURN')))"
					  , ID_Pallet, j.Tipo_Macchina.c_str(), j.ID_Macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (ADOQuery->Eof) {
			//Cerco nelle baie il pallet dal quale devo prelevare il pezzo
			res = (CercaPalletInBaia(ID_Pallet) == 0) ? false : true;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

bool TDBDataModule::JobInoltrabilePerCambioPallet(TJob j) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	bool res = false;
	int MiaPri, PriMiss;
	TPallet p;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		//Leggo il pallet che voglio gestire
		LeggiPallet(j.ID_Pallet, p);
		//Controllo se il pallet � vuoto... se non lo � vado avanti e far� altri controlli in altre funzioni
		if (p.ID_Pezzo > 0) {
			res = true;
		} else {
			//Devo controllare se � un drop in baia e verificare se esistono missioni con priorit� maggiore
			if ((j.Tipo_Missione == "DROP") && (j.Tipo_Macchina == "S")) {
				strsql.printf("SELECT * FROM Missioni WHERE (Tipo_Missione = 'DROP') AND (Tipo_Macchina = 'S') AND (ID_Pallet <> %d)", j.ID_Pallet);
				ADOQuery->SQL->Text = strsql;
				ADOQuery->Open();
				MiaPri = j.Priorita;
				if (!ADOQuery->Eof) {
					PriMiss = ReadInt(ADOQuery, "Priorita");
					//Controllo
					if (MiaPri < PriMiss) {
						ADOQuery->Close();
						delete ADOQuery;
						return res;
					} else {
						res = true;
					}
				} else {
					//In teoria devo controllare se nei job ce n'� uno con priorit� maggiore
					strsql.printf("SELECT * FROM Jobs WHERE (Tipo_Missione = 'DROP') AND (Tipo_Macchina = 'S') AND (ID_Pallet <> %d)", j.ID_Pallet);
					ADOQuery->SQL->Text = strsql;
					ADOQuery->Open();
					MiaPri = j.Priorita;
					if (!ADOQuery->Eof) {
						PriMiss = ReadInt(ADOQuery, "Priorita");
						//Controllo
						if (MiaPri < PriMiss) {
							ADOQuery->Close();
							delete ADOQuery;
							return res;
						} else {
							res = true;
						}
					} else {
						res = true;
					}
				}
			} else if ((j.Tipo_Missione == "PICK") && (j.Tipo_Macchina == "S")) {
				//Controllo i job... vedo se esiste un job con priorit� pi� alta da eseguire prima
				strsql.printf("SELECT * FROM Jobs WHERE (ID_Pallet = %d) AND (Tipo_Missione = 'DROP')", j.ID_Pallet);
				ADOQuery->SQL->Text = strsql;
				ADOQuery->Open();
				if (ADOQuery->Eof) {
					res = true;
				} else {
					//Un drop esiste... verifico la priorit�
					MiaPri = j.Priorita;
					PriMiss = ReadInt(ADOQuery, "Priorita");
					if (MiaPri < PriMiss) {
						ADOQuery->Close();
						delete ADOQuery;
						return res;
					}
				}
			} else if ((j.Tipo_Missione == "DROP") && (j.Tipo_Macchina == "M")) {
				//Se ci sono dei drop per lo stesso pallet precedenti non abilito la missione
				strsql.printf("SELECT * FROM Missioni WHERE (Tipo_Missione = 'DROP') AND (ID_Pallet = %d)", j.ID_Pallet);
				ADOQuery->SQL->Text = strsql;
				ADOQuery->Open();
				MiaPri = j.Priorita;
				if (!ADOQuery->Eof) {
					PriMiss = ReadInt(ADOQuery, "Priorita");
					//Controllo
					if (MiaPri < PriMiss) {
						ADOQuery->Close();
						delete ADOQuery;
						return res;
					} else {
						res = true;
					}
				} else {
					//Devo controllare se nei job ce n'� uno con priorit� maggiore
					strsql.printf("SELECT * FROM Jobs WHERE (Tipo_Missione = 'DROP') AND (ID_Pallet = %d)", j.ID_Pallet);
					ADOQuery->SQL->Text = strsql;
					ADOQuery->Open();
					MiaPri = j.Priorita;
					if (!ADOQuery->Eof) {
						PriMiss = ReadInt(ADOQuery, "Priorita");
						//Controllo
						if (MiaPri < PriMiss) {
							ADOQuery->Close();
							delete ADOQuery;
							return res;
						} else {
							res = true;
						}
					} else {
						res = true;
					}
				}
			} else {
				res = true;
			}
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

bool TDBDataModule::NonEsisteJobConPriMaggiore(TJob j) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	bool res = false;
	int MiaPri, PriMiss;
	//Se non esiste un job con priorit� maggiore posso inoltrare
	// TRUE = NON ESISTE NESSUN JOB
	//FALSE = HO TROVATO UN JOB CON PRIORIT� MAGGIORE (non devo inoltrare)

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP (1) * FROM Jobs WHERE ((ID_Pallet = %d))", j.ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			PriMiss = ReadInt(ADOQuery, "Priorita");
			MiaPri = j.Priorita;
			//Controllo
			if (MiaPri < PriMiss) {
				ADOQuery->Close();
				delete ADOQuery;
				return res;
			} else {
				res = true;
			}
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::InoltraJobs() {
	TMissioneDB m;
	TJob j;
	TPallet p;
	TPezzo pz;
	int TotaliRichiesti, Disponibili, Inviati, Vietati;
	int res, RowCount;
	UINT i;
	AnsiString TableName;
	TRecordList TabJobs;

	memset(&m, 0, sizeof(TMissioneDB));
	memset(&j, 0, sizeof(TJob));
	try {
		//Leggo il valore dell'abilitazione su DB
		TableName.printf("CodaLavori ORDER BY Priorita DESC, Disponibili - Vietati DESC, Creazione");
		CaricaTabella(TableName, TabJobs);
		for (i = 0; i < TabJobs.size() ; i++) {
			//Se il valore di ritorno � 1 significa che nel gruppo pallet in esame
			//esiste un pallet con ID = 0 ma non � stato trovato alcun pallet in magazzino
			//del nuovo tipo pallet che andr� a sostituire l'altro, quindi passo al job (gruppopallet) successivo
			res = CheckIdPalletFromGruppoPallet(TabJobs[i]["GruppoPallet"], RowCount);
			if (res == 1) {
				//Salto all'indice iEsimo dopo il gruppo pallet
				//i += (RowCount - 1);
				continue;
			}
			else if (res == 2) {
				//Dato che ho aggiornato la tabella Jobs, rifaccio la lettura
				CaricaTabella(TableName, TabJobs);
			}

			j.ID_Job             = TabJobs[i]["ID_Job"].ToIntDef(0);
			j.ID_Pallet          = TabJobs[i]["ID_Pallet"].ToIntDef(0);
			j.ID_Macchina        = TabJobs[i]["ID_Macchina"].ToIntDef(0);
			j.Tipo_Macchina      = TabJobs[i]["Tipo_Macchina"];
			j.Tipo_Missione      = TabJobs[i]["Tipo_Missione"];
			j.Programma_Macchina = TabJobs[i]["Programma_Macchina"];
			j.ID_Articolo        = TabJobs[i]["ID_Articolo"].ToIntDef(0);
			j.Priorita           = TabJobs[i]["Priorita"].ToIntDef(0);
			j.GruppoFase		 = TabJobs[i]["GruppoFase"];
			j.GruppoPallet		 = TabJobs[i]["GruppoPallet"];
			j.ID_Pezzo			 = TabJobs[i]["ID_Pezzo"].ToIntDef(0);
			j.AzionePallet		 = TabJobs[i]["AzionePallet"].ToIntDef(0);
			j.GruppoLavorazione  = TabJobs[i]["GruppoLavorazione"];
			j.Descrizione_Op	 = TabJobs[i]["Descrizione_Op"];
			j.Commento			 = TabJobs[i]["Commento"];
            j.ID_Lotto			 = TabJobs[i]["ID_Lotto"].ToIntDef(0);
            j.ID_Lavoro			 = TabJobs[i]["ID_Lavoro"].ToIntDef(0);
            j.IndiceLavoro		 = TabJobs[i]["IndiceLavoro"].ToIntDef(0);
			TotaliRichiesti      = TabJobs[i]["Totali"].ToIntDef(0);
			Inviati			     = TabJobs[i]["Inviati"].ToIntDef(0);
			Disponibili          = TabJobs[i]["Disponibili"].ToIntDef(0);
			Vietati              = TabJobs[i]["Vietati"].ToIntDef(0);

			//Controllo missione per pezzi
			if ((j.Tipo_Missione == "PICK_PZ_TURN") || (j.Tipo_Missione == "PICK_PZ")) {
				//Qui a questo punto nell'ID pallet ho contenuto in realt� l'id del pezzo.
				//Devo verificare se posso fare la missione di giro pezzo o di riporto del pezzo
				LeggiPalletDaPezzo(j.ID_Pezzo, p);
				// Se ho trovato il pallet
				if (p.ID_Pallet > 0) {
					if (JobInoltrabilePerPezzo(j, p.ID_Pallet)) {
						m.ID_Pallet = j.ID_Pallet;
						m.ID_Macchina = j.ID_Macchina;
						m.Tipo_Macchina = j.Tipo_Macchina;
						m.Tipo_Missione = j.Tipo_Missione;
						m.Programma_Macchina = j.Programma_Macchina;
						m.ID_Articolo = j.ID_Articolo;
						m.Priorita = j.Priorita;
						m.ID_Job = j.ID_Job;
						m.GruppoFase = j.GruppoFase;
						m.GruppoPallet = j.GruppoPallet;
						m.ID_Pezzo = j.ID_Pezzo;
						m.AzionePallet = j.AzionePallet;
						m.GruppoLavorazione = j.GruppoLavorazione;
						m.Descrizione_Op = j.Descrizione_Op;
                        m.ID_Lotto = j.ID_Lotto;
                        m.ID_Lavoro = j.ID_Lavoro;
                        m.IndiceLavoro = j.IndiceLavoro;
						CreaMissione(m);
						CancellaJob(j.ID_Job);
					}
				}
			}
			else if ((j.Tipo_Missione == "DROP_PZ")) {	//Missione di riporto pezzo in baia
				//Qui ho un pallet vuoto in baia e il pezzo sul trespolo...
				//Leggo il pallet per verificare che il pallet in baia sia un pallet vuoto e che sia di tipo 2 o 3 (C o D)
				LeggiPalletInMacchina(j.ID_Macchina, "S", p);
				//Verifico tipo pallet
				if ((p.ID_TipoPallet > 0) && ((p.ID_TipoPallet == 2) || (p.ID_TipoPallet == 3))) {
					if ((!EsisteGiaMissione("DROP", j.Tipo_Macchina, j.ID_Macchina)) && (!EsisteGiaMissione("PICK", j.Tipo_Macchina, j.ID_Macchina))) {
						m.ID_Pallet = j.ID_Pallet;
						m.ID_Macchina = j.ID_Macchina;
						m.Tipo_Macchina = j.Tipo_Macchina;
						m.Tipo_Missione = j.Tipo_Missione;
						m.Programma_Macchina = j.Programma_Macchina;
						m.ID_Articolo = j.ID_Articolo;
						m.Priorita = j.Priorita;
						m.ID_Job = j.ID_Job;
						m.GruppoFase = j.GruppoFase;
						m.GruppoPallet = j.GruppoPallet;
						m.ID_Pezzo = j.ID_Pezzo;
						m.AzionePallet = j.AzionePallet;
						m.GruppoLavorazione = j.GruppoLavorazione;
						m.Descrizione_Op = j.Descrizione_Op;
                        m.ID_Lotto = j.ID_Lotto;
                        m.ID_Lavoro = j.ID_Lavoro;
                        m.IndiceLavoro = j.IndiceLavoro;
						CreaMissione(m);
						CancellaJob(j.ID_Job);
					}
				}
			}
			else {  //Missioni normali
				if (JobInoltrabile(j)) {
                    m.ID_Pallet = j.ID_Pallet;
                    m.ID_Macchina = j.ID_Macchina;
                    m.Tipo_Macchina = j.Tipo_Macchina;
                    m.Tipo_Missione = j.Tipo_Missione;
                    m.Programma_Macchina = j.Programma_Macchina;
                    m.ID_Articolo = j.ID_Articolo;
                    m.Priorita = j.Priorita;
                    m.ID_Job = j.ID_Job;
                    m.GruppoFase = j.GruppoFase;
                    m.GruppoPallet = j.GruppoPallet;
                    m.ID_Pezzo = j.ID_Pezzo;
                    m.AzionePallet = j.AzionePallet;
                    m.GruppoLavorazione = j.GruppoLavorazione;
                    m.Descrizione_Op = j.Descrizione_Op;
                    m.ID_Lotto = j.ID_Lotto;
                    m.ID_Lavoro = j.ID_Lavoro;
                    m.IndiceLavoro = j.IndiceLavoro;
                    //Creo la missione
                    m.ID_Missione = CreaMissione(m);
                    //Cancello il job
                    CancellaJob(j.ID_Job);
                    //Aggiorno la tabella dei lavori in corso
                    AggiornaMissioneLavoriInCorso(j.ID_Job, m.ID_Missione);
				}
			}
		}
	} catch(...) {}
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaJobLavoriInCorso(int ID_Job, int ID_Lavoro, int Indice)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET ID_Job = %d WHERE (ID_Lavoro = %d AND Indice = %d)", ID_Job, ID_Lavoro, Indice);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ResettaLavoriInCorso(int ID_Lavoro, int Indice)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET ID_Job = 0, ID_Missione = 0 WHERE (ID_Lavoro = %d AND Indice = %d)", ID_Lavoro, Indice);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaMissioneLavoriInCorso(int ID_Job, int ID_Missione)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET ID_Missione = %d WHERE (ID_Job = %d)", ID_Missione, ID_Job);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CompletaFaseLavoriInCorso(int ID_Lavoro, int Indice)
{
	AnsiString strsql, IdPallet;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET FaseCompletata = 1 WHERE (ID_Lavoro = %d AND Indice = %d)", ID_Lavoro, Indice);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
        if (res) {
			//Leggo ID Pallet per quel lavoro
			strsql.sprintf("WHERE (ID_Lavoro = %d AND Indice = %d)", ID_Lavoro, Indice);
			LeggiCampoTabella("LavoriInCorso", "ID_Pallet", strsql, IdPallet);

            //Controllo lo stato dei lavori in corso per quel pallet
            AggiornaLavoriInCorso(ID_Lavoro, IdPallet.ToIntDef(0));
        }
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::RiattivaLavoriInCorso(int ID_Lavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE LavoriInCorso SET FaseCompletata = 0, ID_Job = 0, ID_Missione = 0 WHERE (ID_Lavoro = %d)", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EliminaLavoriInCorso(int ID_Lavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE LavoriInCorso WHERE (ID_Lavoro = %d)", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EliminaLotto(int ID_Lavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE Lotti WHERE (ID_Lavoro = %d)", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaLavoroDaEseguire(int ID_Pallet)
{
	TJob j;
	TFase f;
    TPallet p;
	TPezzo pz;
	TLavorazione l;
	AnsiString strsql, GruppoPallet, GruppoFase, GruppoLavorazione;
	AnsiString toper;
	TRecordList TabLavori;
    int res, IdLav, IdFase, ID_Pezzo, ID_Lotto, prior;

	// Modifica per selezionare solo lavori da lotti attivi
	strsql.sprintf("Select TOP (1) LIC.* FROM LavoriInCorso AS LIC "
				   "INNER JOIN Lotti AS L ON LIC.ID_Lavoro = L.ID_Lavoro "
				   "WHERE LIC.LavoroAttivo = 1 AND LIC.FaseCompletata = 0 AND LIC.ID_Pallet = %d AND (L.Sospeso = 0) "
				   "ORDER BY LIC.ID_Lavoro, LIC.Indice", ID_Pallet);
	if (CaricaTabellaQ(strsql, TabLavori) == 0) {
		// Se non ho nessun lotto attivo controllo se c'� in corso una lavorazione singola
		strsql.sprintf("SELECT TOP (1) * "
					   "FROM LavoriInCorso AS LIC "
					   "WHERE (LavoroAttivo = 1) AND (FaseCompletata = 0) AND (ID_Pallet = %d) "
					   "AND (NOT EXISTS (SELECT * FROM Lotti AS L "
					   "WHERE (ID_Pallet = LIC.ID_Pallet) AND (ID_Lavoro = LIC.ID_Lavoro)))", ID_Pallet);
		if (CaricaTabellaQ(strsql, TabLavori) == 0) {
			return -1;
		}
	}

    //Ho generato un Job per questo lavoro ma non � ancora stato tramutato in missione
    if (TabLavori[0]["ID_Job"].ToIntDef(0) != 0) {
        return -1;
    }

    //Resetto la variabile a 0, al primo giro l'indice del pallet � sempre presente
    GruppoPallet = "0";

    //Prendo la priorit� 
    IdLav = TabLavori[0]["ID_Lavorazione"].ToIntDef(0);
    IdFase = TabLavori[0]["ID_Fase"].ToIntDef(0);

    //leggo la struttura delle lavorazioni
    if (LeggiLavorazione(IdLav, l) == 0) {
        return -1;
    }

    //Leggo la struttura delle fasi
    if (LeggiFase(IdLav, IdFase, f) == 0) {
        return -1;
    }

    //Resetto la struttura JOBS
    memset(&j, 0, sizeof(TJob));

    //Genero un ID univoco per tutti i JOB generati per quel pallet
    GetGruppoLavorazioneID(IdLav, GruppoLavorazione);

    prior = TabLavori[0]["Priorita"].ToIntDef(0);

    //Prendo i dati del pallet in esame
    LeggiPallet(ID_Pallet, p);
    ID_Pezzo = p.ID_Pezzo;

    // Imposto la descrizione dell'operazione come la descrizione della Fase
    j.Descrizione_Op = f.Descrizione;

    //Leggo il Codice operatore
    strsql.sprintf("WHERE (ID_Operazione = %d)", f.ID_Operazione);
    LeggiCampoTabella("OperazioniFasi", "Codice_Op", strsql, toper);

    //Se la baia non � stata selezionata durante la compilazione della fase
    //setto questo flag per discriminare durante la scelta nel server per riportare
    //la baia utilizzata per tutte le missioni della fase.
    if (f.ID_Macchina == 0) {
        //In questo modo se l'operatore esegue due lavorazioni uguali, identifico in modo univoco le missioni
        //che appartengono alla stessa lavorazione-fase-pallet di partenza
        GetGruppoFaseID(IdLav, IdFase, j.GruppoFase);
    }
    else {
        j.GruppoFase = "0";
    }

    //Controllo se il pallet � associato ad un lotto di produzione
    ID_Lotto = PalletAssociatoLotto(ID_Pallet);

    //Memorizzo le chiavi per rintracciare la fase in esecuzione
    j.ID_Lavoro = TabLavori[0]["ID_Lavoro"].ToIntDef(0);
    j.IndiceLavoro = TabLavori[0]["Indice"].ToIntDef(0);
    j.ID_Lotto = ID_Lotto;

    //Identifico in modo univoco tutti i JOB generati per quel pallet
    j.GruppoLavorazione = GruppoLavorazione;

    //Fase di ribaltamento pezzo o cambio plattorello
    if (toper == "RBL" || toper == "CHN") {
        //Creo la missione di deposito in baia
        j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = "S";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = "";
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
        j.AzionePallet = 0;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB

        //Creo la missione di prelievo pezzo dal pallet ribaltandolo
        j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = (toper == "RBL") ? "PICK_PZ_TURN" : "PICK_PZ";
        j.Tipo_Macchina = "S";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = "";
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
        j.AzionePallet = 0;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB

        //Nel momento in cui vado a portare il macchina il nuovo pallet a cui dovr�
        //successivamente assegnare il nuovo ID Pallet, utilizzo il nuovo indice
        //per carcare il nuovo Tipo Pallet
        GetGruppoPalletID(IdLav, IdFase, GruppoPallet);

        //Nel server nella InoltraJob andr� a cercare il nuovo ID Pallet
        //per quel tipo di plattorello che servir� successivamente
        //Memorizzo l'indice della fase a cui si trovata il cambio plattorello
        j.ID_Pallet = ID_Pallet = 0;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = "S";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = "";
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = GruppoPallet;
        j.AzionePallet = 0;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB

        //Creo la missione di deposito pezzo sul pallet
        j.ID_Pallet = 0;
        j.Tipo_Missione = "DROP_PZ";
        j.Tipo_Macchina = "S";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = "";
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = GruppoPallet;
        j.AzionePallet = 0;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB
    }
    //Fase di ribaltamento pezzo o cambio plattorello
    else if (toper == "CHN_M") {
        //Creo la missione di deposito in baia
        j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = "S";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = "";
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
        j.AzionePallet = 1;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB

        //Nel momento in cui vado a portare il macchina il nuovo pallet a cui dovr�
        //successivamente assegnare il nuovo ID Pallet, utilizzo il nuovo indice
        //per carcare il nuovo Tipo Pallet
        GetGruppoPalletID(IdLav, IdFase, GruppoPallet);

        //Nel server nella InoltraJob andr� a cercare il nuovo ID Pallet
        //per quel tipo di plattorello che servir� successivamente
        //Memorizzo l'indice della fase a cui si trovata il cambio plattorello
        j.ID_Pallet = ID_Pallet = 0;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = "S";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = "";
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = GruppoPallet;
        j.AzionePallet = 0;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB
    }
    //Carico/Scarico stessa baia
    else if (toper == "OPM") {
        j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = "S";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = "";
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
        j.AzionePallet = 0;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB
    }
    else if (toper == "LVG") {
        //Missione per la saldatura, prima deposito sul banco esterno
        j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = "DROP";
        //j.Tipo_Macchina = "BI";
        // NO! La missione deve andare alla lavatrice, solo se � occupata va al banchetto
        // lo verificheremo sul server al momento dell'esecuzione
        j.Tipo_Macchina = "M";
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = f.Programma_MU;
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
        j.AzionePallet = 0;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB
    }
    else if (toper == "CRC"){
        //Attrezzaggio in baia
		j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = f.Tipo_Macchina;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = f.Programma_MU;
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
        if (ID_Pezzo == 0) {
            j.AzionePallet = 2;
			// Serve per inserire il pezzo grezzo
			SvuotaPallet(p);
            //Resetto la struttura Pezzo
            memset(&pz, 0, sizeof(TPezzo));
            pz.ID_Articolo = l.ID_Articolo;
			ID_Pezzo = CreaPezzo(pz);
            //Aggiungo il pezzo sul pallet
			AggiornaPezzoSuPallet(ID_Pallet, ID_Pezzo);
        }
		else {
            j.AzionePallet = 0;
        }
        j.ID_Pezzo = ID_Pezzo;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB
    }
    else if (toper == "SCR"){
        //Scarico attrezzaggio in baia
        j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = f.Tipo_Macchina;
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = f.Programma_MU;
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
		j.AzionePallet = 0; //3;
		j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB
    }
    else {
        //Deposito semplice in macchina standard
        j.ID_Pallet = ID_Pallet;
        j.Tipo_Missione = "DROP";
        j.Tipo_Macchina = f.Tipo_Macchina;
        j.ID_Pezzo = ID_Pezzo;
        j.ID_Macchina = f.ID_Macchina;
        j.Programma_Macchina = f.Programma_MU;
        j.ID_Articolo = l.ID_Articolo;
        j.GruppoPallet = (j.ID_Pallet == 0) ? GruppoPallet : (AnsiString)("0");
        j.AzionePallet = 0;
        j.Priorita = prior;
        res = CreaJobDaFase(j);	//Creo il JOB
    } 

    //Significa che il pallet � gi� presente nella postazione di destinazione
    if (res == -1) {
        // Dichiaro questa fase completata cos� posso passare alla successiva
        // ma solo se il pezzo sul pallet non ha dato errori tipo ctrl pel fallito
        LeggiPezzo(ID_Pezzo, pz);
        if (pz.Stato_Pezzo < STATO_PEZZO_ERRORE_PEL) {            
        	//Aggiorno lo stato del pezzo all'indice in corso
            AggiornaStatoPezzo(pz.ID_Pezzo, TabLavori[0]["Indice"].ToIntDef(0) + 1);
            CompletaFaseLavoriInCorso(TabLavori[0]["ID_Lavoro"].ToIntDef(0), TabLavori[0]["Indice"].ToIntDef(0));
        }
    }
    else {
        //Se ho creato e attivato un job aggiorno la tabella
        if (j.ID_Job) {
            AggiornaJobLavoriInCorso(j.ID_Job, TabLavori[0]["ID_Lavoro"].ToIntDef(0), TabLavori[0]["Indice"].ToIntDef(0));
        }
    }
    return 0;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaJobDaFase(TJob &j)
{
	TPallet p;
    TMissioneDB m;

    //Controllo che il pallet non sia gi� presente nella stazione di destinazione
    if (j.ID_Macchina == 0) {
        CercaPallet(j.ID_Pallet, j.Tipo_Macchina, p);
    }
    else {
    	LeggiPalletInMacchina(j.ID_Macchina, j.Tipo_Macchina, p);
    }
    if (p.ID_Pallet == j.ID_Pallet) {
    	memset(&m, 0, sizeof(m));
        m.AzionePallet = j.AzionePallet;
		m.ID_Pezzo = j.ID_Pezzo;
        m.GruppoLavorazione = j.GruppoLavorazione;
        //Anche se il pallet � gi� presente nella stazione, devo completare le sue azioni
        CompletaAzionePallet(m, p);
        return -1;
    }
    else {
        //Creo il JOB
        j.ID_Job = CreaJob(j);

        //Attivo il job
        AttivaJob(j.ID_Job);
        return j.ID_Job;
    }
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaLavoriInCorso(int ID_Lavoro, int ID_Pallet)
{
	AnsiString strsql;
	TRecordList TabNumLavori;
	int res;
	TPallet p;
	TPezzo pz;

	try {
    	//Conto il numero delle fasi che mi rimangono da completare
//    	strsql.sprintf("Select count(*) AS Num FROM LavoriInCorso WHERE LavoroAttivo = 1 AND FaseCompletata = 0 AND ID_Lavoro = %d", ID_Lavoro);
    	strsql.sprintf("Select count(*) AS Num FROM LavoriInCorso WHERE FaseCompletata = 0 AND ID_Lavoro = %d", ID_Lavoro);
		CaricaTabellaQ(strsql, TabNumLavori);

        //Se tutte le fasi sono state completate allora gestisco il fine lavorazione
        if (TabNumLavori[0]["Num"].ToIntDef(-1) == 0) {
            //Controllo se il pallet � stato associato ad un Lotto
			if (PalletAssociatoLotto(ID_Pallet)) {
				//Incremento la qta prodotta e aggiorno il lotto
				LeggiPallet(ID_Pallet, p);
				LeggiPezzo(p.ID_Pezzo, pz);
				if (pz.Stato_Pezzo < STATO_PEZZO_ERRORE_PEL) {
					AggiornaQtaLotto(ID_Lavoro);
				}
				//Log
				Log("SERVER", "DB", "Pallet %d associato ad un Lotto: Incremento la Qta Prodotta", ID_Pallet);
                //Controllo se ho completato la quantit� richiesta del lotto
                if (QtaMancanteFineLotto(ID_Lavoro) <= 0) {                
                    //Svuoto il pallet in modo da togliere i riferimenti al pezzo
                    SvuotaPallet(p);
                    //Elimino il lotto
					EliminaLotto(ID_Lavoro);
                    //Elimino le fasi di lavoro associate a quel pallet
					EliminaLavoriInCorso(ID_Lavoro);
                    //Log
					Log("SERVER", "DB", "Pallet %d associato ad un Lotto: Qta Totale Prodotta, elimino le fasi e il lotto", ID_Pallet);
				}
                else {
                    //Riattivo le fasi di lavoro associate a quel pallet
                    res = RiattivaLavoriInCorso(ID_Lavoro);
                    //Log
					Log("SERVER", "DB", "Pallet %d associato ad un Lotto: Riattivo le fasi", ID_Pallet);
                }
            }
            else {
            	//Elimino le fasi di lavoro associate a quel pallet
				res = EliminaLavoriInCorso(ID_Lavoro);
                //Log
				Log("SERVER", "DB", "Pallet %d: elimino le fasi completate", ID_Pallet);
            }
        }
        else {
        	//Se ci sono altri lavori eseguo il ciclo
            CercaLavoroDaEseguire(ID_Pallet);
        }
        
    }
    catch (...) {}

    return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteLavoriInCorso(int ID_Lavorazione, int ID_Pallet)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.sprintf("Select * FROM LavoriInCorso WHERE ID_Lavorazione = %d AND ID_Pallet = %d", ID_Lavorazione, ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = 1;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::CompletaAzionePallet(TMissioneDB m, TPallet p)
{
    switch (m.AzionePallet) {
    case 1:
        //Elimino l'associazione pallet-pezzo non eliminando il pezzo a DB
        SvuotaPallet(p);
        break;
    case 2:
        //Associo il pezzo al pallet partendo dalla missione
        if (m.ID_Pezzo != 0) {
            //Creo il nuovo pezzo e lo associo al pallet
            p.ID_Pezzo = m.ID_Pezzo;
            //Aggiorno il pallet
            AggiornaPallet(p);
            //Aggiorno il nuovo ID Pezzo a tutti i JOB creati per quel gruppo lavorazione
            AggiornaIdPezzoJob(p.ID_Pezzo, m.GruppoLavorazione);
        }
        break;
    case 3:
        //Elimino il pezzo
        CancellaPezzo(p.ID_Pezzo);
        //Elimino l'associazione pallet-pezzo non eliminando il pezzo a DB
        SvuotaPallet(p);
        break;
    default:
        if (p.ID_Pezzo == 0) {
            //Associo ID pezzo del pallet partendo dalla missione
            p.ID_Pezzo = m.ID_Pezzo;
            //Aggiorno il pallet
            AggiornaPallet(p);
        }
        break;
    }
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaBaieFasi(AnsiString GruppoFase, int ID_Macchina)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		//Aggiornamento missioni
		strsql.printf("UPDATE Missioni SET Id_Macchina = %d WHERE (GruppoFase = '%s') AND (ID_Macchina = 0)", ID_Macchina, GruppoFase.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
		//Aggiornamento Job
		strsql.printf("UPDATE Jobs SET Id_Macchina = %d WHERE (GruppoFase = '%s') AND (ID_Macchina = 0)", ID_Macchina, GruppoFase.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

bool TDBDataModule::ControlloPalletDaLavare(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int Abilita_Lavaggio, Lavaggio_Eseguito;
	bool res = false;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM ElencoPallet WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			Abilita_Lavaggio	= ReadInt(ADOQuery, "Abilita_Lavaggio");
			Lavaggio_Eseguito	= ReadInt(ADOQuery, "Lavaggio_Eseguito");
			if (Abilita_Lavaggio && !Lavaggio_Eseguito) {
				res = true;
			}
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ProgrammaLavaggioPallet(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM ElencoPallet WHERE ID_Pallet = %d", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res	= ReadInt(ADOQuery, "Programma_Lavaggio");
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ImpostaPezzoLavato(int ID_Pezzo, int lavato) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pezzi SET Lavaggio_Eseguito = %d WHERE ID_Pezzo = %d", lavato, ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::GetSelfGenID(AnsiString TableName) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT IDENT_CURRENT( '%s' ) AS ID", TableName.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int ProssimoStatoPezzo(int Stato_Attuale, int Stato_Richiesto) {
	int Stato, Stato_Errore, res = Stato_Attuale;

	Stato_Errore = Stato_Attuale / 100;
	Stato = Stato_Attuale % 100;
	if (Stato_Attuale >= STATO_PEZZO_ERRORE_PEL) {
		res = Stato_Attuale;
	} else if (Stato_Richiesto >= STATO_PEZZO_ERRORE_PEL) {
		res = Stato + Stato_Richiesto;
	} else {
		res = Stato_Richiesto;
	}
//      Gestione 2 fasi di lavorazione tipo Hoffman
//		switch (Stato_Richiesto) {
//		case STATO_PEZZO_GREZZO:
//			res = ((Stato == STATO_PEZZO_LAVORATO_FASE2) ? STATO_PEZZO_LAVORATO : STATO_PEZZO_GREZZO);
//			break;
//		case STATO_PEZZO_ATTREZZATO:
//			res = ((Stato == STATO_PEZZO_LAVORATO) ? STATO_PEZZO_ATTREZZATO_FASE2 : STATO_PEZZO_ATTREZZATO);
//			break;
//		case STATO_PEZZO_LAVORATO:
//			res = ((Stato == STATO_PEZZO_ATTREZZATO_FASE2) ? STATO_PEZZO_LAVORATO_FASE2 : STATO_PEZZO_LAVORATO);
//			break;
//		}
//	}
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaStatoPezzo(int ID_Pezzo, int NuovoStato) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	TPezzo pz;
	int Stato, res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		if (NuovoStato == -1) {
			strsql.printf("UPDATE Pezzi SET Stato_Pezzo = Stato_Pezzo + 1 WHERE ID_Pezzo = %d", ID_Pezzo);
		} else {
			LeggiPezzo(ID_Pezzo, pz);
			Stato = ProssimoStatoPezzo(pz.Stato_Pezzo, NuovoStato);
			strsql.printf("UPDATE Pezzi SET Stato_Pezzo = %d WHERE ID_Pezzo = %d", Stato, ID_Pezzo);
		}
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ResettaErrorePezzo(int ID_Pezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pezzi SET Stato_Pezzo = Stato_Pezzo %% 100 WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaPezzo(TPezzo pz) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO Pezzi "
					  "(ID_Articolo, Stato_Pezzo, Misura_Eseguita, "
					  "OffsetX, OffsetY, OffsetZ, OffsetA, OffsetB, OffsetC, OffsetROT, Gap, GapMin, GapMax, H, Descrizione) "
					  "VALUES (%d, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, '%s') ",
			pz.ID_Articolo,
			pz.Stato_Pezzo,
			pz.Misura_Eseguita,
			pz.OffsetX,
			pz.OffsetY,
			pz.OffsetZ,
			pz.OffsetA,
			pz.OffsetB,
			pz.OffsetC,
			pz.OffsetROT,
			pz.Gap,
			pz.GapMin,
			pz.GapMax,
			pz.H,
			pz.Descrizione.c_str()
			);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return GetSelfGenID("Pezzi");
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiPezzo(int ID_Pezzo, TPezzo &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
    int res = 0;

	memset(&p, 0, sizeof(TPezzo));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pezzi WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			p.ID_Pezzo           = ReadInt(ADOQuery, "ID_Pezzo");
			p.ID_Articolo        = ReadInt(ADOQuery, "ID_Articolo");
			p.Stato_Pezzo        = ReadInt(ADOQuery, "Stato_Pezzo");
			p.Abilita_Lavaggio   = ReadInt(ADOQuery, "Abilita_Lavaggio");
			p.Lavaggio_Eseguito  = ReadInt(ADOQuery, "Lavaggio_Eseguito");
			p.Programma_Lavaggio = ReadInt(ADOQuery, "Programma_Lavaggio");
			p.Abilita_Misura     = ReadInt(ADOQuery, "Abilita_Misura");
			p.Misura_Eseguita    = ReadInt(ADOQuery, "Misura_Eseguita");
			p.OffsetX            = ReadFloat(ADOQuery, "OffsetX");
			p.OffsetY            = ReadFloat(ADOQuery, "OffsetY");
			p.OffsetZ            = ReadFloat(ADOQuery, "OffsetZ");
			p.OffsetA            = ReadFloat(ADOQuery, "OffsetA");
			p.OffsetB            = ReadFloat(ADOQuery, "OffsetB");
			p.OffsetC            = ReadFloat(ADOQuery, "OffsetC");
			p.OffsetROT          = ReadFloat(ADOQuery, "OffsetROT");
			p.Gap          		 = ReadFloat(ADOQuery, "Gap");
			p.GapMin          	 = ReadFloat(ADOQuery, "GapMin");
			p.GapMax          	 = ReadFloat(ADOQuery, "GapMax");
			p.H          		 = ReadFloat(ADOQuery, "H");
			p.Descrizione		 = ReadString(ADOQuery, "Descrizione");
            res = 1;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
    return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ImpostaPresetPezzo(TPezzo p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Pezzi SET OffsetX = %lf, "
									   "OffsetY = %lf, "
									   "OffsetZ = %lf, "
									   "OffsetA = %lf, "
									   "OffsetB = %lf, "
									   "OffsetC = %lf, "
									   "OffsetROT = %lf, "
									   "Gap = %lf, "
									   "GapMin = %lf, "
									   "GapMax = %lf, "
									   "H = %lf, "
									   "Misura_Eseguita = 1 "
									   "WHERE ID_Pezzo = %d",
									   p.OffsetX,
									   p.OffsetY,
									   p.OffsetZ,
									   p.OffsetA,
									   p.OffsetB,
									   p.OffsetC,
									   p.OffsetROT,
									   p.Gap,
									   p.GapMin,
									   p.GapMax,
									   p.H,
									   p.ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CommentoJob(int JobID, AnsiString commento) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE JOBS SET Commento = '%s' WHERE ID_Job = %d", commento.c_str(), JobID);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaIdPezzoJob(int ID_Pezzo, AnsiString Gruppo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE JOBS SET ID_Pezzo = %d WHERE (GruppoLavorazione = '%s') AND (ID_Pezzo = 0)", ID_Pezzo, Gruppo.c_str());
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CommentoMissione(int MissID, AnsiString commento) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE Missioni SET Commento = '%s' WHERE ID_Missione = %d", commento.c_str(), MissID);
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogProdInizioLavoro(int ID_Pallet, int ID_Macchina, AnsiString CodiceArticolo, AnsiString Programma) {
	TADOQuery *ADOQuery;
	AnsiString strsql;
    TPallet p;

    LeggiPallet(ID_Pallet, p);
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO StoricoProduzione (DataOra, Attivazione, Inizio_Lavoro, ID_Pallet, Codice_Articolo, ID_Macchina, Tipo_Macchina, Programma_Macchina, Acquisito) "
					  "VALUES (GetDate(), GetDate(), GetDate(), %d, '%s', %d, 'M', '%s', 0)",
                      ID_Pallet,
                      CodiceArticolo.c_str(),
                      ID_Macchina,
                      Programma.c_str()
                      );
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogLavoroProd(AnsiString campo, int ID_Pallet, int ID_Macchina)
{
	TADOQuery *ADOQuery;
	AnsiString strsql;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE StoricoProduzione SET %s = GetDate() WHERE (ID_Pallet = %d) AND (ID_Macchina = %d) AND (%s IS NULL)", campo.c_str(), ID_Pallet, ID_Macchina, campo.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogProdLavoro(AnsiString campo, int ID_Pallet, AnsiString Tipo_Macchina, int ID_Macchina)
{
	TADOQuery *ADOQuery;
	AnsiString strsql, cond;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		cond.printf("(ID_Pallet = %d) AND (Tipo_Macchina = '%s') AND (ID_Macchina = %d) AND (%s IS NULL)",
			ID_Pallet, Tipo_Macchina.c_str(), ID_Macchina, campo.c_str());
		strsql.printf("UPDATE StoricoProduzione SET %s = GetDate() WHERE %s AND (DataOra = "
					  "(SELECT MAX(DataOra) AS res FROM StoricoProduzione AS S "
					  "WHERE %s))", campo.c_str(), cond.c_str(), cond.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogDestProd(int ID_Job, int nmag, int npos) {
	TADOQuery *ADOQuery;
	AnsiString strsql;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE StoricoProduzione SET Magazzino_Destinazione = %d, Postazione_Destinazione = %d WHERE (ID_Job = %d) AND (Magazzino_Destinazione IS NULL)", nmag, npos, ID_Job);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LogPrelievoMissione() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE StoricoMissioni SET Prelievo = GetDate() WHERE Prelievo IS NULL AND Deposito IS NULL");
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LogDepositoMissione() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE StoricoMissioni SET Deposito = GetDate() WHERE Deposito IS NULL");
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::MissioneInviata() {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE TOP (1) FROM CODAMIX WHERE Strobe = 1");
		ADOQuery->SQL->Text = strsql;
		res = ADOQuery->ExecSQL();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiArticoloInProduzione(AnsiString codice, int MU, dati_articolo &d) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		d.Codice = "";
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM PROD_ATTIVA WHERE (Codice = '%s') AND (MU = %d) AND (Esecuzione <> 0) AND (Completato = 0)", codice.c_str(), MU);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			d.Codice = codice;
			d.PezziPerPallet = ADOQuery->FieldByName("PezziPerPallet")->Value;
			d.Attrezzatura   = ADOQuery->FieldByName("Attrezzatura")->Value;
			d.Controlli		 = ADOQuery->FieldByName("Controlli")->Value;
			d.Altro			 = ADOQuery->FieldByName("Altro")->Value;
			d.Produzione     = ADOQuery->FieldByName("Produzione")->Value;
			d.Quantita       = ADOQuery->FieldByName("Quantita")->Value;
			d.Finiti       	 = ADOQuery->FieldByName("Finiti")->Value;
			d.Esecuzione     = ADOQuery->FieldByName("Esecuzione")->Value;
			d.Completato     = ADOQuery->FieldByName("Completato")->Value;
		}
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::LavoraArticolo(AnsiString codice, int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE PROD_ATTIVA SET Esecuzione = 1, Inizio = GetDate() WHERE (MU = %d) AND (Codice = '%s') AND (Esecuzione = 0)", MU, codice.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CompletaArticolo(AnsiString codice, int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE PROD_ATTIVA SET Completato = 1 WHERE (MU = %d) AND (Codice = '%s') AND (Esecuzione = 1)", MU, codice.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaArticoloCompletato(AnsiString codice, int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE PROD_ATTIVA WHERE (MU = %d) AND (Codice = '%s') AND (Esecuzione = 1) AND (Completato = 1)", MU, codice.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::ProssimoArticolo(dati_articolo &d, double t, int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM PROD_ATTIVA WHERE (MU = %d) AND (Completato = 0) AND (Esecuzione = 0) AND ((Attrezzatura + Controlli) <= %f ) ORDER BY Attrezzatura DESC", MU, t);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			d.Codice 		 = ADOQuery->FieldByName("Codice")->Value;
			d.PezziPerPallet = ADOQuery->FieldByName("PezziPerPallet")->Value;
			d.Attrezzatura   = ADOQuery->FieldByName("Attrezzatura")->Value;
			d.Controlli   	 = ADOQuery->FieldByName("Controlli")->Value;
			d.Altro		   	 = ADOQuery->FieldByName("Altro")->Value;
			d.Produzione     = ADOQuery->FieldByName("Produzione")->Value;
			d.Quantita       = ADOQuery->FieldByName("Quantita")->Value;
			d.Finiti       	 = ADOQuery->FieldByName("Finiti")->Value;
			d.Esecuzione     = ADOQuery->FieldByName("Esecuzione")->Value;
			d.Completato     = ADOQuery->FieldByName("Completato")->Value;
			res = 1;
		} else {
			strsql.printf("SELECT * FROM PROD_ATTIVA WHERE (MU = %d) AND (Completato = 0) AND (Esecuzione = 0) ORDER BY (Produzione - Attrezzatura) DESC", MU);
			ADOQuery->SQL->Text = strsql;
			ADOQuery->Open();
			if (!ADOQuery->Eof) {
				d.Codice 		 = ADOQuery->FieldByName("Codice")->Value;
				d.PezziPerPallet = ADOQuery->FieldByName("PezziPerPallet")->Value;
				d.Attrezzatura   = ADOQuery->FieldByName("Attrezzatura")->Value;
				d.Controlli		 = ADOQuery->FieldByName("Controlli")->Value;
				d.Altro			 = ADOQuery->FieldByName("Altro")->Value;
				d.Produzione     = ADOQuery->FieldByName("Produzione")->Value;
				d.Quantita       = ADOQuery->FieldByName("Quantita")->Value;
				d.Finiti       	 = ADOQuery->FieldByName("Finiti")->Value;
				d.Esecuzione     = ADOQuery->FieldByName("Esecuzione")->Value;
				d.Completato     = ADOQuery->FieldByName("Completato")->Value;
				res = 1;
			}
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::AggiornaArticolo(AnsiString codice, int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	dati_articolo d;
	int qta;

	try {
		LeggiArticoloInProduzione(codice, MU, d);
		if (d.Codice.IsEmpty())
			return;
		qta = d.Finiti + d.PezziPerPallet;
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		if (qta < d.Quantita) {
			strsql.printf("UPDATE PROD_ATTIVA SET Finiti = %d WHERE (MU = %d) AND (Codice = '%s') AND (Completato = 0) AND (Esecuzione = 1)", qta, MU, codice.c_str());
			LogQtaLavoro(codice, qta);
		} else {
			strsql.printf("UPDATE PROD_ATTIVA SET Finiti = %d, Completato = 1 WHERE (MU = %d) AND (Codice = '%s') AND (Completato = 0) AND (Esecuzione = 1)", qta, MU, codice.c_str());
			LogFineLavoro(codice, qta);
		}
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiMaxArticoliMacchina(AnsiString Tipo_Macchina, int ID_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine "
					  "WHERE (ID_Macchina = %d) "
					  "AND (Tipo_Macchina = '%s') ", ID_Macchina, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "N_Articoli");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiNArticoliAttiviMacchina(int ID_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT COUNT(*) AS N "
					  "FROM PROD_ATTIVA "
					  "WHERE (MU = %d) AND (Esecuzione <> 0) AND (Completato = 0) ", ID_Macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "N");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiNArticoliProgrammatiMacchina(int ID_Macchina) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT COUNT(*) AS N "
					  "FROM PROD_ATTIVA "
					  "WHERE (MU = %d) AND (Completato = 0) ", ID_Macchina);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "N");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::ContaPalletInGiro(int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int n = 0, id1 = 0, id2 = 0, res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Macchine "
					  "WHERE (ID_Macchina = %d) "
					  "AND (Tipo_Macchina = 'M') ", MU);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			id1 = ReadInt(ADOQuery, "ID_Pallet");
			id2 = ReadInt(ADOQuery, "Adattatore");
		}
		ADOQuery->Close();
		if (id1)
			res = 1;
		if (id2)
			res++;
		// Conto pallet attrezzati o finiti in magazzino
		strsql.printf("SELECT COUNT(*) AS N  FROM Pallet "
					  "INNER JOIN PROD_ATTIVA ON Pallet.Codice = PROD_ATTIVA.Codice "
					  "INNER JOIN Postazioni ON Pallet.ID_Pallet = Postazioni.ID_Pallet "
					  "WHERE (PROD_ATTIVA.Esecuzione = 1) AND (PROD_ATTIVA.Completato = 0) AND (Pallet.Stato_Pallet <> 0) "
					  "GROUP BY PROD_ATTIVA.MU HAVING (PROD_ATTIVA.MU = %d)",
					  MU);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			n = ReadInt(ADOQuery, "N");
		}
		ADOQuery->Close();
		res +=n;
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPalletVuotoInMagazzino(int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet_da_attrezzare "
					  "WHERE (MU = %d) AND (Stato_Pallet = 0)", MU);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Postazione");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPalletAttrezzatoInMagazzino(int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet_da_attrezzare "
					  "WHERE (MU = %d) AND (Stato_Pallet = 1)", MU);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Postazione");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPalletFinitoInMagazzino(int MU) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Pallet_da_attrezzare "
					  "WHERE (MU = %d) AND (Stato_Pallet = 2)", MU);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Postazione");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiProgrammaPallet(int id) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM ProgrammiPallet WHERE (ID_Pallet = %d) ", id);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "Programma");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::LogQtaLavoro(AnsiString codice, int qta)
{
	TADOQuery *ADOQuery;
	AnsiString strsql;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("UPDATE StoricoLavori SET PezziProdotti = %d WHERE (Codice = '%s') AND (Inizio IS NOT NULL) AND (Fine IS NULL)", qta, codice.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiCampoTabella(AnsiString Tabella, AnsiString CampoDaLeggere, AnsiString ClauslaWhere, AnsiString &Valore) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	Valore = ""; //Inizializzazione valore di ritorno
	//Creazione stringa Query
	strsql.printf("SELECT %s FROM %s %s", CampoDaLeggere.c_str(), Tabella.c_str(), ClauslaWhere.c_str());
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		// Verifica risultati
		if (!ADOQuery->Eof) {
			Valore = ADOQuery->FieldByName(CampoDaLeggere)->AsString;
		}
	} catch (Exception &E) {
		//return E
		res = -1;
	}
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiAbilitazioneVerificaUtensili(int MU)
{
	AnsiString Stato, sClausula;

	sClausula.printf("WHERE DescrizioneCampo = 'VerificaUtensiliMU%d'", MU);
	LeggiCampoTabella("Parametri", "ValoreCampo", sClausula.c_str(), Stato);
	return Stato.ToIntDef(0);
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiAbilitazioneControlloBalluff()
{
	AnsiString Stato, sClausula;

	sClausula.printf("WHERE DescrizioneCampo = 'ControlloBalluffON'");
	LeggiCampoTabella("Parametri", "ValoreCampo", sClausula.c_str(), Stato);
	return Stato.ToIntDef(0);
}
//---------------------------------------------------------------------------

void TDBDataModule::LeggiArticoloDaIdPezzo(int ID_Pezzo, TArticolo &a) {
	TPezzo pz;

	//Cerco ID pezzo dal pallet
	LeggiPezzo(ID_Pezzo, pz);
	//Cerco Articolo dal pezzo
	LeggiArticolo(pz.ID_Articolo, a);
}
//---------------------------------------------------------------------------

bool TDBDataModule::LeggiArticolo(int ID_Articolo, TArticolo &a) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	bool res = false;

	memset(&a, 0, sizeof(TArticolo));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Articoli WHERE ID_Articolo = %d", ID_Articolo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			a.ID_Articolo                  = ReadInt(ADOQuery, "ID_Articolo");
			a.ID_TipoPezzo                 = ReadInt(ADOQuery, "ID_TipoPezzo");
			a.Codice_Articolo              = ReadString(ADOQuery, "Codice_Articolo");
			a.Descrizione_Articolo         = ReadString(ADOQuery, "Descrizione_Articolo");
			a.DiamentroPezzo_Semilavorato  = ReadFloat(ADOQuery, "Diametro_Pz_SemiL");
			a.DiamentroPezzo_Grezzo	   	   = ReadFloat(ADOQuery, "Diametro_Pezzo_Grezzo");
			a.AltezzaPezzo	   	           = ReadFloat(ADOQuery, "Altezza_Pz");
			a.ftc                          = ReadInt(ADOQuery, "ftc_Controllo");
			a.Faccia_Pinza                 = ReadInt(ADOQuery, "Faccia_Pinza");
			a.TStazionamento			   = ReadInt(ADOQuery, "TStazionamento");
			res = true;
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::SvuotaPallet(TPallet &p) {
	if (p.ID_Pezzo) {
		p.ID_Pezzo = 0;
		p.Programma = "";
		p.Stato_Pallet = 0;
        AggiornaPallet(p);
	}
}
//---------------------------------------------------------------------------

int TDBDataModule::CheckIdPalletFromGruppoPallet(AnsiString Gruppo, int &RowCount) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;
	int IdPallet;
	TFase f;
	AnsiString lav, fase;
	std::string line;
	std::vector<std::string> vectline;

	if (Gruppo.ToIntDef(-1) == 0) {
		return res;
	}
	try {
		//Verifico se il pezzo corrisponde
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs WHERE GruppoPallet = '%s'", Gruppo.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ADOQuery->First();
			while (!ADOQuery->Eof) {
				if (ReadInt(ADOQuery, "ID_Pallet") == 0) {
					res = 1;
					break;
				}
				ADOQuery->Next();
			}
		}
		ADOQuery->Close();
	} catch(...) {};
	delete ADOQuery;
	if (res == 1) {
		line = Gruppo.c_str();
		//Trovo ID Lavorazione e ID fase del gruppo fase
		split(vectline, line, boost::is_any_of("-"));
		lav = vectline[1].c_str();
		fase = vectline[2].c_str();
		//Leggo la fase
		LeggiFase(lav.ToIntDef(0), fase.ToIntDef(0), f);
		if(CercaTipoPalletMagazzino(f.ID_TipoPalletXCambio, IdPallet) == 0) {
			try {
				ADOQuery = new TADOQuery(NULL);
				ADOQuery->Connection = ADOConnection1;
				strsql.printf("UPDATE Jobs SET ID_Pallet = %d WHERE ((GruppoPallet = '%s') AND (ID_Pallet = 0))", IdPallet, Gruppo.c_str());
				ADOQuery->SQL->Text = strsql;
				ADOQuery->ExecSQL();
				ADOQuery->Close();
				res = 2;
			} catch(...) {};
			delete ADOQuery;
		}
		else {
			try {
				ADOQuery = new TADOQuery(NULL);
				ADOQuery->Connection = ADOConnection1;
				strsql.printf("select count(*) as conta from jobs where (GruppoPallet = '%s')", Gruppo.c_str());
				ADOQuery->SQL->Text = strsql;
				ADOQuery->Open();
				//Conto il numero di fasi di quel gruppo fase
				if (!ADOQuery->Eof) {
					RowCount = ReadInt(ADOQuery, "conta");
				}
				ADOQuery->Close();
			} catch(...) {};
			delete ADOQuery;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPalletInBaia(int idPallet) {
	TPallet p, pb;
	int res = 0, npos = -1, i;

    DBDataModule->LeggiPallet(idPallet, p);
    if (p.ID_Pallet > 0) {
        //Cerco nelle baie
        for (i = 1; (i <= N_STAZIONI_OP) && (res == 0); i++) {
            //Leggo il pallet
            LeggiPalletInMacchina(i, "S", pb);
            //Cerco la posizione del pallet in baia
            npos = DBDataModule->PosizioneMacchina("S", i, pb.ID_TipoPallet);
            //Verifico se in baia � presente il pallet che sto cercando
            if ((npos > 0) && (pb.ID_Pallet == p.ID_Pallet)) {
                res = i;
            }
        }
    }
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaTipoPalletMagazzino(int ID_TipoPallet, int &ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Magazzino                                                                         "
					  "	WHERE  Magazzino.ID_Pallet NOT IN (SELECT Jobs.ID_Pallet FROM Jobs INNER JOIN                  "
					  "										   Magazzino ON Jobs.ID_Pallet = Magazzino.ID_Pallet       "
					  "										   WHERE Jobs.ID_Pallet = Magazzino.ID_Pallet)             "
					  "	AND Magazzino.ID_Pallet NOT IN (SELECT Missioni.ID_Pallet FROM Missioni INNER JOIN             "
					  "										   Magazzino ON Missioni.ID_Pallet = Magazzino.ID_Pallet   "
					  "										   WHERE Missioni.ID_Pallet = Magazzino.ID_Pallet)         "
					  "	AND Magazzino.ID_TipoPallet = %d AND Magazzino.ID_Pezzo = 0 AND Magazzino.ID_Pezzo IS NOT NULL "
					  , ID_TipoPallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ID_Pallet = ReadInt(ADOQuery, "ID_Pallet");
			res = 0;
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiFase(int ID_Lavorazione, int ID_Fase, TFase &f)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
    int res = 0;

	memset(&f, 0, sizeof(TFase));
	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Fasi WHERE ID_Lavorazione = %d AND ID_Fase = %d", ID_Lavorazione, ID_Fase);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			f.ID_Lavorazione 	   = ReadInt(ADOQuery, "ID_Lavorazione");
			f.ID_Fase			   = ReadInt(ADOQuery, "ID_Fase");
			f.Codice 			   = ReadString(ADOQuery, "Codice");
			f.Descrizione 		   = ReadString(ADOQuery, "Descrizione");
			f.ID_Operazione		   = ReadInt(ADOQuery, "ID_Operazione");
			f.ID_Macchina 		   = ReadInt(ADOQuery, "ID_Macchina");
			f.Tipo_Macchina 	   = ReadString(ADOQuery, "Tipo_Macchina");
			f.Programma_MU 		   = ReadString(ADOQuery, "Programma_MU");
			f.ID_TipoPallet   	   = ReadInt(ADOQuery, "ID_TipoPallet");
			f.MessaggioOp 		   = ReadString(ADOQuery, "Messaggio_Op");
			f.OrdineSeq       	   = ReadInt(ADOQuery, "Ordine_Seq");
			f.Abilitazione		   = ReadInt(ADOQuery, "Abilitazione");
			f.ID_TipoPalletXCambio = ReadInt(ADOQuery, "ID_TipoPalletxCambio");
            res = 1;
		}
		ADOQuery->Close();
	}
	catch(...) {};
	delete ADOQuery;
    return res;
}
//---------------------------------------------------------------------------

void TDBDataModule::AggiornaExtraAltezzaPallet(int ID_Pallet) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	//Questa query la utilizzo nella gestione dell'aggiornamento automantico del flag extra altezza
	//dei pallet. Nel caso in cui l'operatore cambi il diamentro massimo tale per cui il pallet deve
	//essere posizionato nei posti in alto dei magazzini, aggiorno tutta la tabella
	try {
		strsql.sprintf("UPDATE 	pallet SET Extra_Altezza = 0  WHERE pallet.ID_Pallet = %d		  "
				  " UPDATE p                                                                 	  "
				  "	SET    Extra_Altezza = 1                                                      "
				  "	FROM   Pallet AS p INNER JOIN                                        		  "
				  "		   Pezzi AS pz ON p.ID_Pezzo = pz.ID_Pezzo INNER JOIN                     "
				  "		   Articoli AS a ON pz.ID_Articolo = a.ID_Articolo CROSS JOIN             "
				  "		   Parametri AS par                                                       "
				  "		   WHERE   (par.Indice = 1 AND a.Diametro_Pezzo_Grezzo > par.ValoreCampo) "
				  "        AND P.ID_Pallet = %d ", ID_Pallet, ID_Pallet);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

void TDBDataModule::CancellaPezzo(int ID_Pezzo) {
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("DELETE FROM Pezzi WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
		strsql.printf("DELETE FROM Misurazioni WHERE ID_Pezzo = %d", ID_Pezzo);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

bool TDBDataModule::ToolPresentInTableLife(int tool, int VitaResidua, int Programma)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int TempoUtilizzo;
	bool res = true;

	try
    {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM TempiLavoroUtensile WHERE ((Utensile = %d) AND (Programma = %d))", tool, Programma);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		//Verifico se ho in tabella l'utensile per il programma richiesto
		if (ADOQuery->Eof) 	{
			//Utensile non presente in tabella
			res = true;
		}
        else {
			//Ora controllo se la vita scritta in tabella � sufficiente a terminare la lavorazione
			TempoUtilizzo = ReadInt(ADOQuery, "Min_Lavoro");
			//Controllo durata restante
			if ((VitaResidua - TempoUtilizzo) < 0) {
				//Tempo di lavoro utensile non sufficente
				res = false;
			}
		}
		ADOQuery->Close();
	}
	catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::MemorizzaVitaUtensile(int tool, int VitaResidua, int Programma)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM TempiLavoroUtensile WHERE ((Utensile = %d) AND (Programma = %d))", tool, Programma);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			ADOQuery->Close();
			strsql.printf("UPDATE TempiLavoroUtensile SET Vita_Memoria = %d WHERE ((Utensile = %d) AND (Programma = %d))", VitaResidua, tool, Programma);
			ADOQuery->SQL->Text = strsql;
//			Log("SERVER", "DB", strsql);
			res = ADOQuery->ExecSQL();
		} else {
			ADOQuery->Close();
			strsql.printf("INSERT INTO TempiLavoroUtensile (Programma, Utensile, Min_Lavoro, Vita_Memoria) "
						  "VALUES (%d, %d, 0, %d)", Programma, tool, VitaResidua);
			ADOQuery->SQL->Text = strsql;
//			Log("SERVER", "DB", strsql);
			res = ADOQuery->ExecSQL();
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::RicalcolaUsoUtensile(int tool, int vita_attuale, int Programma)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int vita, nuovavita, res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM TempiLavoroUtensile WHERE ((Utensile = %d) AND (Programma = %d))", tool, Programma);
		ADOQuery->SQL->Text = strsql;
//		Log("SERVER", "DB", strsql);
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			vita = ReadInt(ADOQuery, "Vita_Memoria");
			ADOQuery->Close();
            if (vita <= vita_attuale) {
				nuovavita = vita_attuale - vita;
            }
            else {
				nuovavita = vita - vita_attuale;
            }
//			Log("SERVER", "DB", "Vita Attuale = " + IntToStr(vita_attuale) + " Vita Memoria " + IntToStr(vita));
			if (nuovavita > 0) {
				strsql.printf("UPDATE TempiLavoroUtensile SET Min_Lavoro = %d WHERE ((Utensile = %d) AND (Programma = %d))", nuovavita, tool, Programma);
				ADOQuery->SQL->Text = strsql;
//				Log("SERVER", "DB", strsql);
				res = ADOQuery->ExecSQL();
			}
		} else {
			ADOQuery->Close();
		}
	} catch(...) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::PalletAssociatoLotto(int ID_Pallet)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lotti WHERE (ID_Pallet = %d)", ID_Pallet);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if(!ADOQuery->Eof) {
        	res = ReadInt(ADOQuery, "ID_Lotto");
        }
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaQtaLotto(int ID_Lavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Lotti WHERE (ID_Lavoro = %d)", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if(!ADOQuery->Eof) {
            strsql.printf("UPDATE Lotti SET QTA_Prodotta = QTA_Prodotta + 1 WHERE (ID_Lavoro = %d)", ID_Lavoro);
            ADOQuery->SQL->Text = strsql;
            res = ADOQuery->ExecSQL();
        }
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::QtaMancanteFineLotto(int ID_Lavoro)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("Select (Qta - Qta_Prodotta) AS Num From Lotti WHERE (ID_Lavoro = %d)", ID_Lavoro);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if(!ADOQuery->Eof) {
        	res = ReadInt(ADOQuery, "Num");
        }
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaLavoro(TLavoriInCorso lav)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("INSERT INTO LavoriInCorso "
					  "(ID_Lavoro, Indice, ID_Lavorazione, ID_Fase, ID_Pallet, LavoroAttivo, FaseCompletata, Priorita) "
					  "VALUES (%d, %d, %d, %d, %d, %d, %d, %d)",
            lav.ID_Lavoro,
            lav.Indice,
			lav.ID_Lavorazione,
			lav.ID_Fase,
			lav.ID_Pallet,
            lav.LavoroAttivo,
            lav.FaseCompletata,
			lav.Priorita
			);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
        lav.ID_Lavoro = GetSelfGenID("LavoriInCorso");
	} catch(Exception &E) {};
	delete ADOQuery;
	return lav.ID_Lavoro;
}
//---------------------------------------------------------------------------

int TDBDataModule::NewIDLavoro()
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int id, newid = -1;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql = "SELECT DISTINCT ID_Lavoro FROM LavoriInCorso ORDER BY ID_Lavoro";
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		newid = 1;
		while (!ADOQuery->Eof) {
			id = ReadInt(ADOQuery, "ID_Lavoro");
			if (newid == id) {
				newid++;
				ADOQuery->Next();
			} else {
				break;
			}
		}
		res = newid;
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CreaLavoriInCorso(int ID_Pallet, int ID_Lavorazione, int LavoroAttivo, int priorita)
{
    TLavoriInCorso lav;
	TRecordList TabFasi;
	AnsiString strsql;
	int IdLavoro;

    strsql.printf("FasiLavorazione where (ID_Lavorazione = %d AND Abilitazione = 1) ORDER BY Ordine_Seq", ID_Lavorazione);
    CaricaTabella(strsql, TabFasi);

    //Trovo il nuovo ID univoco da descrivere le fasi delle lavorazioni
    IdLavoro = NewIDLavoro();

    for (UINT i = 0; i < TabFasi.size(); i++) {
        memset(&lav, 0, sizeof(lav));
        lav.ID_Lavoro = IdLavoro;
        lav.Indice = i;
        lav.ID_Lavorazione = TabFasi[i]["ID_Lavorazione"].ToIntDef(0);
        lav.ID_Fase = TabFasi[i]["ID_Fase"].ToIntDef(0);
        lav.ID_Pallet = ID_Pallet;
        lav.LavoroAttivo = LavoroAttivo;
		lav.Priorita = priorita;
        CreaLavoro(lav);
    }
	return IdLavoro;
}
//---------------------------------------------------------------------------

void TDBDataModule::AttivaLavoroInCorso(int ID_Lavoro) {
	AnsiString strSQLUpdate;
	TADOQuery *ADOQuery;

	try {
		//Sblocco la macchina selezionata
		strSQLUpdate.printf("UPDATE LavoriInCorso SET LavoroAttivo = 1 WHERE ID_Lavoro = %d", ID_Lavoro);
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSQLUpdate;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

int TDBDataModule::EsisteGiaJobAttivo(AnsiString tpmiss, AnsiString tpmacc, int nmacc)
{
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = 0;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * FROM Jobs "
					  "WHERE ID_Macchina = %d AND Tipo_Macchina = '%s' AND Tipo_Missione = '%s' AND (Active IS NOT NULL)  ORDER BY Priorita DESC, Creazione, ID_Job ",
			nmacc, tpmacc.c_str(), tpmiss.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			res = ReadInt(ADOQuery, "ID_Pallet");
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiAbilitazioneStandby(int MU)
{
	AnsiString Stato, sClausula;

	sClausula.printf("WHERE DescrizioneCampo = 'AbilitaStandbyMU%d'", MU);
	LeggiCampoTabella("Parametri", "ValoreCampo", sClausula.c_str(), Stato);
	return Stato.ToIntDef(0);
}
//---------------------------------------------------------------------------

int TDBDataModule::AggiornaStatoMacchinaUtensile(int MU, int iStato)
{
	int res = 0, iIdPallet, iIdPalletOld;
	AnsiString strSql, sStato, sStatoOld;
	TADOQuery *ADOQuery;

	try {
    	//Leggo il pallet in macchina
    	iIdPallet = LeggiAdattatoreInMacchina("M", MU);

        //Ottengo la stringa associato allo stato
        sStato = DescProgramStatus[iStato];

		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strSql.printf("SELECT * FROM StoricoProduzioneStati Where ID_Macchina = %d ORDER BY DataOra DESC", MU);
		ADOQuery->SQL->Text = strSql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			// Messaggio gi� loggato
			iIdPalletOld = ReadInt(ADOQuery, "ID_Pallet");
			sStatoOld = ReadString(ADOQuery, "Stato");
			if (iIdPalletOld == iIdPallet && sStato == sStatoOld) {
            	Log("SERVER", "DB", "MU%d: Cambio stato %s ignorato sul pallet %d", MU, sStato.c_str(), iIdPallet);
				ADOQuery->Close();
				delete ADOQuery;
				return -1;
			}
		}
		ADOQuery->Close();

		//Sblocco la macchina selezionata
		strSql.printf("INSERT INTO StoricoProduzioneStati (DataOra, ID_Pallet, ID_Macchina, Stato) VALUES (GetDate(), %d, %d, '%s')", iIdPallet, MU, sStato.c_str());
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		ADOQuery->SQL->Text = strSql;
		res = ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;

	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::CercaPallet(int ID_Pallet, AnsiString Tipo_Macchina, TPallet &p) {
	AnsiString strsql;
	TADOQuery *ADOQuery;
	int res = -1;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT * "
					  "FROM Macchine AS M           "
					  "WHERE (M.ID_Pallet = %d ) "
					  "AND (M.Tipo_Macchina = '%s') ", ID_Pallet, Tipo_Macchina.c_str());
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
        	res = !LeggiPallet(ID_Pallet, p);
		}
		ADOQuery->Close();
	} catch(Exception &E) {};
	delete ADOQuery;
	return res;
}
//---------------------------------------------------------------------------

int TDBDataModule::LeggiAbilitazioneParametro(int index) {
	AnsiString Stato, sClausula;

	sClausula.printf("WHERE Indice = %d", index);
	LeggiCampoTabella("Parametri", "ValoreCampo", sClausula.c_str(), Stato);
	return Stato.ToIntDef(0);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void TDBDataModule::EventoRobot(AnsiString Sorgente, AnsiString Tipo, char *s, ...)
{
	va_list argptr;
	AnsiString logstr;

	//Costruisco la stringa
	va_start(argptr, s);
	logstr.vprintf(s, argptr);
	va_end(argptr);

	//Log
	EventoRobot(Sorgente, Tipo, logstr);
}
//---------------------------------------------------------------------------

void TDBDataModule::EventoRobot(AnsiString Sorgente, AnsiString Tipo, AnsiString Evento)
{
	TADOQuery *ADOQuery;
	AnsiString strsql, ev;

	try {
		ADOQuery = new TADOQuery(NULL);
		ADOQuery->Connection = ADOConnection1;
		strsql.printf("SELECT TOP (1) * FROM EventiRobot ORDER BY ID DESC");
		ADOQuery->SQL->Text = strsql;
		ADOQuery->Open();
		if (!ADOQuery->Eof) {
			// Messaggio gi� loggato
			ev = ReadString(ADOQuery, "Evento");
			if (ev == Evento) {
				ADOQuery->Close();
				delete ADOQuery;
				return;
			}
		}
		ADOQuery->Close();
		strsql.printf("INSERT INTO EventiRobot (Sorgente, Tipo, Evento) VALUES ('%s', '%s', '%s')", Sorgente, Tipo, Evento);
		ADOQuery->SQL->Text = strsql;
		ADOQuery->ExecSQL();
	} catch(...) {};
	delete ADOQuery;
}
//---------------------------------------------------------------------------

