
//---------------------------------------------------------------------------

#pragma hdrstop

#include <System.IOUtils.hpp>
#include "Main.h"
#include "logtxt.h"
#include "DB.h"
#include "ClientData.h"
#include "Fanuc.h"
#include "Events.h"
#include "Socket.h"
#include "Tools.h"
#include "PLCThread.h"
#include "FocasThread.h"
#include "Util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "trayicon"
#pragma resource "*.dfm"
TMainForm *MainForm;
TClientData ClientData;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
	: TForm(Owner)
{
	int id;
    CancellaLogVecchi();
	Richiesta_chiusura = false;
	LogTxt("*** Avvio ***");
	DBDataModule->Log("SERVER", "MAIN", "Software restarted");

    //Ciclo creazione oggetti Robot
    for (id = 0; id < N_ROBOTS; id++) {
		Fanuc[id] = new TFanuc(id);
    }

    //Ciclo creazione oggetti Macchine Utensili
	for (id = 0; id < N_MACCHINE_UT; id++) {
		Focas[id] = new TFocasThread(id + 1);
	}

	PLCThread = new TPLCThread(true);
	PLCThread->Resume();

	Avvio = true;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Exit1Click(TObject *Sender)
{
	LogTxt("*** Chiusura ***");

#ifdef ISSERVICE
	WinExec("net stop Service", SW_HIDE);
#else
	Richiesta_chiusura = true;
	Close();
#endif
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Apri1Click(TObject *Sender)
{
	Application->Restore();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BitBtn1Click(TObject *Sender)
{
	LogTxt("*** Chiusura ***");

#ifdef ISSERVICE
	WinExec("net stop Service", SW_HIDE);
#else
	Richiesta_chiusura = true;
	Close();
#endif
}
//---------------------------------------------------------------------------

void  TMainForm::AccendiLed(int n, TColor c)
{
	TShape *s;

	switch(n) {
	case 0:
		s = shpDB;
		break;
	case 1:
		s = shpRobot;
		break;
	case 2:
		s = shpPLC;
		break;
	case 3:
		s = shpM1;
		break;
	case 4:
		s = shpM2;
		break;
	case 5:
		s = shpM3;
		break;
	default:
		return;
	}
	s->Brush->Color = c;
}
//---------------------------------------------------------------------------

void  TMainForm::VisTCiclo(int n, AnsiString val)
{
	TLabel *s;

	switch(n) {
	case 1:
		s = TRobot;
		break;
	case 2:
		s = TPLC;
		break;
	case 3:
		s = T1;
		break;
	case 4:
		s = T2;
		break;
	case 5:
		s = T3;
		break;
	default:
		return;
	}
	s->Caption = val;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	Application->MainFormOnTaskBar = false;
	Hide();
	WindowState = wsMinimized;
	CanClose = Richiesta_chiusura;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	int id;

	//Ciclo start oggetti Robots
	for (id = 0; id < N_MACCHINE_UT; id++) {
		if (Focas[id]->m_bEnabled) {
			Focas[id]->Start();
		}
	}

	//Ciclo start oggetti Robots
    for (id = 0; id < N_ROBOTS; id++) {
		if (Fanuc[id]) {
			Fanuc[id]->Start();
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	int id;
	int tentativi = 0;

	Timer1->Enabled = false;
	SocketDataModule->TimerSendClientData->Enabled = false;

	for (id = 0; id < N_MACCHINE_UT; id++) {
		if (Focas[id]->m_bEnabled) {
			Focas[id]->StopThread();
		}
	}

	//Ciclo arresto oggetti Robot
	for (id = 0; id < N_ROBOTS; id++) {
		if (Fanuc[id]) {
			Fanuc[id]->StopThread();
		}
	}

	//Ciclo distruzione oggetti Macchine Utensili
	for (id = 0; id < N_MACCHINE_UT; id++) {
		if (Focas[id]) {
			delete Focas[id];
		}
	}

	//Ciclo distruzione oggetti Robot
	for (id = 0; id < N_ROBOTS; id++) {
		if (Fanuc[id]) {
			delete Fanuc[id];
		}
	}

	PLCThread->StopThread();
	Sleep(500);
	while ((!PLCThread->Suspended) && tentativi < 100) {
		Sleep(100);
        tentativi++;
	}
	delete PLCThread;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Timer1Timer(TObject *Sender)
{
	int id = 0, i;

	if (Avvio) {
		Application->MainFormOnTaskBar = false;
		Hide();
		WindowState = wsMinimized;
		Avvio = false;
	}
    else if (WindowState == wsNormal) {
		AccendiLed(id, DBDataModule->verificaConnessioneDB() ? clLime : clRed);

		// Robot
		id++;
        if (Fanuc[0]) {
			AccendiLed(id, Fanuc[0]->Connected ? clLime : clRed);
			VisTCiclo(id,  Fanuc[0]->CycleTime);
		}

		// PLC
		id++;
		AccendiLed(id, PLCThread->connected ? clLime : clRed);
		VisTCiclo(id,  PLCThread->CycleTime);

        //Ciclo distruzione oggetti Macchine Utensili
		for (i = 0; i < N_MACCHINE_UT; i++) {
			id++;
        	//Controllo se il puntatore � corretto e la macchina abilitata alla connessione
			if (Focas[i] && Focas[i]->m_bEnabled) {
				AccendiLed(id, Focas[i]->Connected ? clLime : clRed);
				VisTCiclo(id,  Focas[i]->CycleTime);
            }
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ApplicationEvents1Minimize(TObject *Sender)
{
	Application->MainFormOnTaskBar = false;
	Hide();
	WindowState = wsMinimized;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ApplicationEvents1Restore(TObject *Sender)
{
	Application->MainFormOnTaskBar = true;
	Show();
	WindowState = wsNormal;
	Application->BringToFront();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TrayIcon1DblClick(TObject *Sender)
{
	if (WindowState == wsMinimized) {
		Application->MainFormOnTaskBar = true;
		Show();
		WindowState = wsNormal;
		Application->BringToFront();
	}
	else {
		Application->MainFormOnTaskBar = false;
		Hide();
		WindowState = wsMinimized;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Image5Click(TObject *Sender)
{
	int n = NumeroProgramma("C:\\Meccano\\Programs\\meccano-1.NC");
	//Focas[2]->SelectProgram(50, 0);
	//Focas[2]->SelectProgram(11, 0);
	// Cancello il programma
	//Focas[0]->CancellaFile30I(n,"");
	//Scrivo il programma in macchina
	//Focas[2]->WriteProgram4("C:\\Meccano\\Programmi__\\O0125", n);
	//Seleziono il programma
	//Focas[2]->SelectProgram(n);

    //Setto il bit di fine carico
//	Fanuc[0]->WritePulse(TAGW_EXTRAH_S1, 1000);
//	Fanuc[0]->WritePulse(TAGW_EXTRAH_S2, 2000);
//    PLCThread->SetBit(14,2,6);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Image1Click(TObject *Sender)
{
	//Focas[0]->SelectProgram(50, 0);
	//Focas[0]->SelectProgram(12, 1);
}
//---------------------------------------------------------------------------

