//---------------------------------------------------------------------------

#pragma hdrstop
#include <System.SysUtils.hpp>
#include <System.WideStrUtils.hpp>
#include <System.Character.hpp>
//#include <System.SyncObjs.hpp>
#include <IniFiles.hpp>
#include <stdio.h>
#include "PLCThread.h"
#include "LogTxt.h"
#include "DB.h"
#pragma package(smart_init)

//#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall TPLCThread::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

TPLCThread *PLCThread;
//---------------------------------------------------------------------------
                        

__fastcall TPLCThread::TPLCThread(bool CreateSuspended)
	: TThread(CreateSuspended)
{
	int i, lang;
	TIniFile *ini;

	NameThreadForDebugging((AnsiString)("PLCThread"), ThreadID);
	Stopped = false;
	ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	PLCAddress = ini->ReadString("PLC", "PLCAddress", "127.0.0.1");
	lang = ini->ReadInteger("Language", "ID", 1040);
	delete ini;
	initialized = false;
	connected = false;
	tentativi = 0;
	memset(DB, 0, sizeof(DB));
	for (i = 0 ; i < 512; i++) {
		allbak[i] = false;
	}
	CaricaTestiAllarmi(lang);
	firstscan = false;
    m_SetupPageActive = false;
    m_AlarmNum = 0;
}
//---------------------------------------------------------------------------

void TPLCThread::InizializzaPLC()
{
	fds.rfd = openSocket(PLCPort, PLCAddress.c_str());
	fds.wfd = fds.rfd;
	if (fds.rfd > 0) {
		di = daveNewInterface(fds, "IF1", MPILocal, daveProtoISOTCP, daveSpeed187k);
		daveSetTimeout(di, 500);//5000000);
		dc = daveNewConnection(di, MPIRemote, PLCRack, PLCSlot);	// per S7-1500 PLCSlot=0
		PLCError = daveConnectPLC(dc);
	}
    else
		PLCError = -1;
	if (!PLCError) {
		initialized = true;
		connected = true;
	}
	t1 = clock();
	CycleTime = "0ms";
}
//---------------------------------------------------------------------------

void TPLCThread::ConnettiPLC() {
	PLCError = daveConnectPLC(dc);
	if (PLCError == 0)
		connected = true;
}
//---------------------------------------------------------------------------

void TPLCThread::DisconnettiPLC() {
	if (connected) {
		daveDisconnectPLC(dc);
		daveDisconnectAdapter(di);
	}
	initialized = false;
	connected = false;
	tentativi = 0;
}
//---------------------------------------------------------------------------

void __fastcall TPLCThread::Execute()
{
	int db, add, numw;
	AnsiString s;
	unsigned char *buffer;
	time_t t;
 	int step = 0;
    bool bForzaRiletturaStato = false;

	Sleep(500);
	while (!Stopped) {
		Sleep(35);
		try {
			if (!initialized) {
				InizializzaPLC();
			}
			else if (!connected) {
				ConnettiPLC();
			}
			else if (AsyncCom.empty() || bForzaRiletturaStato) {
				switch (step) {
				case 0:
					db = DB_TIPOALLARMI;
                    add = 0;
					numw = 8;
                	break;
				case 1:
					db = DB_STAZIONI;
					add = 0;
					numw = 13;
					break;
				case 2:
					db = DB_ALLARMI;
					add = 0;
					numw = 8;
					break;
				case 3:
					db = DB_PLC_MU_IN;
					add = 0;
					numw = 3;
                	break;
				case 4:
					db = 14;
					add = 0;
					numw = 3;
                	break;
                }
				// Lettura dati da PLC
                buffer = DB[db] + add;
                PLCError = daveReadBytes(dc, daveDB, db, add, numw, buffer);
				if (PLCError == 0) {
                	//Nel caso abbia letto la DB degli allarmi aggiorno il sinottico
                	if (db == DB_ALLARMI) {
						AggiornaAllarmi(db, numw);
                    }

					tentativi = 0;
					step++;
					if (step > 5) {
						step = 0;
						t2 = clock();
						t = t2 - t1;
						t1 = t2;
						CycleTime.printf("%dms", t);
						firstscan = true;
                        bForzaRiletturaStato = false;
					}
				}
				else {
                    LogTxt("PLCError = %d, %s, Step = %d", PLCError, daveStrerror(PLCError), step);
					tentativi++;
					if (tentativi > 10) {
						DisconnettiPLC();
                        tentativi = 0;
                    }
				}
			}
			else {
				// Esegui scrittura PLC
				switch (AsyncCom[0].cmd) {
				case WR_BYTE:
					PLCError = daveWriteBytes(dc, daveDB, AsyncCom[0].db, AsyncCom[0].add, AsyncCom[0].n, AsyncCom[0].data);
					break;
				case WR_WORD:
					PLCError = daveWriteBytes(dc, daveDB, AsyncCom[0].db, AsyncCom[0].add, AsyncCom[0].n, AsyncCom[0].data);
					break;
				case WR_DWORD:
					PLCError = daveWriteBytes(dc, daveDB, AsyncCom[0].db, AsyncCom[0].add, AsyncCom[0].n, AsyncCom[0].data);
					break;
				case WR_BLOCK:
					PLCError = daveWriteBytes(dc, daveDB, AsyncCom[0].db, AsyncCom[0].add, AsyncCom[0].n, AsyncCom[0].data);
					break;
				case SET_BIT:
					PLCError = daveSetBit(dc, daveDB, AsyncCom[0].db, AsyncCom[0].add, AsyncCom[0].n);
					break;
				case RST_BIT:
					PLCError = daveClrBit(dc, daveDB, AsyncCom[0].db, AsyncCom[0].add, AsyncCom[0].n);
					break;
				}
				if (PLCError == 0) {
					tentativi = 0;
					AsyncCom.erase(AsyncCom.begin());
				}
				else {
					tentativi++;
                    bForzaRiletturaStato = true;
					if (tentativi > 10) {
						DisconnettiPLC();
                        tentativi = 0;
                    }
				}
			}
		} catch (...) {}
	}
	DisconnettiPLC();
    Suspend();
}
//---------------------------------------------------------------------------

void TPLCThread::StopThread() {
	Stopped = true;
//	TInterlocked::Exchange(Stopped, 1);
//	/* Wait for this thread to finish executing */
//	this->WaitFor();
}
//---------------------------------------------------------------------------

void TPLCThread::AggiornaAllarmi(int db, int dw) {
	bool allarmi[512];
    BYTE i;
	int j, numw, numdb, baseall;

    numw = dw;
    numdb = db;
    baseall = 1;

	for (j = 0; j < numw; j++) {
		for (i = 0; i < 8; i++) {
			m_AlarmNum = j * 8 + i + baseall;
			// Aggiorno allarmi
			allarmi[m_AlarmNum] = GetBit(DB[numdb][j], i);
			if (allarmi[m_AlarmNum] && (!allbak[m_AlarmNum])) {
				// Nuovo allarme
				SYNCHROnall = m_AlarmNum;
				if (GetBit(DB[DB_TIPOALLARMI][j], i)) {
					Synchronize(SegnalaAllarme);
				}
				else {
					Synchronize(SegnalaWarning);
				}
			}
            else if (!allarmi[m_AlarmNum] && allbak[m_AlarmNum]) {
				// Allarme resettato
				SYNCHROnall = m_AlarmNum;
				Synchronize(AcquisisciSegnalazione);
			}
			allbak[m_AlarmNum] = allarmi[m_AlarmNum];
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPLCThread::SegnalaAllarme() {
	DBDataModule->Segnalazione(SYNCHROnall, alltxt[SYNCHROnall], 1);
}
//---------------------------------------------------------------------------

void __fastcall TPLCThread::SegnalaWarning() {
	DBDataModule->Segnalazione(SYNCHROnall, alltxt[SYNCHROnall], 0);
}
//---------------------------------------------------------------------------

void __fastcall TPLCThread::AcquisisciSegnalazione() {
	DBDataModule->AcquisisciSegnalazioneAttiva(SYNCHROnall);
}
//---------------------------------------------------------------------------

unsigned char* TPLCThread::DataBlock(int db) {
	unsigned char *res = NULL;

	res = DB[db];
	return res;
}
//---------------------------------------------------------------------------

void TPLCThread::WriteData(int db, int add, int N, int cmd, unsigned char *data)
{
	PLCWriteCom c;

	c.db = db;
	c.add = add;
	c.n = N;
	c.cmd = cmd;
	if (data != NULL) {
		memcpy(c.data, data, 512);
	} else {
		c.data[0] = 0;
	}
	AsyncCom.push_back(c);
}
//---------------------------------------------------------------------------

void TPLCThread::WriteByte(int db, int add, unsigned char value)
{
	unsigned char buffer[10], *data;

	buffer[0] = value;
	data = DataBlock(db);
	if (data != NULL) {
		memcpy(data + add, buffer, 1);
	}
	WriteData(db, add, 1, WR_BYTE, buffer);
}
//---------------------------------------------------------------------------

void TPLCThread::WriteWord(int db, int add, int value)
{
	unsigned char buffer[10], *data;

	FormatDBW(value, buffer);
	data = DataBlock(db);
	if (data != NULL) {
		memcpy(data + add, buffer, 2);
	}
	WriteData(db, add, 2, WR_WORD, buffer);
}
//---------------------------------------------------------------------------

void TPLCThread::WriteDWord(int db, int add, long int value)
{
	unsigned char buffer[10], *data;

	FormatDBD(value, buffer);
	data = DataBlock(db);
	if (data != NULL) {
		memcpy(data + add, buffer, 4);
	}
	WriteData(db, add, 4, WR_DWORD, buffer);
}
//---------------------------------------------------------------------------

void TPLCThread::WriteBlock(int db, int add, int lenght, unsigned char *value)
{
	unsigned char *data;

	data = DataBlock(db);
	if (data != NULL) {
		memcpy(data + add, value, lenght);
	}
	WriteData(db, add, lenght, WR_BLOCK, value);
}
//---------------------------------------------------------------------------

void TPLCThread::SetBit(int db, int add, int bit)
{
	unsigned char *data;

	data = DataBlock(db);
	if (data != NULL) {
		data[add] |= (BYTE)(1 << bit);
	}
	WriteData(db, add, bit, SET_BIT);
}
//---------------------------------------------------------------------------

void TPLCThread::ResetBit(int db, int add, int bit)
{
	unsigned char *data;

	data = DataBlock(db);
	if (data != NULL) {
		data[add] &= (BYTE)(~(1 << bit));
	}
	WriteData(db, add, bit, RST_BIT);
}
//---------------------------------------------------------------------------

void TPLCThread::WriteTimer(int db, int add, double value)
{
	unsigned char buffer[10], *data;

	FormatTimer(value, buffer);
	data = DataBlock(db);
	if (data != NULL) {
		memcpy(data + add, buffer, 2);
	}
	WriteData(db, add, 2, WR_WORD, buffer);
}
//---------------------------------------------------------------------------

void clean(WideChar *s) {
	WideChar *p;

	if (WStrLen(s) <= 0) {
    	return;
	}
	p = s + WStrLen(s) - 1;
	while ((*p == 10) || (*p == 13)) {
		*p = 0;
		p = s + WStrLen(s);
	}
}
//---------------------------------------------------------------------------

void parse(wchar_t *record, wchar_t *delim, wchar_t arr[][100], int *fldcnt) {
	wchar_t *p = wcstok(record, delim);
	int fld = 0;

	while (p) {
		wcscpy(arr[fld], p);
		clean(arr[fld]);
		fld++;
		p = wcstok('\0', delim);
	}
	*fldcnt = fld;
}
//---------------------------------------------------------------------------

void TPLCThread::LeggiTestiAllarmi(AnsiString FileName) {
	wchar_t tmp[1024];
	int n, fldcnt=0;
	wchar_t arr[5][100];
	FILE *in;

	tmp[0] = 0x0;
	arr[0][0] = 0x0;
	arr[1][0] = 0x0;
	arr[2][0] = 0x0;
	arr[3][0] = 0x0;
	arr[4][0] = 0x0;
	in = fopen(FileName.c_str(), "r");
	if (in != NULL) {
		while (fgetws(tmp, sizeof(tmp), in) != 0) /* read a record */ {
			int i = 0;
			parse(tmp, L";", arr, &fldcnt); /* whack record into fields */
			n = StrToInt(arr[0]);
			alltxt[n] = IntToStr(n) + ": " + arr[1];
		}
		fclose(in);
	}
}
//---------------------------------------------------------------------------

void TPLCThread::CaricaTestiAllarmi(int lang) {
	TIniFile *Ini;
	AnsiString FileName;
	int i;

	Ini = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	FileName = Ini->ReadString("Language", "FileAllarmi" + IntToStr(lang), "Allarmi.txt");
	delete Ini;
	alltxt[0] = "";
	for (i = 1 ; i < 512; i++) {
		alltxt[i] = "Allarme " + IntToStr(i);
	}
	LeggiTestiAllarmi(FileName);
}
//---------------------------------------------------------------------------

