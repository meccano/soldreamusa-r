//---------------------------------------------------------------------------

#ifndef SocketH
#define SocketH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ScktComp.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

typedef struct {
	unsigned short l_onoff;
	unsigned short l_linger;
} TMyLinger;
//---------------------------------------------------------------------------

class TSocketDataModule : public TDataModule
{
__published:	// IDE-managed Components
	TServerSocket *ServerSocket1;
	TTimer *TimerSendClientData;
	void __fastcall ServerSocket1ClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
	void __fastcall ServerSocket1ClientError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall TimerSendClientDataTimer(TObject *Sender);
	void __fastcall DataModuleDestroy(TObject *Sender);
public:		// User declarations
	__fastcall TSocketDataModule(TComponent* Owner);
	void UpdateClientData();
};
//---------------------------------------------------------------------------
extern PACKAGE TSocketDataModule *SocketDataModule;
//---------------------------------------------------------------------------
#endif
