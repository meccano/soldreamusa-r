// ****************************************************************************
//          Sintesi S.r.l.
//          Viale Virgilio,58/H - 41100 MODENA - ITALY
//          Tel. +39-59-847070
//          Fax. +39-59-848728
//
//          All rights reserved.
//          Partial or complete copy and use of information contained in this
//          document for pourposes different from those listed in the licence
//          is forbiddend and protected under the copyright laws.
//          Any modification to this file, if not authorized and suggested
//          by Sintesi S.r.l. could cause malfunctions of the programs and is
//          made under complete user's responsability.
//
// ****************************************************************************
// MODULE NAME		:	IDFUN20.H
//
// DESCRIPTION		:
//
// NOTES				:
//
// IDCOMM VERSION	: 2.00
//
// ****************************************************************************
#ifndef __IDFUNC20_H
#define __IDFUNC20_H

#if defined(BORLANDCPP)
	#define __EXTFUN	extern "C"
#elif defined (BORLANDC)
	#define __EXTFUN	extern
#endif

#include "idtypes25.h"

__EXTFUN
PDEVICE __stdcall			// return: Device reference
DeviceOn(	LPSTR,			// Device name
				LPSTR				// Device group name
	);

__EXTFUN
BOOL	__stdcall
DeviceInit( PDEVICE,		// Device reference
				LPSTR,			// Device parameter string
				LPSTR				// Device timeout string
	);

__EXTFUN
BOOL __stdcall
DeviceOff(	PDEVICE		// Device reference
	);

__EXTFUN
PID __stdcall				// return: ID reference
IdOn(		LPSTR,			// Protocol name
			SHORT,      	// ID model number
      	SHORT,			// # of Tags - Not used in IdComm 2.x - For 1.0 version compatibility only
			PDEVICE			// Device reference
	);

__EXTFUN
BOOL	__stdcall
IdInit(  PID,					// ID reference
			SHORT,				// waitTime
			LPSTR,		  		// ID parameter string
			SHORT					// ID working mode (NORMAL, DEMO, SUSPENDED)
	);

__EXTFUN
BOOL	__stdcall
IdOff(	PID					// ID reference
	);

__EXTFUN
PTAG __stdcall
TagOn(		PID,				//	ID reference
				LPSTR,			// addressString
				WORD,				// repetition
				WORD,				// conversion
				DWORD,				// read idle time
            PSHORT			// Unit Size
	);

__EXTFUN
BOOL	__stdcall
TagOff(  PTAG					//	Tag reference,
	);


__EXTFUN
BOOL	__stdcall
PeekTag(  	PTAG,				//	Tag reference,
				LPVOID				// Values Buffer
	);

__EXTFUN
BOOL	__stdcall
ReadTag( 	PTAG,				//	Tag reference,
				LPVOID				// Values Buffer
	);

__EXTFUN
BOOL	__stdcall
WriteTag(  	PTAG,				//	Tag reference,
				LPVOID				// Values Buffer
	);


__EXTFUN
BOOL __stdcall
TagUnitSize(	LPSTR			protocol,
				   SHORT			model,
				   LPSTR 		addressString,
               PSHORT		pUnitSize
);

   __EXTFUN
BOOL	__stdcall
IdCommGetError(	PSHORT,	// Level 2 error code reference
						PSHORT	// Level 1 error code reference
	);

__EXTFUN
BOOL	__stdcall
IdCommErrorCount(	DWORD *	// Error List Dimension
	);

__EXTFUN
BOOL	__stdcall
IdCommGetVersion(	PSHORT,		// Major Version
						PSHORT,		// Minor Version
						PSHORT		// Update Version
	);

__EXTFUN
VOID	__stdcall
IdCommCloseAll( VOID
	);

__EXTFUN
BOOL __stdcall
DeviceIoctl(	PDEVICE,	// Device Reference
					WORD,    // Operation
               LPVOID	// Parameter
	);

__EXTFUN
BOOL __stdcall
_DevTest(			PDEVICE,	// Device Reference
					WORD,    // Operation
               LPVOID	// Parameter
	);

/* -----------------------------------------------------------------------------
	Debugging Functions
-----------------------------------------------------------------------------*/

__EXTFUN
BOOL __stdcall
IdCommStartDebug(	LPSTR, // application Name
						BOOL  // debug IdComm
						);

__EXTFUN
BOOL __stdcall
IdCommStopDebug(	VOID
						);

__EXTFUN
VOID __stdcall
IdCommSendDebugMsg(	LPSTR, // sender
							LPSTR, // msg
                     WORD // msgType
                     );


/* -----------------------------------------------------------------------------
	Serial Number Registration Functions
-----------------------------------------------------------------------------*/
__EXTFUN
BOOL __stdcall
RegisterIdCommBase(	LPSTR   // serial number
                     );

__EXTFUN
BOOL __stdcall
RegisterIdCommProtocol(	LPSTR , 	// serial number
                        SHORT		// protocol number
                        );

/* -----------------------------------------------------------------------------
	IdComm 1.0 Compatibility Functions
-----------------------------------------------------------------------------*/
__EXTFUN
BOOL __stdcall
IdSetTag(	PID,		  	// Id Reference
				WORD,				// Tag Idx
				LPSTR,			// Aaddress String
            WORD,				// Repetition
            WORD,				// Conversion
				DWORD				// Read Idle Time
	);

__EXTFUN
BOOL __stdcall
IdReadTag(	PID,		// Id Reference
				SHORT,      // Tag idx
            LPVOID      // Values Buffer
	);

   __EXTFUN
BOOL __stdcall
IdPeekTag(	PID,		// Id Reference
				SHORT,      // Tag idx
            LPVOID      // Values Buffer
	);

__EXTFUN
BOOL __stdcall
IdWriteTag(	PID,		// Id Reference
				SHORT,      // Tag idx
            LPVOID      // Values Buffer
	);

#endif

