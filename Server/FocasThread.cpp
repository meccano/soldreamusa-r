//---------------------------------------------------------------------------

#pragma hdrstop

#include <System.SysUtils.hpp>
#include <System.Character.hpp>
#include <IniFiles.hpp>
#include <stdio.h>
#include "FocasThread.h"
#include "PLCThread.h"
#include "DB.h"
#include "Fanuc.h"
#include "LogTxt.h"
#include "util.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(&UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall thFocas::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------
TFocasThread *Focas[N_MACCHINE_UT];

__fastcall TFocasThread::TFocasThread(int n, bool CreateSuspended)
	: TThread(CreateSuspended)
{
	TIniFile *pIni;

	TimeoutStart = 0;
	ID = n;
	NameThreadForDebugging("FocasThread" + IntToStr(ID), ThreadID);
	Lock = new TCriticalSection();
	Connected = false;
	Stopped = 0;
	Step = 0;
    m_M21 = 0;
	TxQueue.clear();
	forza_rilettura_stato = false;
	TABELLA_UTENSILI_LETTA = false;
	pIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	Address 			= pIni->ReadString("MU" + IntToStr(ID), "Address", "127.0.0.1");
	Port 				= pIni->ReadInteger("MU" + IntToStr(ID), "Port", 8193);
	DataServerAddress 	= pIni->ReadString("MU" + IntToStr(ID), "DataServerAddress", Address);
	DataServerPort 		= pIni->ReadInteger("MU" + IntToStr(ID), "DataServerPort", 21);
	DataServerUser 		= pIni->ReadString("MU" + IntToStr(ID), "DataServerUser", "");
	DataServerPassword 	= pIni->ReadString("MU" + IntToStr(ID), "DataServerPassword", "");
	TypeCN				= pIni->ReadInteger("MU" + IntToStr(ID), "TypeCN", 1);
    //Flag che indica se devo connettere oppure no la macchina utensile (start al thread)
	m_bEnabled 			= pIni->ReadInteger("MU" + IntToStr(ID), "Enabled", 1);
	delete pIni;
}
//---------------------------------------------------------------------------

__fastcall TFocasThread::~TFocasThread()
{
	delete Lock;
}
//---------------------------------------------------------------------------

void TFocasThread::Resettone()
{
	TxQueue.clear();
	TimeoutStart = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFocasThread::Execute()
{
	time_t t;
	IODBPMC buf;
 	ODBM macro;
	ODBERR err;
 	IODBTD Correttori;
	short num, ret;
	AnsiString str;

	while (!Stopped) {
		Sleep(35);
		try {
			if (!Connected) {
				Connect();
			}
			else if (TxQueue.empty() || forza_rilettura_stato) {
				switch(Step) {
				case 0:
					ret = cnc_rdprgnum(h, &prg);
					if (ret != EW_OK) {
						Disconnect();
						continue;
					}
					Step++;
					break;
				case 1:
					switch(TypeCN) {
					case CN_FANUC_31i:
						ret = cnc_statinfo2(h, &statinfo);
						if (ret != EW_OK) {
							Disconnect();
							continue;
						}
						break;
					case CN_FANUC_16i:
						ret = cnc_statinfo(h, &statinfo16i);
						if (ret != EW_OK) {
							Disconnect();
							continue;
						} else {
							statinfo.alarm		= statinfo16i.alarm	;
							statinfo.emergency	= statinfo16i.emergency;
							statinfo.aut		= statinfo16i.aut		;
							statinfo.run		= statinfo16i.run		;
							statinfo.motion		= statinfo16i.motion	;
						}
						break;
					}
                    Step++;
					break;
				default:
					Step = 0;
					t2 = clock();
					t = t2 - t1;
					t1 = t2;
					CycleTime.printf("%dms", t);
					forza_rilettura_stato = false;
                    break;
				}
			}
            else {
				switch (TxQueue[0].comando) {
                case COM_WRITED:
                    d.type_a = 9;
					d.type_d = 1;
                    d.datano_s = TxQueue[0].intp[0];
                    d.datano_e = TxQueue[0].intp[0] + 1;
                    d.u.idata[0] = TxQueue[0].intp[1];
                    ret = pmc_wrpmcrng(h, 8 + 2 * 1, &d);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
                case COM_WRITER:
                    r.type_a = 5;
                    r.type_d = 0;
                    r.datano_s = TxQueue[0].intp[0];
                    r.datano_e = TxQueue[0].intp[0] + 1;
                    r.u.idata[0] = TxQueue[0].intp[1];
                    ret = pmc_wrpmcrng(h, 8 + 2 * 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
					}
					if (ret == EW_OK) {
						LogTxt("COM_WRITER: R%d valore %u (%X)", TxQueue[0].intp[0], TxQueue[0].intp[1], TxQueue[0].intp[1]);
					}
					Sleep(250);
                    TxQueue.pop_front();
					break;
                case COM_SET_BIT_R:
					ret = pmc_rdpmcrng(h, 5 /* R */, 0 /* byte */, TxQueue[0].intp[0], TxQueue[0].intp[0] + 1, 8 + 1 * 2, &buf);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    r.type_a = 5;
                    r.type_d = 0;
                    r.datano_s = TxQueue[0].intp[0];
                    r.datano_e = TxQueue[0].intp[0];
                    r.u.cdata[0] = (BYTE)(buf.u.cdata[0] | (1 << TxQueue[0].intp[1]));
                    ret = pmc_wrpmcrng(h, 8 + 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    Sleep(250);
                    TxQueue.pop_front();
                    break;
                case COM_RESET_BIT_R:
					ret = pmc_rdpmcrng(h, 5 /* R */, 0 /* byte */, TxQueue[0].intp[0], TxQueue[0].intp[0] + 1, 8 + 1 * 2, &buf);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    r.type_a = 5;
                    r.type_d = 0;
                    r.datano_s = TxQueue[0].intp[0];
                    r.datano_e = TxQueue[0].intp[0];
                    r.u.cdata[0] = (BYTE)(buf.u.cdata[0] & ~(1 << TxQueue[0].intp[1]));
                    ret = pmc_wrpmcrng(h, 8 + 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    Sleep(250);
                    TxQueue.pop_front();
                    break;
                case COM_PULSE_BIT_R:
					ret = pmc_rdpmcrng(h, 5 /* R */, 0 /* byte */, TxQueue[0].intp[0], TxQueue[0].intp[0] + 1, 8 + 1 * 2, &buf);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    r.type_a = 5;
                    r.type_d = 0;
                    r.datano_s = TxQueue[0].intp[0];
                    r.datano_e = TxQueue[0].intp[0];
                    r.u.cdata[0] = (BYTE)(buf.u.cdata[0] | (1 << TxQueue[0].intp[1]));
                    ret = pmc_wrpmcrng(h, 8 + 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando + (float)0.1), ret);
						LogTxt("Pulse R%d.%d SET FAILED #%d", TxQueue[0].intp[0], TxQueue[0].intp[1], TxQueue[0].LapsCounter);
						continue;
					}
					if (ret == EW_OK) {
						LogTxt("Pulse R%d.%d SET DONE", TxQueue[0].intp[0], TxQueue[0].intp[1]);
					}
					Sleep(TxQueue[0].intp[2]);
                    r.type_a = 5;
                    r.type_d = 0;
                    r.datano_s = TxQueue[0].intp[0];
					r.datano_e = TxQueue[0].intp[0];
                    r.u.cdata[0] = (BYTE)(buf.u.cdata[0] & ~(1 << TxQueue[0].intp[1]));
                    ret = pmc_wrpmcrng(h, 8 + 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando + (float)0.2), ret);
						LogTxt("Pulse R%d.%d RESET FAILED #%d", TxQueue[0].intp[0], TxQueue[0].intp[1], TxQueue[0].LapsCounter);
                        continue;
                    }
					if (ret == EW_OK) {
						LogTxt("Pulse R%d.%d RESET DONE", TxQueue[0].intp[0], TxQueue[0].intp[1]);
					}
					TxQueue.pop_front();
                	break;
                case COM_WRITE_PROGRAM3:
                	ret = ReadFile(TxQueue[0].stringp[0], buffer);
                    if (ret) {
                    	ret = DL3(TxQueue[0].intp[0], buffer);
                       	if ((!ret) && (TxQueue[0].LapsCounter > 0)) {
                            SetFunctionError((float)(TxQueue[0].comando), 0);
                            continue;
                       	}
                    }
                    else if (TxQueue[0].LapsCounter > 0) {
						SetFunctionError((float)(TxQueue[0].comando) + (float)0.1, 0, "", true, true);
                        continue;
                    }
					TxQueue.pop_front();
                   break;
                case COM_WRITE_PROGRAM4:
					ret = ReadFile(TxQueue[0].stringp[0], buffer);
                    if (ret) {
						ret = DL4(TxQueue[0].intp[0], buffer, TxQueue[0].stringp[1]);
                       	if ((!ret) && (TxQueue[0].LapsCounter > 0)) {
							SetFunctionError((float)(TxQueue[0].comando), 0);
                            continue;
                       	}
                    }
                    else if (TxQueue[0].LapsCounter > 0) {
						SetFunctionError((float)(TxQueue[0].comando) + (float)0.1, 0, "", true, true);
                        continue;
                    }
                    Sleep(250);
					TxQueue.pop_front();
                   break;
                case COM_READ_PROGRAM3:
                    if (UL3(buffer, TxQueue[0].intp[0])) {
                        WriteFile(TxQueue[0].stringp[0], buffer);
                    }
                    TxQueue.pop_front();
                    break;
                case COM_READ_PROGRAM4:
                    if (UL4(buffer, TxQueue[0].stringp[1])) {
                        WriteFile(TxQueue[0].stringp[0], buffer);
                    }
                    TxQueue.pop_front();
                    break;
                case COM_WRITE_MACRO_R:
                    m.datano_s = TxQueue[0].intp[0];
                    m.datano_e = TxQueue[0].intp[0];
                    m.data[0].mcr_val = TxQueue[0].intp[1];
                    m.data[0].dec_val = 0;
                    ret = cnc_wrmacror(h, 8 + 8 * 1, &m);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
                case COM_WRITE_MACRO_DOUBLE:
                    ret = cnc_wrmacro(h, TxQueue[0].intp[0], 10, TxQueue[0].intp[1], 4);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
                case COM_WRITE_MACRO:
                    ret = cnc_wrmacro(h, TxQueue[0].intp[0], 10, TxQueue[0].intp[1], 0);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
				case COM_SELECT_PROGRAM:
					if (((clock() - TimeoutStart) / CLOCKS_PER_SEC) > TIMEOUT_START) {
						Resettone();
						continue;
					}
					// Verifica che sia in auto (MEMORY) ma in STOP
					ret = -1;
					if ((statinfo.aut == 1) && (statinfo.run == 0)) {
						// Seleziona programma macchina
						ret = cnc_search(h, (short)TxQueue[0].intp[0]);
						if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
							SetFunctionError((float)(TxQueue[0].comando), ret, "cnc_search");
							continue;
                        }
                    }
                    else if (TxQueue[0].LapsCounter > 0) {
                        //Sincronizzo la scrittura dell'errore a DB
                    	LogTxt("Aut = %d, run = %d, prog = %d", statinfo.aut, statinfo.run, (short)TxQueue[0].intp[0]);
                        Sleep(500);
						SetFunctionError((float)(TxQueue[0].comando) + (float)0.1, 0);
                        continue;
					}

					//Sincronizzo lo start cycle attraverso il robot
					if (TxQueue[0].intp[1] && (ret == EW_OK)) {
						Synchronize(StartCycle);
					}

					//Cancello la coda
					TxQueue.pop_front();
					break;
				case COM_START_PROGRAM:
					// Invia Segnale START
                    //WriteR(7300, 1);
					r.type_a = 5;
					r.type_d = 0;
                    r.datano_s = 7300;
                    r.datano_e = 7301;
					r.u.idata[0] = 1;
					ret = pmc_wrpmcrng(h, 8 + 2 * 1, &r);
					if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), ret);
						continue;
					}
                    Sleep(200);
					// Resetto Start
                    //WriteR(7300, 0);
					r.type_a = 5;
					r.type_d = 0;
                    r.datano_s = 7300;
                    r.datano_e = 7301;
                    r.u.idata[0] = 0;
					ret = pmc_wrpmcrng(h, 8 + 2 * 1, &r);
					if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando) + (float)0.1, ret);
                        continue;
					}
					TxQueue.pop_front();
					break;
				case COM_FINE_CICLO:
					// Invia Segnale Fine Ciclo
					//WriteR(7013, 1);
                    r.type_a = 5;
                    r.type_d = 0;
                    r.datano_s = 7013;
                    r.datano_e = 7014;
                    r.u.idata[0] = 1;
                    ret = pmc_wrpmcrng(h, 8 + 2 * 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
                        SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    Sleep(200);
                    // Resetto Fine Ciclo
                    //WriteR(7013, 0);
                    r.type_a = 5;
                    r.type_d = 0;
                    r.datano_s = 7013;
                    r.datano_e = 7014;
                    r.u.idata[0] = 0;
                    ret = pmc_wrpmcrng(h, 8 + 2 * 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando) + (float)0.1, ret);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
                case COM_WRITE_PROGRAM_DS:
                	ret = DL_DS(TxQueue[0].stringp[0], TxQueue[0].stringp[1]); 
                    if ((!ret) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), 0);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
                case COM_FIND_AND_DELETE:
                    // Se esiste il file nel dataserver lo cancello
                    if (FindFile_DS(TxQueue[0].stringp[0])) {
                    	ret = DelFile_DS(TxQueue[0].stringp[0]);
                        if ((!ret) && (TxQueue[0].LapsCounter > 0)) {
							SetFunctionError((float)(TxQueue[0].comando), 0);
                            continue;
                        }
                    }
                    TxQueue.pop_front();
                    break;
                case COM_DELETE_ALL_DS:
                	ret = DelAll_DS();
                    if ((!ret) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), 0);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
                case COM_DELETE_FILE_DS:
                	ret = DelFile_DS(TxQueue[0].stringp[0]);
                    if ((!ret) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), 0);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
				case COM_DEL_FILE_I30:
					ret = cnc_pdf_del(h, TxQueue[0].stringp[0]);
					if ((ret != EW_OK) && (ret != EW_DATA) && (TxQueue[0].LapsCounter > 0)) {
						str.printf("COM_DEL_FILE_I30: file %s", TxQueue[0].stringp[0]);
						SetFunctionError((float)(TxQueue[0].comando), ret, str);
                        continue;
					}
					if (ret == EW_OK) {
						LogTxt("COM_DEL_FILE_I30: file %s cancellato con successo", TxQueue[0].stringp[0]);
					}
					TxQueue.pop_front();
                    break;
                case COM_DEL_FILE_0I_15IB:
                    ret = cnc_delete(h, TxQueue[0].intp[0]);
                    if ((ret != EW_OK) && (ret != EW_DATA) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
                case COM_SEND_SETUP:
                    //Alzo il setUp D107.3
                    r.type_a = 9;
                    r.type_d = 0;
                    r.datano_s = 107;
                    r.datano_e = 108;
                    r.u.idata[0] = 8;
                    ret = pmc_wrpmcrng(h, 8 + 2 * 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando), ret);
                        continue;
                    }
                    //Aspetto 500 milli
                    Sleep(500);
                    //Tolgo il setUp
                    r.type_a = 9;
                    r.type_d = 0;
                    r.datano_s = 107;
                    r.datano_e = 108;
                    r.u.idata[0] = 0;
                    ret = pmc_wrpmcrng(h, 8 + 2 * 1, &r);
                    if ((ret != EW_OK) && (TxQueue[0].LapsCounter > 0)) {
						SetFunctionError((float)(TxQueue[0].comando) + (float)0.1, ret);
                        continue;
                    }
                    TxQueue.pop_front();
                    break;
				case COM_WRITE_CORRETTORI:
					memset(&Correttori, 0, sizeof(Correttori));
					Correttori.datano 	= TxQueue[0].intp[0];
					Correttori.type     = TxQueue[0].intp[1];
					Correttori.tool_num = TxQueue[0].intp[1];
					Correttori.h_code   = TxQueue[0].intp[2];
					Correttori.d_code   = TxQueue[0].intp[3];
					Correttori.tool_inf	= TxQueue[0].intp[4];
					ret =  cnc_wr1tlifedata(h, &Correttori);
					if (ret == EW_ATTRIB) {
						TxQueue.pop_front();
						break;
					}
					if (ret != EW_OK) {
						ret = cnc_getdtailerr(h, &err);
						if ((ret == EW_REJECT) || (ret == EW_NUMBER)) {     //EW_REJECT == 13
							forza_rilettura_stato = true;
						}
                        else if (ret != EW_ATTRIB && (TxQueue[0].LapsCounter > 0)) {   // EW_ATTRIB == 4
                         	SetFunctionError((float)(TxQueue[0].comando), 0);
							continue;
						}
					}
					TxQueue.pop_front();
					break;
				}
			}
		} catch(...) {}
	}
	Disconnect();
}
//---------------------------------------------------------------------------

void TFocasThread::StopThread() {
	TInterlocked::Exchange(Stopped, 1);
	/* Wait for this thread to finish executing */
	this->WaitFor();
}
//---------------------------------------------------------------------------

void TFocasThread::Connect() {
	// parametri: ip address, port, timeout, handle
	if (cnc_allclibhndl3(Address.c_str(), Port, 5, &h) == EW_OK) {
		Connected = true;
	}
}
//---------------------------------------------------------------------------

void TFocasThread::Disconnect()
{
	cnc_freelibhndl(h);
	Connected = false;
}
//---------------------------------------------------------------------------

bool TFocasThread::DL3(int nprog, char *s)
{
	short ret;
	long len, n;
	ODBERR err;
	AnsiString FileName;

	//Se ho un programma da scaricare provo a scaricarlo
	if (nprog >= 0) {
		//Cancello il programma
		ret = cnc_delete(h, (short)nprog);
	}
	if (ret != EW_OK) {
        if (ret != EW_DATA) {
			return false;
        }
        else {
            ret = cnc_getdtailerr(h, &err);
            if (ret == EW_DATA) {
		        return false;
        	}
        }
	}
	ret = cnc_dwnstart3(h, 0); // DL program
	if (ret != EW_OK) {
        ret = cnc_dwnend3(h);
        return false;
	}
	len = strlen(s);
	while (len > 0) {
		n = len;
		ret = cnc_download3(h, &n, s);
		if (ret == EW_BUFFER) {
			// buffer pieno... occupato
			Sleep(100);
			continue;
		} else if (ret == EW_OK) {
			// scritti n caratteri
			len -= n;    // caratteri che restano da scrivere
			s = s + n;
		} else {
			break;
		}
	}
	ret = cnc_dwnend3(h);
	if (ret != EW_OK) {
		ret = cnc_getdtailerr(h, &err);
		return false;
	}
	return true;
}
//---------------------------------------------------------------------------

bool TFocasThread::DL4(int nprog, char *s, char *path)
{
	short ret, ret2;
	long len, n;
	ODBERR err;
	char str[240];

	ret = cnc_pdf_slctmain(h, "//CNC_MEM/USER/PATH1/O0666");
	if (ret != EW_OK) {
		if (ret == EW_DATA) {
			ret = cnc_getdtailerr(h, &err);
            if (ret == EW_DATA) {
                 // In questo caso il file � selezionato sul cnc
                LogTxt("Errore cnc_pdf_slctmain: cnc_getdtailerr: %d, %d ", err.err_no, err.err_dtno);
                return false;
            }
		}
        else {
			LogTxt("Errore cnc_pdf_slctmain: %d", ret);
			return false;
        }
	}

	//Se ho un programma da scaricare provo a scaricarlo
	if (nprog >= 0) {
		//Cancello il programma
        sprintf(str, "%sO%04d", path, nprog);
        ret = cnc_pdf_del(h, str);
	}
    if (ret != EW_OK) {
        if (ret != EW_DATA) {
             LogTxt("Errore cnc_pdf_del: %d ", ret);
             return false;
        }
        else {
            ret = cnc_getdtailerr(h, &err);
            if (ret == EW_DATA) {
                 // In questo caso il file � selezionato sul cnc
                LogTxt("Errore cnc_pdf_del: cnc_getdtailerr: %d, %d ", err.err_no, err.err_dtno);
                return false;
            }
        }
    }
	LogTxt("Inizio trasferimento: O%d in %s", nprog, path);
	ret = cnc_dwnstart4(h, 0, path);
	if (ret != EW_OK) {
        ret = cnc_dwnend4(h);
		LogTxt("Errore cnc_dwnend4: %d ", ret);
		return false;
	}
	len = strlen(s);
	while (len > 0) {
		n = len;
		ret = cnc_download4(h, &n, s);
		if (ret == EW_BUFFER) {
			// buffer pieno... occupato
			Sleep(20);
			continue;
		}
        else if (ret == EW_OK) {
			// scritti n caratteri
			len -= n;    // caratteri che restano da scrivere
			s = s + n;
		}
        else {
			LogTxt("Errore cnc_download4: %d ", ret);
			if (ret == EW_OVRFLOW) {
				LogMsg.printf("MU:%d CNC not enought space for program. Make enough free area in CNC memory.", ID);
				Synchronize(ScriviAllarme);
			}
			break;
		}
	}
	ret = cnc_dwnend4(h);
	if (ret != EW_OK) {

		ret2 = cnc_getdtailerr(h, &err);
		LogTxt("Errore cnc_dwnend4: %d. cnc_getdtailerr: %d, %d ", ret, err.err_no, err.err_dtno);
		return false;
	}
	LogTxt("Ok trasferimento: O%d in %s", nprog, path);
	return true;
}
//---------------------------------------------------------------------------

bool TFocasThread::ReadFile(AnsiString FileNameStr, char *buf) {
	FILE *f;
	bool res = false;

	f = fopen(FileNameStr.c_str(), "r");
	if (f) {
		while (!feof(f)) {
			fread(buf, 1, 1, f);
			buf++;
		}
		res = true;
	}
	fclose(f);
	*buf = 0;
	return res;
}
//---------------------------------------------------------------------------

bool TFocasThread::WriteFile(AnsiString FileNameStr, char *buf) {
	FILE *f;
	bool res = false;

	f = fopen(FileNameStr.c_str(), "w+");
	if (f) {
		while (buf[0] != 0) {
			fwrite(buf, 1, 1, f);
            buf++;
		}
		res = true;
	}
	fclose(f);
	return res;
}
//---------------------------------------------------------------------------

bool TFocasThread::UL3(char *s, int nprog)
{
	short ret;
	long len;

	ret = cnc_upstart3(h, 0, nprog, nprog); // DL program
	if (ret != EW_OK) {
		cnc_upend3(h);
		return false;
	}
	do {
		len = BUFSIZE;
		ret = cnc_upload3(h, &len, s);
		if (ret == EW_BUFFER) {
			// buffer pieno... occupato
			Sleep(100);
			continue;
		} else if (ret == EW_OK) {
			// sletti n caratteri
			if (s[len - 1] == '%') {
				s[len] = 0;
				break;
			} else {
				s = s + len;
			}
		} else {
			break;
		}
	} while ((ret == EW_OK) || (ret == EW_BUFFER));
	ret = cnc_upend3(h);
	return (ret == EW_OK);
}
//---------------------------------------------------------------------------

bool TFocasThread::UL4(char *s, char *file_name)
{
	short ret;
	long len;
	ODBERR err;

	ret = cnc_upstart4(h, 0, file_name); // DL program
	if (ret != EW_OK) {
		cnc_getdtailerr(h, &err);
		LogTxt("Errore cnc_upstart4: cnc_getdtailerr: %d, %d ", err.err_no, err.err_dtno);
		cnc_upend4(h);
		return false;
	}
	do {
		len = BUFSIZE;
		ret = cnc_upload4(h, &len, s);
		if (ret == EW_BUFFER) {
			// buffer pieno... occupato
			Sleep(100);
			continue;
		} else if (ret == EW_OK) {
			// sletti n caratteri
			if (s[len - 1] == '%') {
				s[len] = 0;
				break;
			} else {
				s = s + len;
			}
		} else {
			break;
		}
	} while ((ret == EW_OK) || (ret == EW_BUFFER));
	ret = cnc_upend4(h);
	return (ret == EW_OK);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteD(int address, int value)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITED;
	Dati.intp[0] = address;
	Dati.intp[1] = value;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::SetBitR(int address, int bit)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_SET_BIT_R;
	Dati.intp[0] = address;
	Dati.intp[1] = bit;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::ResetBitR(int address, int bit)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_RESET_BIT_R;
	Dati.intp[0] = address;
	Dati.intp[1] = bit;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::PulseBitR(int address, int bit, int wait)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_PULSE_BIT_R;
	Dati.intp[0] = address;
	Dati.intp[1] = bit;
	Dati.intp[2] = wait;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteR(int address, int value)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITER;
	Dati.intp[0] = address;
	Dati.intp[1] = value;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteMacro(int address, int value)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITE_MACRO;
	Dati.intp[0] = address;
	Dati.intp[1] = value;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteMacroDouble(int address, double value)
{
	WriteCom Dati;
	AnsiString s;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITE_MACRO_DOUBLE;
	Dati.intp[0] = address;
	Dati.intp[1] = (long) (value * 10000);
	//s.printf("%f", value);
	//strcpy(Dati.stringp[0], s.c_str());
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteMacroR(int address, int value)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITE_MACRO_R;
	Dati.intp[0] = address;
	Dati.intp[1] = value;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteProgram3(AnsiString filename, int nprogram)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITE_PROGRAM3;
	Dati.intp[0] = nprogram;
	strcpy(Dati.stringp[0], filename.c_str());
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteProgram4(AnsiString filename, int nprogram, AnsiString CncPath)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITE_PROGRAM4;
	Dati.intp[0] = nprogram;
	strcpy(Dati.stringp[0], filename.c_str());
    if (CncPath.IsEmpty()) {
		strcpy(Dati.stringp[1], "//CNC_MEM/USER/PATH1/");
    }
    else {
		strcpy(Dati.stringp[1], CncPath.c_str());
    }
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::ReadProgram3(int nprogram, AnsiString PathSalvataggio)
{
	WriteCom Dati;
	AnsiString filename;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_READ_PROGRAM3;
	Dati.intp[0] = nprogram;
    //Controllo se hanno inserito lo slash finale
    if (!PathSalvataggio.IsPathDelimiter(PathSalvataggio.Length())) {
        //Lo imposto io a mano
        PathSalvataggio += "\\";
    }
//	filename = PathSalvataggio + "O" + RightStr("0000" + IntToStr(nprogram), 4) ;
	filename = PathSalvataggio + IntToStr(nprogram);
	strcpy(Dati.stringp[0], filename.c_str());
 	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::ReadProgram4(int nprogram, AnsiString PathSalvataggio, AnsiString CncPath)
{
	AnsiString prog;

	prog = "O" + System::Strutils::RightStr("0000" + IntToStr(nprogram), 4);
    ReadProgram4(prog, PathSalvataggio, CncPath);
}
//---------------------------------------------------------------------------

void TFocasThread::ReadProgram4(AnsiString nprogram, AnsiString PathSalvataggio, AnsiString CncPath)
{
	WriteCom Dati;
	AnsiString prog;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_READ_PROGRAM4;

    //Controllo se hanno inserito lo slash finale
    if (!PathSalvataggio.IsPathDelimiter(PathSalvataggio.Length())) {
        PathSalvataggio += "\\";	//Lo imposto io a mano
    }

	//Compongo il percorso dove andare a salvare il file
    strcpy(Dati.stringp[0], PathSalvataggio.c_str());
	strcat(Dati.stringp[0], nprogram.c_str());

    //Controllo che si sia impostato un percorso
    if (CncPath.IsEmpty()) {
		CncPath = "//CNC_MEM/USER/PATH1/";
    }

    //Compongo il percorso e il nome del PP all'interno della memoria del CN
	prog = "O" + RightStr("0000" + nprogram, 4);
    strcpy(Dati.stringp[1], CncPath.c_str());
	strcat(Dati.stringp[1], prog.c_str());

	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::ReadProgram4(AnsiString FileName, AnsiString CncPath)
{
	WriteCom Dati;
    AnsiString nprogram;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_READ_PROGRAM4;

	//Compongo il percorso dove andare a salvare il file
    strcpy(Dati.stringp[0], FileName.c_str());

    //Controllo che si sia impostato un percorso
    if (CncPath.IsEmpty()) {
		CncPath = "//CNC_MEM/USER/PATH1/";
    }

    //Compongo il percorso e il nome del PP all'interno della memoria del CN
    nprogram = "O" + System::Strutils::RightStr("0000" + IntToStr(NumeroProgramma(FileName)), 4);
    strcpy(Dati.stringp[1], CncPath.c_str());
	strcat(Dati.stringp[1], nprogram.c_str());

	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::SelectProgram(int nprogram, int commandstart)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_SELECT_PROGRAM;
	Dati.intp[0] = nprogram;
	Dati.intp[1] = commandstart;
	TimeoutStart = clock();
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::StartProgram()
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_START_PROGRAM;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::InviaFineCiclo()
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_FINE_CICLO;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::FindAndDelete_DS(int nProgram)
{
	AnsiString filename;
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
    filename.sprintf("O%04d", nProgram);
	Dati.comando = COM_FIND_AND_DELETE;
	strcpy(Dati.stringp[0], filename.c_str());
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::WriteProgram_DS(AnsiString SourceFile, AnsiString DestFile)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_WRITE_PROGRAM_DS;
	strcpy(Dati.stringp[0], SourceFile.c_str());
	strcpy(Dati.stringp[1], DestFile.c_str());
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::DeleteAll_DS()
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_DELETE_ALL_DS;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::DeleteFile_DS(AnsiString filename)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_DELETE_FILE_DS;
	strcpy(Dati.stringp[0], filename.c_str());
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::SetCorrettori(int Gruppo, int Num_Utensile, int H, int R, int Stato)
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.intp[0] = Gruppo;
	Dati.intp[1] = Num_Utensile;
	Dati.intp[2] = H;
	Dati.intp[3] = R;
	Dati.intp[4] = Stato;
	Dati.comando = COM_WRITE_CORRETTORI;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

bool TFocasThread::DL_DS(AnsiString SourceFile, AnsiString DestFile)
{
	TIdFTP *IdFTP;
	bool res = false;
	AnsiString filename;

	try {
		// set up FTP component
		IdFTP = new TIdFTP(NULL);
		IdFTP->Host = DataServerAddress;
        IdFTP->Port = DataServerPort;
        IdFTP->Username = DataServerUser;
        IdFTP->Password = DataServerPassword;
        IdFTP->ProxySettings->ProxyType = fpcmNone;
		IdFTP->Passive = true;
		// Connect to host
		IdFTP->Connect();
		IdFTP->Put(SourceFile, DestFile);
		res = true;
	} catch(...) {}
	IdFTP->Disconnect(false);
	delete IdFTP;
	return res;
}
//---------------------------------------------------------------------------

bool TFocasThread::DelAll_DS()
{
	TIdFTP *IdFTP;
	bool res = false;
	int i;
	AnsiString filename;

	try {
		// set up FTP component
		IdFTP = new TIdFTP(NULL);
		IdFTP->Host 	= DataServerAddress;
		IdFTP->Port 	= DataServerPort;
		IdFTP->Username = DataServerUser;
		IdFTP->Password = DataServerPassword;
		IdFTP->ProxySettings->ProxyType = fpcmNone;
		IdFTP->Passive = true;
		// Connect to host
		IdFTP->Connect();
		IdFTP->List();
		for(i = 0; i < IdFTP->DirectoryListing->Count; ++i) {
			filename = ExtractFileName(IdFTP->DirectoryListing->Items[i]->FileName);
			if (filename.c_str()[0] != '.') {
				IdFTP->Delete(filename);
			}
		}
		res = true;
	} catch(...) {
	}
//	IdFTP->Quit();
	IdFTP->Disconnect(false);
	delete IdFTP;
	return res;
}
//---------------------------------------------------------------------------

bool TFocasThread::DelFile_DS(AnsiString filename)
{
	TIdFTP *IdFTP;
	bool res = false;

	try {
		// set up FTP component
		IdFTP = new TIdFTP(NULL);
		IdFTP->Host 	= DataServerAddress;
		IdFTP->Port 	= DataServerPort;
		IdFTP->Username = DataServerUser;
		IdFTP->Password = DataServerPassword;
		IdFTP->ProxySettings->ProxyType = fpcmNone;
		IdFTP->Passive = true;
		// Connect to host
		IdFTP->Connect();
		IdFTP->Delete(filename);
		res = true;
	} catch(...) {
	}
//	IdFTP->Quit();
	IdFTP->Disconnect(false);
	delete IdFTP;
	return res;
}
//---------------------------------------------------------------------------

bool TFocasThread::FindFile_DS(AnsiString sFileNamePRG)
{
	TIdFTP *IdFTP;
	bool res = false;
	int i;
	AnsiString filename;

	try
    {
		// set up FTP component
		IdFTP = new TIdFTP(NULL);
		IdFTP->Host 	= DataServerAddress;
		IdFTP->Port 	= DataServerPort;
		IdFTP->Username = DataServerUser;
		IdFTP->Password = DataServerPassword;
		IdFTP->ProxySettings->ProxyType = fpcmNone;
		IdFTP->Passive = true;
		// Connect to host
		IdFTP->Connect();
		IdFTP->List();
		for(i = 0; i < IdFTP->DirectoryListing->Count && !res; ++i) {
			filename = ExtractFileName(IdFTP->DirectoryListing->Items[i]->FileName);
			if (filename.c_str()[0] != '.')
                res = (filename == sFileNamePRG);
		}
	}
    catch(...) {
	}
	IdFTP->Disconnect(false);
	delete IdFTP;
	return res;
}
//---------------------------------------------------------------------------

void TFocasThread::ImpostaAbilitaGestioneUtensili(int Value) {
	TInterlocked::Exchange(AbilitaGestioneUtensili, Value);
}
//---------------------------------------------------------------------------

void TFocasThread::ImpostaTABELLA_UTENSILI_LETTA(bool Value) {
	TInterlocked::Exchange(TABELLA_UTENSILI_LETTA, Value);
}
//---------------------------------------------------------------------------

bool TFocasThread::LeggiTABELLA_UTENSILI_LETTA() {
	return TABELLA_UTENSILI_LETTA;
}
//---------------------------------------------------------------------------

void TFocasThread::CancellaFile30I(int Programma, AnsiString CncPath)
{
	AnsiString Prg;
	WriteCom Dati;

	if (Programma > 0) {
    	if (CncPath.IsEmpty()) {
			Prg.printf("//CNC_MEM/USER/PATH1/O%04d", Programma);
        }
        else {
			Prg.printf("%sO%04d", CncPath.c_str(), Programma);
        }
		memset(&Dati, 0, sizeof(WriteCom));
		strcpy(Dati.stringp[0], Prg.c_str());
		Dati.comando = COM_DEL_FILE_I30;
		Dati.LapsCounter = 10;
		TxQueue.push_back(Dati);
	}
}
//---------------------------------------------------------------------------

void TFocasThread::CancellaFile0I_15IB(int Programma)
{
	WriteCom Dati;

	if (Programma > 0) {
		memset(&Dati, 0, sizeof(WriteCom));
		Dati.intp[0] = Programma;
		Dati.comando = COM_DEL_FILE_0I_15IB;
		Dati.LapsCounter = 10;
		TxQueue.push_back(Dati);
	}
}
//---------------------------------------------------------------------------

void TFocasThread::SendSETUP()
{
	WriteCom Dati;

	memset(&Dati, 0, sizeof(WriteCom));
	Dati.comando = COM_SEND_SETUP;
	Dati.LapsCounter = 10;
	TxQueue.push_back(Dati);
}
//---------------------------------------------------------------------------

void TFocasThread::SetFunctionError(float comando, int errore, AnsiString strlog, bool rileggi, bool disconnetti)
{
	//Valorizzo i dati per comporre la stringa di errore
    FunctionError = comando;
    ErrorNumber = errore;

    //Sincronizzo la scrittura dell'errore a DB
    Synchronize(WriteFunctionError);

    //Scrivo anche sul file di LOG
	LogTxt("Focas Function: %g, ErrorNumber: %d", comando, errore);

   	//Decremento il numero di retry
	TxQueue[0].LapsCounter--;

	// Loggo se la chiamata ha fallito tutti i tentativi
	if (TxQueue[0].LapsCounter <= 0) {
		LogTxt("Focas Function: %s, Falliti tutti i tentativi", strlog.c_str());
	}

	//Controllo se devo rieseguire un ciclo di lettura dei valori
	if (rileggi) {
    	forza_rilettura_stato = true;
    }

    //Controllo se devo disconnettere la comunicazione
    if (disconnetti) {
		Disconnect();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFocasThread::WriteFunctionError()
{
	DBDataModule->Log("SERVER", "FOCAS", "Function: %g, ErrorNumber: %d", FunctionError, ErrorNumber);
}
//---------------------------------------------------------------------------

void __fastcall TFocasThread::ScriviAllarme()
{
	DBDataModule->Segnalazione(1009, LogMsg, 1);
}
//---------------------------------------------------------------------------

void __fastcall TFocasThread::StartCycle()
{
	Fanuc[0]->StartCycleMU(ID);
}
//---------------------------------------------------------------------------

void __fastcall TFocasThread::Ok()
{
	LogTxt("MU%d OK", ID);
}
//---------------------------------------------------------------------------

bool TFocasThread::CancellaFileDaMacchina(int nProgram)
{
	short ret;
	//Seleziono un programma presente nel CN, per far si che il programma venga cancellato
	cnc_search(h, PROGRAMMA_ROTAZIONE_TAVOLA);
	ret = cnc_delete(h, (short)nProgram);
	return (ret == EW_OK);
}
//---------------------------------------------------------------------------

AnsiString TFocasThread::ErroreFocasCnc(int n) {
}
