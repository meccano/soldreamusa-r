//---------------------------------------------------------------------------
#pragma hdrstop

#include <stdio.h>
#include <time.h>
#include <inifiles.hpp>
#include "Socket.h"
#include "DB.h"
#include "Main.h"
#include "LogTxt.h"
#include "ClientData.h"
#include "Events.h"
#include "Fanuc.h"
#include "FocasThread.h"
#include "PLCThread.h"
#include "Tools.h"
#include "util.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSocketDataModule *SocketDataModule;
//---------------------------------------------------------------------------

__fastcall TSocketDataModule::TSocketDataModule(TComponent* Owner)
	: TDataModule(Owner)
{
	memset(&ClientData, 0, sizeof(ClientData));
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::DataModuleCreate(TObject *Sender)
{
	TMyLinger l;
	TIniFile *pIni;

	l.l_onoff = 0;
	l.l_linger = 0;
	pIni = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
	ServerSocket1->Port = pIni->ReadInteger("Socket", "PortNumber", 11000);
	delete pIni;
	ServerSocket1->Open();
	setsockopt((SOCKET)ServerSocket1->Socket->SocketHandle, SOL_SOCKET, SO_LINGER, (char*)&l, 4);
	TimerSendClientData->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::ServerSocket1ClientError(
	  TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
	  int &ErrorCode)
{
	ErrorCode = 0;
}
//---------------------------------------------------------------------------


void __fastcall TSocketDataModule::TimerSendClientDataTimer(
	  TObject *Sender)
{
	int i;

	TimerSendClientData->Enabled = false;
	UpdateClientData();
	strcpy(ClientData.Sync, "SYNC");
	for(i = 0; i < ServerSocket1->Socket->ActiveConnections; i++) {
		ServerSocket1->Socket->Connections[i]->SendBuf(&ClientData, sizeof(ClientData));
	}
	TimerSendClientData->Enabled = true;
}
//---------------------------------------------------------------------------

void TSocketDataModule::UpdateClientData()
{
	int id;

	ClientData.watchdog++;
	if (ClientData.watchdog > 9999)
		ClientData.watchdog = 0;

    //Copio i dati della missione in corso
	memcpy(&ClientData.Missione, &EventsDM->m_MissionRun, sizeof(ClientData.Missione));

	//Robot Data
    for (id = 0; id < N_ROBOTS; id++) {
    	if (Fanuc[id]) {
			ClientData.Robot_Connected	 					= Fanuc[id]->Connected;
			ClientData.DatiRBT[id].Override			        = Fanuc[id]->R[11];
			ClientData.DatiRBT[id].Pinza				    = Fanuc[id]->R[12];
			ClientData.DatiRBT[id].ErrAltezzaBaia[0]		= Fanuc[id]->R[13];
			ClientData.DatiRBT[id].ErrAltezzaBaia[1]		= Fanuc[id]->R[14];
			ClientData.DatiRBT[id].Stato.Errore             = Fanuc[id]->RobotInError();
			ClientData.DatiRBT[id].Stato.Automatico         = Fanuc[id]->RobotInAuto();
			ClientData.DatiRBT[id].Stato.Running            = Fanuc[id]->RobotInRun();
			ClientData.DatiRBT[id].Stato.HOME               = Fanuc[id]->RobotInHome();
			ClientData.DatiRBT[id].Stato.Presenza_pallet    = Fanuc[id]->IsPalletPresent();
			memcpy(ClientData.DatiRBT[id].R, Fanuc[id]->R, sizeof(ClientData.DatiRBT[id].R));
		}
	}

	ClientData.PLC_Connected = PLCThread->connected;

	for (id = 0; id < N_MACCHINE_UT; id++) {
		if (Focas[id]) {
			ClientData.M_Connected[id] = Focas[id]->Connected;
			ClientData.DatiMU[id].alarm = Focas[id]->statinfo.alarm;
            ClientData.DatiMU[id].emergency = Focas[id]->statinfo.emergency;
            ClientData.DatiMU[id].aut = Focas[id]->statinfo.aut;
            ClientData.DatiMU[id].run = Focas[id]->statinfo.run;
            ClientData.DatiMU[id].motion = Focas[id]->statinfo.motion;
			ClientData.DatiMU[id].running_prg = Focas[id]->prg.data;
			ClientData.DatiMU[id].main_prg = Focas[id]->prg.mdata;
		}
        //Copio la tabella utensili
		memcpy(ClientData.DatiMU[id].ToolsTab, dmTools->m_ToolsTabMU[id].TableTools, sizeof(ClientData.DatiMU[id].ToolsTab));
	}

	//Dati PLC
	memcpy(ClientData.PLC.DB, PLCThread->DB, sizeof(ClientData.PLC.DB));
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::ServerSocket1ClientRead(TObject *Sender,
	  TCustomWinSocket *Socket)
{
	AnsiString Answer, s, MsgType, msg;
	int m, n, val, param[10], db, dw, b, dblw, bt, nprogram;
	SHORT w;
	TPallet pm;
	TPezzo pz;

	memset(param, 0 , sizeof(param));
	Answer = Socket->ReceiveText();
	LogTxt(Answer.c_str());
	MsgType = Parser("MSGTYPE=", Answer, "|");
	do {
		if (MsgType == "WRITETAG") {
			s = Parser("POS=", Answer, "|");
			n = s.ToIntDef(1);
			s = Parser("VAL=", Answer, "|");
			val = s.ToIntDef(0);
			switch(n) {
			case 1:
//				BallufRFID1DM->IDWrite = val;
				break;
			}
		}
        else if (MsgType == "ABORTMISS") {
			EventsDM->GestisciMissioneAbortita();
            Fanuc[0]->EraseMission();
        }
        else if (MsgType == "OVERRIDE") {
            s = Parser("VAL=", Answer, "|");
            w = (SHORT)s.ToIntDef(0);
            Fanuc[0]->WriteShort(TAGW_OVERRIDE, &w);
        }
		else if (MsgType == "SETBITPLC") {
			s = Parser("DB=", Answer, "|");
			db = s.ToIntDef(0);
			s = Parser("DW=", Answer, "|");
			dw = s.ToIntDef(0);
			s = Parser("BIT=", Answer, "|");
			b = s.ToIntDef(0);
			PLCThread->SetBit(db, dw, b);
		}
		else if (MsgType == "RESETBITPLC") {
			s = Parser("DB=", Answer, "|");
			db = s.ToIntDef(0);
			s = Parser("DW=", Answer, "|");
			dw = s.ToIntDef(0);
			s = Parser("BIT=", Answer, "|");
			b = s.ToIntDef(0);
			PLCThread->ResetBit(db, dw, b);
		}
		else if (MsgType == "WRITEWORDPLC") {
			s = Parser("DB=", Answer, "|");
			db = s.ToIntDef(0);
			s = Parser("DW=", Answer, "|");
			dw = s.ToIntDef(0);
			s = Parser("VAL=", Answer, "|");
			w = s.ToIntDef(0);
			PLCThread->WriteWord(db, dw, w);
		}
		else if (MsgType == "WRITEDWORDPLC") {
			s = Parser("DB=", Answer, "|");
			db = s.ToIntDef(0);
			s = Parser("DW=", Answer, "|");
			dw = s.ToIntDef(0);
			s = Parser("VAL=", Answer, "|");
			dblw = s.ToIntDef(0);
			PLCThread->WriteDWord(db, dw, dblw);
		}
		else if (MsgType == "WRITEBYTEPLC") {
			s = Parser("DB=", Answer, "|");
			db = s.ToIntDef(0);
			s = Parser("DW=", Answer, "|");
			dw = s.ToIntDef(0);
			s = Parser("VAL=", Answer, "|");
			bt = s.ToIntDef(0);
			PLCThread->WriteByte(db, dw, bt);
		}
        else if (MsgType == "UNLOAD_MU") {
            s = Parser("MU=", Answer, "|");
            m = s.ToIntDef(0);
            EventsDM->ScaricaMacchina(m);
            DBDataModule->Log("SERVER", "SOCKET", "MU%d: Inviato comando manuale di scarico pallet dalla macchina", m);
        }
		else if (MsgType == "STARTPROGRAM") {
			s = Parser("MU=", Answer, "|");
			m = s.ToIntDef(0);
			if ((m > 0) && (m <= N_MACCHINE_UT)) {
                //eseguo lo start al programma principale in macchina
                Focas[m - 1]->StartCycle();
                //Log
                DBDataModule->Log("SERVER", "SOCKET", "MU%d: Start programma", m);
            }
        }
        else if (MsgType == "SENDPROGRAM") {
			s = Parser("MU=", Answer, "|");
			m = s.ToIntDef(0);
			s = Parser("PRG=", Answer, "|");
			if ((m > 0) && (m <= N_MACCHINE_UT) && (s != "")) {
                nprogram = NumeroProgramma(s);
                if (nprogram > 0) {
                    //Scrivo il programma in macchina
                    Focas[m - 1]->WriteProgram4(s, nprogram);
                    //Log
					DBDataModule->Log("SERVER", "SOCKET", "MU%d: Inviato programma %s", m, s.c_str());
                }
			}
        }
        MsgType = Parser("|MSGTYPE=", Answer, "|");
        if (!MsgType.IsEmpty()) {
            Answer = Answer.SubString(Answer.Pos("|MSGTYPE=") + 1, Answer.Length());
        }
	} while (!MsgType.IsEmpty());
}
//---------------------------------------------------------------------------

void __fastcall TSocketDataModule::DataModuleDestroy(TObject *Sender)
{
	ServerSocket1->Close();
}
//---------------------------------------------------------------------------

