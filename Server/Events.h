//---------------------------------------------------------------------------

#ifndef EventsH
#define EventsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Character.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdExplicitTLSClientServerBase.hpp>
#include <IdFTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <vector>
#include "DB.h"
#include "time.h"
//---------------------------------------------------------------------------

enum EventsCommand
{
    EV_ROBOT_IN_ATTESA = 1,
    EV_FINE_MISSIONE_ROBOT,
	EV_MISSIONE_ERRATA,
	EV_MISSIONE_ABORTITA,
	EV_FINE_PRG_MU1,
	EV_FINE_PRG_MU2,
	EV_FINE_PRG_MU3,
	EV_PRELEVA_PALLET_BAIA1,
	EV_PRELEVA_PALLET_BAIA2,
	EV_EVENT_RBT1,
	EV_ALARM_RBT1
};
//---------------------------------------------------------------------------

#define EV_FINE_PRG_MU(n)			(EV_FINE_PRG_MU1 + n - 1)
#define EV_PRELEVA_PALLET_BAIA(n)	(EV_PRELEVA_PALLET_BAIA1 + n - 1)
//---------------------------------------------------------------------------

#define MAX_EVENT				100
//---------------------------------------------------------------------------

struct TEvento {
	int IDEvento;
	int intParam[10];
	AnsiString strParam;
	time_t t;
};
//---------------------------------------------------------------------------

class TEventsDM : public TDataModule
{
__published:	// IDE-managed Components
	TTimer *TimerScanEvents;
	TTimer *TimerScanLavoriInCorso;
	void __fastcall TimerScanEventsTimer(TObject *Sender);
//	void __fastcall TimerScanLavoriInCorsoTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TEventsDM(TComponent* Owner);
	__fastcall ~TEventsDM();
	void NewEvent(int IDev, int *intParam = NULL);
	void NewEvent(int IDev, AnsiString strParam);
	bool ExistsEvent(int IDev);
	void EraseEvent(int n);
	bool AttesaMissione();
	bool OKPrelievoMacchina(int MU, AnsiString &mess);
	bool OKDepositoMacchina(int MU, AnsiString &mess);
	bool OKPrelievoInBaia(int nbaia);
	bool OKDepositoInBaia(int nbaia);
	int GestisciPrelievo();
	int GestisciFineMissione();
	void GestisciMissioneAbortita();
//  int CercaLavoriInCorsoSuPallet();
	int CercaBaiaDeposito(int ID_TipoPallet);
	int CercaBaiaPrelievo(int ID_Pallet);
	int CercaDeposito(TMissione &m);
	int CercaMacchinaGruppo(int gruppo, AnsiString tmacc, int ID_TipoPallet);
	int CercaPrelievo(TMissione &m);
	int CercaPalletInBaia(TMissione &m , int NPallet);
	int GeneraMissione();
	int GeneraMissioneDeposito(int IndexRecordList, int ID_TipoPallet, TMissione &m);
    int ScaricaMacchina(int MU);
	int LeggiTestiEventiRobot();
	int LeggiTestiAllarmiRobot();
	void LogEventoRobot(int iNumRobot, int n);
	void LogAllarmeRobot(int iNumRobot, int n);
//	int CercaLottiAttivi();
	std::vector<TEvento> EventList;
	int Eventi[MAX_EVENT];
	int EventiBAK[MAX_EVENT];
	TRecordList TabMissioni;
	TRecordList TabPostazioni;
	TMissione m_MissionRun;
	AnsiString m_alltxt[512];
	bool m_allbak[512];
	AnsiString m_avvtxt[512];
	bool m_avvbak[512];
	AnsiString EventiRobot_txt[N_ROBOTS][1000];
	AnsiString AllarmiRobot_txt[N_ROBOTS][1000];
};
//---------------------------------------------------------------------------
extern PACKAGE TEventsDM *EventsDM;
//---------------------------------------------------------------------------
#endif
